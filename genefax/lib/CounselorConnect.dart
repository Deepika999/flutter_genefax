import 'dart:convert';
import 'dart:math';

import 'package:ai/ChatMessage.dart';
import 'package:ai/RouteNames.dart';
import 'package:ai/SizeConfig.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:zendesk_flutter_plugin/zendesk_flutter_plugin.dart';
import 'package:zendesk_flutter_plugin/chat_models.dart';

import 'Animations/ListItemAnimator.dart';
import 'Constant.dart';
import 'MessageType.dart';

class CounselorConnect extends StatefulWidget {
  double chatTextSize = 16.0;

  CounselorConnect({this.chatTextSize});

  @override
  State<StatefulWidget> createState() {
//    _StateCounselorConnect createState() => _StateCounselorConnect();
    return _StateCounselorConnect();
  }
}

class _StateCounselorConnect extends State<CounselorConnect> {
  String _platformVersion = 'Unknown';
  String _chatStatus = 'Uninitialized';
  String _zendeskAccountkey = '16xspF9sbIp82AFfl2gvF6lk41Nmqbym';

  final ZendeskFlutterPlugin _chatApi = ZendeskFlutterPlugin();

  StreamSubscription<ConnectionStatus> _chatConnectivitySubscription;
  StreamSubscription<AccountStatus> _chatAccountSubscription;
  StreamSubscription<List<Agent>> _chatAgentsSubscription;
  StreamSubscription<List<ChatItem>> _chatItemsSubscription;

  String _agentStatus = 'Offline';

  bool isAgentTyping = false;

  final ScrollController _scrollController = new ScrollController();
  List<ChatMessage> _list = <ChatMessage>[];
  List<ChatMessage> _temp = <ChatMessage>[];
  final TextEditingController _tec_ask_question = new TextEditingController();
  FocusNode _focus = new FocusNode();

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await _chatApi.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    _chatConnectivitySubscription =
        _chatApi.onConnectionStatusChanged.listen(_chatConnectivityUpdated);
    _chatAccountSubscription =
        _chatApi.onAccountStatusChanged.listen(_chatAccountUpdated);
    _chatAgentsSubscription =
        _chatApi.onAgentsChanged.listen(_chatAgentsUpdated);
    _chatItemsSubscription =
        _chatApi.onChatItemsChanged.listen(_chatItemsUpdated);

    String chatStatus;
    try {
      await _chatApi.init(_zendeskAccountkey);
      chatStatus = 'INITIALIZED';
      await _chatApi.startChat('Test Visitor Name', department: 'Card');
    } on PlatformException {
      chatStatus = 'Failed to initialize.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
      _chatStatus = chatStatus;
    });
  }

  @override
  void dispose() {
    _chatConnectivitySubscription.cancel();
    _chatAccountSubscription.cancel();
    _chatAgentsSubscription.cancel();
    _chatItemsSubscription.cancel();
    _chatApi.endChat();
    super.dispose();
  }

  void _chatConnectivityUpdated(ConnectionStatus status) {
    print('chatConnectivityUpdated: $status');
  }

  void _chatAccountUpdated(AccountStatus status) {
    print('chatAccountUpdated: $status');
    switch (status) {
      case AccountStatus.ONLINE:
        setState(() {
          _agentStatus = 'Online';
          if (_list.isEmpty) {
            ChatMessage initialMessage = new ChatMessage(
                showReadMore: false,
                nextMessage: "",
                ID: 0,
                isEditable: false,
                textFontSize: widget.chatTextSize,
                text: "How may i help you?",
                type: MessageType.LEFT,
                optionList: null,
                backgroundType: 0);
            _list.add(initialMessage);
          }
        });

        break;
      case AccountStatus.OFFLINE:
        setState(() {
          _agentStatus = 'Offline';
        });
        break;
      case AccountStatus.UNKNOWN:
        setState(() {
          _agentStatus = 'Away';
          if (_list.isEmpty) {
            ChatMessage initialMessage = new ChatMessage(
                showReadMore: false,
                nextMessage: "",
                ID: 0,
                isEditable: false,
                textFontSize: widget.chatTextSize,
                text: "How may i help you?",
                type: MessageType.LEFT,
                optionList: null,
                backgroundType: 0);
            _list.add(initialMessage);
          }
        });
        break;
    }
  }

  void _chatAgentsUpdated(List<Agent> agents) {
    print('chatAgentsUpdated: ${agents.last.isTyping}');
    setState(() {
      isAgentTyping = agents.last.isTyping;
    });
  }

  void _chatItemsUpdated(List<ChatItem> chatLog) {
    print('chatItemsUpdated:');
    if(_list.length>1) {
      _list.removeRange(1, _list.length);
    }

//    int index = chatLog.indexWhere((element) => element.message=="Last Message");
    if (chatLog != null) {
      for (int index = 0; index < chatLog.length; index++) {
        ChatItem e = chatLog[index];
        print('PRASANNA ITEM DETAILS : NICK NAME : ${e.nick} & MESSAGE : ${e.message}');
        if (e.nick!=null && e.nick.contains('agent') && e.message != null) {
          // LEFT SIDE
         setState(() {
           ChatMessage initialMessage = new ChatMessage(
               showReadMore: false,
               nextMessage: "",
               ID: 0,
               isEditable: false,
               textFontSize: widget.chatTextSize,
               text: e.message,
               type: MessageType.LEFT,
               optionList: null,
               backgroundType: 0);
           _list.add(initialMessage);
         });
        } else if (e.nick!=null && e.nick.contains('visitor') && e.message != null) {
          // RIGHT SIDE
         setState(() {
           ChatMessage initialMessage = new ChatMessage(
               showReadMore: false,
               nextMessage: "",
               ID: 0,
               isEditable: false,
               textFontSize: widget.chatTextSize,
               text: e.message,
               type: MessageType.RIGHT,
               optionList: null,
               backgroundType: 0);
           _list.add(initialMessage);
         });
        }
      }
    }
    if(_list.isNotEmpty) {
      scrollList();
    }
  }

  @override
  Widget build(BuildContext context) {

    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);


    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: IconButton(icon:Icon(Icons.arrow_back),color: Colors.white,
            onPressed:() => Navigator.of(context).popAndPushNamed(RouteHomePage),
          ),
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Genetic Expert',style: TextStyle(color: Colors.white),),
              Container(
                margin: EdgeInsets.only(top: 0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.centerLeft,
                      width: 10,
                      height: 10,
                      decoration: BoxDecoration(
                          color: getAccountStatusColor(),
                          borderRadius:
                              new BorderRadius.all(Radius.circular(5))),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 3),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        isAgentTyping == true ? 'typing...' : _agentStatus,
                        style: headerTextStyleBrandingMediumAgentStatus,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          backgroundColor: headerColor,
        ),
        body: SafeArea(
          child: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
//                colors: [Color(0xFFf7f2f2), Color(0xFFf7f2f2),Color(0xFFf7f2f2)])),
                      colors: [
                    chatScreenBackground,
                    chatScreenBackground,
                    chatScreenBackground
                  ])),
//        color: Color.fromRGBO(232, 239, 245, 1),
//        color: Color(0xffEAEEF3),
              child: Column(
                children: <Widget>[
                  Flexible(
                    child: Stack(children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            bottom: ScreenUtil.screenHeightDp * 0.01),
                        child: ListView.builder(
                          controller: _scrollController,
                          padding: new EdgeInsets.all(8.0),
                          itemCount: _list.length,
                          itemBuilder: _getListItem,
                        ),
                      ),
                      Visibility(visible:_agentStatus=='Offline',child: Container(padding:EdgeInsets.only(left: 5,right: 5),color :Colors.black.withOpacity(0.8), child: Center(child: Text('All of our agents are busy at this time. Please wait for the moment or you can drop offline message to our agent. As soon as the agent gets free, He will rich you on your registered email.',textAlign: TextAlign.center,style: TextStyle(fontSize: ScreenUtil().setSp(18),color: Colors.white, ),),),))
                    ],),
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
//                          color: headerColor.withOpacity(0.2),
                          height: ScreenUtil().setHeight(52)),
                      Container(
                        height: ScreenUtil().setHeight(42),
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            border: Border.all(width: 1.2, color: headerColor),
                            borderRadius: new BorderRadius.circular(27.0)),
                        padding: EdgeInsets.only(left: 20, right: 0),
                        margin: EdgeInsets.only(
                            left: ScreenUtil.screenWidthDp * 0.02,
                            right: ScreenUtil.screenWidthDp * 0.02,
                            top: ScreenUtil.screenWidthDp * 0.01,
                            bottom: ScreenUtil.screenWidthDp * 0.01),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Flexible(
                              child: TextField(
                                focusNode: _focus,
                                onEditingComplete: _focus.unfocus,
                                textCapitalization:
                                    TextCapitalization.sentences,
                                controller: _tec_ask_question,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.symmetric(
                                      vertical: 15.0),
                                  hintText: _agentStatus=='Offline'?"offline message here...":"type here....",
                                  hintStyle: TextStyle(
                                      color: Color.fromRGBO(153, 153, 153, 1),
                                      fontSize: ScreenUtil().setSp(14)),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.transparent),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.transparent),
                                  ),
                                ),
                                keyboardType: TextInputType.multiline,
                              ),
                            ),
                            new Container(
                              margin: EdgeInsets.only(right: 0, bottom: 0),
                              child: new IconButton(
                                  icon: new Icon(
                                    Icons.send,
                                    color: sendIconColor,
                                  ),
                                  onPressed: () => _askQuestion(
                                      "${_tec_ask_question.text}")),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  getAccountStatusColor() {
    if (_agentStatus == 'Online') {
      return Colors.green;
    } else if (_agentStatus == 'Offline') {
      return Colors.red;
    } else if (_agentStatus == 'Away') {
      return Colors.amberAccent;
    }
  }

  Widget _getListItem(BuildContext context, int index) {
    double maxWidth = MediaQuery.of(context).size.width * 0.70;

    double _spacingRound = 12;
    var _elevation = 0.9;

    ChatMessage message = _list[index];

    if (message.type == (MessageType.RIGHT)) {
      print("QUESTION LENGTH : ${message.text.length}");
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
//              height: _getVerticalSpacing(message.backgroundType),
              height: 8,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerRight,
                        child: WidgetAnimator(
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(5),
                                  bottomLeft: Radius.circular(15),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
//                            color: Color(0xff59A9B7),
//                            color: Color(0xff03A9F4),
                            color: rightBubbleBG,
                            margin: EdgeInsets.only(
                                right: 3.0, top: 5.0, left: 10.0),
                            child: new Container(
                              padding: EdgeInsets.all(8.0),
                              margin: EdgeInsets.all(3.5),
                              child: new Text(message.text,
                                  style: TextStyle(
                                      color: rightTextColor,
                                      fontFamily: 'BrandingMedium',
                                      fontSize: ScreenUtil()
                                          .setSp(widget.chatTextSize)),
                                  overflow: TextOverflow.clip),
                            ),
                          ),
                        ))),
                Visibility(
                  visible: message.isEditable,
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          /*HapticFeedback.vibrate();
                          print("EDIT BUTTON CLICKED : " + "${message.ID}");
                          if (_staticData.length == 0 &&
                              _list[_list.length - 1].type !=
                                  MessageType.LOADER) {
                            displayAlertForChangeResponse(
                                "Change Response",
                                "Please note that changing a response will require you to restart the conversation from that point in the chat history.",
                                message.ID);
                          }*/
                        },
                        child: Container(
                          padding: EdgeInsets.all(5.0),
                          margin: EdgeInsets.only(top: 5.0),
                          child: Image.asset(
                            'images/edit.png',
                            color: headerColor,
                            height: 16,
                            width: 16,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/user.png",
                          color: userIconColor,
                          width: 35,
                          height: 35,
                          alignment: Alignment.center),
                    ))
              ],
            ),
            SizedBox(
              height: 10,
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.LEFT)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: WidgetAnimator(
                          Column(
                            children: <Widget>[
                              Card(
                                  shape:
                                      _renderBackground(message.backgroundType),
                                  elevation: _elevation,
                                  color: leftBubbleBG,
                                  margin: EdgeInsets.only(
                                      right: 10.0, top: 0.0, left: 7.0),
                                  child: new Container(
                                    child: new Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        new Container(
                                          child: Text(message.text,
                                              style: TextStyle(
                                                  color: Color(0xff51575C),
                                                  fontFamily: 'BrandingMedium',
                                                  fontSize: ScreenUtil()
                                                      .setSp(widget.chatTextSize)),
                                              overflow: TextOverflow.clip),
                                          padding: EdgeInsets.all(5.0),
                                          margin: EdgeInsets.all(8),
                                      /*    child: Column(
                                            children: <Widget>[
                                              Html(
//                                            data: """ ${message.text} """,
                                                data:
                                                    """ ${message.text.toString().split(r"##")[0]} """,
                                                defaultTextStyle: TextStyle(
                                                    fontFamily:
                                                        'BrandingMedium',
                                                    fontSize: ScreenUtil()
                                                        .setSp(widget
                                                            .chatTextSize),
                                                    color: Colors.black87
//                                            color: Color(0xff51575C)
                                                    ),
                                                linkStyle: const TextStyle(
                                                  color: Colors.blue,
                                                  decoration:
                                                      TextDecoration.underline,
                                                ),
                                              ),
//                                          renderResources(message.text)
                                            ],
                                          ),*/

                                          /*padding: EdgeInsets.all(0.0),
                                          margin: EdgeInsets.all(2),
                                          child: Column(
                                            children: <Widget>[
                                              HtmlWidget(
//                                            """ ${message.text.toString().split(r"##")[0]} """,
                                                """ ${message.text} """,
                                                hyperlinkColor: Colors.blue,
                                                textStyle: TextStyle(
                                                    fontFamily: 'BrandingMedium',
                                                    fontSize: widget.chatTextSize,
                                                    color: Colors.black87
//                                            color: Color(0xff51575C)
                                                    ),
                                              ),
                                              renderResources(message.text)
                                            ],
                                          ),*/
                                        ),
//                                  _highlightText(text),
                                      ],
                                    ),
                                  )),
                            ],
                          ),
                        )))
              ],
            )
          ],
        ),
      );
    } else if (message.type == ("ERROR")) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  topRight: Radius.circular(12),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(12)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(3.5),
                                    child: new Text(message.text,
                                        style: TextStyle(
                                            color: Colors.red,
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.LOADER)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(0),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                      padding: EdgeInsets.all(2.5),
                                      margin: EdgeInsets.all(3.5),
                                      child: new Image(
                                          width: 35,
                                          fit: BoxFit.fitWidth,
                                          alignment: Alignment.center,
                                          height: 27,
                                          image: new AssetImage(
                                              "images/loader.gif"))),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == ("OPTIONS")) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                    width: maxWidth,
                                    padding: EdgeInsets.all(8.0),
                                    child: new ListView.builder(
                                      shrinkWrap: true,
                                      itemBuilder: (_, int index) =>
                                          message.optionList[index],
                                      itemCount: message.optionList.length,
                                    ),
                                  ),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.VERIFY_OTP)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0, bottom: 5.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(_spacingRound),
                                    child: new Text(message.text,
                                        style: TextStyle(
                                            color: Color(0xff51575C),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  new Container(
                                      padding: EdgeInsets.all(15.0),
                                      child: new Image(
                                          width: 35,
                                          fit: BoxFit.fitWidth,
                                          alignment: Alignment.center,
                                          height: 27,
                                          image: new AssetImage(
                                              "images/typing_loader.gif"))),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.PERSONAL_DETAILS)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(_spacingRound),
                                    child: Text("Details provided",
                                        style: TextStyle(
                                            color: Color(0xff51575C),
                                            fontFamily: 'BrandingSemiBold',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Divider(
                                    color: Color.fromRGBO(52, 52, 52, 1),
                                    height: 0.5,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 18, bottom: 7),
                                    child: Text("Name",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                        message.personalDetailModel.name,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Gender",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                        message.personalDetailModel.gender,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Age",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(message.personalDetailModel.age,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Ethnicity",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                        message.personalDetailModel.ethnicity,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                ],
                              ),
                            ))))
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      );
    } else if (message.type == (MessageType.DIGNOSED_RELATIVE_DETAILS)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                  flex: 1,
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: new Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15),
                                topRight: Radius.circular(15),
                                bottomLeft: Radius.circular(5),
                                bottomRight: Radius.circular(15)),
                          ),
                          elevation: _elevation,
                          color: Colors.white,
                          margin:
                              EdgeInsets.only(right: 10.0, top: 5.0, left: 7.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                padding: EdgeInsets.all(8.0),
                                margin: EdgeInsets.only(
                                    top: 3.5,
                                    bottom: 3.5,
                                    right: 15,
                                    left: 3.5),
                                child: Text("Relative's Details provided",
                                    style: TextStyle(
                                        color: Color(0xff51575C),
                                        fontFamily: 'BrandingSemiBold',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Divider(
                                  color: Color.fromRGBO(52, 52, 52, 1),
                                  height: 0.5),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Gender",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(message.relativeDetailModel.gender,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Age",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(message.relativeDetailModel.age,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Cancer Type",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(message.relativeDetailModel.cancer,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                            ],
                          ))),
                )
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      );
    } else if (message.type == (MessageType.DIVIDER)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: ScreenUtil.screenHeightDp * 0.045,
            ),
            Row(children: <Widget>[
              Expanded(
                child: new Container(
                    margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                    child: Divider(
                      color: Color(0xFF798797),
                      height: 36,
                    )),
              ),
              Text(
                message.text,
                style: TextStyle(color: Color(0xFF687889)),
              ),
              Expanded(
                child: new Container(
                    margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                    child: Divider(
                      color: Color(0xFF798797),
                      height: 36,
                    )),
              ),
            ]),
            SizedBox(
              height: ScreenUtil.screenHeightDp * 0.022,
            ),
          ],
        ),
      );
    } else if (message.type == (MessageType.WELCOME_IMAGE)) {
      return new Container(
        width: ScreenUtil.screenWidthDp,
        margin: EdgeInsets.all(50),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Flexible(
                    child: new Container(
                        alignment: Alignment.center,
                        child: Html(
                          data: """ ${message.text} """,
                          defaultTextStyle: TextStyle(
                              fontFamily: 'BrandingMedium',
                              color: Color(0xff51575C)),
                          linkStyle: const TextStyle(
                            color: Colors.redAccent,
                          ),
                        )))
              ],
            )
          ],
        ),
      );
    }
  }

  double _getVerticalSpacing(int backgroundType) {
    switch (backgroundType) {
      case -1:
        return 8.0;
        break;
      case 0:
        return 8.0;
        break;
      case 1:
        return 8.0;
        break;
      case 2:
        return 8.0;
        break;
      default:
        return 8.0;
    }
  }

  RoundedRectangleBorder _renderBackground(int backgroundType) {
    /* return RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(2),
          topRight: Radius.circular(2),
          bottomLeft: Radius.circular(12),
          bottomRight: Radius.circular(12)),
    );*/

    switch (backgroundType) {
      case 0:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
        break;
      case 1:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
        break;
      case 2:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15)),
        );
        break;
      default:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
    }
  }

  void scrollList() {
    /* Timer(
        Duration(milliseconds: 300),
            () => _scrollController
            .jumpTo(_scrollController.position.maxScrollExtent));*/
    Timer(
        Duration(milliseconds: 200),
        () => _scrollController.animateTo(
            _scrollController.position.maxScrollExtent,
            curve: Curves.linear,
            duration: Duration(milliseconds: 300)));
  }

  _askQuestion(String question) async {
//    FocusScope.of(context).requestFocus(FocusNode());
    _tec_ask_question.clear();
    if(_agentStatus=='Offline') {
      await _chatApi.sendOfflineMessage(question);
    }else if(_agentStatus=='Online' || _agentStatus=='Away'){
      await _chatApi.sendMessage(question);
    }

  }
}
