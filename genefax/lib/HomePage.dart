import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:ai/CounselorConnect.dart';
import 'package:ai/RouteManager.dart';
import 'package:ai/RouteNames.dart';
import 'package:ai/locator.dart';
import 'package:ai/main.dart';
import 'package:ai/services/push_notification_service.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:ai/CommingSoon.dart';
import 'package:ai/Constant.dart';
import 'package:ai/KnowledgeBot.dart';
import 'package:ai/Log.dart';
import 'package:ai/MyriadPretest.dart';
import 'package:ai/OptraHCS.dart';
import 'package:ai/ReadSpeed.dart';
import 'package:ai/ScheduleAppointment.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uni_links/uni_links.dart';

import 'SizeConfig.dart';
import 'screens/Notifications.dart';
import 'services/navigation_service.dart';

enum UniLinksType { string, uri }

class HomePage extends StatefulWidget {

  @override
  _StateHomePage createState() => _StateHomePage();
}

class _StateHomePage extends State<HomePage> with WidgetsBindingObserver {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);

    switch(state){
      case AppLifecycleState.resumed:
        Log.printLog('HomePage',' ApplifecycleState Resumed');
        break;
      case AppLifecycleState.inactive:
        // TODO: Handle this case.
        break;
      case AppLifecycleState.paused:
        // TODO: Handle this case.
        break;
      case AppLifecycleState.detached:
        // TODO: Handle this case.
        break;
    }

  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(

      ),
      /*navigatorKey: locator<NavigationService>().navigationKey,
      initialRoute: RouteHomePage,*/
      onGenerateRoute: generateRoute,
      home: NavigationDrawerPage(),
    );
  }
}


class NavigationDrawerPage extends StatefulWidget {
  NavigationDrawerPage({Key key}) : super(key: key);

  @override
  _NavigationDrawerPageSate createState() => _NavigationDrawerPageSate();
}

class _NavigationDrawerPageSate extends State<NavigationDrawerPage>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  final GlobalKey<StateHomeScreen> _keyMyriadPreTest = GlobalKey();

  int _selectedDrawerIndex = 0;
  bool isReadSpeed = false;
  String selectedReadSpeed = "1x";
  bool isFontSize = false;
  bool isSpeedCollapse = false;
  bool isFontCollapse = false;
  bool _isNotificationSwitched = false;
  Color color;
  Color color1;
  Color color2;
  Color color3;
  Color color4;
  Color color5;
  Color color6;

  String selectedSubtiltleFont = "Normal";
  List<Item> _readSpeeddata = generateItems(1, 'Read Speed');
  List<Item> _fontSizeData = generateItems(1, 'Font Size');
  final List<String> _readFastList = [
    '0.5x',
    '1x',
    '1.5x',
    '2x',
  ];

  final List<String> _fontSizeList = [
    'Small',
    'Normal',
    'Large',
  ];

  double _ChangingFontSize = 14;

  String _latestLink = 'Unknown';
  Uri _latestUri;

  StreamSubscription _sub;
  UniLinksType _type = UniLinksType.string;
  TabController _tabController;
  final _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool isDeepLink = false;
  String access;

  MyriadPretest pretest;

  int selectedTabIndex = 0;

  String version = "";

  int unReadNotificationCount = 0;

  FirebaseMessaging _messaginService = new FirebaseMessaging();

  void _select(Choice choice) {
    if (choice.title == 'Ask Any Question') {
      _keyMyriadPreTest.currentState.showFreeTextUI();
    } else if (choice.title == 'Reload Chat') {
      _keyMyriadPreTest.currentState.reloadChat();
    } else if (choice.title == 'Schedule Appointment') {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => ScheduleAppointment()));
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      Log.printLog('HomePage', 'Screen resumed');
      checkForNotificationCount();
    }
  }

  @override
  initState() {
    super.initState();

    handleAppLifecycleState();

    checkForNotificationCount();
    WidgetsBinding.instance.addObserver(this);
    _messaginService.configure(
      onMessage: (Map<String, dynamic> message) async {
        // THIS CALLBACK GETS INVOKED WHEN APP IS IN THE FOREGROUND
        Log.printLog('HomePage', 'New notifiction received');
        addNotificationMessage(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        // THIS CALLBACK GETS INVOKED WHEN APP IS NOT RUNNING AND OPENED VIA NOTIFICATION CLICK
      },
      onResume: (Map<String, dynamic> message) async {
        // THIS CALLBACK GETS INVOKED WHEN APP IS IN THE BACKGROUND AND OPENED FROM NOTIFICATION
      },
    );

    initPlatformState();
    _getReadSpeed();
    color = Colors.transparent;
    _initPackageInfo();
    /* PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      String appName = packageInfo.appName;
      String packageName = packageInfo.packageName;
      version = packageInfo.version;
      String buildNumber = packageInfo.buildNumber;
    });*/
  }

  void handleAppLifecycleState() {
    SystemChannels.lifecycle.setMessageHandler((msg) {
      print('SystemChannels> $msg');
      switch (msg) {
        case "AppLifecycleState.paused":
          break;
        case "AppLifecycleState.inactive":
          break;
        case "AppLifecycleState.resumed":
          print('HomePage::::: OnResumed called');
          checkForNotificationCount();
          break;
        default:
      }
      return;
    });
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      version = info.version;
    });
  }

  @override
  dispose() {
    if (_sub != null) _sub.cancel();
    _tabController.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  initPlatformState() async {
    if (_type == UniLinksType.string) {
      await initPlatformStateForStringUniLinks();
    } else {
      await initPlatformStateForUriUniLinks();
    }
  }

  /// An implementation using a [String] link
  initPlatformStateForStringUniLinks() async {
    // Attach a listener to the links stream
    _sub = getLinksStream().listen((String link) {
      if (!mounted) return;
      setState(() {
        _latestLink = link ?? 'Unknown';
        _latestUri = null;
        try {
          if (link != null) {
            _latestUri = Uri.parse(link);
            isDeepLink = true;
          } else {
            isDeepLink = false;
          }
        } on FormatException {}
      });
    }, onError: (err) {
      if (!mounted) return;
      setState(() {
        _latestLink = 'Failed to get latest link: $err.';
        _latestUri = null;
        isDeepLink = false;
      });
    });

    // Attach a second listener to the stream
    getLinksStream().listen((String link) {
      print('got link: $link');
    }, onError: (err) {
      print('got err: $err');
    });

    // Get the latest link
    String initialLink;
    Uri initialUri;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      initialLink = await getInitialLink();
      print('initial link: $initialLink');
      if (initialLink != null) {
        isDeepLink = true;
        initialUri = Uri.parse(initialLink);
      } else {
        isDeepLink = false;
      }
    } on PlatformException {
      initialLink = 'Failed to get initial link.';
      initialUri = null;
      isDeepLink = false;
    } on FormatException {
      initialLink = 'Failed to parse the initial link as Uri.';
      initialUri = null;
      isDeepLink = false;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _latestLink = initialLink;
      _latestUri = initialUri;
    });
  }

  /// An implementation using the [Uri] convenience helpers
  initPlatformStateForUriUniLinks() async {
    // Attach a listener to the Uri links stream
    _sub = getUriLinksStream().listen((Uri uri) {
      if (!mounted) return;
      setState(() {
        _latestUri = uri;
        _latestLink = uri?.toString() ?? 'Unknown';
      });
    }, onError: (err) {
      if (!mounted) return;
      setState(() {
        _latestUri = null;
        _latestLink = 'Failed to get latest link: $err.';
      });
    });

    // Attach a second listener to the stream
    getUriLinksStream().listen((Uri uri) {
      print('got uri: ${uri?.path} ${uri?.queryParametersAll}');
    }, onError: (err) {
      print('got err: $err');
    });

    // Get the latest Uri
    Uri initialUri;
    String initialLink;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      initialUri = await getInitialUri();
      print('initial uri: ${initialUri?.path}'
          ' ${initialUri?.queryParametersAll}');
      initialLink = initialUri?.toString();
    } on PlatformException {
      initialUri = null;
      initialLink = 'Failed to get initial uri.';
    } on FormatException {
      initialUri = null;
      initialLink = 'Bad parse the initial link as Uri.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _latestUri = initialUri;
      _latestLink = initialLink;
      final queryParams = _latestUri?.queryParametersAll?.entries?.toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    String selectedSubtiltle;
    bool isReadSpeed = false;
    bool isFontSize = false;

//    SystemChrome.setEnabledSystemUIOverlays([]);
//    SystemChrome.setEnabledSystemUIOverlays ([SystemUiOverlay.top]);
//    SystemChrome.setEnabledSystemUIOverlays ([SystemUiOverlay.bottom]);
//    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: headerColor, systemNavigationBarColor: Colors.black));

    var size = MediaQuery.of(context).size;

    final queryParams = _latestUri?.queryParametersAll?.entries?.toList();

    if (queryParams != null) {
      for (MapEntry<String, List<String>> entry in queryParams) {
        print('Key === ${entry.key}');
        print('Value == ${entry.value[0]}');
        access = entry.value[0];
      }
    }

    print("DEVICE WIDTH : ${MediaQuery.of(context).size.width}");

    return Scaffold(
      appBar: AppBar(
        title: Container(
          alignment: Alignment.center,
//          color: Colors.grey,
          height: 60.0,
//          width: size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  width: 90,
                  //color: Colors.red,
                  child: Text(
                    geneFaxProText,
                    style: headerTextStyleBrandingBold,
                    textAlign: TextAlign.center,
                  )),
              Flexible(
                child: Container(
                    // color: Colors.blueGrey,
                    child: Text(
                  geneFaxProText1,
                  style: headerTextStyleBrandingMedium,
                )),
              )
            ],
          ),
          //child: Text(geneFaxProText)
        ),
        brightness: Brightness.dark,
        backgroundColor: headerColor,
        actions: <Widget>[
          // action button
          /* IconButton(
            icon: Icon(Icons.child_care),
            onPressed: () {

            },
          ),*/
          new Stack(
            children: <Widget>[
              new IconButton(
                  icon: Icon(Icons.notifications),
                  iconSize: 25,
                  alignment: Alignment.bottomCenter,
                  onPressed: () {
                    /*Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Notifications(
                                chatTextSize: _ChangingFontSize)));*/

                    Navigator.pushNamed(context, RouteNotifications,arguments: _ChangingFontSize);

                  }),
              unReadNotificationCount != 0
                  ? new Positioned(
                      right: 10,
                      top: 10,
                      child: new Container(
                        padding: EdgeInsets.all(2),
                        decoration: new BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        constraints: BoxConstraints(
                          minWidth: 14,
                          minHeight: 14,
                        ),
                        child: Text(
                          '$unReadNotificationCount',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 8,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  : new Container(
                      height: 25,
                      width: 25,
                    )
            ],
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          KnowledgeBot(chatTextSize: _ChangingFontSize)));
            },
            child: Image.asset(
              'images/knowledgebase.png',
              height: 25,
              width: 25,
              color: drawerImgClr,
            ),
          ),
          SizedBox(
            width: ScreenUtil().setWidth(5),
          ),
          IconButton(
            icon: Icon(Icons.chat),
            onPressed: () {
//             handleCounselorConnect();
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          CounselorConnect(chatTextSize: _ChangingFontSize)));
//              _onSelectItem(2);
            },
          ),
          // overflow menu
          Visibility(
            visible: isDeepLink == true
                ? selectedTabIndex == 0 ? true : false
                : false,
            child: PopupMenuButton<Choice>(
              onSelected: _select,
              itemBuilder: (BuildContext context) {
                return choices
                    .skip(isDeepLink == true
                        ? selectedTabIndex == 0 ? 0 : 2
                        : selectedTabIndex == 0 ? 2 : 2)
                    .map((Choice choice) {
                  return PopupMenuItem<Choice>(
                    value: choice,
                    child: Text(choice.title),
                  );
                }).toList();
              },
            ),
          ),
        ],
      ),
      body: _getDrawerItemWidget(_selectedDrawerIndex),
      drawer: SizedBox(
        width: MediaQuery.of(context).size.width <= 360
            ? MediaQuery.of(context).size.width * 0.70
            : MediaQuery.of(context).size.width * 0.65,
        child: Drawer(
          child: Container(
            color: drawerBackgrdClr,
            child: Column(
              children: <Widget>[
                Flexible(
                  child: ListView(
                    padding: EdgeInsets.zero,
                    children: <Widget>[
                      Container(
                        height: 150, //size.height * 0.13,
                        color: drawerBackgrdClr,
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: ScreenUtil.screenWidthDp,
                              child: Image.asset('images/bg.png',
                                  fit: BoxFit.fill),
                            ),
                            Center(
                                child: Text('GeneFAX™',
                                    style: headertextStyleBrandingSemibold)),
                            //Image.asset('images/gene.jpg', fit: BoxFit.fill),
                          ],
                        ),
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            height: 68, //size.height * 0.075,
                            child: Material(
                              child: InkWell(
                                child: Center(
                                  child: ListTile(
                                    title: Text('Home',
                                        style: textStyleBrandingSemibold),
                                    leading: Image.asset(
                                      'images/dr_home.png',
                                      height: 30,
                                      width: 30,
                                      color: drawerImgClr,
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  _onSelectItem(0);
                                },
                              ),
                              color: homeBackgroundColor,
                            ),
                            color: homeBackgroundColor,
                          ),
                          /*Visibility(
                            visible: isDeepLink == true ? false : true,
                            child: Column(
                              children: <Widget>[
                                Divider(
                                  //color: Theme.of(context).primaryColor,
                                  //color: Colors.grey,
                                  height: 1,
                                  thickness: 2.5,
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 0),
                                  height: 68, //size.height * 0.075,
                                  child: Material(
                                    child: InkWell(
                                      child: Center(
                                        child: ListTile(
                                          title: Text('Knowledge BOT',
                                              style: textStyleBrandingSemibold),
                                          leading: Image.asset(
                                            'images/knowledgebase.png',
                                            height: 30,
                                            width: 30,
                                            color: drawerImgClr,
                                          ),
                                        ),
                                      ),
                                      onTap: () {
                                        _onSelectItem(1);
                                      },
                                    ),
                                    color: askAnyBckClr,
                                  ),
                                  color: askAnyBckClr,
                                ),
                              ],
                            ),
                          ),*/
                          Divider(
                            //color: Theme.of(context).primaryColor,
                            //color: Colors.grey,
                            height: 1,
                            thickness: 2.5,
                          ),
                          _DarkModeSwitch(),

//                          Divider(
//                            //color: Theme.of(context).primaryColor,
//                            //color: Colors.grey,
//                            height: 1,
//                            thickness: 2.5,
//                          ),
//                          new ColorPicker(
//                              color: Colors.blue,
//                              onChanged: (value) {
//                                setState(() {
//                                  dynamicColor(value);
//                                });
//                              }),
//                          Container(
//                            margin: EdgeInsets.only(top: 0),
//                            height: 68, //size.height * 0.075,
//                            child: Material(
//                              child: InkWell(
//                                child: Center(
//                                  child: ListTile(
//                                    title: Text('Ask Any Question',
//                                        style: textStyleBrandingSemibold),
//                                    leading: Image.asset(
//                                      'images/dr_question.png',
//                                      height: 30,
//                                      width: 30,
//                                      color: drawerImgClr,
//                                    ),
//                                  ),
//                                ),
//                                onTap: () {
//                                  _onSelectItem(1);
//                                },
//                              ),
//                              color: askAnyBckClr,
//                            ),
//                            color: askAnyBckClr,
//                          ),
                          _buildSpeedPanel(_readFastList, _readSpeeddata, '1x'),
                          _buildFontPanel(
                              _fontSizeList, _fontSizeData, 'Normal'),
                        ],
                      )
                    ],
                  ),
                ),
                Visibility(
                  visible: true,
                  child: Container(
                      padding: EdgeInsets.only(
                          left: 30.0, right: 30.0, bottom: 15.0),
                      child: Text(
                        'Version $version',
                        style: textStyleDrawerVersion,
                      )),
                ),
                Container(
                  width: ScreenUtil.screenHeightDp,
                  padding: EdgeInsets.only(
//                    top: ScreenUtil.screenHeightDp * 0.01,
                      bottom: ScreenUtil.screenHeightDp * 0.03,
                      right: ScreenUtil.screenHeightDp * 0.00),
//          color: Color.fromRGBO(41, 85, 102, 1.0),
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Powered by',
                        style: TextStyle(
                            fontFamily: 'BrandingSemiLightItalic',
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(13)),
                        textAlign: TextAlign.end,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Image.asset(
                        'images/footer_genefax.png',
                        height: 10,
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _getDrawerItemWidget(int pos) {
    setState(() {
      selectedTabIndex = pos;
    });
    if (isDeepLink == true) {
      if (access != null) {
        if (access == 'ft46t3') {
          switch (pos) {
            case 0:
              pretest = MyriadPretest(
                  chatTextSize: _ChangingFontSize, key: _keyMyriadPreTest);
              return pretest;
            case 1:
              return new KnowledgeBot(chatTextSize: _ChangingFontSize);
            case 2:
              return new CounselorConnect(chatTextSize: _ChangingFontSize);
            default:
              return new CommingSoon();
          }
        }
      }
    } else {
      switch (pos) {
        case 0:
          /*pretest = MyriadPretest(
              chatTextSize: _ChangingFontSize, key: _keyMyriadPreTest);
          return pretest;*/
          return OptraHCS(chatTextSize: _ChangingFontSize);
        case 1:
          return new KnowledgeBot(chatTextSize: _ChangingFontSize);
//          return new ConnectWithCounselor();
          break;
        case 2:
          return new CounselorConnect(chatTextSize: _ChangingFontSize);
        default:
          return new CommingSoon();
      }
    }
  }

  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
    Navigator.of(context).pop(); // close the drawer
  }

  Widget _buildSpeedPanel(
      List<String> _optionDataList, List<Item> _data, String subTitle) {
    return Container(
      color: readSpeedBckClr, //color: Theme.of(context).primaryColor,
      margin: EdgeInsets.only(left: 0.0, right: 0.0, top: 0.0),
      child: Theme(
        data: Theme.of(context).copyWith(cardColor: readSpeedBckClr),
        child: ExpansionPanelList(
          expansionCallback: (int index, bool isExpanded) {
            setState(() {
              _data[index].isExpanded = !isExpanded;
              color = Colors.transparent;
              color1 = Colors.transparent;
              color2 = Colors.transparent;
              color3 = Colors.transparent;
            });
          },
          children: _data.map<ExpansionPanel>((Item item) {
            return ExpansionPanel(
              canTapOnHeader: true,
              headerBuilder: (BuildContext context, bool isExpanded) {
                return ListTile(
                  title: Text(
                    item.headerValue,
                    style: textStyleBrandingSemibold,
                  ),
                  leading: Image.asset(
                    'images/dr_speed.png',
                    height: 30,
                    width: 30,
                    color: drawerImgClr,
                  ),
                  subtitle: isReadSpeed
                      ? Text(
                          selectedReadSpeed,
                          style: textStyleBrandingBoldLight,
                        )
                      : Text(
                          selectedReadSpeed,
                          style: textStyleBrandingBoldLight,
                        ),
                );
              },
              body: Container(
                color: speedFontSubMenuClr,
                child: Column(
                  children: <Widget>[
                    Container(
                      child: ListTile(
                          title: Text(
                            _optionDataList[0],
                            style: textStyleBrandingBoldLight,
                          ),
                          leading: Visibility(
                            visible: false,
                            child: Image.asset(
                              'images/dr_speed.png',
                              height: 24,
                              width: 24,
                              color: drawerImgClr,
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              color = highlightClr;
                              print('Select Option is ${_optionDataList[0]}');
                              isReadSpeed = true;
                              selectedReadSpeed = _optionDataList[0];
                              item.isExpanded = false;
                            });
                            updateReadSpeedInPreferences(ReadSpeed.slow);
                          }),
                      color: color,
                    ),
                    Container(
                      color: color1,
                      child: ListTile(
                          title: Text(
                            _optionDataList[1],
                            style: textStyleBrandingBoldLight,
                          ),
                          leading: Visibility(
                            visible: false,
                            child: Image.asset(
                              'images/dr_speed.png',
                              height: 24,
                              width: 24,
                              color: drawerImgClr,
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              color1 = highlightClr;
                              print('Select Option is ${_optionDataList[1]}');
                              isReadSpeed = true;
                              selectedReadSpeed = _optionDataList[1];
                              item.isExpanded = false;
                            });
                            updateReadSpeedInPreferences(ReadSpeed.normal);
                          }),
                    ),
                    Container(
                      color: color2,
                      child: ListTile(
                          title: Text(
                            _optionDataList[2],
                            style: textStyleBrandingBoldLight,
                          ),
                          leading: Visibility(
                            visible: false,
                            child: Image.asset(
                              'images/dr_speed.png',
                              height: 24,
                              width: 24,
                              color: drawerImgClr,
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              color2 = highlightClr;
                              print('Select Option is ${_optionDataList[2]}');
                              isReadSpeed = true;
                              selectedReadSpeed = _optionDataList[2];
                              item.isExpanded = false;
                            });
                            updateReadSpeedInPreferences(ReadSpeed.medium);
                          }),
                    ),
                    Container(
                      color: color3,
                      child: ListTile(
                          title: Text(
                            _optionDataList[3],
                            style: textStyleBrandingBoldLight,
                          ),
                          leading: Visibility(
                            visible: false,
                            child: Image.asset(
                              'images/dr_speed.png',
                              height: 24,
                              width: 24,
                              color: drawerImgClr,
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              color3 = highlightClr;
                              print('Select Option is ${_optionDataList[3]}');
                              isReadSpeed = true;
                              selectedReadSpeed = _optionDataList[3];
                              item.isExpanded = false;
                            });
                            updateReadSpeedInPreferences(ReadSpeed.high);
                          }),
                    ),
                  ],
                ),
              ),
              isExpanded: item.isExpanded,
            );
          }).toList(),
        ),
      ),
    );
  }

  Widget _buildFontPanel(
      List<String> _optionDataList, List<Item> _data, String subTitle) {
    return Container(
      color: fontBckClr, //color: Theme.of(context).primaryColor,
      margin: EdgeInsets.only(left: 0.0, right: 0.0, top: 0.0),
      child: Theme(
        data: Theme.of(context).copyWith(cardColor: fontBckClr),
        child: ExpansionPanelList(
          expansionCallback: (int index, bool isExpanded) {
            setState(() {
              _data[index].isExpanded = !isExpanded;
              color4 = Colors.transparent;
              color5 = Colors.transparent;
              color6 = Colors.transparent;
            });
          },
          children: _data.map<ExpansionPanel>((Item item) {
            return ExpansionPanel(
              canTapOnHeader: true,
              headerBuilder: (BuildContext context, bool isExpanded) {
                return ListTile(
                  title: Text(
                    item.headerValue,
                    style: textStyleBrandingSemibold,
                  ),
                  leading: Image.asset(
                    'images/dr_font.png',
                    height: 28,
                    width: 28,
                    color: drawerImgClr,
                  ),
                  subtitle: isFontSize
                      ? Text(
                          selectedSubtiltleFont,
                          style: textStyleBrandingBoldLight,
                        )
                      : Text(
                          "Normal",
                          style: textStyleBrandingBoldLight,
                        ),
                );
              },
              body: Container(
                color: speedFontSubMenuClr,
                child: Column(
                  children: <Widget>[
                    Container(
                      color: color4,
                      child: ListTile(
                          title: Text(
                            _optionDataList[0],
                            style: textStyleBrandingBoldLight,
                          ),
                          leading: Image.asset(
                            'images/dr_font.png',
                            height: 18,
                            width: 18,
                            color: drawerImgClr,
                          ),
                          onTap: () {
                            setState(() {
                              color4 = highlightClr;
                              print('Select Option is ${_optionDataList[0]}');
                              isFontSize = true;
                              fontSize = 16.0;
                              selectedSubtiltleFont = _optionDataList[0];
                              item.isExpanded = false;
                              setState(() {
                                _ChangingFontSize = 12;
                                updateFontSizeInPreferences(_ChangingFontSize);
                              });
                            });
                          }),
                    ),
                    Container(
                      color: color5,
                      child: ListTile(
                          title: Text(
                            _optionDataList[1],
                            style: textStyleBrandingBoldLight,
                          ),
                          leading: Image.asset(
                            'images/dr_font.png',
                            height: 20,
                            width: 20,
                            color: drawerImgClr,
                          ),
                          onTap: () {
                            setState(() {
                              color5 = highlightClr;
                              print('Select Option is ${_optionDataList[1]}');
                              isFontSize = true;
                              fontSize = 18.0;
                              selectedSubtiltleFont = _optionDataList[1];
                              setState(() {
                                _ChangingFontSize = 15;
                                updateFontSizeInPreferences(_ChangingFontSize);
                              });
                              item.isExpanded = false;
                            });
                          }),
                    ),
                    Container(
                      color: color6,
                      child: ListTile(
                          title: Text(
                            _optionDataList[2],
                            style: textStyleBrandingBoldLight,
                          ),
                          leading: Image.asset(
                            'images/dr_font.png',
                            height: 24,
                            width: 24,
                            color: drawerImgClr,
                          ),
                          onTap: () {
                            setState(() {
                              color6 = highlightClr;
                              print('Select Option is ${_optionDataList[2]}');
                              isFontSize = true;
                              fontSize = 20.0;
                              selectedSubtiltleFont = _optionDataList[2];
                              item.isExpanded = false;
                              setState(() {
                                _ChangingFontSize = 16;
                                updateFontSizeInPreferences(_ChangingFontSize);
                              });
                            });
                          }),
                    ),
                  ],
                ),
              ),
              isExpanded: item.isExpanded,
            );
          }).toList(),
        ),
      ),
    );
  }

  void updateFontSizeInPreferences(double changingFontSize) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setDouble(Log.sp_fontSize, changingFontSize);
    prefs.commit();
  }

  void updateReadSpeedInPreferences(ReadSpeed speed) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (speed == ReadSpeed.slow) {
      await prefs.setInt(Log.sp_readSpeed, Log.slow);
      prefs.commit();
    } else if (speed == ReadSpeed.normal) {
      await prefs.setInt(Log.sp_readSpeed, Log.normal);
      prefs.commit();
    } else if (speed == ReadSpeed.medium) {
      await prefs.setInt(Log.sp_readSpeed, Log.medium);
      prefs.commit();
    } else if (speed == ReadSpeed.high) {
      await prefs.setInt(Log.sp_readSpeed, Log.high);
      prefs.commit();
    }

    print('READ SPEED CHANGED TO : ${prefs.getInt(Log.sp_readSpeed)}');
  }

  _getReadSpeed() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      if (prefs.containsKey(Log.sp_readSpeed)) {
        print(
            'HOME PAGE INITIAL STAGE : CURRENT READ SPEED IS : ${prefs.getInt(Log.sp_readSpeed)}');
        switch (prefs.getInt((Log.sp_readSpeed))) {
          case Log.slow:
            setState(() {
              selectedReadSpeed = "0.5x";
            });
            break;

          case Log.normal:
            setState(() {
              selectedReadSpeed = "1x";
            });
            break;
          case Log.medium:
            setState(() {
              selectedReadSpeed = "1.5x";
            });
            break;

          case Log.high:
            setState(() {
              selectedReadSpeed = "2x";
            });
            break;
        }
      } else {
        print('HOME PAGE INITIAL STAGE : CURRENT READ SPEED IS : NULL');
        setState(() {
          selectedReadSpeed = "1x";
        });
      }
    });
  }

  Widget _DarkModeSwitch() {
    return Container(
      height: 68,
      color: themeSwitcherBckClr,
      child: ListTile(
          title: Text('Dark Mode', style: textStyleBrandingSemibold),
          trailing: Switch(
//          focusColor: Colors.white,
//          activeTrackColor: Colors.white,
            onChanged: (val) {
              setState(() {
                _isNotificationSwitched = val;

                if (val == true) {
                  dynamicColor(switchOnColor);
                } else {
                  dynamicColor(switchOFFColor);
                }
              });
            },
            value: _isNotificationSwitched,
            activeColor: Colors.white,
          ),
          leading: Transform.rotate(
            angle: 150 * pi / 180,
            child: Icon(
              Icons.brightness_2,
              color: Colors.white,
              size: 34,
            ),
          )),
    );
  }

  void dynamicColor(Color CustomColor) {
    setState(() {
      // fontImgHeaderColor = CustomColor;
      drawerBackgrdClr = CustomColor; //Color.fromRGBO(0, 25, 52, 1.0);
      homeBackgroundColor = CustomColor; //Color.fromRGBO(0, 36, 75, 1.0);
      askAnyBckClr = CustomColor; //Color.fromRGBO(0, 36, 75, 1.0);
      readSpeedBckClr = CustomColor; //Color.fromRGBO(0, 36, 75, 1.0);
      fontBckClr = CustomColor; //Color.fromRGBO(0, 36, 75, 1.0);
      themeSwitcherBckClr = CustomColor;
      optionColor = CustomColor;
      //drawerImgClr = CustomColor;
      speedFontSubMenuClr = CustomColor; //Color.fromRGBO(0, 56, 95, 1.0);
      headerColor = CustomColor; //Color.fromRGBO(0, 153, 180, 1.0);
      drawerHeaderColor = CustomColor;
      //optionTextColor = CustomColor; //Color.fromRGBO(89, 148, 172, 1);
      rightBubbleBG = CustomColor;
      userIconColor = CustomColor;
      textFieldCursorColor = CustomColor;
      textFieldUnFocussedColor = CustomColor;
      textFieldFocussedColor = CustomColor;
      themeColor = CustomColor;
      selectedEthnicityColor = CustomColor;
      unselectedEthnicityColor = Colors.grey;
      sendIconColor = CustomColor;
      changeResponseLoader = CustomColor;
      closeBtnClr = CustomColor;
      textFieldDecorationClr = CustomColor;
      alertBtnClr = CustomColor;
      autosuggestionProgressBar = CustomColor;
      // leftBubbleBG = CustomColor;
      myriadUserIconColor = CustomColor;
      splashTryButtonColor = CustomColor;
      SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
      SystemChrome.setSystemUIOverlayStyle(
          SystemUiOverlayStyle(statusBarColor: CustomColor.withOpacity(0)));
    });
  }

  static const platform = const MethodChannel('flutter.native/helper');

  Future handleCounselorConnect() async {
    if (Platform.isAndroid) {
      try {
        await platform.invokeMethod('chatWithCounselor');
      } on PlatformException catch (e) {
        debugPrint("Exception Message : ${e.message}");
      }
    }
  }

  void checkForNotificationCount() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List notifications = prefs.getStringList(PREF_NOTIFICATION_DATA);
    var jsonArray = json.decode(notifications.toString());
    if (notifications != null) {
      int counter = 0;
      for (int index = 0; index < notifications.length; index++) {
        var object = jsonDecode(notifications[index]);
        Log.printLog('HomePage', 'isRead value : ${object['isRead']}');
        if (object['isRead'].toString() == 'false') {
          counter++;
        }
      }
      setState(() {
        unReadNotificationCount = counter;
      });
    }
  }

  void addNotificationMessage(Map<String, dynamic> message) async {
    var notification = message['notification'];
    var title = notification['title'];
    var body = notification['body'];
    var data = message['data'];
    var view = data['view'];
    var type = data['type'];
    var imageUrl = data['image_url'];

    NotificationModel model = new NotificationModel(
        title: title,
        message: body,
        imageUrl: imageUrl,
        view: view,
        type: type,
        isRead: false);
    String json = jsonEncode(model);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> notifications = prefs.getStringList(PREF_NOTIFICATION_DATA);
    if (notifications == null) {
      notifications = new List<String>();
    }
    notifications.insert(0, json);
    prefs.setStringList(PREF_NOTIFICATION_DATA, notifications);

    checkForNotificationCount();
  }
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Reload Chat', icon: Icons.question_answer),
  const Choice(title: 'Ask Any Question', icon: Icons.question_answer),
//  const Choice(title: 'Schedule Appointment', icon: Icons.person),
//  const Choice(title: 'Knowledge Bot', icon: Icons.person),
//  const Choice(title: 'Counselor Connect', icon: Icons.person)
];

// stores ExpansionPanel state information
class Item {
  Item({
    this.headerValue,
    this.isExpanded = false,
    // this.readSpeeds
  });

  String headerValue;
  bool isExpanded;

//  dynamic readSpeeds = new List<String>();
}

List<Item> generateItems(int numberOfItems, String name) {
  return List.generate(numberOfItems, (int index) {
    return Item(
      headerValue: name,
    );
  });
}
