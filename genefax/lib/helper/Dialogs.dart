import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ai/Constant.dart';
import 'package:ai/Loader.dart';
import 'package:ai/Log.dart';

class Dialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    /* return showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        content:
        new Text("Please wait...", style: TextStyle(color: Color(0xFF6E6E6E))),
      ),
    );*/

    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Color.fromRGBO(232, 239, 245, 1),
                  children: <Widget>[
                    Center(
                      /*child: Column(children: [
                        Theme(
                            data: Theme.of(context)
                                .copyWith(accentColor: changeResponseLoader),
                            child: CircularProgressIndicator()),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Please Wait....",
                          style: TextStyle(color: changeResponseLoader),
                        )
                      ]
                      ),*/
                      child: Loader(),
                    )
                  ])

          );

        });
  }
}
