//Colors
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Preferences

final String PREF_NOTIFICATION_DATA = 'notification_data';

/*const Color headerColor = Color.fromRGBO(0, 153, 180, 1.0);
const Color drawerHeaderColor = Color(0xFFFFFFFF);
const Color chatBotBottomContainerBG = Color(0xFFFFFFFF); // Color.fromRGBO(213, 224, 236, 1)*/

//////  faint Drawer DIPIKA
//Color fontImgHeaderColor = Color(0xFFFFFFFF); //Color(0xFFFFFFFF);
//Color drawerBackgrdClr = Color(0xff3B698F); //Color.fromRGBO(0, 25, 52, 1.0);
//Color homeBackgroundColor = Color(0xff4F8BC0); //Color.fromRGBO(0, 36, 75, 1.0);
//Color askAnyBckClr = Color(0xff4F8BC0);
//Color themeSwitcherBckClr = Color(0xff4F8BC0); //Color.fromRGBO(0, 36, 75, 1.0);
//Color readSpeedBckClr = Color(0xff4F8BC0); //Color.fromRGBO(0, 36, 75, 1.0);
//Color fontBckClr = Color(0xff4F8BC0); //Color.fromRGBO(0, 36, 75, 1.0);
//Color drawerImgClr = Color(0xFFFFFFFF);
//Color speedFontSubMenuClr = Color(0xff4F8BC0);
//
//Color switchOnColor = Color.fromRGBO(33, 150, 243, 1.0);
//Color switchOFFColor = Color(0xff00BFFF);

////  faint Drawer DIPIKA Dynamic Default
Color fontImgHeaderColor = Color(0xFFFFFFFF); //Color(0xFFFFFFFF);
Color drawerBackgrdClr = Color(0xff00BFFF); //Color.fromRGBO(0, 25, 52, 1.0);
Color homeBackgroundColor = Color(0xff00BFFF); //Color.fromRGBO(0, 36, 75, 1.0);
Color askAnyBckClr = Color(0xff00BFFF);
Color themeSwitcherBckClr = Color(0xff00BFFF); //Color.fromRGBO(0, 36, 75, 1.0);
Color readSpeedBckClr = Color(0xff00BFFF); //Color.fromRGBO(0, 36, 75, 1.0);
Color fontBckClr = Color(0xff00BFFF); //Color.fromRGBO(0, 36, 75, 1.0);
Color drawerImgClr = Color(0xFFFFFFFF);
Color speedFontSubMenuClr = Color(0xff00BFFF);
//start Old Dark
//Color highlightClr = Color(0xFF091A31); //Color(0xFFFFFFFF);
//Color switchOnColor = Color(0xff000000);
//End Old Dark

Color switchOFFColor = Color(0xff00BFFF);

//New Dark
Color highlightClr = Color(0xFF091A31); //Color(0xFFFFFFFF);
Color switchOnColor = Color(0xff000000);

// MAIN SCREEN

//const Color statusBarColor = Color(0xFFFFFFFF);
//const Color splashTryButtonColor = Color(0xff00BFFF);
//const TextStyle splashTextStyleBrandingBoldSmall =
//TextStyle(fontFamily: 'BrandingMedium', fontSize: 14, color: Colors.black);

//////  Deep sky theme
Color headerColor = Color(0xff00BFFF); //Color.fromRGBO(0, 153, 180, 1.0);
Color drawerHeaderColor = Color(0xFFFFFFFF);
//const Color chatBotBottomContainerBG = Color(0xFF757575); //Colors.transparent;  // Color.fromRGBO(213, 224, 236, 1);
Color chatBotBottomContainerBG = Colors.transparent;
// Color.fromRGBO(213, 224, 236, 1);
Color optionColor = Color(0xff00BFFF); //Color.fromRGBO(89, 148, 172, 1);
Color optionTextColor = Colors.white; //Color.fromRGBO(89, 148, 172, 1);
Color rightBubbleBG = Color(0xff00BFFF);
Color rightTextColor = Colors.white;
Color userIconColor = Color(0xff00BFFF);
Color chatScreenBackground = Color(0xffEFEFEF);
Color textFieldCursorColor = Color(0xff00BFFF);
Color textFieldUnFocussedColor = Colors.blueGrey;
Color textFieldFocussedColor = Color(0xff00BFFF);
Color themeColor = Color(0xff00BFFF);
Color selectedEthnicityColor = Color(0xff00BFFF);
Color unselectedEthnicityColor = Colors.grey;
Color sendIconColor = Color(0xff00BFFF);
Color changeResponseLoader = Color(0xff00BFFF);
Color closeBtnClr = Color(0xff00BFFF);
Color textFieldDecorationClr = Color(0xff00BFFF);
Color alertBtnClr = Color(0xff00BFFF);
Color autosuggestionProgressBar = Color(0xff00BFFF);
Color leftBubbleBG = Color(0xffFFFFFF);
Color myriadUserIconColor = Color(0xff00BFFF);
Color subMenuCustomColor =
    Color(0xff00BFFF); //Color.fromRGBO(0, 153, 180, 1.0);

////  blue theme
//const Color headerColor = Color(0xff03A9F4); //Color.fromRGBO(0, 153, 180, 1.0);
//const Color drawerHeaderColor = Color(0xFFFFFFFF);
////const Color chatBotBottomContainerBG = Color(0xFF757575); //Colors.transparent;  // Color.fromRGBO(213, 224, 236, 1);
//const Color chatBotBottomContainerBG = Colors.transparent;
//// Color.fromRGBO(213, 224, 236, 1);
//const Color optionColor = Color(0xff03A9F4); //Color.fromRGBO(89, 148, 172, 1);
//const Color optionTextColor = Colors.white; //Color.fromRGBO(89, 148, 172, 1);
//const Color rightBubbleBG = Color(0xff03A9F4);
//const Color rightTextColor = Color(0xffFFFFFF);
//const Color userIconColor = Color(0xff40829A);
//const Color chatScreenBackground = Color(0xffEFEFEF);
//const Color textFieldCursorColor = Colors.blue;
//const Color textFieldUnFocussedColor = Colors.blueGrey;
//const Color textFieldFocussedColor = Colors.blue;
//const Color themeColor = Color(0xff6BA6BB);
//const Color selectedEthnicityColor = Color(0xff03A9F4);
//const Color unselectedEthnicityColor = Colors.grey;
//const Color sendIconColor = Color(0xff03A9F4);
//const Color changeResponseLoader = Color(0xff03A9F4);
//const Color closeBtnClr = Color(0xff03A9F4);
//const Color textFieldDecorationClr = Color(0xff03A9F4);
//const Color alertBtnClr = Color(0xff03A9F4);
//const Color autosuggestionProgressBar = Color(0xff03A9F4);
//
//const Color myriadUserIconColor = Color(0xff03A9F4);

// status bar
const Color statusBarColor = Color(0xFFFFFFFF);
Color splashTryButtonColor = Color(0xff00BFFF);
const TextStyle splashTextStyleBrandingBoldSmall =
    TextStyle(fontFamily: 'BrandingBold', fontSize: 16, color: Colors.white);

//TestStyle
double fontSize = 18;
const TextStyle headertextStyleBrandingSemibold = TextStyle(
    fontFamily: 'BrandingSemibold', fontSize: 24, color: Colors.white);
const TextStyle drawerheaderTextStyleBrandingBold =
    TextStyle(fontFamily: 'BrandingBold', fontSize: 20, color: Colors.white);
const TextStyle headerTextStyleBrandingBold =
    TextStyle(fontFamily: 'BrandingBold', fontSize: 18);
const TextStyle headerTextStyleBrandingMedium =
    TextStyle(fontFamily: 'BrandingMedium', fontSize: 14);
const TextStyle headerTextStyleBrandingMediumAgentStatus =
TextStyle(fontFamily: 'BrandingMedium', fontSize: 14,color: Colors.white);
const TextStyle footerTextStyleBrandingMedium =
    TextStyle(fontFamily: 'BrandingMedium', fontSize: 14, color: Colors.white);
const TextStyle textStyleBrandingBoldSmall =
    TextStyle(fontFamily: 'BrandingBold', fontSize: 16, color: Colors.black);
const TextStyle textStyleBrandingBoldNormal =
    TextStyle(fontFamily: 'BrandingBold', fontSize: 18, color: Colors.black);
const TextStyle textStyleBrandingBoldLarge =
    TextStyle(fontFamily: 'BrandingBold', fontSize: 20, color: Colors.black);
TextStyle textStyleBrandingSemibold = TextStyle(
    fontFamily: 'BrandingSemibold', fontSize: 16, color: Colors.white);
const TextStyle textStyleBrandingBoldLight = TextStyle(
    fontFamily: 'BrandingSemiBold', fontSize: 16, color: Colors.white);
const TextStyle textStyleDrawerVersion = TextStyle(
    fontFamily: 'BrandingSemiBold', fontSize: 14, color: Colors.white);

const TextStyle brandingMedium =
    TextStyle(fontFamily: 'BrandingMedium', fontSize: 14);

//String
const String footerText = "© Copyright OptraHEALTH Inc.";
const String geneFaxProText = "GeneFAX™";
const String geneFaxProText1 = "Digital Genetic Companion";

void vibrateDevice() {
  HapticFeedback.vibrate();
}
