import 'dart:developer';

import 'package:shared_preferences/shared_preferences.dart';

class Log {
  String title = "";
  String message = "";
  static bool displayLog = true;
  static String sp_fontSize = "fontsize";
  static String sp_readSpeed = "readspeed";

  static const int slow = 60;
  static const int normal = 15;
  static const int medium = 10;
  static const int high = 5;

  static var API_STACK = "http://pro.genefax.ai/GeneFAXWebRestHCS/mastercontroller/"; // Production
//  static var API_STACK_MYRIAD = "http://13.89.217.245:8080/GeneFAXProMyriadRest/mastercontroller/";
//  static var API_STACK_MYRIAD = "http://52.165.154.50:8080/GeneFAXProMyriadRestTest/mastercontroller/"; //Production
  static var API_STACK_MYRIAD = "http://13.89.217.245:8080/GeneFAXProMyriadRestMobile/mastercontroller/"; //Development

  static int LIST_ITEM_ANIM_DURATION = 400;

  static double radius = 2.0;

  static double formFieldRadius = 5.0;

  Log(this.title, this.message);

  static printLog(String title, String message) {
    if (displayLog) {
      log('$title : $message');
    }
  }

  static int getMessageDelayDuration(int messageLength, int speed) {
    if (speed == null || speed == 0) {
      return messageLength * normal;
    } else {
      return messageLength * (speed);
    }
  }

  static int getLoaderDelayDuration(int messageLength, int speed) {
    if (speed == null || speed == 0) {
      if(speed==normal) {
        return messageLength * (speed>0?(speed/2):speed);
      }
      return messageLength * normal;
    } else {
      /*if(speed==normal) {
        return messageLength * (speed>0?(speed/2):speed);
      }*/
      return messageLength * (speed);
    }
  }

  static bool isUserMale(String gender) {
    if (gender == "Male") {
      return true;
    } else {
      return false;
    }
  }

  static int getUserAge(String criteria) {
    if (criteria == "Below 45") {
      return 0;
    } else if (criteria == "Between 45 - 60") {
      return 1;
    } else if (criteria == "Above 60") {
      return 2;
    }
  }
}
