import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:ai/Animations/ListItemAnimator.dart';
import 'package:ai/ChatMessage.dart';
import 'package:ai/Constant.dart';
import 'package:ai/MessageType.dart';
import 'package:ai/helper/Dialogs.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'Log.dart';
import 'SizeConfig.dart';

var _nextQuestionID = 1;
var _currentQuestionID = 1;
String _cancerTypeValue = "";

class MyriadPretest extends StatefulWidget {
  final Function function;

  MyriadPretest({this.chatTextSize, Key key, this.function}) : super(key: key);

  double chatTextSize = 16.0;
  double buttonTextSize = 15.0;
  double formFieldTextSize = 15.0;

  TextStyle optionButtonStyle;

  StateHomeScreen myAppState = new StateHomeScreen();

//  MyriadPretest({this.chatTextSize});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return StateHomeScreen();
  }

  void updateFont(double changingFontSize) {
    chatTextSize = changingFontSize;
    myAppState.updateFont(changingFontSize);
  }
}

class OptionsModel {
  int eqm_id;
  String question;
  String more_info;
  String commnets;
  bool is_descriptive;
  int inserted_timestamp;
  int eom_id;
  int question_id;
  String option;
  int nextq_id;
  int nextq_id2;
  int redirect_to;
  String btn_id;

  OptionsModel(
      {this.eqm_id,
      this.question,
      this.more_info,
      this.commnets,
      this.is_descriptive,
      this.inserted_timestamp,
      this.eom_id,
      this.question_id,
      this.option,
      this.nextq_id,
      this.nextq_id2,
      this.redirect_to,
      this.btn_id});
}

class DiseaseModel {
  int id;
  String name;

  DiseaseModel({this.id, this.name});
}

class AutoSuggetionModel {
  int id;
  String name;

  AutoSuggetionModel({this.id, this.name});
}

class StateHomeScreen extends State<MyriadPretest>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  FocusNode _focus = new FocusNode();
  final ScrollController _scrollControllerAutoComplete = new ScrollController();
  final ScrollController _scrollController = new ScrollController();
  List<ChatMessage> _list = <ChatMessage>[];
  final List<ChatMessage> _staticData = <ChatMessage>[];
  List<OptionsModel> _items = new List<OptionsModel>();
  bool _showHi = false;
  bool _showOptions = false;
  bool _show2 = false;
  bool _show4 = false;
  bool _showFooter = false;
  bool _isEmailAddressSubmitted = false;
  bool _show5 = false;
  bool _show6 = false;
  bool _show10 = false;
  bool _show11 = false;
  bool _show23 = false;
  bool _show25 = false;
  bool _show26 = false;
  bool _show27 = false;
  bool _show28 = false;
  bool _show31 = false;
  bool _show39 = false;
  bool _show38 = false;
  bool _show18 = false;
  bool _show41 = false;
  bool _show20 = false;
  bool _showAskQuestion = false;
  bool _showAskMoreOptions = false;

  List<CancerTypes> _cancerTypes = [];

  String _selectedCondition = "Select condition";
  String _selectedGender = "Select gender";
  String _selectedGenderForRelative = "Select gender";
  String _selectedCancerType = "Select cancer";

  int _userAge;
  bool _userGender;

  final TextEditingController _tec_ask_question = new TextEditingController();
  final TextEditingController _tec_relative_age = new TextEditingController();
  final TextEditingController _tec_email = new TextEditingController();
  final TextEditingController _tec_disease_list = new TextEditingController();

  static List<DiseaseModel> diseaseList = new List<DiseaseModel>();
  static List<AutoSuggetionModel> autoSuggestionList =
      new List<AutoSuggetionModel>();
  bool loading = true;

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  var _tagSpacing = 30.0;

  List<Item> cancerData;

  var _columnCount = null;
  var _shouldSymmetry = false;

  int _readSpeed = Log.normal;

  double _autoSuggestionHeight = 33.0;

  bool _isKeyboardOpen = false;

  bool isPatientPregnant = false;

  var buttonHeight = 40.0;

  bool checkboxValueCity = false;
  List<String> allEthnicities = [
    "African or African American",
    "Ashkenazi Jewish",
    "East Asian e.g. Chinese, Japanese",
    "French Canadian or Cajun",
    "Hispanic",
    "Middle Eastern",
    "Native American",
    "Northern European e.g. British, German",
    "Other/Mixed Caucasian",
    "Pacific Islander",
    "South Asian e.g. Indian, Pakistani",
    "Southeast Asian e.g. Filipino, Vietnamese",
    "Southern European e.g. Italian, Greek",
    "Unknown"
  ];

  List<String> selectedEthnicity = [];

  var _appointmentDate = 3;

  String resultData = "";
  bool _showTestUI = false;

  void displayAlert(String title, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text(""),
        content: new Text("$message"),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text("Close", style: TextStyle(color: closeBtnClr)),
              onPressed: () {
                Navigator.pop(context, 'Cancel');
              })
        ],
      ),
    );
  }

  void updateFont(double font) {
    setState(() {
      widget.chatTextSize = font;
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      print("PRETEST - SCREEN RESUMED");
    }
  }

  Future<double> updateFontSizeFromPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print(
        'FONT SIZE IS : ${prefs.containsKey(Log.sp_fontSize) ? prefs.getDouble(Log.sp_fontSize) : 0}');
    return prefs.containsKey(Log.sp_fontSize)
        ? prefs.getDouble(Log.sp_fontSize)
        : 15;
  }

  void displayAlertForChangeResponse(
      String title, String message, int messageId) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("$title",
            style: TextStyle(
              fontStyle: FontStyle.normal,
            )),
        content:
            new Text("\n$message", style: TextStyle(color: Color(0xFF6E6E6E))),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text(
                "Cancel",
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () {
                vibrateDevice();
                Navigator.pop(context, 'Cancel');
              }),
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text("Change", style: TextStyle(color: closeBtnClr)),
              onPressed: () {
                vibrateDevice();
                Navigator.pop(context, 'Cancel');
                Dialogs.showLoadingDialog(context, _keyLoader);
                _getMessage(messageId).then((message) {
                  Navigator.of(_keyLoader.currentContext).pop();
                  setState(() {
                    if (_list.last.type == MessageType.DIVIDER) {
                      _list.removeLast();
                    }
                    _list.add(new ChatMessage(
                        ID: _currentQuestionID,
                        isEditable: false,
                        text: "Changing Response",
                        type: MessageType.DIVIDER,
                        context: context,
                        backgroundType: null,
                        highlightedWords: null,
                        textFontSize: widget.chatTextSize));

                    _list.add(new ChatMessage(
                        ID: _currentQuestionID,
                        isEditable: false,
                        text: message,
                        type: MessageType.LEFT,
                        context: context,
                        backgroundType: null,
                        highlightedWords: null,
                        textFontSize: widget.chatTextSize));
                  });
                  scrollList();
                  handleUIVisibility();
                });
              })
        ],
      ),
    );
  }

  void displayAlertWithActionLable(
      String title, String message, String actionLable) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("$title"),
        content: new Text("$message"),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child:
                  new Text(actionLable, style: TextStyle(color: alertBtnClr)),
              onPressed: () {
                Navigator.pop(context, 'Cancel');
              })
        ],
      ),
    );
  }

  void displayRetry(String title, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("$title"),
        content: new Text("$message"),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text("Retry", style: TextStyle(color: alertBtnClr)),
              onPressed: () {
                Navigator.pop(context, 'Cancel');
                if (_currentQuestionID == 1) {
                  _loadData();
                } else {
                  _loadNext();
                }
              })
        ],
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    _focus.addListener(_onFocusChange);
    /* Model().getItems().then((items){
      _items = items;
    });*/

    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          _isKeyboardOpen = visible;
        });
      },
    );

    _loadData();

    WidgetsBinding.instance.addObserver(this);

    super.initState();
  }

  Future<String> _getMessage(int ID) async {
    String url =
        "${Log.API_STACK_MYRIAD}getQuestionsModuleWise?q_id=$ID&module=PretestMyriad&type=module&authtoken=tSN4DRaEQ5";

    Log.printLog("API URL", url);

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.post(url);
        var jsonData = json.decode(data.body);

        Log.printLog("API RESPONSE", jsonData.toString());

        var jsonResponse = jsonData['data'];

        if (jsonResponse.length > 1) {
          var jsonObject = jsonData['data'][0];
          Log.printLog("QUESTION : ", "${jsonObject['question']}");
          Log.printLog("NXT Q ID : ", "${jsonObject['nextq_id']}");
          Log.printLog("CUR Q ID : ", "${jsonObject['question_id']}");
          setState(() {
            _nextQuestionID = jsonObject['nextq_id'] as int;
            _currentQuestionID = jsonObject['question_id'] as int;
          });

          _items.clear();
          for (int index = 0; index < jsonResponse.length; index++) {
            var jsonData = jsonResponse[index];
            _items.add(OptionsModel(
              eqm_id: jsonData['eqm_id'] as int,
              question: jsonData['question'] as String,
              more_info: jsonData['more_info_mobile'] as String,
              commnets: jsonData['commnets'] as String,
              is_descriptive: jsonData['is_descriptive'] as bool,
              inserted_timestamp: jsonData['inserted_timestamp'] as int,
              eom_id: jsonData['eom_id'] as int,
              question_id: jsonData['question_id'] as int,
              option: jsonData['option'] as String,
              nextq_id: jsonData['nextq_id'] as int,
              nextq_id2: jsonData['nextq_id2'] as int,
              redirect_to: jsonData['redirect_to'] as int,
              btn_id: jsonData['btn_id'] as String,
            ));
          }

          String question = jsonObject['question'];
          question = question.replaceAll("{patientName}", "Emma");
          question = question.replaceAll("{patientFirstName}", "Emma");
          question = question.replaceAll(
              "{patientProviderName}", "Greenwood Genetics Center");
          question =
              question.replaceAll("{ProviderName}", "Greenwood Genetic Center");
          DateTime today = new DateTime.now();
          var afterThreeDaysDate =
              today.add(new Duration(days: _appointmentDate));
          String next3DaysAppointmentDate =
              "${afterThreeDaysDate.month.toString().padLeft(2, '0')}/${afterThreeDaysDate.day.toString().padLeft(2, '0')}/${afterThreeDaysDate.year.toString()}";

          question = question.replaceAll(
              "{AppointmentDate}", next3DaysAppointmentDate);
          List<String> arrList = question.split(r"$$");
          Log.printLog("MY DATA", " $arrList");
//          showStaticMessages();
          return arrList[arrList.length - 1];
        } else if (jsonResponse.length > 0 && jsonResponse.length == 1) {
          var jsonObject = jsonData['data'][0];
          Log.printLog("QUESTION : ", "${jsonObject['question']}");
          Log.printLog("NXT Q ID : ", "${jsonObject['nextq_id']}");
          Log.printLog("CUR Q ID : ", "${jsonObject['question_id']}");
          setState(() {
            _nextQuestionID = jsonObject['nextq_id'] as int;
            _currentQuestionID = jsonObject['question_id'] as int;
          });

          String question = jsonObject['question'];
          question = question.replaceAll("{patientName}", "Emma");
          question = question.replaceAll("{patientFirstName}", "Emma");
          question = question.replaceAll(
              "{patientProviderName}", "Greenwood Genetics Center");
          question =
              question.replaceAll("{ProviderName}", "Greenwood Genetic Center");
          DateTime today = new DateTime.now();
          var afterThreeDaysDate =
              today.add(new Duration(days: _appointmentDate));
          String next3DaysAppointmentDate =
              "${afterThreeDaysDate.month.toString().padLeft(2, '0')}/${afterThreeDaysDate.day.toString().padLeft(2, '0')}/${afterThreeDaysDate.year.toString()}";
          question = question.replaceAll(
              "{AppointmentDate}", next3DaysAppointmentDate);
          List<String> arrList = question.split(r"$$");
          Log.printLog("MY DATA", " $arrList");
//          showStaticMessages();
          return arrList[arrList.length - 1];
        }
      }
    } on SocketException catch (_) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
      setState(() {
        _list.removeLast();
        /*  _list.add(ChatMessage(
        ID: _currentQuestionID,isEditable: false,textFontSize: widget.textFontSize,
            text: "Please check your internet connection and try again.",
            type: "ERROR",
            optionList: null,backgroundType: 0));*/
      });
      displayRetry("", "Please check internet connection and try again.");
    }
  }

  Future<void> _getResumingMessages(int ID) async {
    String url =
        "${Log.API_STACK_MYRIAD}getQuestionsModuleWise?q_id=$ID&module=PretestMyriad&type=module&authtoken=tSN4DRaEQ5";

    Log.printLog("API URL", url);

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.post(url);
        var jsonData = json.decode(data.body);

        Log.printLog("API RESPONSE", jsonData.toString());

        var jsonResponse = jsonData['data'];

        if (jsonResponse.length > 1) {
          var jsonObject = jsonData['data'][0];
          Log.printLog("QUESTION : ", "${jsonObject['question']}");
          Log.printLog("NXT Q ID : ", "${jsonObject['nextq_id']}");
          Log.printLog("CUR Q ID : ", "${jsonObject['question_id']}");
          setState(() {
            _nextQuestionID = jsonObject['nextq_id'] as int;
            _currentQuestionID = jsonObject['question_id'] as int;
          });

          _items.clear();
          for (int index = 0; index < jsonResponse.length; index++) {
            var jsonData = jsonResponse[index];
            _items.add(OptionsModel(
              eqm_id: jsonData['eqm_id'] as int,
              question: jsonData['question'] as String,
              more_info: jsonData['more_info_mobile'] as String,
              commnets: jsonData['commnets'] as String,
              is_descriptive: jsonData['is_descriptive'] as bool,
              inserted_timestamp: jsonData['inserted_timestamp'] as int,
              eom_id: jsonData['eom_id'] as int,
              question_id: jsonData['question_id'] as int,
              option: jsonData['option'] as String,
              nextq_id: jsonData['nextq_id'] as int,
              nextq_id2: jsonData['nextq_id2'] as int,
              redirect_to: jsonData['redirect_to'] as int,
              btn_id: jsonData['btn_id'] as String,
            ));
          }

          String question = jsonObject['question'];
          question = question.replaceAll("{patientName}", "Emma");
          question = question.replaceAll("{patientFirstName}", "Emma");
          question = question.replaceAll(
              "{patientProviderName}", "Greenwood Genetics Center");
          question =
              question.replaceAll("{ProviderName}", "Greenwood Genetic Center");
          DateTime today = new DateTime.now();
          var afterThreeDaysDate =
          today.add(new Duration(days: _appointmentDate));
          String next3DaysAppointmentDate =
              "${afterThreeDaysDate.month.toString().padLeft(2, '0')}/${afterThreeDaysDate.day.toString().padLeft(2, '0')}/${afterThreeDaysDate.year.toString()}";

          question = question.replaceAll(
              "{AppointmentDate}", next3DaysAppointmentDate);
          List<String> arrList = question.split(r"$$");
          Log.printLog("MY DATA", " $arrList");
          for (int index = 0; index < arrList.length; index++) {
            if (index == 0) {
              _staticData.add(ChatMessage(
                  ID: _currentQuestionID,
                  isEditable: false,
                  textFontSize: widget.chatTextSize,
                  backgroundType: _list.length > 0 ? 0 : -1,
                  text: arrList[index],
                  type: MessageType.LEFT,
                  optionList: null));
            } else if (index == arrList.length - 1) {
              _staticData.add(ChatMessage(
                  ID: _currentQuestionID,
                  isEditable: false,
                  textFontSize: widget.chatTextSize,
                  backgroundType: 2,
                  text: arrList[index],
                  type: MessageType.LEFT,
                  optionList: null));
            } else {
              _staticData.add(ChatMessage(
                  ID: _currentQuestionID,
                  isEditable: false,
                  textFontSize: widget.chatTextSize,
                  backgroundType: 1,
                  text: arrList[index],
                  type: MessageType.LEFT,
                  optionList: null));
            }
          }
          showStaticMessages();
//          return arrList[arrList.length - 1];
        } else if (jsonResponse.length > 0 && jsonResponse.length == 1) {
          var jsonObject = jsonData['data'][0];
          Log.printLog("QUESTION : ", "${jsonObject['question']}");
          Log.printLog("NXT Q ID : ", "${jsonObject['nextq_id']}");
          Log.printLog("CUR Q ID : ", "${jsonObject['question_id']}");
          setState(() {
            _nextQuestionID = jsonObject['nextq_id'] as int;
            _currentQuestionID = jsonObject['question_id'] as int;
          });

          String question = jsonObject['question'];
          question = question.replaceAll("{patientName}", "Emma");
          question = question.replaceAll("{patientFirstName}", "Emma");
          question = question.replaceAll(
              "{patientProviderName}", "Greenwood Genetics Center");
          question =
              question.replaceAll("{ProviderName}", "Greenwood Genetic Center");
          DateTime today = new DateTime.now();
          var afterThreeDaysDate =
          today.add(new Duration(days: _appointmentDate));
          String next3DaysAppointmentDate =
              "${afterThreeDaysDate.month.toString().padLeft(2, '0')}/${afterThreeDaysDate.day.toString().padLeft(2, '0')}/${afterThreeDaysDate.year.toString()}";
          question = question.replaceAll(
              "{AppointmentDate}", next3DaysAppointmentDate);
          List<String> arrList = question.split(r"$$");
          Log.printLog("MY DATA", " $arrList");
          for (int index = 0; index < arrList.length; index++) {
            if (index == 0) {
              _staticData.add(ChatMessage(
                  ID: _currentQuestionID,
                  isEditable: false,
                  textFontSize: widget.chatTextSize,
                  backgroundType: _list.length > 0 ? 0 : -1,
                  text: arrList[index],
                  type: MessageType.LEFT,
                  optionList: null));
            } else if (index == arrList.length - 1) {
              _staticData.add(ChatMessage(
                  ID: _currentQuestionID,
                  isEditable: false,
                  textFontSize: widget.chatTextSize,
                  backgroundType: 2,
                  text: arrList[index],
                  type: MessageType.LEFT,
                  optionList: null));
            } else {
              _staticData.add(ChatMessage(
                  ID: _currentQuestionID,
                  isEditable: false,
                  textFontSize: widget.chatTextSize,
                  backgroundType: 1,
                  text: arrList[index],
                  type: MessageType.LEFT,
                  optionList: null));
            }
          }
          showStaticMessages();
//          return arrList[arrList.length - 1];
        }
      }
    } on SocketException catch (_) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
      setState(() {
        _list.removeLast();
        /*  _list.add(ChatMessage(
        ID: _currentQuestionID,isEditable: false,textFontSize: widget.textFontSize,
            text: "Please check your internet connection and try again.",
            type: "ERROR",
            optionList: null,backgroundType: 0));*/
      });
      displayRetry("", "Please check internet connection and try again.");
    }
  }

  _loadData() async {
    /*String testurl =
        "http://pro.genefax.ai/GeneFAXWebRestHCS/mastercontroller/getRecommendationsCategory?cat=4&pin=51218&authtoken=tSN4DRaEQ5";

    Log.printLog("API URL", testurl);
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
      var data = await http.post(testurl);
      var jsonData = json.decode(data.body);
      Log.printLog("API RESPONSE", jsonData["data"].toString());

      setState(() {
        _showTestUI = true;
        resultData = jsonData["data"].toString();
      });
    }

    return;*/

    http: //13.89.217.245:8080/GeneFAXProMyriadRest/mastercontroller/getQuestionsModuleWise?q_id=1&module=PretestMyriad&type=module&authtoken=tSN4DRaEQ5

    String url =
        "${Log.API_STACK_MYRIAD}getQuestionsModuleWise?q_id=1&module=PretestMyriad&type=module&authtoken=tSN4DRaEQ5";

//    String url = "https://api.myjson.com/bins/1ev1pw";
//    String url = "https://api.myjson.com/bins/bhcok";
//    String url = "https://api.myjson.com/bins/10vlss"; //questionID=7
//    String url = "https://api.myjson.com/bins/l84b0"; //questionID=8
//    String url = "https://api.myjson.com/bins/14piro";

    Log.printLog("API URL", url);

    try {
      setState(() {
        _list.clear();
        _list.add(ChatMessage(
            ID: _currentQuestionID,
            isEditable: false,
            textFontSize: widget.chatTextSize,
            type: MessageType.LOADER,
            optionList: null,
            backgroundType: 0));
      });

      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.post(url);
        var jsonData = json.decode(data.body);

        setState(() {
          if (_list.last.type == MessageType.LOADER) {
            _list.removeLast();
          }
        });

        Log.printLog("API RESPONSE", jsonData.toString());

        var jsonObject = jsonData['data'][0];
        Log.printLog("QUESTION : ", "${jsonObject['question_mobile']}");
        Log.printLog("NXT Q ID : ", "${jsonObject['nextq_id']}");
        Log.printLog("CUR Q ID : ", "${jsonObject['question_id']}");
        setState(() {
          _nextQuestionID = jsonObject['nextq_id'] as int;
          _currentQuestionID = jsonObject['question_id'] as int;
        });
        String question = jsonObject['question_mobile'];
        question = question.replaceAll("{patientName}", "Emma");
        question = question.replaceAll("{patientFirstName}", "Emma");
        question = question.replaceAll(
            "{patientProviderName}", "Greenwood Genetics Center");
        question =
            question.replaceAll("{ProviderName}", "Greenwood Genetic Center");

        List<String> arrList = question.split(r"$$");

        Log.printLog("MY DATA", " $arrList");

        for (int index = 0; index < arrList.length; index++) {
          String data = arrList[index];
          _staticData.add(ChatMessage(
            ID: _currentQuestionID,
            isEditable: false,
            textFontSize: widget.chatTextSize,
            text: data,
            type: MessageType.LEFT,
            optionList: null,
            backgroundType: _list.length > 0 ? 0 : -1,
          ));
        }

        /*  for (int index = 0; index < arrList.length; index++) {
          if (index == 0) {
            String data = arrList[index];

            _staticData.add(ChatMessage(
              ID: _currentQuestionID,
              isEditable: false,
              textFontSize: widget.chatTextSize,
              text: data,
              type: MessageType.LEFT,
              optionList: null,
              backgroundType: _list.length > 0 ? 0 : -1,
            ));
          } else if (index == arrList.length - 1) {
            String data = arrList[index];
            data = data.replaceAll(r"item1 ^^", "");
            data = data.replaceAll(r"item2 ^^", "");

            _staticData.add(ChatMessage(
              ID: _currentQuestionID,
              isEditable: false,
              textFontSize: widget.chatTextSize,
              text: data,
              type: MessageType.LEFT,
              optionList: null,
              backgroundType: 2,
            ));
          } else {
            String data = arrList[index];
            data = data.replaceAll(r"item1 ^^", "");
            data = data.replaceAll(r"item2 ^^", "");

            _staticData.add(ChatMessage(
              ID: _currentQuestionID,
              isEditable: false,
              textFontSize: widget.chatTextSize,
              text: data,
              type: MessageType.LEFT,
              optionList: null,
              backgroundType: 1,
            ));
          }
        }*/

        /* _staticData.removeLast();
        _staticData.removeLast();
        _staticData.removeLast();

        String dataImage = arrList[1];
        String dataImage1 = arrList[2].substring(0, arrList[2].length - 2);
        String dataImage2 = arrList[3].substring(0, arrList[3].length - 2);
        dataImage1 = dataImage1.replaceAll(r"item1 ^^", "");
        dataImage1 = dataImage1.replaceAll(r"item2 ^^", "");

        dataImage2 = dataImage2.replaceAll(r"item1 ^^", "");
        dataImage2 = dataImage2.replaceAll(r"item2 ^^", "");

        StringBuffer buffer = new StringBuffer(dataImage);
        buffer.write("\n${dataImage1}");
        buffer.write("${dataImage2}");

        _staticData.add(ChatMessage(
          ID: _currentQuestionID,
          isEditable: false,
          textFontSize: widget.chatTextSize,
          text: buffer.toString(),
          type: MessageType.LEFT,
          optionList: null,
          backgroundType: 2,
        ));*/

        showStaticMessages();
      }
    } on SocketException catch (_) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
      setState(() {
        _list.removeLast();
        /*  _list.add(ChatMessage(
        ID: _currentQuestionID,isEditable: false,textFontSize: widget.textFontSize,
            text: "Please check your internet connection and try again.",
            type: "ERROR",
            optionList: null,backgroundType: 0));*/
      });
      displayRetry("", "Please check internet connection and try again.");
    }
  }

  _loadNext() async {
    hideAll();

    setState(() {
      _items.clear();
    });
    String questionID = "";

    new Future.delayed(Duration(milliseconds: Log.LIST_ITEM_ANIM_DURATION * 2),
        () async {
      if (_nextQuestionID == 0) {
        questionID = _cancerTypeValue;
      } else {
        questionID = _nextQuestionID.toString();
      }

      String url =
          "${Log.API_STACK_MYRIAD}getQuestionsModuleWise?q_id=$questionID&module=PretestMyriad&type=module&authtoken=tSN4DRaEQ5";

      Log.printLog("API URL", url);
      try {
        setState(() {
          _list.add(ChatMessage(
              ID: _currentQuestionID,
              isEditable: false,
              textFontSize: widget.chatTextSize,
              type: MessageType.LOADER,
              optionList: null,
              backgroundType: 0));
        });
        scrollList();

        final result = await InternetAddress.lookup('google.com');
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
          var data = await http.post(url);
          var jsonData = json.decode(data.body);

          setState(() {
            if (_list.last.type == MessageType.LOADER) {
              _list.removeLast();
            }
          });

          Log.printLog("API RESPONSE", jsonData.toString());
          var jsonResponse = jsonData['data'];

          if (jsonResponse.length > 1) {
            var jsonObject = jsonData['data'][0];
            Log.printLog("QUESTION : ", "${jsonObject['question_mobile']}");
            Log.printLog("NXT Q ID : ", "${jsonObject['nextq_id']}");
            Log.printLog("CUR Q ID : ", "${jsonObject['question_id']}");
            setState(() {
              _nextQuestionID = jsonObject['nextq_id'] as int;
              _currentQuestionID = jsonObject['question_id'] as int;
            });

            _items.clear();
            for (int index = 0; index < jsonResponse.length; index++) {
              var jsonData = jsonResponse[index];
              _items.add(OptionsModel(
                eqm_id: jsonData['eqm_id'] as int,
                question: jsonData['question_mobile'] as String,
                more_info: jsonData['more_info_mobile'] as String,
                commnets: jsonData['commnets'] as String,
                is_descriptive: jsonData['is_descriptive'] as bool,
                inserted_timestamp: jsonData['inserted_timestamp'] as int,
                eom_id: jsonData['eom_id'] as int,
                question_id: jsonData['question_id'] as int,
                option: jsonData['option'] as String,
                nextq_id: jsonData['nextq_id'] as int,
                nextq_id2: jsonData['nextq_id2'] as int,
                redirect_to: jsonData['redirect_to'] as int,
                btn_id: jsonData['btn_id'] as String,
              ));
            }

            String question = jsonObject['question_mobile'];
            question = question.replaceAll("{patientName}", "Emma");
            question = question.replaceAll("{patientFirstName}", "Emma");
            question = question.replaceAll(
                "{patientProviderName}", "Greenwood Genetics Center");
            question = question.replaceAll(
                "{ProviderName}", "Greenwood Genetic Center");
            DateTime today = new DateTime.now();
            var afterThreeDaysDate =
                today.add(new Duration(days: _appointmentDate));
            String next3DaysAppointmentDate =
                "${afterThreeDaysDate.month.toString().padLeft(2, '0')}/${afterThreeDaysDate.day.toString().padLeft(2, '0')}/${afterThreeDaysDate.year.toString()}";

            question = question.replaceAll(
                "{AppointmentDate}", next3DaysAppointmentDate);
            List<String> arrList = question.split(r"$$");
            Log.printLog("MY DATA", " $arrList");

            for (int index = 0; index < arrList.length; index++) {
              if (index == 0) {
                _staticData.add(ChatMessage(
                    ID: _currentQuestionID,
                    isEditable: false,
                    textFontSize: widget.chatTextSize,
                    backgroundType: _list.length > 0 ? 0 : -1,
                    text: arrList[index],
                    type: MessageType.LEFT,
                    optionList: null));
              } else if (index == arrList.length - 1) {
                _staticData.add(ChatMessage(
                    ID: _currentQuestionID,
                    isEditable: false,
                    textFontSize: widget.chatTextSize,
                    backgroundType: 2,
                    text: arrList[index],
                    type: MessageType.LEFT,
                    optionList: null));
              } else {
                _staticData.add(ChatMessage(
                    ID: _currentQuestionID,
                    isEditable: false,
                    textFontSize: widget.chatTextSize,
                    backgroundType: 1,
                    text: arrList[index],
                    type: MessageType.LEFT,
                    optionList: null));
              }
            }
            showStaticMessages();
          } else if (jsonResponse.length > 0 && jsonResponse.length == 1) {
            var jsonObject = jsonData['data'][0];
            Log.printLog("QUESTION : ", "${jsonObject['question_mobile']}");
            Log.printLog("NXT Q ID : ", "${jsonObject['nextq_id']}");
            Log.printLog("CUR Q ID : ", "${jsonObject['question_id']}");
            setState(() {
              _nextQuestionID = jsonObject['nextq_id'] as int;
              _currentQuestionID = jsonObject['question_id'] as int;
            });

            String question = jsonObject['question_mobile'];
            question = question.replaceAll("{patientName}", "Emma");
            question = question.replaceAll("{patientFirstName}", "Emma");
            question = question.replaceAll(
                "{patientProviderName}", "Greenwood Genetics Center");
            question = question.replaceAll(
                "{ProviderName}", "Greenwood Genetics Center");
            DateTime today = new DateTime.now();
            var afterThreeDaysDate =
                today.add(new Duration(days: _appointmentDate));
            String next3DaysAppointmentDate =
                "${afterThreeDaysDate.month.toString().padLeft(2, '0')}/${afterThreeDaysDate.day.toString().padLeft(2, '0')}/${afterThreeDaysDate.year.toString()}";
            question = question.replaceAll(
                "{AppointmentDate}", next3DaysAppointmentDate);
            List<String> arrList = question.split(r"$$");
            Log.printLog("MY DATA", " $arrList");

            for (int index = 0; index < arrList.length; index++) {
              if (index == 0) {
                _staticData.add(ChatMessage(
                    ID: _currentQuestionID,
                    isEditable: false,
                    textFontSize: widget.chatTextSize,
                    backgroundType: _list.length > 0 ? 0 : -1,
                    text: arrList[index],
                    type: MessageType.LEFT,
                    optionList: null));
              } else if (index == arrList.length - 1) {
                _staticData.add(ChatMessage(
                    ID: _currentQuestionID,
                    isEditable: false,
                    textFontSize: widget.chatTextSize,
                    backgroundType: 2,
                    text: arrList[index],
                    type: MessageType.LEFT,
                    optionList: null));
              } else {
                _staticData.add(ChatMessage(
                    ID: _currentQuestionID,
                    isEditable: false,
                    textFontSize: widget.chatTextSize,
                    backgroundType: 1,
                    text: arrList[index],
                    type: MessageType.LEFT,
                    optionList: null));
              }
            }
            showStaticMessages();
          }

          if (_currentQuestionID == 25) {
            setState(() {
              _shouldSymmetry = true;
              _columnCount = 0;
            });
          }

          if (_currentQuestionID == 6) {
            setState(() {
              _shouldSymmetry = false;
              _columnCount = null;
            });
          }

          if (_currentQuestionID == 41) {
            setState(() {
              _shouldSymmetry = true;
              _columnCount = 0;
            });
          }
//prasad
          if (_items.length > 4) {
            setState(() {
              _tagSpacing = 30.0;
            });
          } else {
            setState(() {
              _tagSpacing = 15.0;
            });
          }
        }
      } on SocketException catch (_) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
        setState(() {
          _list.removeLast();
          /*  _list.add(ChatMessage(
        ID: _currentQuestionID,isEditable: false,textFontSize: widget.textFontSize,
            text: "Please check your internet connection and try again.",
            type: "ERROR",
            optionList: null,backgroundType: 0));*/
        });
        displayRetry("", "Please check internet connection and try again.");
      }
    });
  }

  _askQuestion(String question) async {
    _tec_ask_question.clear();
    hideAll();
    ChatMessage object3 = new ChatMessage(
        ID: _currentQuestionID,
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: question,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _list.add(object3);
    });
    scrollList();
    String url =
        "${Log.API_STACK_MYRIAD}getAnswerFreeTextMPretest?question=$question&authtoken=tSN4DRaEQ5";
    Log.printLog("API URL", url);
    try {
      setState(() {
        _list.add(ChatMessage(
            ID: _currentQuestionID,
            isEditable: false,
            textFontSize: widget.chatTextSize,
            type: MessageType.LOADER,
            optionList: null,
            backgroundType: 0));
      });

      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.post(url);
        var jsonData = json.decode(data.body);

        setState(() {
          if (_list.last.type == MessageType.LOADER) {
            _list.removeLast();
          }
        });

        Log.printLog("API RESPONSE", jsonData.toString());

        var jsonObject = jsonData['data'][0];
        Log.printLog("QUESTION : ", "${jsonObject['answer']}");

        String question = jsonObject['answer'];
        question = question.replaceAll("{patientName}", "Emma");
        question = question.replaceAll("{patientFirstName}", "Emma");
        question = question.replaceAll(
            "{patientProviderName}", "Greenwood Genetics Center");
        question =
            question.replaceAll("{ProviderName}", "Greenwood Genetic Center");

        List<String> arrList = question.split(r". ");

        Log.printLog("MY DATA", " $arrList");
        _staticData.clear();
        for (int index = 0; index < arrList.length; index++) {
          String data = arrList[index];
          _staticData.add(ChatMessage(
            ID: _currentQuestionID,
            isEditable: false,
            textFontSize: widget.chatTextSize,
            text: data,
            type: MessageType.LEFT,
            optionList: null,
            backgroundType: _list.length > 0 ? 0 : -1,
          ));
        }
        showAnswer();
      }
    } on SocketException catch (_) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
      setState(() {
        _list.removeLast();
        /*  _list.add(ChatMessage(
        ID: _currentQuestionID,isEditable: false,textFontSize: widget.textFontSize,
            text: "Please check your internet connection and try again.",
            type: "ERROR",
            optionList: null,backgroundType: 0));*/
      });
      displayRetry("", "Please check internet connection and try again.");
    }
  }

  _onFocusChange() {
    debugPrint("TextField Focus: ${_focus.hasFocus}");
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    widget.optionButtonStyle = TextStyle(
        color: optionTextColor,
        fontFamily: 'BrandingMedium',
        fontSize: ScreenUtil().setSp(widget.buttonTextSize));

//    return Center(child: _showTestUI?Text(resultData):Text("Loading..."));

    return SafeArea(
      child: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
//                colors: [Color(0xFFf7f2f2), Color(0xFFf7f2f2),Color(0xFFf7f2f2)])),
                  colors: [
                chatScreenBackground,
                chatScreenBackground,
                chatScreenBackground
              ])),
//        color: Color.fromRGBO(232, 239, 245, 1),
//        color: Color(0xffEAEEF3),
          child: Column(
            children: <Widget>[
              Flexible(
                child: Container(
//                color: Colors.white,
                  /*  child: ListView.builder(
                    controller: _scrollController,
                    padding: new EdgeInsets.all(8.0),
                    itemBuilder: (_, int index) => _list[index],
                    itemCount: _list.length,
                  ),*/
                  child: ListView.builder(
                    controller: _scrollController,
                    padding: new EdgeInsets.all(8.0),
                    itemCount: _list.length,
                    itemBuilder: _getListItem,
                  ),
                ),
              ),
              Visibility(
                visible: _showHi,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5.0),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Hi!",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handlePersonalDetailForm(),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show2,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            height: buttonHeight,
                            child: RaisedButton(
                              color: optionColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(Log.radius),
                                    topRight: Radius.circular(Log.radius),
                                    bottomLeft: Radius.circular(Log.radius),
                                    bottomRight: Radius.circular(Log.radius)),
                              ),
                              child:
                                  Text("Yes", style: widget.optionButtonStyle),
                              onPressed: () => handle2("Yes"),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(10),
                        ),
                        Expanded(
                          child: Container(
                            height: buttonHeight,
                            child: RaisedButton(
                              color: optionColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(Log.radius),
                                    topRight: Radius.circular(Log.radius),
                                    bottomLeft: Radius.circular(Log.radius),
                                    bottomRight: Radius.circular(Log.radius)),
                              ),
                              child: Text("Come back later",
                                  style: widget.optionButtonStyle),
                              onPressed: () => handle2("Come back later"),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show4,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              borderRadius: new BorderRadius.circular(5.0)),
                          padding: EdgeInsets.only(left: 0, right: 0),
                          child: TextField(
                            cursorColor: textFieldDecorationClr,
                            // cursor color
                            controller: _tec_email,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                              hintText: "Enter email address",
                              isDense: true,
                              contentPadding: EdgeInsets.only(
                                  left: 10.0,
                                  right: 10.0,
                                  top: 10.0,
                                  bottom: 10.0),
                              hintStyle: TextStyle(
                                  color: Color.fromRGBO(153, 153, 153, 1)),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: textFieldDecorationClr, width: 1.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: textFieldDecorationClr, width: 1.0),
                              ),
                            ),
                            keyboardType: TextInputType.emailAddress,
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.012,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Submit",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle4("submit"),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(10),
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Skip",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle4("skip"),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show5,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5.0),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Yes, I'm pregnant",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle5(0),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(10),
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("No",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle5(2),
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: ScreenUtil.screenHeightDp * 0.012),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("I am planning to conceive",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle5(1),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show6,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        _getMultipleSelectUI(),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.012,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Continue",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle6(),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show10,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Yes!",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle10(0),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: ScreenUtil().setHeight(10),
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("No...",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle10(1),
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: ScreenUtil.screenHeightDp * 0.012),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("I've already been tested",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle10(2),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show11,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Got it",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle11("Got it"),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show23,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Should I be concerned?",
                                      style: widget.optionButtonStyle),
                                  onPressed: () =>
                                      handle23("Should I be concerned?"),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show25,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: ScreenUtil().setHeight(40),
                          width: ScreenUtil.screenWidthDp,
                          padding: EdgeInsets.only(right: 5),
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              border: Border.all(color: Colors.blueGrey),
                              borderRadius: new BorderRadius.circular(5.0)),
                          child: DropdownButtonHideUnderline(
                            child: Listener(
                            /*  onPointerDown: (_) => FocusScope.of(context)
                                  .unfocus(focusPrevious: false),*/
                              child: DropdownButton<String>(
                                items: <String>[
                                  'Birth defect',
                                  'Autism',
                                  'Intellectual delay',
                                  'Down syndrome',
                                  'Genetic disease',
                                  'Other'
                                ].map((String _selectedValue) {
                                  return new DropdownMenuItem<String>(
                                    value: _selectedValue,
                                    child: new Text(
                                      _selectedValue,
                                      style: TextStyle(color: Colors.black),
                                      textAlign: TextAlign.right,
                                    ),
                                  );
                                }).toList(),
                                hint: Container(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Text(_selectedCondition,
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                          color: _selectedCondition ==
                                                  'Select condition'
                                              ? Colors.grey
                                              : Colors.black)),
                                ),
                                onChanged: (String _selectedValue) {
                                  vibrateDevice();
                                  setState(() {
                                    /*FocusScope.of(context)
                                              .requestFocus(new FocusNode());*/
                                    _selectedCondition = _selectedValue;
                                  });
                                },
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.010,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Continue",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle25(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show26,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("What's the next step?",
                                      style: widget.optionButtonStyle),
                                  onPressed: () =>
                                      handle26("What's the next step?"),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show28,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Got it!",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle28("Got it!"),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show20,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: 50,
                                child: RaisedButton(
                                  padding:
                                      EdgeInsets.only(bottom: 5.0, top: 5.0),
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text(
                                    "Makes sense",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                  onPressed: () => handle20(0),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(10),
                            ),
                            Expanded(
                              child: Container(
                                height: 50,
                                child: RaisedButton(
                                  padding:
                                      EdgeInsets.only(bottom: 5.0, top: 5.0),
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("What kind of management?",
                                      style: widget.optionButtonStyle,
                                      textAlign: TextAlign.center),
                                  onPressed: () => handle20(1),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show31,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text(
                                      "I want to have carrier screening",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle31(0),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.01,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text(
                                    "I am interested but want to talk to a genetic counselor",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                  onPressed: () => handle31(1),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.01,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("I am not sure this is for me",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle31(2),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _showAskQuestion,
                child: Column(
                  children: <Widget>[
                    Divider(
                      height: 1,
                      color: Colors.grey,
                    ),
                    Container(
                      color: Colors.white,
                      padding: EdgeInsets.only(
                          left: ScreenUtil.screenWidthDp * 0.02,
                          top: ScreenUtil.screenWidthDp * 0.007,
                          bottom: ScreenUtil.screenWidthDp * 0.02,
                          right: ScreenUtil.screenWidthDp * 0.02),
                      child: Column(
                        children: <Widget>[
                          loading
                              ? LinearProgressIndicator()
                              : Container(
                                  height: _autoSuggestionHeight,
                                  child: ListView.builder(
                                    controller: _scrollControllerAutoComplete,
                                    padding: EdgeInsets.only(bottom: 5.0),
                                    itemCount: autoSuggestionList == null
                                        ? 0
                                        : autoSuggestionList.length,
                                    itemBuilder: _getAutoSuggestions,
                                  ),
                                ),
                          SizedBox(
                            height: ScreenUtil.screenHeightDp * 0.01,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                border: Border.all(color: Colors.blueGrey),
                                borderRadius: new BorderRadius.circular(27.0)),
                            padding: EdgeInsets.only(left: 20, right: 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Flexible(
                                  child: TextField(
                                    onChanged: (String value) {
                                      _getAutoSuggestionQuestionList(value);
                                    },
                                    focusNode: _focus,
                                    onEditingComplete: _focus.unfocus,
                                    textCapitalization:
                                        TextCapitalization.sentences,
                                    controller: _tec_ask_question,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                      hintText:
                                          "I want to ask my own question....",
                                      hintStyle: TextStyle(
                                          color:
                                              Color.fromRGBO(153, 153, 153, 1),
                                          fontSize: ScreenUtil().setSp(14)),
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.transparent),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.transparent),
                                      ),
                                    ),
                                    keyboardType: TextInputType.multiline,
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.only(right: 0, bottom: 0),
                                  child: new IconButton(
                                      icon: new Icon(
                                        Icons.send,
                                        color: sendIconColor,
                                      ),
                                      onPressed: () =>
                                          _askQuestion(_tec_ask_question.text)),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: _showAskMoreOptions,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: buttonHeight,
                              child: RaisedButton(
                                color: optionColor,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(Log.radius),
                                      topRight: Radius.circular(Log.radius),
                                      bottomLeft: Radius.circular(Log.radius),
                                      bottomRight: Radius.circular(Log.radius)),
                                ),
                                child: Text("I have another question",
                                    style: widget.optionButtonStyle),
                                onPressed: () => handleShowMoreOptions(0),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: ScreenUtil.screenHeightDp * 0.01,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: buttonHeight,
                              child: RaisedButton(
                                color: optionColor,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(Log.radius),
                                      topRight: Radius.circular(Log.radius),
                                      bottomLeft: Radius.circular(Log.radius),
                                      bottomRight: Radius.circular(Log.radius)),
                                ),
                                child: Text(
                                    "I don't have any further questions",
                                    style: widget.optionButtonStyle),
                                onPressed: () => handleShowMoreOptions(1),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Visibility(
                visible: _show39,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              borderRadius: new BorderRadius.circular(5.0)),
                          padding: EdgeInsets.only(left: 0, right: 0),
                          child: TextField(
                            cursorColor: textFieldDecorationClr,
                            // cursor color
                            controller: _tec_email,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                              hintText: "Enter email address",
                              isDense: true,
                              contentPadding: EdgeInsets.only(
                                  left: 10.0,
                                  right: 10.0,
                                  top: 10.0,
                                  bottom: 10.0),
                              hintStyle: TextStyle(
                                  color: Color.fromRGBO(153, 153, 153, 1)),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: textFieldDecorationClr, width: 1.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: textFieldDecorationClr, width: 1.0),
                              ),
                            ),
                            keyboardType: TextInputType.emailAddress,
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.01,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Submit",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle39("submit"),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Skip",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle39("skip"),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _showOptions,
                child: FractionallySizedBox(
                  widthFactor: 1.0,
                  child: Container(
                    color: chatBotBottomContainerBG,
                    padding: EdgeInsets.only(
                        left: ScreenUtil.screenWidthDp * 0.02,
                        right: ScreenUtil.screenWidthDp * 0.02,
                        top: ScreenUtil.screenWidthDp * 0.02,
                        bottom: ScreenUtil.screenWidthDp * 0.02),
                    /* padding: EdgeInsets.only(
                        left: ScreenUtil.screenWidthDp * 0.015,
                        right: ScreenUtil.screenWidthDp * 0.015,
                        top: ScreenUtil.screenWidthDp * 0.015,
                        bottom: ScreenUtil.screenWidthDp * 0.015),*/
                    child: Container(
                      padding: EdgeInsets.all(5.0),
                      child: Tags(
                        spacing: ScreenUtil.screenWidthDp * 0.03,
                        runSpacing: ScreenUtil.screenWidthDp * 0.025,
                        columns: _columnCount,
                        symmetry: _shouldSymmetry,
                        itemCount: _items.length,
                        itemBuilder: (int index) {
                          final item = _items[index];
                          return Container(
                            height: buttonHeight,
                            child: ItemTags(
                              // Each ItemTags must contain a Key. Keys allow Flutter to
                              // uniquely identify widgets.
                              key: Key(index.toString()),
                              index: index,
                              // required
                              title: item.option,
                              pressEnabled: true,
                              singleItem: true,
                              activeColor: optionColor,
                              color: optionColor,
                              textColor: optionTextColor,
                              borderRadius: BorderRadius.circular(2.0),
                              elevation: 2,
                              padding: EdgeInsets.only(
                                  left: 15, right: 15, top: 9, bottom: 9),
                              textStyle: widget.optionButtonStyle,
                              onPressed: (item) => {handleOptionsClicked(item)},
                              onLongPressed: (item) => {
                                Log.printLog(
                                    "ITEM LONG PRESSED : ", item.toString())
                              },
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show27,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Ok",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle27("Ok"),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show38,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Got it",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle38("Got it"),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _showFooter,
                child: Container(
                  color: Colors.white,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.005,
                      bottom: ScreenUtil.screenWidthDp * 0.005,
                      right: ScreenUtil.screenWidthDp * 0.005,
                      top: ScreenUtil.screenWidthDp * 0.005),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Powered by ',
                              style: TextStyle(
                                  fontFamily: 'BrandingSemiLightItalic',
//                              fontStyle: FontStyle.italic,
                                  color: Colors.black,
                                  fontSize: ScreenUtil().setSp(13)),
                            ),
                            SizedBox(
                              width: 0,
                            ),
                            Image.asset(
                              'images/footer_genefax.png',
                              height: 9,
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show18,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Hmmm..sounds concerning",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle18(0),
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: ScreenUtil.screenHeightDp * 0.01),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              flex: 3,
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("What can i do about it?",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle18(1),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: ScreenUtil.screenWidthDp * 0.03,
                            ),
                            Expanded(
                              flex: 2,
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  color: optionColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Lets move on",
                                      style: widget.optionButtonStyle),
                                  onPressed: () => handle18(2),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show41,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        loading
                            ? LinearProgressIndicator(
                                backgroundColor: autosuggestionProgressBar,
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(Colors.white),
                              )
                            : Container(
                                height: _autoSuggestionHeight,
                                child: ListView.builder(
                                  controller: _scrollControllerAutoComplete,
                                  padding: EdgeInsets.only(bottom: 5.0),
                                  itemCount: diseaseList == null
                                      ? 0
                                      : diseaseList.length,
                                  itemBuilder: _getSuggestions,
                                ),
                              ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.01,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              borderRadius: new BorderRadius.circular(5.0)),
                          padding: EdgeInsets.only(left: 0, right: 0),
                          child: Theme(
                            data: Theme.of(context)
                                .copyWith(splashColor: Colors.white),
                            child: Column(
                              children: <Widget>[
                                TextField(
                                  cursorColor: textFieldDecorationClr,
                                  // cursor color
                                  onChanged: (String value) {
                                    _getDiseaseList(value);
                                  },
                                  controller: _tec_disease_list,
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                    hintText: "Enter disease",
                                    isDense: true,
                                    contentPadding: EdgeInsets.only(
                                        left: 5.0,
                                        right: 5.0,
                                        top: 10.0,
                                        bottom: 10.0),
                                    hintStyle: TextStyle(
                                        color:
                                            Color.fromRGBO(153, 153, 153, 1)),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: textFieldDecorationClr,
                                          width: 1.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: textFieldDecorationClr,
                                          width: 1.0),
                                    ),
                                  ),
                                  keyboardType: TextInputType.text,
                                )
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.01,
                        ),
                        Visibility(
                          visible: _isKeyboardOpen ? false : true,
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: buttonHeight,
                                  child: RaisedButton(
                                    color: optionColor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(Log.radius),
                                          topRight: Radius.circular(Log.radius),
                                          bottomLeft:
                                              Radius.circular(Log.radius),
                                          bottomRight:
                                              Radius.circular(Log.radius)),
                                    ),
                                    child: Text("Continue",
                                        style: widget.optionButtonStyle),
                                    onPressed: () => handle41(),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
//          generated.GeneratedWidget()
            ],
          ),
        ),
      ),
    );
  }

  void showAnswer() {
    _getReadSpeed();
    if (_list.length > 0) {
      setState(() {
        ChatMessage loader = new ChatMessage(
            ID: _currentQuestionID,
            isEditable: false,
            textFontSize: widget.chatTextSize,
            text: "",
            type: MessageType.LOADER,
            optionList: null,
            backgroundType: 0);
        _list.add(loader);
      });
      scrollList();
      if (_staticData.length > 0) {
        new Future.delayed(
            Duration(
                milliseconds: Log.getLoaderDelayDuration(
                    _staticData[0].text.toString().length, _readSpeed)), () {
          setState(() {
            _list.removeLast();
            _list.add(_staticData[0]);
          });

          scrollList();

          _staticData.removeAt(0);
          if (_staticData.length == 0) {
            new Future.delayed(Duration(milliseconds: 1000), () {
              setState(() {
                _showAskMoreOptions = true;
              });
              scrollList();
            });
          } else if (_staticData.length > 0) {
            // This is for actual message reading duration
            new Future.delayed(
                Duration(
                    milliseconds: Log.getMessageDelayDuration(
                        _staticData[0].text.toString().length, _readSpeed)),
                () {
              showAnswer();
            });
          }
        });
      } else {
        // EXIT AND DO NEXT TASK
        new Future.delayed(Duration(milliseconds: 1000), () {
          setState(() {
            _showAskMoreOptions = true;
          });
          scrollList();
        });
      }
    } else {
      if (_staticData.length > 0) {
        setState(() {
          _list.add(_staticData[0]);
        });
        scrollList();

        new Future.delayed(
            Duration(
                milliseconds: Log.getMessageDelayDuration(
                    _list.last.text.toString().length, _readSpeed)), () {
          _staticData.removeAt(0);
          if (_staticData.length > 0) {
            showAnswer();
          }
        });
      }
    }
  }

  void showStaticMessages() {
    _getReadSpeed();
    if (_list.length > 0) {
      setState(() {
        ChatMessage loader = new ChatMessage(
            ID: _currentQuestionID,
            isEditable: false,
            textFontSize: widget.chatTextSize,
            text: "",
            type: MessageType.LOADER,
            optionList: null,
            backgroundType: 0);
        _list.add(loader);
      });
      scrollList();
      if (_staticData.length > 0) {
        new Future.delayed(
            Duration(
                milliseconds: Log.getLoaderDelayDuration(
                    _staticData[0].text.toString().length, _readSpeed)), () {
          setState(() {
            _list.removeLast();
            _list.add(_staticData[0]);
          });

          scrollList();

          _staticData.removeAt(0);
          if (_staticData.length == 0) {
            handleUIVisibility();
          } else if (_staticData.length > 0) {
            // This is for actual message reading duration
            new Future.delayed(
                Duration(
                    milliseconds: Log.getMessageDelayDuration(
                        _staticData[0].text.toString().length, _readSpeed)),
                () {
              showStaticMessages();
            });
          }
        });
      } else {
        // EXIT AND DO NEXT TASK
        handleUIVisibility();
      }
    } else {
      if (_staticData.length > 0) {
        setState(() {
          _list.add(_staticData[0]);
        });
        scrollList();
/*
        print(
            "DURATION IS : ${Log.getMessageDelayDuration(_list.last.text.toString().length, _readSpeed)}");*/

        new Future.delayed(
            Duration(
                milliseconds: Log.getMessageDelayDuration(
                    _list.last.text.toString().length, _readSpeed)), () {
          _staticData.removeAt(0);
          if (_staticData.length > 0) {
            showStaticMessages();
          }
        });
      }
    }
  }

  void handleOptionsClicked(Item item) {
    vibrateDevice();
    if (_items.length > 0) {
      var model = _items.elementAt(item.index);
      /*var model = _items.elementAt(item.index);
      if (_currentQuestionID == 6) {
        _model6 = model;
      }
      if (_currentQuestionID == 7) {
        _model7 = model;
      }

      if (model.option == "Order Test") {
        displayAlertWithActionLable(
            "Order Test",
            "Your order is under review by our genetic counselors. We will provide you an update by email soon.",
            "Ok");
        return;
      }

      if (model.option == "Learn more about HCS") {
        displayAlertWithActionLable("", "Comming soon....", "Ok");
        return;
      }

      if (model.option == "Talk to genetic counselor") {
        displayAlertWithActionLable("", "Comming soon....", "Ok");
        return;
      }*/
      hideAll();
      setState(() {
        if (_currentQuestionID == 20) {
          _list.add(ChatMessage(
              ID: _currentQuestionID,
              isEditable: _items.length > 1 ? true : false,
              textFontSize: widget.chatTextSize,
              text: model.option,
              type: MessageType.RIGHT,
              backgroundType: 0));
          _items.clear();

          _nextQuestionID = model.nextq_id;
          if (model.option == "What kind of management?" && isPatientPregnant) {
            _nextQuestionID = model.nextq_id2;
          }
          _loadNext();
        } else if (model.nextq_id == null && _currentQuestionID == 30) {
          _list.add(new ChatMessage(
              ID: _currentQuestionID,
              isEditable: false,
              context: context,
              text: 'Ask any question',
              type: MessageType.DIVIDER));
          scrollList();
          setState(() {
            loading = false;
            _showAskQuestion = true;
            _autoSuggestionHeight = 0;
            autoSuggestionList.clear();
          });
        } else {
          _list.add(ChatMessage(
              ID: _currentQuestionID,
              isEditable: _items.length > 1 ? true : false,
              textFontSize: widget.chatTextSize,
              text: model.option,
              type: MessageType.RIGHT,
              backgroundType: 0));
          _items.clear();

          _nextQuestionID = model.nextq_id;
          if (_currentQuestionID == 31 && _isEmailAddressSubmitted == false) {
            _nextQuestionID = model.nextq_id2;
          }
          _loadNext();
        }
      });
    }
  }

  void handlePersonalDetailForm() {
    vibrateDevice();
    ChatMessage obj = new ChatMessage(
        ID: _currentQuestionID,
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: "Hi!",
        type: MessageType.RIGHT,
        backgroundType: 0);

    setState(() {
      _showHi = false;
      _showOptions = false;
      _list.add(obj);
    });
    _loadNext();
  }

  void scrollList() {
    /* Timer(
        Duration(milliseconds: 300),
            () => _scrollController
            .jumpTo(_scrollController.position.maxScrollExtent));*/
    Timer(
        Duration(milliseconds: 200),
        () => _scrollController.animateTo(
            _scrollController.position.maxScrollExtent,
            curve: Curves.linear,
            duration: Duration(milliseconds: 300)));
  }

  void handleUIVisibility() {
    hideAll();
    switch (_currentQuestionID) {
      case 1:
        // Show personal details form
        setState(() {
          _showHi = true;
        });
        break;
      case 2:
        setState(() {
          _show2 = true;
        });
        break;
      case 4:
        setState(() {
          _show4 = true;
        });
        break;
      case 5:
        setState(() {
          _show5 = true;
        });
        break;
      case 6:
      case 33:
        setState(() {
//          _selectedEthnicities.clear();
          _show6 = true;
        });
        break;
      case 7:
      case 8:
      case 9:
      case 13:
      case 21:
        setState(() {
          _showOptions = true;
          _shouldSymmetry = true;
          _columnCount = 2;
        });
        break;
      case 10:
        setState(() {
          _show10 = true;
        });
        break;
      case 11:
      case 37:
      case 22:
        setState(() {
          _show11 = true;
        });
        break;
      case 15:
      case 16:
      case 24:
      case 19:
        setState(() {
          _showOptions = true;
          _shouldSymmetry = true;
          _columnCount = 1;
        });
        break;
      case 20:
        setState(() {
          _show20 = true;
        });
        break;
      case 31:
        setState(() {
          _show31 = true;
        });
        break;
      case 23:
        setState(() {
          _show23 = true;
        });
        break;
      case 25:
        setState(() {
          _selectedCondition = "Select condition";
          _show25 = true;
        });
        break;
      case 26:
        setState(() {
          _show26 = true;
        });
        break;
      case 28:
        setState(() {
          _show28 = true;
        });
        break;
      case 29:
      case 30:
      case 34:
        setState(() {
          _showOptions = true;
          _shouldSymmetry = true;
          _columnCount = 2;
        });
        break;
      case 39:
        setState(() {
          _show39 = true;
        });
        break;
      case 27:
        setState(() {
          _show27 = true;
        });
        break;
      case 38:
      case 12:
        setState(() {
          _show38 = true;
        });
        break;
      case 36:
      case 17:
        setState(() {
          _shouldSymmetry = true;
          _columnCount = 1;
          _showOptions = true;
        });
        break;

      case 18:
        setState(() {
          _show18 = true;
        });
        break;
      case 41:
        _getDiseaseList("");
        setState(() {
          _show41 = true;
          _autoSuggestionHeight = 0;
          _tec_disease_list.clear();
        });
        break;
      /*  default:
        setState(() {
          _showOptions = true;
          _showHi = false;
          _show4 = false;
          _show5 = false;
          _show2 = false;
        });
        break;*/
    }
    scrollList();
  }

  _getAutoSuggestionQuestionList(String search) async {
    String url =
        "${Log.API_STACK_MYRIAD}getAutosuggestMPretest?text=${search.length > 0 ? search : ""}";

    Log.printLog("API URL", url);

    setState(() {
      loading = true;
    });

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.post(url);
        var jsonData = json.decode(data.body);
        Log.printLog("API RESPONSE", jsonData.toString());
        List<String> _suggestionList = List.from(jsonData);
        Log.printLog("DISEASE LIST : ", "$_suggestionList");
        autoSuggestionList.clear();
        setState(() {
          loading = false;
        });
        if (_suggestionList.length == 0) {
          setState(() {
            _autoSuggestionHeight = 0;
            autoSuggestionList.clear();
          });
          return;
        }

        for (int index = 0; index < _suggestionList.length; index++) {
          autoSuggestionList.add(
              new AutoSuggetionModel(id: index, name: _suggestionList[index]));
        }
        setState(() {
          if (_suggestionList.length > 4) {
            _autoSuggestionHeight = 132.0;
          } else if (_suggestionList.length > 1 && _suggestionList.length < 4) {
            _autoSuggestionHeight = (33 * _suggestionList.length) as double;
          } else if (_suggestionList.length > 0) {
            _autoSuggestionHeight = 33.0;
          } else {
            _autoSuggestionHeight = 0;
          }
        });
        scrollList();
      }
    } on SocketException catch (_) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
      setState(() {
        /*  _list.add(ChatMessage(
        ID: _currentQuestionID,isEditable: false,textFontSize: widget.textFontSize,
            text: "Please check your internet connection and try again.",
            type: "ERROR",
            optionList: null,backgroundType: 0));*/
      });
      displayRetry("", "Please check internet connection and try again.");
    }
  }

  _getDiseaseList(String search) async {
    String url =
        "${Log.API_STACK_MYRIAD}getAutosuggestMPretestDisease?text=${search.length > 0 ? search : "disease"}";

    Log.printLog("API URL", url);

    setState(() {
      loading = true;
    });

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.post(url);
        var jsonData = json.decode(data.body);
        Log.printLog("API RESPONSE", jsonData.toString());
        List<String> _diseaseList = List.from(jsonData);
        Log.printLog("DISEASE LIST : ", "$_diseaseList");
        diseaseList.clear();
        for (int index = 0; index < _diseaseList.length; index++) {
          diseaseList
              .add(new DiseaseModel(id: index, name: _diseaseList[index]));
        }
        setState(() {
          if (diseaseList.length > 4) {
            _autoSuggestionHeight = 132.0;
          } else if (diseaseList.length > 1 && diseaseList.length < 4) {
            _autoSuggestionHeight = (33 * diseaseList.length) as double;
          } else if (diseaseList.length > 0) {
            _autoSuggestionHeight = 33.0;
          } else {
            _autoSuggestionHeight = 0;
          }
        });

        setState(() {
          loading = false;
        });

        scrollList();
      }
    } on SocketException catch (_) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
      setState(() {
        /*  _list.add(ChatMessage(
        ID: _currentQuestionID,isEditable: false,textFontSize: widget.textFontSize,
            text: "Please check your internet connection and try again.",
            type: "ERROR",
            optionList: null,backgroundType: 0));*/
      });
      displayRetry("", "Please check internet connection and try again.");
    }
  }

  void handle4(String buttonText) {
    vibrateDevice();
    if (buttonText == 'submit') {
      if (_tec_email.text.length == 0) {
        displayAlert("", "Please provide email address");
        return;
      } else if (!RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(_tec_email.text)) {
        displayAlert("", "Invalid email address");
        return;
      }

      _isEmailAddressSubmitted = true;

      ChatMessage object3 = new ChatMessage(
          ID: _currentQuestionID,
          isEditable: false,
          textFontSize: widget.chatTextSize,
          text: _tec_email.text,
          type: MessageType.RIGHT,
          optionList: null,
          backgroundType: 0);

      _tec_email.clear();

      setState(() {
        _list.add(object3);
      });
    } else {
      _isEmailAddressSubmitted = false;
      _tec_email.clear();
      ChatMessage object3 = new ChatMessage(
          ID: _currentQuestionID,
          isEditable: false,
          textFontSize: widget.chatTextSize,
          text: "Skip",
          type: MessageType.RIGHT,
          optionList: null,
          backgroundType: 0);

      setState(() {
        _list.add(object3);
      });
    }
    scrollList();
    _loadNext();
  }

  void handle5(int position) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        ID: _currentQuestionID,
        isEditable: true,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    _nextQuestionID = _items[position].nextq_id;

    if (position == 0) {
      // Patient is pregnant
      isPatientPregnant = true;
    } else {
      // Patient not pregnant
      isPatientPregnant = false;
    }

    setState(() {
      _list.add(object3);
    });
    scrollList();
    _loadNext();
  }

  _getReadSpeed() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('CURRENT READ SPEED IS : ${prefs.getInt(Log.sp_readSpeed)}');
    setState(() {
      if (prefs.containsKey(Log.sp_readSpeed)) {
        _readSpeed = prefs.getInt(Log.sp_readSpeed);
      } else {
        _readSpeed = Log.normal;
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _scrollController.dispose();
    _scrollControllerAutoComplete.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  handle2(String text) {
    vibrateDevice();
    if (text == 'Yes') {
      ChatMessage object3 = new ChatMessage(
          ID: _currentQuestionID,
          isEditable: true,
          textFontSize: widget.chatTextSize,
          text: text,
          type: MessageType.RIGHT,
          personalDetailModel: null,
          relativeDetailModel: null,
          optionList: null,
          backgroundType: 0);

      _nextQuestionID = _items[0].nextq_id;

      setState(() {
        _list.add(object3);
      });
      scrollList();
      _loadNext();
    } else {
      ChatMessage object3 = new ChatMessage(
          ID: _currentQuestionID,
          isEditable: true,
          textFontSize: widget.chatTextSize,
          text: text,
          type: MessageType.RIGHT,
          personalDetailModel: null,
          relativeDetailModel: null,
          optionList: null,
          backgroundType: 0);

      _nextQuestionID = _items[1].nextq_id;

      setState(() {
        _list.add(object3);
      });
      scrollList();
      _loadNext();
    }
  }

  handle6() {
    vibrateDevice();
    if (selectedEthnicity == null || selectedEthnicity.length == 0) {
      displayAlert("", "Please select atleast one ethnicity");
      return;
    } else {
      StringBuffer buffer = new StringBuffer();
      for (int index = 0; index < selectedEthnicity.length; index++) {
        buffer.write("${selectedEthnicity[index]}, ");
      }

      String text = buffer.toString().substring(0, buffer.length - 2);

      ChatMessage object3 = new ChatMessage(
          ID: _currentQuestionID,
          isEditable: true,
          textFontSize: widget.chatTextSize,
          text: text,
          type: MessageType.RIGHT,
          personalDetailModel: null,
          relativeDetailModel: null,
          optionList: null,
          backgroundType: 0);

      setState(() {
        _list.add(object3);
      });
      scrollList();
      _loadNext();
    }
  }

  _getMultipleSelectUI() {
    return Container(
      width: ScreenUtil.screenWidthDp,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: userIconColor),
          borderRadius: new BorderRadius.circular(5.0)),
      padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 0.0, bottom: 0.0),
      child: selectedEthnicity.length > 3
          ? Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                selectedEthnicity.length == 0
                    ? Stack(
                        children: <Widget>[
                          Container(
                            height: buttonHeight,
                            margin: EdgeInsets.only(top: 3.5),
                            alignment: Alignment.topLeft,
                            child: Text(
                              'Select Ethnicity',
                              style: TextStyle(color: unselectedEthnicityColor),
                            ),
                          ),
                          Container(
                            alignment: Alignment.topRight,
                            child: Icon(
                              Icons.arrow_drop_down,
                              color: themeColor,
                            ),
                          ),
                        ],
                      )
                    : Stack(
                        children: <Widget>[
                          Container(
                            height: buttonHeight,
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Selected Ethnicity',
                              style: TextStyle(color: themeColor),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            height: buttonHeight,
                            child: Icon(
                              Icons.arrow_drop_down,
                              color: themeColor,
                            ),
                          ),
                        ],
                      ),
                Container(
                  margin: EdgeInsets.only(bottom: ScreenUtil().setHeight(10)),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: RaisedButton(
                          elevation: 0,
                          child: Text(
                            '${selectedEthnicity.length} selected',
                            style: TextStyle(color: themeColor),
                          ),
                          onPressed: () {
                            // Open Dialog
                            showDialog(
                                context: context,
                                builder: (context) {
                                  vibrateDevice();
                                  return _MultiselectEthnicityDialog(
                                      cities: allEthnicities,
                                      selectedCities: selectedEthnicity,
                                      onSelectedCitiesListChanged: (cities) {
                                        setState(() {
                                          selectedEthnicity = cities;
                                        });
                                        scrollList();
                                      });
                                });
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ],
            )
          : GestureDetector(
              onTap: () {
                vibrateDevice();
                showDialog(
                    context: context,
                    builder: (context) {
                      return _MultiselectEthnicityDialog(
                          cities: allEthnicities,
                          selectedCities: selectedEthnicity,
                          onSelectedCitiesListChanged: (cities) {
                            setState(() {
                              selectedEthnicity = cities;
                            });
                            scrollList();
                          });
                    });
              },
              child: Container(
                width: ScreenUtil.screenWidthDp,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    selectedEthnicity.length == 0
                        ? Stack(
                            children: <Widget>[
                              Container(
                                height: buttonHeight,
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  'Select Ethnicity',
                                  style: TextStyle(
                                      color: unselectedEthnicityColor),
                                ),
                                color: Colors.transparent,
                              ),
                              Container(
                                height: buttonHeight,
                                alignment: Alignment.centerRight,
                                child: Icon(
                                  Icons.arrow_drop_down,
                                  color: themeColor,
                                ),
                              ),
                            ],
                          )
                        : Stack(
                            children: <Widget>[
                              Container(
                                height: buttonHeight,
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  'Selected Ethnicities',
                                  style: TextStyle(color: themeColor),
                                ),
                              ),
                              Container(
                                height: buttonHeight,
                                alignment: Alignment.centerRight,
                                child: Icon(
                                  Icons.arrow_drop_down,
                                  color: themeColor,
                                ),
                              ),
                            ],
                          ),
                    Visibility(
                      visible: selectedEthnicity.length == 0 ? false : true,
                      child: Container(
                        margin:
                            EdgeInsets.only(bottom: ScreenUtil().setHeight(10)),
                        width: ScreenUtil.screenWidthDp,
                        padding: EdgeInsets.all(5.0),
                        child: Tags(
                          spacing: ScreenUtil.screenWidthDp * 0.03,
                          runSpacing: ScreenUtil.screenWidthDp * 0.020,
                          symmetry: false,
                          itemCount: selectedEthnicity.length,
                          itemBuilder: (int index) {
                            return Container(
                              height: 35,
                              child: ItemTags(
                                key: Key(index.toString()),
                                index: index,
                                title: selectedEthnicity[index],
                                pressEnabled: true,
                                singleItem: true,
                                activeColor: selectedEthnicityColor,
                                color: selectedEthnicityColor,
                                textColor: optionTextColor,
                                borderRadius: BorderRadius.circular(2.0),
                                elevation: 2,
                                padding: EdgeInsets.only(
                                    left: 15, right: 15, top: 0, bottom: 0),
//                                  textStyle: widget.optionButtonStyle,
                                textStyle: TextStyle(fontSize: 12),
                                onPressed: (value) => {
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        vibrateDevice();
                                        return _MultiselectEthnicityDialog(
                                            cities: allEthnicities,
                                            selectedCities: selectedEthnicity,
                                            onSelectedCitiesListChanged:
                                                (cities) {
                                              setState(() {
                                                selectedEthnicity = cities;
                                              });
                                              scrollList();
                                            });
                                      })
                                },
                                onLongPressed: (value) => {
                                  Log.printLog("ITEM LONG PRESSED : ", "$value")
                                },
                              ),
                            );
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
    );
  }

  handle10(int position) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        ID: _currentQuestionID,
        isEditable: true,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    _nextQuestionID = _items[position].nextq_id;

    setState(() {
      _list.add(object3);
    });
    scrollList();
    _loadNext();
  }

  handle11(String message) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        ID: _currentQuestionID,
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: message,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _list.add(object3);
    });
    scrollList();
    _loadNext();
  }

  handle23(String message) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        ID: _currentQuestionID,
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: message,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _list.add(object3);
    });
    scrollList();
    _loadNext();
  }

  handle25() {
    vibrateDevice();
    if (_selectedCondition == 'Select condition') {
      displayAlert("", "Please select a condition");
      return;
    } else {
      switch (_selectedCondition) {
        case "Birth defect":
        case "Autism":
        case "Intellectual delay":
        case "Down syndrome":
          ChatMessage object3 = new ChatMessage(
              ID: _currentQuestionID,
              isEditable: true,
              textFontSize: widget.chatTextSize,
              text: _selectedCondition,
              type: MessageType.RIGHT,
              personalDetailModel: null,
              relativeDetailModel: null,
              optionList: null,
              backgroundType: 0);

          setState(() {
            _list.add(object3);
          });
          scrollList();
          _loadNext();
          break;
        case "Genetic disease":
        case "Other":
          ChatMessage object3 = new ChatMessage(
              ID: _currentQuestionID,
              isEditable: true,
              textFontSize: widget.chatTextSize,
              text: _selectedCondition,
              type: MessageType.RIGHT,
              personalDetailModel: null,
              relativeDetailModel: null,
              optionList: null,
              backgroundType: 0);

          setState(() {
            _list.add(object3);
          });
          scrollList();
          _nextQuestionID = 41;
          _loadNext();
          break;
      }
    }
  }

  handle26(String message) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        ID: _currentQuestionID,
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: message,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _list.add(object3);
    });
    scrollList();
    _loadNext();
  }

  handle28(String message) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        ID: _currentQuestionID,
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: message,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _list.add(object3);
    });
    scrollList();
    _loadNext();
  }

  void handle39(String buttonText) {
    vibrateDevice();
    if (buttonText == 'submit') {
      if (_tec_email.text.length == 0) {
        displayAlert("", "Please provide email address");
        return;
      } else if (!RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(_tec_email.text)) {
        displayAlert("", "Invalid email address");
        return;
      }
      ChatMessage object3 = new ChatMessage(
          ID: _currentQuestionID,
          isEditable: false,
          textFontSize: widget.chatTextSize,
          text: _tec_email.text,
          type: MessageType.RIGHT,
          optionList: null,
          backgroundType: 0);

      setState(() {
        _list.add(object3);
      });
    } else {
      ChatMessage object3 = new ChatMessage(
          ID: _currentQuestionID,
          isEditable: true,
          textFontSize: widget.chatTextSize,
          text: "Skip",
          type: MessageType.RIGHT,
          optionList: null,
          backgroundType: 0);

      setState(() {
        _list.add(object3);
      });
    }
    _nextQuestionID = 40;

    scrollList();

    _loadNext();
  }

  Widget _getAutoSuggestions(BuildContext context, int index) {
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    return GestureDetector(
      onTap: () {
        vibrateDevice();
        setState(() {
          _tec_ask_question.text = autoSuggestionList[index].name;
          _autoSuggestionHeight = 0;
          autoSuggestionList.clear();
        });
      },
      child: Column(
        children: <Widget>[
          Container(
            color: Colors.white,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 30,
                  decoration: BoxDecoration(
                      color: Color(0xFF4CAF50),
                      borderRadius: new BorderRadius.only(
                          topLeft: Radius.circular(0),
                          bottomLeft: Radius.circular(0))),
                  width: ScreenUtil.screenWidthDp * 0.01,
                ),
                SizedBox(
                  width: 3,
                ),
                Flexible(
                  child: Text(autoSuggestionList[index].name,
//                    maxLines: 2,
//                    overflow: TextOverflow.ellipsis,
//                    textAlign: TextAlign.justify,
                      style: TextStyle(fontSize: ScreenUtil().setSp(12))),
                )
              ],
            ),
          ),
          SizedBox(
            height: ScreenUtil.screenHeightDp * 0.005,
          )
        ],
      ),
    );
  }

  Widget _getSuggestions(BuildContext context, int index) {
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    return GestureDetector(
      onTap: () {
        vibrateDevice();
        setState(() {
          _tec_disease_list.text = diseaseList[index].name;
          _autoSuggestionHeight = 0;
          diseaseList.clear();
        });
      },
      child: Column(
        children: <Widget>[
          Container(
            color: Colors.white,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 30,
                  decoration: BoxDecoration(
                      color: Color(0xFF4CAF50),
                      borderRadius: new BorderRadius.only(
                          topLeft: Radius.circular(0),
                          bottomLeft: Radius.circular(0))),
                  width: ScreenUtil.screenWidthDp * 0.01,
                ),
                SizedBox(
                  width: 3,
                ),
                Text(diseaseList[index].name,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.justify,
                    style: TextStyle(fontSize: ScreenUtil().setSp(12)))
              ],
            ),
          ),
          SizedBox(
            height: ScreenUtil.screenHeightDp * 0.005,
          )
        ],
      ),
    );
  }

  Widget _getListItem(BuildContext context, int index) {
    double maxWidth = MediaQuery.of(context).size.width * 0.70;
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    double _spacingRound = 12;
    var _elevation = 0.9;

    ChatMessage message = _list[index];

    if (message.type == (MessageType.RIGHT)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
//              height: _getVerticalSpacing(message.backgroundType),
              height: 8,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerRight,
                        child: WidgetAnimator(
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(5),
                                  bottomLeft: Radius.circular(15),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
//                            color: Color(0xff59A9B7),
//                            color: Color(0xff03A9F4),
                            color: rightBubbleBG,
                            margin: EdgeInsets.only(
                                right: 3.0, top: 5.0, left: 10.0),
                            child: new Container(
                              padding: EdgeInsets.all(8.0),
                              margin: EdgeInsets.all(3.5),
                              child: new Text(message.text,
                                  style: TextStyle(
                                      color: rightTextColor,
                                      fontFamily: 'BrandingMedium',
                                      fontSize: ScreenUtil()
                                          .setSp(widget.chatTextSize)),
                                  overflow: TextOverflow.clip),
                            ),
                          ),
                        ))),
                Visibility(
                  visible: message.isEditable,
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          HapticFeedback.vibrate();
                          print("EDIT BUTTON CLICKED : " + "${message.ID}");
                          if (_staticData.length == 0 &&
                              _list[_list.length - 1].type !=
                                  MessageType.LOADER) {
                            displayAlertForChangeResponse(
                                "Change Response",
                                "Please note that changing a response will require you to restart the conversation from that point in the chat history.",
                                message.ID);
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.all(5.0),
                          margin: EdgeInsets.only(top: 5.0),
                          child: Image.asset(
                            'images/edit.png',
                            color: headerColor,
                            height: 16,
                            width: 16,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/user.png",
                          color: userIconColor,
                          width: 35,
                          height: 35,
                          alignment: Alignment.center),
                    ))
              ],
            ),
            SizedBox(
              height: 10,
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.LEFT)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: WidgetAnimator(
                          Card(
                              shape: _renderBackground(message.backgroundType),
                              elevation: _elevation,
                              color: leftBubbleBG,
                              margin: EdgeInsets.only(
                                  right: 10.0, top: 0.0, left: 7.0),
                              child: new Container(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      /*child: Text(message.text,
                                          style: TextStyle(
                                              color: Color(0xff51575C),
                                              fontFamily: 'BrandingMedium',
                                              fontSize: ScreenUtil()
                                                  .setSp(widget.textFontSize)),
                                          overflow: TextOverflow.clip),*/
                                      padding: EdgeInsets.all(5.0),
                                      margin: EdgeInsets.all(8),
                                      child: Column(
                                        children: <Widget>[
                                          Html(
//                                            data: """ ${message.text} """,
                                            data:
                                                """ ${message.text.toString().split(r"##")[0]} """,
                                            defaultTextStyle: TextStyle(
                                                fontFamily: 'BrandingMedium',
                                                fontSize: ScreenUtil()
                                                    .setSp(widget.chatTextSize),
                                                color: Colors.black87
//                                            color: Color(0xff51575C)
                                                ),
                                            linkStyle: const TextStyle(
                                              color: Colors.blue,
                                              decoration:
                                                  TextDecoration.underline,
                                            ),
                                          ),
                                          renderResources(message.text)
                                        ],
                                      ),

                                      /*padding: EdgeInsets.all(0.0),
                                      margin: EdgeInsets.all(2),
                                      child: Column(
                                        children: <Widget>[
                                          HtmlWidget(
//                                            """ ${message.text.toString().split(r"##")[0]} """,
                                            """ ${message.text} """,
                                            hyperlinkColor: Colors.blue,
                                            textStyle: TextStyle(
                                                fontFamily: 'BrandingMedium',
                                                fontSize: widget.chatTextSize,
                                                color: Colors.black87
//                                            color: Color(0xff51575C)
                                                ),
                                          ),
                                          renderResources(message.text)
                                        ],
                                      ),*/
                                    ),
//                                  _highlightText(text),
                                  ],
                                ),
                              )),
                        )))
              ],
            )
          ],
        ),
      );
    } else if (message.type == ("ERROR")) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  topRight: Radius.circular(12),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(12)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(3.5),
                                    child: new Text(message.text,
                                        style: TextStyle(
                                            color: Colors.red,
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.LOADER)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(0),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                      padding: EdgeInsets.all(2.5),
                                      margin: EdgeInsets.all(3.5),
                                      child: new Image(
                                          width: 35,
                                          fit: BoxFit.fitWidth,
                                          alignment: Alignment.center,
                                          height: 27,
                                          image: new AssetImage(
                                              "images/loader.gif"))),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == ("OPTIONS")) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                    width: maxWidth,
                                    padding: EdgeInsets.all(8.0),
                                    child: new ListView.builder(
                                      shrinkWrap: true,
                                      itemBuilder: (_, int index) =>
                                          message.optionList[index],
                                      itemCount: message.optionList.length,
                                    ),
                                  ),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.VERIFY_OTP)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0, bottom: 5.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(_spacingRound),
                                    child: new Text(message.text,
                                        style: TextStyle(
                                            color: Color(0xff51575C),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  new Container(
                                      padding: EdgeInsets.all(15.0),
                                      child: new Image(
                                          width: 35,
                                          fit: BoxFit.fitWidth,
                                          alignment: Alignment.center,
                                          height: 27,
                                          image: new AssetImage(
                                              "images/typing_loader.gif"))),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.PERSONAL_DETAILS)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(_spacingRound),
                                    child: Text("Details provided",
                                        style: TextStyle(
                                            color: Color(0xff51575C),
                                            fontFamily: 'BrandingSemiBold',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Divider(
                                    color: Color.fromRGBO(52, 52, 52, 1),
                                    height: 0.5,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 18, bottom: 7),
                                    child: Text("Name",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                        message.personalDetailModel.name,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Gender",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                        message.personalDetailModel.gender,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Age",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(message.personalDetailModel.age,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Ethnicity",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                        message.personalDetailModel.ethnicity,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                ],
                              ),
                            ))))
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      );
    } else if (message.type == (MessageType.DIGNOSED_RELATIVE_DETAILS)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                  flex: 1,
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: new Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15),
                                topRight: Radius.circular(15),
                                bottomLeft: Radius.circular(5),
                                bottomRight: Radius.circular(15)),
                          ),
                          elevation: _elevation,
                          color: Colors.white,
                          margin:
                              EdgeInsets.only(right: 10.0, top: 5.0, left: 7.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                padding: EdgeInsets.all(8.0),
                                margin: EdgeInsets.only(
                                    top: 3.5,
                                    bottom: 3.5,
                                    right: 15,
                                    left: 3.5),
                                child: Text("Relative's Details provided",
                                    style: TextStyle(
                                        color: Color(0xff51575C),
                                        fontFamily: 'BrandingSemiBold',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Divider(
                                  color: Color.fromRGBO(52, 52, 52, 1),
                                  height: 0.5),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Gender",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(message.relativeDetailModel.gender,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Age",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(message.relativeDetailModel.age,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Cancer Type",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(message.relativeDetailModel.cancer,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                            ],
                          ))),
                )
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      );
    } else if (message.type == (MessageType.DIVIDER)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: ScreenUtil.screenHeightDp * 0.045,
            ),
            Row(children: <Widget>[
              Expanded(
                child: new Container(
                    margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                    child: Divider(
                      color: Color(0xFF798797),
                      height: 36,
                    )),
              ),
              Text(
                message.text,
                style: TextStyle(color: Color(0xFF687889)),
              ),
              Expanded(
                child: new Container(
                    margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                    child: Divider(
                      color: Color(0xFF798797),
                      height: 36,
                    )),
              ),
            ]),
            SizedBox(
              height: ScreenUtil.screenHeightDp * 0.022,
            ),
          ],
        ),
      );
    } else if (message.type == (MessageType.WELCOME_IMAGE)) {
      return new Container(
        width: ScreenUtil.screenWidthDp,
        margin: EdgeInsets.all(50),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Flexible(
                    child: new Container(
                        alignment: Alignment.center,
                        child: Html(
                          data: """ ${message.text} """,
                          defaultTextStyle: TextStyle(
                              fontFamily: 'BrandingMedium',
                              color: Color(0xff51575C)),
                          linkStyle: const TextStyle(
                            color: Colors.redAccent,
                          ),
                        )))
              ],
            )
          ],
        ),
      );
    }
  }

  double _getVerticalSpacing(int backgroundType) {
    switch (backgroundType) {
      case -1:
        return 8.0;
        break;
      case 0:
        return 8.0;
        break;
      case 1:
        return 8.0;
        break;
      case 2:
        return 8.0;
        break;
      default:
        return 8.0;
    }
  }

  RoundedRectangleBorder _renderBackground(int backgroundType) {
    /* return RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(2),
          topRight: Radius.circular(2),
          bottomLeft: Radius.circular(12),
          bottomRight: Radius.circular(12)),
    );*/

    switch (backgroundType) {
      case 0:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
        break;
      case 1:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
        break;
      case 2:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15)),
        );
        break;
      default:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
    }
  }

  handle38(String message) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        ID: _currentQuestionID,
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: message,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);
    setState(() {
      _show38 = false;
      _list.add(object3);
    });
    scrollList();
    _loadNext();
  }

  handle27(String message) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        ID: _currentQuestionID,
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: message,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);
    setState(() {
      _list.add(object3);
    });
    scrollList();
    _loadNext();
  }

  void hideAll() {
    setState(() {
      _showHi = false;
      _showOptions = false;
      _show4 = false;
      _show39 = false;
      _showFooter = false;
      _show5 = false;
      _show6 = false;
      _show2 = false;
      _show10 = false;
      _show11 = false;
      _show23 = false;
      _show25 = false;
      _show26 = false;
      _show28 = false;
      _show38 = false;
      _show27 = false;
      _show18 = false;
      _show41 = false;
      _show31 = false;
      _show20 = false;
      _showAskQuestion = false;
      _showAskMoreOptions = false;
    });
  }

  handle18(int position) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        ID: _currentQuestionID,
        isEditable: true,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    _nextQuestionID = _items[position].nextq_id;

    setState(() {
      _list.add(object3);
    });
    scrollList();
    _loadNext();
  }

  handle41() {
    vibrateDevice();
    if (_tec_disease_list.text.length == 0) {
      displayAlert("", "Please provide disease name");
      return;
    }
    hideAll();
    ChatMessage object3 = new ChatMessage(
        ID: _currentQuestionID,
        isEditable: true,
        textFontSize: widget.chatTextSize,
        text: _tec_disease_list.text,
        type: MessageType.RIGHT,
        backgroundType: 0);
    setState(() {
      _list.add(object3);
    });
    scrollList();
    _loadNext();
  }

  handle31(int position) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        ID: _currentQuestionID,
        isEditable: true,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        backgroundType: 0);

    setState(() {
      _list.add(object3);
    });

    scrollList();

    _nextQuestionID = _items[position].nextq_id;
    if (_currentQuestionID == 31 && _isEmailAddressSubmitted == false) {
      _nextQuestionID = _items[position].nextq_id2;
    }

    _loadNext();
  }

  Widget renderResources(String message) {
    List<String> result = message.toString().split(r"##");

    List<Widget> listOfWidget = new List();

    for (int index = 1; index < result.length; index++) {
      double width = 0;
      double height = 0;

      String parameters = result[index].split("#")[1];
      List<String> params = parameters.split(",");
      width = double.parse(params[0].split(":")[1]);
      height = double.parse(params[1].split(":")[1]);
      print("PARAMETER INFO 1: $parameters");
      print("PARAMETER INFO 2: $params");
      print(
          "PARAMETER INFO 3: WIDTH : ${params[0].split(":")[1]} HEIGHT : ${params[1].split(":")[1]}");
      String url = result[index].split("#")[2];

      listOfWidget.add(Center(
        widthFactor: 1,
        child: Container(
            height: height,
            width: width,
            child: FadeInImage.assetNetwork(
                fadeInDuration: const Duration(seconds: 1),
//                placeholder: "images/image_placeholder.gif",
                placeholder: "images/dsklvndfk.gif",
                image: url)),

        /*    child: Stack(
            children: <Widget>[
              Positioned(
                left: 40,
                right: 40,
                child: CircularProgressIndicator(),
              ),
              Positioned(
                left: 40,
                right: 40,
                child: Image.network(url),
              ),
            ],
          ),*/

        /*child: Container(
            child: TransitionToImage(
              image: AdvancedNetworkImage(url, timeoutDuration: Duration(minutes: 1), height: height.toInt(), width: width.toInt()),
              // This is the default placeholder widget at loading status,
              // you can write your own widget with CustomPainter.
              placeholder: CircularProgressIndicator(),
              // This is default duration
              duration: Duration(milliseconds: 500),
            ),
          ),*/

        /*child: Stack(
            children: <Widget>[
              Center(
                  child: Container(
                        child: Image.asset("images/loader.gif", height: 20, width:20)
                    */ /*SizedBox(
                          height: 50.0,
                          width: 50.0,
                          child: CircularProgressIndicator())*/ /*)),
              Center(
                child: FadeInImage.memoryNetwork(
                  image: url,
                  height: height,
                  width: width,
                  placeholder: kTransparentImage,
                ),
              ),
            ],
          )*/
      ));
    }
    if (listOfWidget.length > 0) {
      listOfWidget.add(SizedBox(
        height: ScreenUtil.screenWidthDp * 0.000,
      ));
    }
    return Column(children: listOfWidget);
  }

  handle20(int position) {
    vibrateDevice();
    var model = _items[position];
    ChatMessage object3 = new ChatMessage(
        ID: _currentQuestionID,
        isEditable: true,
        textFontSize: widget.chatTextSize,
        text: model.option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _list.add(object3);
    });
    scrollList();

    _nextQuestionID = model.nextq_id;
    if (model.option == "What kind of management?" && isPatientPregnant) {
      _nextQuestionID = model.nextq_id2;
    }

    _loadNext();
  }

  showFreeTextUI(){
    if(_list.length==2){
      return;
    }
    _list.add(new ChatMessage(
        ID: _currentQuestionID,
        isEditable: false,
        context: context,
        text: 'Ask any question',
        type: MessageType.DIVIDER));
    scrollList();
    hideAll();
    setState(() {
      _showAskQuestion = true;
      _autoSuggestionHeight = 0;
      autoSuggestionList.clear();
      loading = false;
    });
  }

  handleShowMoreOptions(int position) {
    hideAll();
    if (position == 0) {
      setState(() {
        _showAskQuestion = true;
        _autoSuggestionHeight = 0;
        autoSuggestionList.clear();
        loading = false;
      });
    } else {
      // Show resume conversation and
      setState(() {
        _list.add(new ChatMessage(
            ID: _currentQuestionID,
            isEditable: false,
            context: context,
            text: 'Resuming conversation',
            type: MessageType.DIVIDER));
      });
      scrollList();


      if (_currentQuestionID == 31 && _isEmailAddressSubmitted == false) {
        var model = _items[1];
        _nextQuestionID = model.nextq_id2;
      }else if(_currentQuestionID == 30){
        var model = _items[1];
      _items.clear();
      _nextQuestionID = model.nextq_id;
      _loadNext();
      }else{
        _getResumingMessages(_currentQuestionID);
      }
    }
  }

  methodInChild() => print("Method called in child");

  void reloadChat() {
    _tec_email.clear();
    hideAll();
    setState(() {
      _list.clear();
    });
    _loadData();
  }
}

class _MultiselectEthnicityDialog extends StatefulWidget {
  _MultiselectEthnicityDialog({
    this.cities,
    this.selectedCities,
    this.onSelectedCitiesListChanged,
  });

  final List<String> cities;
  final List<String> selectedCities;
  final ValueChanged<List<String>> onSelectedCitiesListChanged;

  @override
  _MultiselectEthnicityDialogState createState() =>
      _MultiselectEthnicityDialogState();
}

class _MultiselectEthnicityDialogState
    extends State<_MultiselectEthnicityDialog> {
  List<String> _tempSelectedCities = [];

  @override
  void initState() {
    _tempSelectedCities = widget.selectedCities;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        padding: EdgeInsets.all(0.0),
        child: Column(
          children: <Widget>[
            Flexible(
              child: ListView.builder(
                  itemCount: widget.cities.length,
                  itemBuilder: (BuildContext context, int index) {
                    final cityName = widget.cities[index];
                    return Container(
                      child: CheckboxListTile(
                          checkColor: Colors.white,
                          activeColor: themeColor,
                          title: Text(cityName),
                          value: _tempSelectedCities.contains(cityName),
                          onChanged: (bool value) {
                            HapticFeedback.vibrate();
                            if (value) {
                              if (!_tempSelectedCities.contains(cityName)) {
                                setState(() {
                                  _tempSelectedCities.add(cityName);
                                });
                              }
                            } else {
                              if (_tempSelectedCities.contains(cityName)) {
                                setState(() {
                                  _tempSelectedCities.removeWhere(
                                      (String city) => city == cityName);
                                });
                              }
                            }
//                          widget.onSelectedCitiesListChanged(_tempSelectedCities);
                          }),
                    );
                  }),
            ),
            Container(
              margin: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  /* Text(
                    'Select Ethnicity',
                    style: TextStyle(fontSize: 18.0, color: Colors.black),
                    textAlign: TextAlign.center,
                  ),*/
                  /* RaisedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      'Cancel',
                      style: TextStyle(color: themeColor),
                    ),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(10),
                  ),*/
                  Expanded(
                    child: RaisedButton(
                      color: themeColor,
                      onPressed: () {
                        HapticFeedback.vibrate();
                        widget.onSelectedCitiesListChanged(_tempSelectedCities);
                        Navigator.pop(context);
                      },
                      child: Text(
                        'Done',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget row(DiseaseModel user) {
  return Container(
    padding: EdgeInsets.only(top: 10, bottom: 10, left: 0, right: 5),
    child: Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          height: 50,
          decoration: BoxDecoration(
              color: Color(0xFF4CAF50),
              borderRadius: new BorderRadius.only(
                  topLeft: Radius.circular(5), bottomLeft: Radius.circular(5))),
          width: ScreenUtil.screenWidthDp * 0.01,
        ),
        Flexible(
            child: Text(user.name,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 16.0)))
      ],
    ),
  );
}

class CancerTypes {
  final String name;

  CancerTypes({this.name});

  factory CancerTypes.fromJson(Map<String, dynamic> json) {
    return new CancerTypes(name: json['name']);
  }
}
