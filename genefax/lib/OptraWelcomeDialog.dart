import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ai/Animations/ListItemAnimator.dart';
import 'package:ai/ChatMessage.dart';
import 'package:ai/Constant.dart';
import 'package:ai/HomePage.dart';
import 'package:ai/Log.dart';
import 'package:ai/MessageType.dart';
import 'package:ai/RouteNames.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart' as http;

import 'SizeConfig.dart';

class OptraWelcomeDialog extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _StateWelcomeDialog();
  }
}

class _StateWelcomeDialog extends State<OptraWelcomeDialog>
    with WidgetsBindingObserver {
  double screenHeightDp;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
//    https://dev.to/pedromassango/onresume-and-onpause-for-widgets-on-flutter-27k2
    if (state == AppLifecycleState.resumed) {
      SystemChrome.setSystemUIOverlayStyle(
          SystemUiOverlayStyle(statusBarColor: headerColor));
    }
  }

  @override
  Widget build(BuildContext context) {
    //    SystemChrome.setEnabledSystemUIOverlays([]);
//    SystemChrome.setEnabledSystemUIOverlays ([SystemUiOverlay.top]);
//    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: headerColor));
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    return Scaffold(
        appBar: AppBar(
          backgroundColor: headerColor,
          centerTitle: true,
          title: Container(
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "GeneFAX™ ",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'BrandingBold',
                      fontSize: ScreenUtil().setSp(18)),
                ),
                Text("Digital Genetic Companion",
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'BrandingSemiLight',
                        fontSize: ScreenUtil().setSp(16)))
              ],
            ),
          ),
        ),
        body: Center(
          child: GestureDetector(
              onTap: () {
                FocusScopeNode currentFocus = FocusScope.of(context);
                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
              },
              child: ChatScreen()),
        ));
    ;
  }
}

final List<ChatMessage> _staticData = <ChatMessage>[];

class ChatScreen extends StatefulWidget {
  num chatTextSize = 15;

  @override
  State createState() => new ChatScreenState();
}

class ChatScreenState extends State<ChatScreen> {
  bool _shouldShowFooter = false;

  void displayAlert(String title, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("$title"),
        content: new Text("$message"),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text("Close"),
              onPressed: () {
                vibrateDevice();
                Navigator.pop(context, 'Cancel');
              })
        ],
      ),
    );
  }

  final TextEditingController _textEditingController =
      new TextEditingController();

  final ScrollController _scrollController = new ScrollController();

  int isFirstMsgShown = -1;
  int index = 0;
  bool _shouldVisible = false;
  bool _shouldOptionVisible = false;

  final List<ChatMessage> _list = <ChatMessage>[];

  double screenHeightDp;

  Future<String> _validatePromoCode(String promoCode) async {
    Log.printLog("YOUR PROMO CODE IS", promoCode);
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.get(
            'http://trial.genefax.ai/GeneFAXWebRestMobileV1.2/mastercontroller/validateOnlyPIN?pin=$promoCode&authtoken=tSN4DRaEQ5');
        var jsonData = json.decode(data.body);
        Log.printLog("VALIDATE PROMO CODE API RESPONSE", jsonData['data']);

        if (jsonData['data'] == "success") {
          new Future.delayed(const Duration(milliseconds: 2000), () {
            setState(() {
              _list.removeLast();
//              _list.removeLast();
            });
            navigateToSubPage(this.context);
          });
        } else {
          setState(() {
            _list.removeLast();
//              _list.removeLast();
          });
          new Future.delayed(const Duration(milliseconds: 300), () {
            setState(() {
              _list.add(new ChatMessage(
                  isEditable: false,
                  text: "Invalid Promo Code. Please try another Promo Code.",
                  type: MessageType.LEFT,
                  optionList: null,
                  highlightedWords: null));
              _shouldVisible = true;
              _shouldOptionVisible = false;
              scrollList();
            });
            setStaticCredentials();
          });
        }
      }
    } on SocketException catch (_) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
      setState(() {
        _list.removeLast();
//              _list.removeLast();
      });
      new Future.delayed(const Duration(milliseconds: 300), () {
        setState(() {
          _list.add(new ChatMessage(
              isEditable: false,
              text: "Please check your internet connection and try again.",
              type: MessageType.LEFT,
              optionList: null,
              highlightedWords: null));
          _shouldVisible = true;
          _shouldOptionVisible = false;
          scrollList();
        });
        setStaticCredentials();
      });
    }
  }

  void _handleOnSubmittedText(String text) {
    vibrateDevice();
    if (!text.isEmpty) {
      // CALL API : VALIDATE PROMO CODE
      // https://trial.genefax.ai/GeneFAXWebRestMobileV1.2/mastercontroller/validateOnlyPIN?pin=40807&authtoken=tSN4DRaEQ5

      _textEditingController.clear();
      ChatMessage object = new ChatMessage(
          isEditable: false,
          text: text,
          type: MessageType.RIGHT,
          optionList: null,
          highlightedWords: null);
      setState(() {
        _list.add(object);
      });

      setState(() {
        _shouldVisible = false;
        _shouldOptionVisible = false;
      });
      scrollList();

      new Future.delayed(const Duration(milliseconds: 1000), () {
        setState(() {
          _list.add(new ChatMessage(
              isEditable: false,
              text: "We are verifying your promo code.\nPlease wait...",
              type: MessageType.VERIFY_OTP,
              optionList: null,
              highlightedWords: null));
          scrollList();
        });

        /* setState(() {
          ChatMessage loader =
              new ChatMessage(isEditable: false,text: "", type: MessageType.LOADER, optionList: null);
          _list.add(loader);
        });*/

        if (text.length > 3 && text.toUpperCase().contains("GFX")) {
          new Future.delayed(const Duration(milliseconds: 2000), () {
            _validatePromoCode(text.substring(3, text.length));
          });
        } else {
          setState(() {
            _list.removeLast();
//              _list.removeLast();
          });
          new Future.delayed(const Duration(milliseconds: 300), () {
            setState(() {
              _list.add(new ChatMessage(
                  isEditable: false,
                  text:
                      "Invalid Promo Code Or Invalid Code Prefix. Please try another Promo Code.",
                  type: MessageType.LEFT,
                  optionList: null,
                  highlightedWords: null));
              _shouldVisible = true;
              _shouldOptionVisible = false;
              scrollList();
            });
            setStaticCredentials();
          });
        }
      });
    }
  }

  Future navigateToSubPage(context) async {
    Future.delayed(Duration(milliseconds: 0), () {
      /*Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomePage()));*/
      Navigator.pushNamedAndRemoveUntil(context, RouteHomePage, (route) => false);
      /* setState(() {
        _shouldOptionVisible = true;
        _shouldVisible = false;
      });*/
    });
//    Navigator.of(context).pushReplacementNamed('/HomeScreen');
  }

  Widget _textComposerWidget() {
    return Column(
      children: <Widget>[
        Visibility(
          visible: _shouldVisible,
          child: IconTheme(
            data: new IconThemeData(color: headerColor),
            /*child: new Container(
              decoration: new BoxDecoration(
                  color: Color(0xfff4f4f4),
                  borderRadius: BorderRadius.all(Radius.circular(27))),
              margin: EdgeInsets.only(left: 8, right: 8),
              child: Row(
                children: <Widget>[
                  new Flexible(
                    child: Container(
                      margin: EdgeInsets.only(left: 15, right: 10),
                      child: new TextField(
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(14),
                            color: Color(0xff51575C),
                            fontFamily: 'BrandingMedium'),
                        decoration: new InputDecoration.collapsed(
                          hintText: "Enter your code here",
                        ),
                        controller: _textEditingController,
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        onSubmitted: _handleOnSubmittedText,
                      ),
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(right: 0, bottom: 0),
                    child: new IconButton(
                        icon: new Icon(Icons.send),
                        onPressed: () => _handleOnSubmittedText(
                            _textEditingController.text)),
                  )
                ],
              ),
            ),*/
            child: Container(
              margin: EdgeInsets.only(left: 8, right: 8, bottom: 8.0, top: 8.0),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  border: Border.all(color: Colors.blueGrey),
                  borderRadius: new BorderRadius.circular(27.0)),
              padding: EdgeInsets.only(left: 20, right: 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    child: TextField(
                      onChanged: (String value) {},
                      controller: _textEditingController,
                      onSubmitted: _handleOnSubmittedText,
                      textCapitalization: TextCapitalization.characters,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        hintText: "Enter your code here",
                        hintStyle: TextStyle(
                            color: Color.fromRGBO(153, 153, 153, 1),
                            fontSize: ScreenUtil().setSp(14)),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.transparent),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.transparent),
                        ),
                      ),
                      keyboardType: TextInputType.multiline,
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(right: 0, bottom: 0),
                    child: new IconButton(
                        icon: new Icon(
                          Icons.send,
                          color: sendIconColor,
                        ),
                        onPressed: () => _handleOnSubmittedText(
                            _textEditingController.text)),
                  )
                ],
              ),
            ),
          ),
          /* replacement: new Container(
            child: Text(""),
          ),*/
        ),
        Visibility(
          visible: _shouldOptionVisible,
          child: Container(
            margin: EdgeInsets.only(left: 8.0, right: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: RaisedButton(
                      hoverColor: Color(0xffFFFFFF),
                      hoverElevation: 0.0,
                      elevation: 0.0,
                      onPressed: () {
                        handleInputField();
                      },
                      color: Colors.transparent,
                      textColor: Colors.white,
                      padding: const EdgeInsets.all(0.0),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(2.0)),
                      child: new Container(
                        width: screenHeightDp,
                        padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                            color: headerColor,
                            borderRadius: new BorderRadius.circular(2.0)),
                        child: Text(
                          "Enter Promo Code",
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'BrandingMedium',
                              fontSize: ScreenUtil().setSp(14)),
                          textAlign: TextAlign.center,
                        ),
                      )),
                ),
                SizedBox(width: 8),
                Expanded(
                  flex: 4,
                  child: RaisedButton(
                      elevation: 0,
                      onPressed: () {
                        moveToRegisterPage();
                      },
                      color: Colors.transparent,
                      textColor: Colors.white,
                      padding: const EdgeInsets.all(0.0),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(2.0)),
                      child: new Container(
                        width: screenHeightDp,
                        padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                            color: headerColor,
                            borderRadius: new BorderRadius.circular(2.0)),
                        child: Text(
                          "Register for Promo Code",
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'BrandingMedium',
                              fontSize: ScreenUtil().setSp(14)),
                          textAlign: TextAlign.center,
                        ),
                      )),
                )
              ],
            ),
          ),
          /*replacement: new Container(
            child: Text(""),
          ),*/
        ),
        Visibility(
          visible: _shouldShowFooter == true ? false : true,
          child: Column(
            children: <Widget>[
              Divider(
                height: 1,
              ),
              Container(
                color: Colors.white,
                width: ScreenUtil.screenHeightDp,
                padding: EdgeInsets.only(
                    top: ScreenUtil.screenHeightDp * 0.01,
                    bottom: ScreenUtil.screenHeightDp * 0.01,
                    right: ScreenUtil.screenHeightDp * 0.03),
//          color: Color.fromRGBO(41, 85, 102, 1.0),
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Powered by',
                      style: TextStyle(
                          fontFamily: 'BrandingSemiLightItalic',
                          color: Colors.black,
                          fontSize: ScreenUtil().setSp(13)),
                      textAlign: TextAlign.end,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Image.asset(
                      'images/footer_genefax.png',
                      height: 10,
                    )
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (isFirstMsgShown < 0) {
      loadStaticData();
      isFirstMsgShown = 1;
    }

    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        scrollList();
        setState(() {
          _shouldShowFooter = visible;
        });
      },
    );

    setState(() {
//      _textEditingController.text = "GFX51218";
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    screenHeightDp = MediaQuery.of(context).size.width;
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: splashTryButtonColor,
        statusBarIconBrightness: Brightness.light));

    return new SafeArea(
        child: Container(
      color: chatScreenBackground,
      child: Column(
        children: <Widget>[
          new Flexible(
              child: new ListView.builder(
            controller: _scrollController,
            padding: new EdgeInsets.all(8.0),
            itemBuilder: _getListItem,
            itemCount: _list.length,
          )),
          new Container(
            child: _textComposerWidget(),
          )
        ],
      ),
    ));
  }

  void loadStaticData() {
    if (_staticData.length > 0) {
      _staticData.clear();
    }

//    "👋 Hi there! I am GeneFAXTM, your digital genetic assistant."
    ChatMessage object1 = new ChatMessage(
        isEditable: false,
        text: "Hi, I am GeneFAX™, your digital genetic assistant.",
        type: MessageType.LEFT,
        optionList: null,
        backgroundType: _list.length > 0 ? 0 : -1,
        highlightedWords: null);

    ChatMessage object2 = new ChatMessage(
        isEditable: false,
        text:
            "I am here to help you identify a cancer screening test that might benefit you. In most likely scenario it will help you relieve your stress and anxiety related to your genetic health.",
        type: MessageType.LEFT,
        optionList: null,
        backgroundType: 1,
        highlightedWords: null);

    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        text:
            "You also have an option to ask your own question during this conversation at any time by clicking  and speak with our genetic counselor at any time clicking ",
        type: MessageType.LEFT,
        optionList: null,
        backgroundType: 2,
        highlightedWords: null);

    _staticData.add(object1);
    _staticData.add(object2);
    _staticData.add(object3);
    /*final List<Options> _options = <Options>[];
    _options.add(new Options(optionName: "Enter Promo Code",));
    _options.add(new Options(optionName: "Register for Promo Code",));
    ChatMessage object4 = new ChatMessage(isEditable: false,
        text:
        "",
        type: "OPTIONS",optionList: _options);
    _staticData.add(object4);*/

    showStaticMessages();
  }

  void showStaticMessages() {
    if (_list.length > 0) {
      // Show loader
        setState(() {
          ChatMessage loader = new ChatMessage(
              isEditable: false,
              text: "",
              type: MessageType.LOADER,
              optionList: null);
          _list.add(loader);
        });


      if (_staticData.length > 0) {
        // This is for displaying loader duration
        new Future.delayed(
            Duration(
                milliseconds: Log.getLoaderDelayDuration(
                    _staticData[0].text.toString().length, null)), () {
          if (_staticData.length > 0) {
            // Remove last cell i.e Loader and add next message.

              setState(() {
                _list.removeLast();
              });
              setState(() {
                _list.add(_staticData[0]);
              });
            _staticData.removeAt(0);
            if (_staticData.length == 0) {
              setState(() {
                _shouldVisible = false;
                _shouldOptionVisible = true;
              });
            } else if (_staticData.length > 0) {
              // This is for actual message reading duration
              new Future.delayed(
                  Duration(
                      milliseconds: Log.getMessageDelayDuration(
                          _list.last.text.toString().length, null)), () {
                showStaticMessages();
              });
            }
          }
        });
      } else {
        setState(() {
          _shouldVisible = false;
          _shouldOptionVisible = true;
        });
      }
    } else {
      if (_staticData.length > 0) {
        setState(() {
          _list.add(_staticData[0]);
        });
        _staticData.removeAt(0);
        new Future.delayed(
            Duration(
                milliseconds: Log.getMessageDelayDuration(
                    _list.last.text.toString().length, null)), () {
          if (_staticData.length > 0) {
              showStaticMessages();
          }
        });
      }
    }
  }

  void handleInputField() {
    vibrateDevice();
    setState(() {
      _shouldVisible = false;
      _shouldOptionVisible = false;
    });
    new Future.delayed(const Duration(seconds: 0), () {
      setState(() {
        ChatMessage loader = new ChatMessage(
            isEditable: false,
            text: "",
            type: MessageType.LOADER,
            optionList: null,
            backgroundType: _list.length > 0 ? 0 : -1,
            highlightedWords: null);
        _list.add(loader);
      });
      scrollList();
      new Future.delayed(const Duration(milliseconds: 2000), () {
        setState(() {
          _list.removeLast();
          ChatMessage msg = new ChatMessage(
              isEditable: false,
              text: "Please enter Promo Code Below",
              type: MessageType.LEFT,
              optionList: null,
              backgroundType: _list.length > 0 ? 0 : -1,
              highlightedWords: null);
          _list.add(msg);
          scrollList();
        });

        new Future.delayed(const Duration(milliseconds: 1000), () {
          setState(() {
            _shouldVisible = true;
            _shouldOptionVisible = false;
          });
          setStaticCredentials();
          scrollList();
        });
      });
    });
  }

  void moveToRegisterPage() {
    vibrateDevice();
    /* Navigator.push(
        context, MaterialPageRoute(builder: (context) => RegisterPage()));*/

    displayAlert("",
        "Please contact us at licenses@genefax.ai to receive a Promo Code.");

//    Navigator.push(context, ScaleRoute(page: RegisterPage()));
  }

  void scrollList() {
    /* Timer(
        Duration(milliseconds: 300),
            () => _scrollController
            .jumpTo(_scrollController.position.maxScrollExtent));*/
    Timer(
        Duration(milliseconds: 300),
        () => _scrollController.animateTo(
            _scrollController.position.maxScrollExtent,
            curve: Curves.linear,
            duration: Duration(milliseconds: 500)));
  }

  Widget _getListItem(BuildContext context, int index) {
    double maxWidth = MediaQuery.of(context).size.width * 0.70;
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    double _spacingRound = 12;
    var _elevation = 0.9;

    ChatMessage message = _list[index];

    if (message.type == (MessageType.RIGHT)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
//              height: _getVerticalSpacing(message.backgroundType),
              height: 8,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerRight,
                        child: WidgetAnimator(
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(5),
                                  bottomLeft: Radius.circular(15),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
//                            color: Color(0xff59A9B7),
//                            color: Color(0xff03A9F4),
                            color: rightBubbleBG,
                            margin: EdgeInsets.only(
                                right: 3.0, top: 5.0, left: 10.0),
                            child: new Container(
                              padding: EdgeInsets.all(8.0),
                              margin: EdgeInsets.all(3.5),
                              child: new Text(message.text,
                                  style: TextStyle(
                                      color: rightTextColor,
                                      fontFamily: 'BrandingMedium',
                                      fontSize: ScreenUtil()
                                          .setSp(widget.chatTextSize)),
                                  overflow: TextOverflow.clip),
                            ),
                          ),
                        ))),
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/user.png",
                          color: userIconColor,
                          width: 35,
                          height: 35,
                          alignment: Alignment.center),
                    ))
              ],
            ),
            SizedBox(
              height: 10,
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.LEFT)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: WidgetAnimator(
                          Card(
                              shape: _renderBackground(message.backgroundType),
                              elevation: _elevation,
                              color: leftBubbleBG,
                              margin: EdgeInsets.only(
                                  right: 10.0, top: 0.0, left: 7.0),
                              child: new Container(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      /*child: Text(message.text,
                                          style: TextStyle(
                                              color: Color(0xff51575C),
                                              fontFamily: 'BrandingMedium',
                                              fontSize: ScreenUtil()
                                                  .setSp(widget.textFontSize)),
                                          overflow: TextOverflow.clip),*/
                                      padding: EdgeInsets.all(5.0),
                                      margin: EdgeInsets.all(8),
                                      child: Column(
                                        children: <Widget>[
                                          Html(
//                                            data: """ ${message.text} """,
                                            data:
                                                """ ${message.text.toString().split(r"##")[0]} """,
                                            defaultTextStyle: TextStyle(
                                                fontFamily: 'BrandingMedium',
                                                fontSize: ScreenUtil()
                                                    .setSp(widget.chatTextSize),
                                                color: Colors.black87
//                                            color: Color(0xff51575C)
                                                ),
                                            linkStyle: const TextStyle(
                                              color: Colors.blue,
                                              decoration:
                                                  TextDecoration.underline,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
//                                  _highlightText(text),
                                  ],
                                ),
                              )),
                        )))
              ],
            )
          ],
        ),
      );
    } else if (message.type == ("ERROR")) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  topRight: Radius.circular(12),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(12)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(3.5),
                                    child: new Text(message.text,
                                        style: TextStyle(
                                            color: Colors.red,
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.LOADER)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(0),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                      padding: EdgeInsets.all(2.5),
                                      margin: EdgeInsets.all(3.5),
                                      child: new Image(
                                          width: 35,
                                          fit: BoxFit.fitWidth,
                                          alignment: Alignment.center,
                                          height: 27,
                                          image: new AssetImage(
                                              "images/loader.gif"))),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == ("OPTIONS")) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                    width: maxWidth,
                                    padding: EdgeInsets.all(8.0),
                                    child: new ListView.builder(
                                      shrinkWrap: true,
                                      itemBuilder: (_, int index) =>
                                          message.optionList[index],
                                      itemCount: message.optionList.length,
                                    ),
                                  ),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.VERIFY_OTP)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: WidgetAnimator(Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0, bottom: 5.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(_spacingRound),
                                    child: new Text(message.text,
                                        style: TextStyle(
                                            color: Color(0xff51575C),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  new Container(
                                      padding: EdgeInsets.only(
                                          top: 0.0,
                                          left: 15.0,
                                          right: 15.0,
                                          bottom: 15.0),
                                      child: new Image(
                                          width: 35,
                                          fit: BoxFit.fitWidth,
                                          alignment: Alignment.center,
                                          height: 27,
                                          image: new AssetImage(
                                              "images/loader.gif"))),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            )))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.PERSONAL_DETAILS)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(_spacingRound),
                                    child: Text("Details provided",
                                        style: TextStyle(
                                            color: Color(0xff51575C),
                                            fontFamily: 'BrandingSemiBold',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Divider(
                                    color: Color.fromRGBO(52, 52, 52, 1),
                                    height: 0.5,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 18, bottom: 7),
                                    child: Text("Name",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                        message.personalDetailModel.name,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Gender",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                        message.personalDetailModel.gender,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Age",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(message.personalDetailModel.age,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Ethnicity",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                        message.personalDetailModel.ethnicity,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                ],
                              ),
                            ))))
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      );
    } else if (message.type == (MessageType.DIGNOSED_RELATIVE_DETAILS)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                  flex: 1,
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: new Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15),
                                topRight: Radius.circular(15),
                                bottomLeft: Radius.circular(5),
                                bottomRight: Radius.circular(15)),
                          ),
                          elevation: _elevation,
                          color: Colors.white,
                          margin:
                              EdgeInsets.only(right: 10.0, top: 5.0, left: 7.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                padding: EdgeInsets.all(8.0),
                                margin: EdgeInsets.only(
                                    top: 3.5,
                                    bottom: 3.5,
                                    right: 15,
                                    left: 3.5),
                                child: Text("Relative's Details provided",
                                    style: TextStyle(
                                        color: Color(0xff51575C),
                                        fontFamily: 'BrandingSemiBold',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Divider(
                                  color: Color.fromRGBO(52, 52, 52, 1),
                                  height: 0.5),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Gender",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(message.relativeDetailModel.gender,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Age",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(message.relativeDetailModel.age,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Cancer Type",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(message.relativeDetailModel.cancer,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                            ],
                          ))),
                )
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      );
    } else if (message.type == (MessageType.DIVIDER)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: ScreenUtil.screenHeightDp * 0.045,
            ),
            Row(children: <Widget>[
              Expanded(
                child: new Container(
                    margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                    child: Divider(
                      color: Color(0xFF798797),
                      height: 36,
                    )),
              ),
              Text(
                message.text,
                style: TextStyle(color: Color(0xFF687889)),
              ),
              Expanded(
                child: new Container(
                    margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                    child: Divider(
                      color: Color(0xFF798797),
                      height: 36,
                    )),
              ),
            ]),
            SizedBox(
              height: ScreenUtil.screenHeightDp * 0.022,
            ),
          ],
        ),
      );
    } else if (message.type == (MessageType.WELCOME_IMAGE)) {
      return new Container(
        width: ScreenUtil.screenWidthDp,
        margin: EdgeInsets.all(50),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Flexible(
                    child: new Container(
                        alignment: Alignment.center,
                        child: Html(
                          data: """ ${message.text} """,
                          defaultTextStyle: TextStyle(
                              fontFamily: 'BrandingMedium',
                              color: Color(0xff51575C)),
                          linkStyle: const TextStyle(
                            color: Colors.redAccent,
                          ),
                        )))
              ],
            )
          ],
        ),
      );
    }
  }

  double _getVerticalSpacing(int backgroundType) {
    switch (backgroundType) {
      case -1:
        return 8.0;
        break;
      case 0:
        return 8.0;
        break;
      case 1:
        return 8.0;
        break;
      case 2:
        return 8.0;
        break;
      default:
        return 8.0;
    }
  }

  RoundedRectangleBorder _renderBackground(int backgroundType) {
    /* return RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(2),
          topRight: Radius.circular(2),
          bottomLeft: Radius.circular(12),
          bottomRight: Radius.circular(12)),
    );*/

    switch (backgroundType) {
      case 0:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
        break;
      case 1:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
        break;
      case 2:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15)),
        );
        break;
      default:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _scrollController.dispose();
    super.dispose();
  }

  void setStaticCredentials() {
    setState(() {
      _textEditingController.text = "GFX51218";
    });
  }
}
