import 'dart:convert';

import 'package:ai/Log.dart';
import 'package:ai/RouteNames.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Constant.dart';
import '../SizeConfig.dart';

class Notifications extends StatefulWidget {
  double chatTextSize = 16.0;

  Notifications({this.chatTextSize});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new StateNotification();
  }
}

class StateNotification extends State<Notifications> {
  List<NotificationModel> _list = new List<NotificationModel>();
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getMessagesFromPreferences();

    /* setState(() {
      _list.add(new NotificationModel(
          title: 'COVID-19 virus updates',
          message:
          'The state government has declared some new models and rules due to covid-19 virus influence. State chief minister Mr. Udhav thakre has announced about next lockdown period from 3 May 2020 to 17th of May as tentative date and it mey postpond till the month of June. All peoples in red zone are about to stay safe at home and not to travel in local areas.\n To check out all the details. Please subscribe to our channel and stay tune. '));

      _list.add(new NotificationModel(
          title: 'Steps to be safe from corona virus',
          message:
          'To say safe and secure from COVID-19. Flollow the steps while being going in public places. This are the general guidlenes circulated from WHO to the peoples who are in Orange zone and green zone.'));
    });*/
  }

  @override
  Widget build(BuildContext context) {
    double maxHeight = MediaQuery.of(context).size.height;
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: headerColor,
          primaryColorDark: headerColor,
          scaffoldBackgroundColor: headerColor,
          accentColor: Colors.white),
      home: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.white,
            onPressed: () =>
                Navigator.of(context).popAndPushNamed(RouteHomePage),
          ),
          title: Text(
            "Notifications",
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: Column(
          children: <Widget>[
            /* Container(
              color: Colors.white,
              height: 50,
              width: 100,
              child: RaisedButton(child: Text('Refresh'),onPressed: (){
                getMessagesFromPreferences();
              },),
            ),*/
            Flexible(
              child: Container(
                  color: Colors.white,
                  child: _list != null && _list.length > 0
                      ? ListView.builder(
                          controller: _scrollController,
                          padding: new EdgeInsets.all(8.0),
                          itemCount: _list != null ? _list.length : 0,
                          itemBuilder: _getListItem)
                      : Center(
                          child: Text("No recent notifications found"),
                        )),
            )
          ],
        ),
      ),
    );
  }

  Widget _getListItem(BuildContext context, int index) {
    double maxWidth = MediaQuery.of(context).size.width * 0.70;
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    if (index == 0) {
      return ExpandableNotifier(
          child: Padding(
        padding: const EdgeInsets.all(10),
        child: Card(
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 150,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.orange,
                    shape: BoxShape.rectangle,
                  ),
                  child: Image.network(
                    'https://infraco.co.za/wp-content/uploads/2020/03/bbi-covid19.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              ScrollOnExpand(
                scrollOnExpand: true,
                scrollOnCollapse: false,
                child: ExpandablePanel(
                  theme: const ExpandableThemeData(
                    headerAlignment: ExpandablePanelHeaderAlignment.center,
                    tapBodyToCollapse: true,
                  ),
                  header: Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        _list[index].title,
                        style: Theme.of(context).textTheme.body2,
                      )),
                  collapsed: Text(
                    _list[index].message,
                    softWrap: true,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  expanded: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      for (var _ in Iterable.generate(1))
                        Padding(
                            padding: EdgeInsets.only(bottom: 10),
                            child: Text(
                              _list[index].message,
                              softWrap: true,
                              overflow: TextOverflow.fade,
                            )),
                    ],
                  ),
                  builder: (_, collapsed, expanded) {
                    return Padding(
                      padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                      child: Expandable(
                        collapsed: collapsed,
                        expanded: expanded,
                        theme: const ExpandableThemeData(crossFadePoint: 0),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ));
    } else if (index == 1) {
      return Padding(
        padding: const EdgeInsets.all(10),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Image.network(
                      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR_m_JOLotf9QQdwLCMc15_DtkIl6Qlnu-fG5YjGIvlLLuPeSxx&usqp=CAU',
                      fit: BoxFit.fill,
                      height: 150,
                      width: 150,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Flexible(
                      child: Text(
                        _list[index].title,
                        style: TextStyle(
                            color: Color(0xff763225),
                            fontSize: ScreenUtil().setSp(18)),
                      ),
                    )
                  ],
                ),
                Text(_list[index].message)
              ],
            ),
          ),
        ),
      );
    } else {
      return Container(
          child: ListTile(
        title: Text(_list[index].title),
        subtitle: Text(_list[index].message),
        selected: true,
      ));
    }
  }

  Future getMessagesFromPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List notifications = prefs.getStringList(PREF_NOTIFICATION_DATA);
    List<String> updatedNotificationList = new List<String>();
    json.decode(notifications.toString());
    if (notifications != null) {
      for (int index = 0; index < notifications.length; index++) {
        Log.printLog('NOTIFICATION AT $index', ' IS ${notifications[index]}');
        var object = jsonDecode(notifications[index]);
        setState(() {
          NotificationModel model = new NotificationModel(
              title: object['title'],
              message: object['message'],
              imageUrl: object['imageUrl'],
              view: object['view'],
              type: object['type'],
              isRead: true
          );
          _list.add(model);
          String json = jsonEncode(model);
          updatedNotificationList.add(json);
        });
      }
      prefs.setStringList(PREF_NOTIFICATION_DATA,updatedNotificationList);
    }
  }
}

class NotificationModel {
  final String title, message, imageUrl, view, type;
  final bool isRead;

  NotificationModel(
      {this.title,
      this.message,
      this.imageUrl,
      this.view,
      this.type,
      this.isRead});

  factory NotificationModel.fromJson(Map<String, dynamic> parsedJSON) {
    return new NotificationModel(
        title: parsedJSON['title'] ?? "",
        message: parsedJSON['message'] ?? "",
        imageUrl: parsedJSON['image_url'] ?? "",
        view: parsedJSON['view'] ?? "",
        type: parsedJSON['type'] ?? "",
        isRead: parsedJSON['isRead'] ?? false);
  }

  Map toJson() => {
        "title": "$title",
        "message": "$message",
        "imageUrl": "$imageUrl",
        "view": "$view",
        "type": "$type",
        "isRead": "$isRead"
      };

/* Map<String,dynamic> toJSON(){
    return {
      "title":"$title",
      "message":"$message",
      "imageUrl":"$imageUrl",
      "view":"$view",
      "type":"$type"
    };
  }*/
}
