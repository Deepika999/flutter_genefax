import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:ai/HomeWebView.dart';
import 'package:ai/Log.dart';
import 'DownloadTranscript.dart';
import 'Feedback.dart';
import 'OptraHCS.dart';
import 'KnowledgeBot.dart';
import 'MyWebView.dart';
import 'MyriadPretest.dart';
import 'Quiz.dart';
import 'ScheduleAppointment.dart';
import 'SizeConfig.dart';

//https://flutter.dev/docs/cookbook/design/tabs

class HomeActivity extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _StateHomeActivity();
  }
}

class _StateHomeActivity extends State<HomeActivity>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  double tabIconSize = 26;
  int _tabIndex = 1;
  TabController _tab;

  @override
  void initState() {
    // TODO: implement initState
    _tab = TabController(initialIndex: _tabIndex, length: 6, vsync: this);
    /* ..addListener(() {
        Log.printLog("CURRENT TAB SELECTION : ", "$_tabIndex");
        setState(() {
          _tab.index = _tabIndex;
        });
      });*/
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _tab.dispose();
    super.dispose();
  }

  void displayAlert(String title, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("$title"),
        content: new Text("$message"),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text("Close"),
              onPressed: () {
                Navigator.pop(context, 'Cancel');
              })
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    ScreenUtil.init(context, width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height, allowFontScaling: true);

    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Color(0xff59A9B7)));
    return MaterialApp(
        theme: new ThemeData(
          brightness: Brightness.light,
          primaryColor: Color(0xff59A9B7),
          //Changing this will change the color of the TabBar
          accentColor: Colors.white54,
        ),
        debugShowCheckedModeBanner: false,
        home: DefaultTabController(
          length: 6,
          child: Scaffold(
            backgroundColor: Color.fromRGBO(232, 239, 245, 1),
            appBar: AppBar(
              backgroundColor: Color(0xff59A9B7),
              centerTitle: true,
              title: Container(
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "GeneFAX™ ",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'BrandingBold',
                          fontSize: ScreenUtil().setSp(18)),
                    ),
                    Text("Digital Genetic Companion",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'BrandingSemiLight',
                            fontSize: ScreenUtil().setSp(16)))
                  ],
                ),
              ),
              bottom: TabBar(
                indicatorColor: Color.fromRGBO(213, 224, 236, 1),
                labelColor: Colors.white,
                controller: _tab,
                tabs: [
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _tab.index = 0;
                      });
                    },
                    child: Tab(
                      icon: Image.asset(
                        "images/knowledgebase.png",
                        height: tabIconSize,
                        width: tabIconSize,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _tab.index = 1;
                      });
                    },
                    child: Tab(
                        icon: Image.asset(
                      "images/home.png",
                      height: tabIconSize,
                      width: tabIconSize,
                      color: Colors.white,
                      fit: BoxFit.fill,
                    )),
                  ),
                  GestureDetector(
                    onTap: () {
                      Log.printLog("LOG", "THIS TAB IS DISABLED");
                    },
                    child: Tab(
                        icon: Image.asset(
                      "images/download_transcript.png",
                      height: tabIconSize,
                      width: tabIconSize,
                          color: Color.fromRGBO(145, 179, 194, 1),
                      fit: BoxFit.fill,
                    )),
                  ),
                  GestureDetector(
                    onTap: () {
                      Log.printLog("LOG", "THIS TAB IS DISABLED");
                    },
                    child: Tab(
                        icon: Image.asset(
                      "images/feedback.png",
                      height: tabIconSize,
                      width: tabIconSize,
                      color: Color.fromRGBO(145, 179, 194, 1),
                      fit: BoxFit.fill,
                    )),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _tab.index = 4;
                      });
//                      _tab.animateTo(4);
                    },
                    child: Tab(
                        icon: Image.asset(
                      "images/quiz.png",
                      height: tabIconSize,
                      width: tabIconSize,
                      fit: BoxFit.fill,
                    )),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _tab.index = 5;
                      });
//                      _tab.animateTo(5);
                    },
                    child: Tab(
                        icon: Image.asset(
                      "images/cc.png",
                      height: tabIconSize,
                      width: tabIconSize,
                      fit: BoxFit.fill,
                    )),
                  )
                ],
              ),
            ),
            /*bottomNavigationBar: SafeArea(
            child: Container(
              color: Color(0xff59A9B7),
              child: TabBar(
                indicatorColor: Colors.transparent,
                labelColor: Colors.white,
                tabs: [
                  Tab(icon: Icon(Icons.home,size: tabIconSize,)),
                  Tab(
                      icon: Image.asset(
                        "images/knowledgebase.png",
                        height: tabIconSize,
                        width: tabIconSize,
                        fit: BoxFit.fill,
                      )),
                  Tab(icon: Icon(Icons.file_download,size: tabIconSize,)),
                  Tab(icon: Icon(Icons.feedback,size: tabIconSize,)),
                  Tab(
                      icon: Image.asset(
                        "images/quiz.png",
                        height: tabIconSize,
                        width: tabIconSize,
                        fit: BoxFit.fill,
                      )),
                  Tab(
                      icon: Image.asset(
                        "images/cc.png",
                        height: tabIconSize,
                        width: tabIconSize,
                        fit: BoxFit.fill,
                      ))
                ],
              ),
            ),
          ),*/
            body: SafeArea(
              child: TabBarView(
                  controller: _tab,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
//              MyWebView(title:'Example',selectedUrl: "http://pro.genefax.ai/hcs/",),
                    MyriadPretest(),
                    OptraHCS(),
                    DownloadTranscript(),
                    FeedbackPage(),
                    Quiz(),
                    ScheduleAppointment(),
                  ]),
            ),
          ),
        ));
  }
}
