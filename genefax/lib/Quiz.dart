import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:ai/PersonalDetailModel.dart';
import 'package:ai/model/CancerRelativeDetail.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_tags/flutter_tags.dart';

import 'ChatMessage.dart';
import 'Log.dart';
import 'MessageType.dart';
import 'SizeConfig.dart';

class Quiz extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _StateQuiz();
  }
}

class OptionsModel {
  String optionText;
  int nextQuestionID;
  int nextQuestionID2;
  String comment;
  bool isCorrect;
  String moreInfo;
  String question;

  OptionsModel(
      {this.optionText,
      this.nextQuestionID,
      this.nextQuestionID2,
      this.comment,
      this.isCorrect,
      this.moreInfo,
      this.question});
}

class _StateQuiz extends State<Quiz> with TickerProviderStateMixin {
  AnimationController _controller;
  AnimationController _controllerHand;
  Animation<Offset> _offsetFloat;
  Animation<Offset> _offsetSwipe;
  var _nextQuestionID = 1;
  String comment = "";
  var _currentQuestionID = 1;
  String _cancerTypeValue = "";

  final ScrollController _scrollController = new ScrollController();
  final List<ChatMessage> _list = <ChatMessage>[];
  final List<ChatMessage> _staticData = <ChatMessage>[];
  List<OptionsModel> _items = new List<OptionsModel>();
  double _fontSize = 14;
  bool _showPersonalDetailsForm = false;
  bool _showOptions = false;
  bool _show27 = false;
  bool _show13 = false;

  String _selectedAge = "Select age";
  String _selectedGender = "Select gender";
  String _selectedCancerType = "Select cancer";
  String _selectedEthnicity = "Select ethnicity";

  final TextEditingController _tec_Name = new TextEditingController();
  final TextEditingController _tec_relative_age = new TextEditingController();

  var _tagSpacing = 30.0;

  OptionsModel model = null;

  var medicalTerms = null;

  var _showStaticInfoPage = false;

  void displayAlert(String title, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("$title"),
        content: new Text("$message"),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text("Close"),
              onPressed: () {
                Navigator.pop(context, 'Cancel');
              })
        ],
      ),
    );
  }

  void displayAlertWithActionLable(
      String title, String message, String actionLable) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("$title"),
        content: new Text("$message"),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text(actionLable),
              onPressed: () {
                Navigator.pop(context, 'Cancel');
              })
        ],
      ),
    );
  }

  void displayRetry(String title, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("$title"),
        content: new Text("$message"),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text("Retry"),
              onPressed: () {
                Navigator.pop(context, 'Cancel');
                if (_currentQuestionID == 1) {
                  _loadNext();
                } else {
                  _loadNext();
                }
              })
        ],
      ),
    );
  }

  /* Map<String, HighlightedWord> words = {
    "cancers": HighlightedWord(
      onTap: () {
        // show dialog here with content description
      },
      textStyle: TextStyle(color: Colors.blue),
    ),
    "Hereditary Canser Screening": HighlightedWord(
      onTap: () {
        // show dialog here with content description
      },
      textStyle: TextStyle(color: Colors.blue),
    ),
  };*/

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));

    _offsetFloat = Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero)
        .animate(_controller)
          ..addStatusListener((AnimationStatus status) {
            if (status == AnimationStatus.completed) {
              new Future.delayed(Duration(milliseconds: 1000), () {
                setState(() {
                  if (_list.length == 1) {
                    _controllerHand.forward();
                    _showStaticInfoPage = true;
                  }
                });
              });
            }
          });

    _controllerHand = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1900));
    /*..addStatusListener((AnimationStatus status) {
            if (status == AnimationStatus.completed) {
              if(_showStaticInfoPage && _controllerHand.isCompleted) {
                _controllerHand.forward();
              }
            }
          });*/

    _offsetSwipe =
        Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset(-1.0, 0.0))
            .animate(_controllerHand);

    _offsetSwipe.addStatusListener((status) {
      if (status == AnimationStatus.completed && _showStaticInfoPage) {
        _controllerHand.repeat();
      }
    });

    _loadNext();
  }

  Future<String> _loadNext() async {
    String questionID = "";

    if (_nextQuestionID == 0) {
      questionID = _cancerTypeValue;
    } else {
      questionID = _nextQuestionID.toString();
    }

    String url =
        "${Log.API_STACK}getQuestionsModuleWise?q_id=$questionID&module=HCS&type=quiz&authtoken=tSN4DRaEQ5";
    Log.printLog("API URL", url);
    try {
      setState(() {
        _list.add(ChatMessage(
            isEditable: false,
            context: this.context,
            type: MessageType.LOADER,
            optionList: null,
            highlightedWords: medicalTerms));
      });
      scrollList();

      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.post(url);

        new Future.delayed(Duration(seconds: 1), () {
          var jsonData = json.decode(data.body);

          setState(() {
            if (_list.last.type == MessageType.LOADER) {
              _list.removeLast();
            }
          });

          Log.printLog("API RESPONSE", jsonData.toString());
          var jsonResponse = jsonData['data'];

          if (jsonResponse.length > 1) {
            var jsonObject = jsonData['data'][0];
            Log.printLog("QUESTION : ", "${jsonObject['question']}");
            Log.printLog("NXT Q ID : ", "${jsonObject['nextq_id']}");
            Log.printLog("CUR Q ID : ", "${jsonObject['question_id']}");

            setState(() {
              _nextQuestionID = jsonObject['nextq_id'] as int;
              _currentQuestionID = jsonObject['question_id'] as int;
            });

            _items.clear();
            for (int index = 0; index < jsonResponse.length; index++) {
              var jsonData = jsonResponse[index];

              _items.add(OptionsModel(
                  optionText: jsonData['option'] as String,
                  nextQuestionID: jsonData['nextq_id'] as int,
                  nextQuestionID2: jsonData['nextq_id'] as int,
                  comment: jsonData['comments'] as String,
                  isCorrect: jsonData['is_correct'] as bool,
                  moreInfo: jsonData['more_info'] as String,
                  question: jsonData['question'] as String));
            }

            Log.printLog("MY OPTIONS", " $_items");

            String question = jsonObject['question'];
            String moreInfo = jsonObject['more_info'];
            if (moreInfo != null && moreInfo.isNotEmpty) {
              _highlightedWordsFunct(moreInfo).then((value) {
                List<String> arrList = question.split(r"$$ ");
                Log.printLog("MY DATA", " $arrList");

                if (model != null) {
                  if (model.isCorrect) {
                    Log.printLog("OPTION MODEL", "IS NOT NULL");
                    _staticData.add(ChatMessage(
                        isEditable: false,
                        context: this.context,
                        text: model.comment,
                        type: MessageType.LEFT,
                        optionList: null,
                        highlightedWords: medicalTerms));
                  } else {
                    _staticData.add(ChatMessage(
                        isEditable: false,
                        context: this.context,
                        text: model.comment,
                        type: MessageType.LEFT,
                        optionList: null,
                        highlightedWords: medicalTerms));
                    if (jsonObject['more_info'] != null) {
                      _staticData.add(ChatMessage(
                        isEditable: false,
                        context: this.context,
                        text: jsonObject['more_info'],
                        type: MessageType.LEFT,
                        optionList: null,
                        highlightedWords: medicalTerms,
                        backgroundType: 2,
                      ));
                    }
                  }
                } else {
                  Log.printLog("OPTION MODEL", "IS NULL");
                }

                for (int index = 0; index < arrList.length; index++) {
                  if (index == 0) {
                    _staticData.add(ChatMessage(
                        isEditable: false,
                        context: this.context,
                        text: arrList[index],
                        type: MessageType.LEFT,
                        optionList: null,
                        highlightedWords: medicalTerms,
                        backgroundType: _list.length > 0 ? 0 : -1));
                  } else if (index == arrList.length - 1) {
                    _staticData.add(ChatMessage(
                        isEditable: false,
                        context: this.context,
                        text: arrList[index],
                        type: MessageType.LEFT,
                        optionList: null,
                        highlightedWords: medicalTerms,
                        backgroundType: 2));
                  } else {
                    _staticData.add(ChatMessage(
                      isEditable: false,
                      context: this.context,
                      text: arrList[index],
                      type: MessageType.LEFT,
                      optionList: null,
                      highlightedWords: medicalTerms,
                      backgroundType: 1,
                    ));
                  }
                }
                showStaticMessages();
              });
            } else {
              List<String> arrList = question.split(r"$$ ");
              Log.printLog("MY DATA", " $arrList");

              if (model != null) {
                if (model.isCorrect) {
                  Log.printLog("OPTION MODEL", "IS NOT NULL");
                  _staticData.add(ChatMessage(
                      isEditable: false,
                      context: this.context,
                      text: model.comment,
                      type: MessageType.LEFT,
                      optionList: null,
                      highlightedWords: medicalTerms));
                } else {
                  _staticData.add(ChatMessage(
                      isEditable: false,
                      context: this.context,
                      text: model.comment,
                      type: MessageType.LEFT,
                      optionList: null,
                      highlightedWords: medicalTerms));
                  if (jsonObject['more_info'] != null) {
                    _staticData.add(ChatMessage(
                      isEditable: false,
                      context: this.context,
                      text: jsonObject['more_info'],
                      type: MessageType.LEFT,
                      optionList: null,
                      highlightedWords: medicalTerms,
                      backgroundType: 2,
                    ));
                  }
                }
              } else {
                Log.printLog("OPTION MODEL", "IS NULL");
              }

              for (int index = 0; index < arrList.length; index++) {
                if (index == 0) {
                  _staticData.add(ChatMessage(
                      isEditable: false,
                      context: this.context,
                      text: arrList[index],
                      type: MessageType.LEFT,
                      optionList: null,
                      highlightedWords: medicalTerms,
                      backgroundType: _list.length > 0 ? 0 : -1));
                } else if (index == arrList.length - 1) {
                  _staticData.add(ChatMessage(
                      isEditable: false,
                      context: this.context,
                      text: arrList[index],
                      type: MessageType.LEFT,
                      optionList: null,
                      highlightedWords: medicalTerms,
                      backgroundType: 2));
                } else {
                  _staticData.add(ChatMessage(
                    isEditable: false,
                    context: this.context,
                    text: arrList[index],
                    type: MessageType.LEFT,
                    optionList: null,
                    highlightedWords: medicalTerms,
                    backgroundType: 1,
                  ));
                }
              }
              showStaticMessages();
            }
          } else if (jsonResponse.length > 0 && jsonResponse.length == 1) {
            var jsonObject = jsonData['data'][0];
            Log.printLog("QUESTION : ", "${jsonObject['question']}");
            Log.printLog("NXT Q ID : ", "${jsonObject['nextq_id']}");
            Log.printLog("CUR Q ID : ", "${jsonObject['question_id']}");
            setState(() {
              _nextQuestionID = jsonObject['nextq_id'] as int;
              _currentQuestionID = jsonObject['question_id'] as int;
            });

            String question = jsonObject['question'];
            String moreInfo = jsonObject['more_info'];
            if (moreInfo != null && moreInfo.isNotEmpty) {
              _highlightedWordsFunct(moreInfo);
            }

            List<String> arrList = question.split(r"$$ ");
            Log.printLog("MY DATA", " $arrList");

            for (int index = 0; index < arrList.length; index++) {
              if (index == 0) {
                _staticData.add(ChatMessage(
                    isEditable: false,
                    context: this.context,
                    text: arrList[index],
                    type: MessageType.LEFT,
                    optionList: null,
                    highlightedWords: medicalTerms,
                    backgroundType: _list.length > 0 ? 0 : -1));
              } else if (index == arrList.length - 1) {
                _staticData.add(ChatMessage(
                    isEditable: false,
                    context: this.context,
                    text: arrList[index],
                    type: MessageType.LEFT,
                    optionList: null,
                    highlightedWords: medicalTerms,
                    backgroundType: 2));
              } else {
                _staticData.add(ChatMessage(
                    isEditable: false,
                    context: this.context,
                    text: arrList[index],
                    type: MessageType.LEFT,
                    optionList: null,
                    highlightedWords: medicalTerms,
                    backgroundType: 1));
              }
            }

            showStaticMessages();
          }

          if (_items.length > 4) {
            setState(() {
              _tagSpacing = 30.0;
            });
          } else {
            setState(() {
              _tagSpacing = 20.0;
            });
          }
        });
      }
    } on SocketException catch (_) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
      setState(() {
        _list.removeLast();
        /*  _list.add(ChatMessage(isEditable: false,context: this.context,
            text: "Please check your internet connection and try again.",
            type: "ERROR",
            optionList: null));*/
      });
      displayRetry("Error", "Please check internet connection and try again.");
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    _controllerHand.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    return Stack(
      children: <Widget>[
        Container(
            color: Color.fromRGBO(232, 239, 245, 1),
            /* child: Column(
          children: <Widget>[
            Flexible(
              child: Container(
                child: ListView.builder(
                  controller: _scrollController,
                  padding: new EdgeInsets.all(8.0),
                  itemBuilder: (_, int index) => _list[index],
                  itemCount: _list.length,
                ),
              ),
            ),
            Visibility(
              visible: _showPersonalDetailsForm,
              child: Container(
                color: Color.fromRGBO(213, 224, 236, 1),
                padding: EdgeInsets.only(
                    left: ScreenUtil.screenHeightDp * 0.03,
                    right: ScreenUtil.screenHeightDp * 0.03,
                    top: ScreenUtil.screenHeightDp * 0.03),
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              padding: EdgeInsets.only(
                                  left: 10, right: 10, bottom: 0.5),
                              child: TextField(
                                textCapitalization: TextCapitalization.words,
                                controller: _tec_Name,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  hintText: "Your name",
                                  hintStyle: TextStyle(
                                      color: Color.fromRGBO(153, 153, 153, 1)),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                  ),
                                ),
                                keyboardType: TextInputType.text,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Flexible(
                            flex: 1,
                            child: Container(
                              padding: EdgeInsets.only(right: 5),
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              alignment: Alignment.centerRight,
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  items: <String>[
                                    'Below 45',
                                    'Between 45 - 60',
                                    'Above 60'
                                  ].map((String _selectedValue) {
                                    return new DropdownMenuItem<String>(
                                      value: _selectedValue,
                                      child: new Text(
                                        _selectedValue,
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    );
                                  }).toList(),
                                  hint: Text(_selectedAge),
                                  onChanged: (String _selectedValue) {
                                    setState(() {
                                      FocusScope.of(context)
                                          .requestFocus(new FocusNode());
                                      _selectedAge = _selectedValue;
                                    });
                                  },
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: ScreenUtil.screenHeightDp * 0.03,
                      ),
                      Row(
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            child: Container(
                              padding: EdgeInsets.only(right: 5),
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              alignment: Alignment.centerRight,
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  items: <String>['Female', 'Male']
                                      .map((String _selectedValue) {
                                    return new DropdownMenuItem<String>(
                                      value: _selectedValue,
                                      child: new Text(
                                        _selectedValue,
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    );
                                  }).toList(),
                                  hint: Text(_selectedGender),
                                  onChanged: (String _selectedValue) {
                                    setState(() {
                                      FocusScope.of(context)
                                          .requestFocus(new FocusNode());
                                      _selectedGender = _selectedValue;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Flexible(
                            flex: 1,
                            child: Container(
                              padding: EdgeInsets.only(right: 5),
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              alignment: Alignment.centerRight,
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  items: <String>[
                                    'Asian',
                                    'African American',
                                    'Caucasian',
                                    'Hispanic',
                                    'Others',
                                    'not Known'
                                  ].map((String _selectedValue) {
                                    return new DropdownMenuItem<String>(
                                      value: _selectedValue,
                                      child: new Text(
                                        _selectedValue,
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    );
                                  }).toList(),
                                  hint: Text(_selectedEthnicity),
                                  onChanged: (String _selectedValue) {
                                    setState(() {
                                      FocusScope.of(context)
                                          .requestFocus(new FocusNode());
                                      _selectedEthnicity = _selectedValue;
                                    });
                                  },
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: ScreenUtil.screenHeightDp * 0.03,
                      ),
                      GestureDetector(
                        onTap: () {
                          handlePersonalDetailForm();
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: (ScreenUtil.screenHeightDp * 0.15) * 0.40,
                          width: ScreenUtil.screenHeightDp * 0.40,
                          decoration: const BoxDecoration(
                              color: Color.fromRGBO(89, 148, 172, 1),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(60.0))),
//                          padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
                          child: Text("Continue",
                              style: new TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'BrandingMedium',
                                  fontSize:
                                      (ScreenUtil.screenHeightDp * 0.40) *
                                          0.10)),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil.screenHeightDp * 0.025,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Visibility(
              visible: _showOptions,
              child: FractionallySizedBox(
                widthFactor: 1.0,
                child: Container(
//                color: Color.fromRGBO(213, 224, 236, 1),
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenHeightDp * 0.02,
                      right: ScreenUtil.screenHeightDp * 0.02,
                      top: ScreenUtil.screenHeightDp * 0.03,
                      bottom: ScreenUtil.screenHeightDp * 0.03),
                  child: SlideTransition(
                    position: _offsetFloat,
                    child: Tags(
                      spacing: 10,
                      runSpacing: 10,
                      direction: Axis.horizontal,
                      alignment: WrapAlignment.center,
                      horizontalScroll: true,
                      itemCount: _items.length,
                      // required
                      itemBuilder: (int index) {
                        final item = _items[index];
                        return ItemTags(
                          // Each ItemTags must contain a Key. Keys allow Flutter to
                          // uniquely identify widgets.
                          key: Key(index.toString()),
                          index: index,
                          // required
                          title: item.optionText,
                          pressEnabled: true,
                          singleItem: true,
                          activeColor: Color.fromRGBO(89, 148, 172, 1),
                          color: Color.fromRGBO(89, 148, 172, 1),
                          textColor: Colors.white,
                          borderRadius: BorderRadius.circular(2.0),
                          elevation: 2,
                          padding: EdgeInsets.only(
                              left: 15, right: 15, top: 9, bottom: 9),
                          textStyle: TextStyle(
                            fontSize: _fontSize,
                          ),
                          onPressed: (item) => {handleOptionsClicked(item)},
                          onLongPressed: (item) => {
                            Log.printLog("ITEM LONG PRESSED : ", item.toString())
                          },
                        );
                      },
                    ),
                  ),
                ),
              ),
            ),
            Visibility(
              visible: _show27,
              child: Container(
                color: Color.fromRGBO(213, 224, 236, 1),
                padding: EdgeInsets.only(
                    left: ScreenUtil.screenHeightDp * 0.03,
                    right: ScreenUtil.screenHeightDp * 0.03,
                    top: ScreenUtil.screenHeightDp * 0.03),
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            child: Container(
                              padding: EdgeInsets.only(right: 5),
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              alignment: Alignment.centerRight,
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  items: <String>['Female', 'Male']
                                      .map((String _selectedValue) {
                                    return new DropdownMenuItem<String>(
                                      value: _selectedValue,
                                      child: new Text(
                                        _selectedValue,
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    );
                                  }).toList(),
                                  hint: Text(_selectedGender),
                                  onChanged: (String _selectedValue) {
                                    setState(() {
                                      FocusScope.of(context)
                                          .requestFocus(new FocusNode());
                                      _selectedGender = _selectedValue;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Flexible(
                            flex: 1,
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              padding: EdgeInsets.only(
                                  left: 10, right: 10, bottom: 0.5),
                              child: TextField(
                                controller: _tec_relative_age,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  hintText: "Relative's Age",
                                  hintStyle: TextStyle(
                                      color: Color.fromRGBO(153, 153, 153, 1)),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                  ),
                                ),
                                keyboardType: TextInputType.number,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: ScreenUtil.screenHeightDp * 0.03,
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 5, left: 5),
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            borderRadius: new BorderRadius.circular(5.0)),
                        alignment: Alignment.centerRight,
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            items: <String>[
                              'Bilateral breast cancer',
                              'Two breast primaries',
                              'Triple negative breast cancer',
                              'Other'
                            ].map((String _selectedValue) {
                              return new DropdownMenuItem<String>(
                                value: _selectedValue,
                                child: new Text(
                                  _selectedValue,
                                  style: TextStyle(color: Colors.black),
                                ),
                              );
                            }).toList(),
                            hint: Text(_selectedCancerType),
                            onChanged: (String _selectedValue) {
                              setState(() {
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                                _selectedCancerType = _selectedValue;
                              });
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil.screenHeightDp * 0.03,
                      ),
                      GestureDetector(
                        onTap: () {
                          handle27();
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: (ScreenUtil.screenHeightDp * 0.15) * 0.40,
                          width: ScreenUtil.screenHeightDp * 0.40,
                          decoration: const BoxDecoration(
                              color: Color.fromRGBO(89, 148, 172, 1),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(60.0))),
//                          padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
                          child: Text("Continue",
                              style: new TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'BrandingMedium',
                                  fontSize:
                                      (ScreenUtil.screenHeightDp * 0.40) *
                                          0.10)),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil.screenHeightDp * 0.025,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Visibility(
              visible: _show13,
              child: Container(
                color: Color.fromRGBO(213, 224, 236, 1),
                padding: EdgeInsets.only(
                    left: ScreenUtil.screenHeightDp * 0.03,
                    right: ScreenUtil.screenHeightDp * 0.03,
                    top: ScreenUtil.screenHeightDp * 0.03),
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(right: 5, left: 5),
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            borderRadius: new BorderRadius.circular(5.0)),
                        alignment: Alignment.centerRight,
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            items: <String>[
                              'Breast Cancer',
                              'Pancreatic Cancer',
                              'Prostate Cancer',
                              'Colorectal Cancer',
                              'Stomach Cancer',
                              'Otherr'
                            ].map((String _selectedValue) {
                              return new DropdownMenuItem<String>(
                                value: _selectedValue,
                                child: new Text(
                                  _selectedValue,
                                  style: TextStyle(color: Colors.black),
                                ),
                              );
                            }).toList(),
                            hint: Text(_selectedCancerType),
                            onChanged: (String _selectedValue) {
                              setState(() {
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                                _selectedCancerType = _selectedValue;
                              });
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil.screenHeightDp * 0.03,
                      ),
                      GestureDetector(
                        onTap: () {
                          handle13();
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: (ScreenUtil.screenHeightDp * 0.15) * 0.40,
                          width: ScreenUtil.screenHeightDp * 0.40,
                          decoration: const BoxDecoration(
                              color: Color.fromRGBO(89, 148, 172, 1),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(60.0))),
//                          padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
                          child: Text("Continue",
                              style: new TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'BrandingMedium',
                                  fontSize:
                                      (ScreenUtil.screenHeightDp * 0.40) *
                                          0.10)),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil.screenHeightDp * 0.025,
                      ),
                    ],
                  ),
                ),
              ),
            ),
//          generated.GeneratedWidget()
          ],
        ),*/

            child: Column(
              children: <Widget>[
                Flexible(
                  child: Container(
                    child: ListView.builder(
                      controller: _scrollController,
                      padding: new EdgeInsets.all(8.0),
                      itemBuilder: (_, int index) => _list[index],
                      itemCount: _list.length,
                    ),
                  ),
                ),
                Visibility(
                  visible: _showPersonalDetailsForm,
                  child: Container(
                    color: Color.fromRGBO(213, 224, 236, 1),
                    padding: EdgeInsets.only(
                        left: ScreenUtil.screenHeightDp * 0.03,
                        right: ScreenUtil.screenHeightDp * 0.03,
                        top: ScreenUtil.screenHeightDp * 0.03),
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Flexible(
                                flex: 1,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 255, 255, 1),
                                      borderRadius:
                                          new BorderRadius.circular(5.0)),
                                  padding: EdgeInsets.only(
                                      left: 10, right: 10, bottom: 0.5),
                                  child: TextField(
                                    textCapitalization:
                                        TextCapitalization.words,
                                    controller: _tec_Name,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                      hintText: "Your name",
                                      hintStyle: TextStyle(
                                          color:
                                              Color.fromRGBO(153, 153, 153, 1)),
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.white),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.white),
                                      ),
                                    ),
                                    keyboardType: TextInputType.text,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                flex: 1,
                                child: Container(
                                  padding: EdgeInsets.only(right: 5),
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 255, 255, 1),
                                      borderRadius:
                                          new BorderRadius.circular(5.0)),
                                  alignment: Alignment.centerRight,
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      items: <String>[
                                        'Below 45',
                                        'Between 45 - 60',
                                        'Above 60'
                                      ].map((String _selectedValue) {
                                        return new DropdownMenuItem<String>(
                                          value: _selectedValue,
                                          child: new Text(
                                            _selectedValue,
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                        );
                                      }).toList(),
                                      hint: Text(_selectedAge),
                                      onChanged: (String _selectedValue) {
                                        setState(() {
                                          FocusScope.of(context)
                                              .requestFocus(new FocusNode());
                                          _selectedAge = _selectedValue;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil.screenHeightDp * 0.03,
                          ),
                          Row(
                            children: <Widget>[
                              Flexible(
                                flex: 1,
                                child: Container(
                                  padding: EdgeInsets.only(right: 5),
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 255, 255, 1),
                                      borderRadius:
                                          new BorderRadius.circular(5.0)),
                                  alignment: Alignment.centerRight,
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      items: <String>['Female', 'Male']
                                          .map((String _selectedValue) {
                                        return new DropdownMenuItem<String>(
                                          value: _selectedValue,
                                          child: new Text(
                                            _selectedValue,
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                        );
                                      }).toList(),
                                      hint: Text(_selectedGender),
                                      onChanged: (String _selectedValue) {
                                        setState(() {
                                          FocusScope.of(context)
                                              .requestFocus(new FocusNode());
                                          _selectedGender = _selectedValue;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                flex: 1,
                                child: Container(
                                  padding: EdgeInsets.only(right: 5),
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 255, 255, 1),
                                      borderRadius:
                                          new BorderRadius.circular(5.0)),
                                  alignment: Alignment.centerRight,
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      items: <String>[
                                        'Asian',
                                        'African American',
                                        'Caucasian',
                                        'Hispanic',
                                        'Others',
                                        'not Known'
                                      ].map((String _selectedValue) {
                                        return new DropdownMenuItem<String>(
                                          value: _selectedValue,
                                          child: new Text(
                                            _selectedValue,
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                        );
                                      }).toList(),
                                      hint: Text(_selectedEthnicity),
                                      onChanged: (String _selectedValue) {
                                        setState(() {
                                          FocusScope.of(context)
                                              .requestFocus(new FocusNode());
                                          _selectedEthnicity = _selectedValue;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil.screenHeightDp * 0.03,
                          ),
                          GestureDetector(
                            onTap: () {
                              handlePersonalDetailForm();
                            },
                            child: Container(
                              alignment: Alignment.center,
                              height: (ScreenUtil.screenHeightDp * 0.15) * 0.40,
                              width: ScreenUtil.screenHeightDp * 0.40,
                              decoration: const BoxDecoration(
                                  color: Color.fromRGBO(89, 148, 172, 1),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(60.0))),
//                          padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
                              child: Text("Continue",
                                  style: new TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'BrandingMedium',
                                      fontSize:
                                          (ScreenUtil.screenHeightDp * 0.40) *
                                              0.10)),
                            ),
                          ),
                          SizedBox(
                            height: ScreenUtil.screenHeightDp * 0.025,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: _showOptions,
                  child: FractionallySizedBox(
                    widthFactor: 1.0,
                    child: Container(
//                color: Color.fromRGBO(213, 224, 236, 1),
                      /*padding: EdgeInsets.only(
                        left: ScreenUtil.screenHeightDp * 0.02,
                        right: ScreenUtil.screenHeightDp * 0.02,
                          top: ScreenUtil.screenHeightDp * 0.03,
                          bottom: ScreenUtil.screenHeightDp * 0.03
                      ),*/
                      child: SlideTransition(
                        position: _offsetFloat,
                        child: Tags(
                          spacing: 10,
                          runSpacing: 10,
                          direction: Axis.horizontal,
                          alignment: WrapAlignment.center,
                          horizontalScroll: true,
                          itemCount: _items.length,
                          // required
                          itemBuilder: (int index) {
                            final item = _items[index];
                            return ItemTags(
                              // Each ItemTags must contain a Key. Keys allow Flutter to
                              // uniquely identify widgets.
                              key: Key(index.toString()),
                              index: index,
                              // required
                              title: item.optionText,
                              pressEnabled: true,
                              singleItem: true,
                              activeColor: Color.fromRGBO(89, 148, 172, 1),
                              color: Color.fromRGBO(89, 148, 172, 1),
                              textColor: Colors.white,
                              borderRadius: BorderRadius.circular(20.0),
                              elevation: 2,
                              padding: EdgeInsets.only(
                                  left: 15, right: 15, top: 9, bottom: 9),
                              textStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(_fontSize),
                              ),
                              onPressed: (item) => {handleOptionsClicked(item)},
                              onLongPressed: (item) => {
                                Log.printLog(
                                    "ITEM LONG PRESSED : ", item.toString())
                              },
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: _show27,
                  child: Container(
                    color: Color.fromRGBO(213, 224, 236, 1),
                    padding: EdgeInsets.only(
                        left: ScreenUtil.screenHeightDp * 0.03,
                        right: ScreenUtil.screenHeightDp * 0.03,
                        top: ScreenUtil.screenHeightDp * 0.03),
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Flexible(
                                flex: 1,
                                child: Container(
                                  padding: EdgeInsets.only(right: 5),
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 255, 255, 1),
                                      borderRadius:
                                          new BorderRadius.circular(5.0)),
                                  alignment: Alignment.centerRight,
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      items: <String>['Female', 'Male']
                                          .map((String _selectedValue) {
                                        return new DropdownMenuItem<String>(
                                          value: _selectedValue,
                                          child: new Text(
                                            _selectedValue,
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                        );
                                      }).toList(),
                                      hint: Text(_selectedGender),
                                      onChanged: (String _selectedValue) {
                                        setState(() {
                                          FocusScope.of(context)
                                              .requestFocus(new FocusNode());
                                          _selectedGender = _selectedValue;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                flex: 1,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(255, 255, 255, 1),
                                      borderRadius:
                                          new BorderRadius.circular(5.0)),
                                  padding: EdgeInsets.only(
                                      left: 10, right: 10, bottom: 0.5),
                                  child: TextField(
                                    controller: _tec_relative_age,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                      hintText: "Relative's Age",
                                      hintStyle: TextStyle(
                                          color:
                                              Color.fromRGBO(153, 153, 153, 1)),
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.white),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.white),
                                      ),
                                    ),
                                    keyboardType: TextInputType.number,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil.screenHeightDp * 0.03,
                          ),
                          Container(
                            padding: EdgeInsets.only(right: 5, left: 5),
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                borderRadius: new BorderRadius.circular(5.0)),
                            alignment: Alignment.centerRight,
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                items: <String>[
                                  'Bilateral breast cancer',
                                  'Two breast primaries',
                                  'Triple negative breast cancer',
                                  'Other'
                                ].map((String _selectedValue) {
                                  return new DropdownMenuItem<String>(
                                    value: _selectedValue,
                                    child: new Text(
                                      _selectedValue,
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  );
                                }).toList(),
                                hint: Text(_selectedCancerType),
                                onChanged: (String _selectedValue) {
                                  setState(() {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                    _selectedCancerType = _selectedValue;
                                  });
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            height: ScreenUtil.screenHeightDp * 0.03,
                          ),
                          GestureDetector(
                            onTap: () {
                              handle27();
                            },
                            child: Container(
                              alignment: Alignment.center,
                              height: (ScreenUtil.screenHeightDp * 0.15) * 0.40,
                              width: ScreenUtil.screenHeightDp * 0.40,
                              decoration: const BoxDecoration(
                                  color: Color.fromRGBO(89, 148, 172, 1),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(60.0))),
//                          padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
                              child: Text("Continue",
                                  style: new TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'BrandingMedium',
                                      fontSize: (ScreenUtil().setSp(14)))),
                            ),
                          ),
                          SizedBox(
                            height: ScreenUtil.screenHeightDp * 0.025,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: _show13,
                  child: Container(
                    color: Color.fromRGBO(213, 224, 236, 1),
                    padding: EdgeInsets.only(
                        left: ScreenUtil.screenHeightDp * 0.03,
                        right: ScreenUtil.screenHeightDp * 0.03,
                        top: ScreenUtil.screenHeightDp * 0.03),
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(right: 5, left: 5),
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                borderRadius: new BorderRadius.circular(5.0)),
                            alignment: Alignment.centerRight,
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                items: <String>[
                                  'Breast Cancer',
                                  'Pancreatic Cancer',
                                  'Prostate Cancer',
                                  'Colorectal Cancer',
                                  'Stomach Cancer',
                                  'Otherr'
                                ].map((String _selectedValue) {
                                  return new DropdownMenuItem<String>(
                                    value: _selectedValue,
                                    child: new Text(
                                      _selectedValue,
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  );
                                }).toList(),
                                hint: Text(_selectedCancerType),
                                onChanged: (String _selectedValue) {
                                  setState(() {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                    _selectedCancerType = _selectedValue;
                                  });
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            height: ScreenUtil.screenHeightDp * 0.03,
                          ),
                          GestureDetector(
                            onTap: () {
                              handle13();
                            },
                            child: Container(
                              alignment: Alignment.center,
                              height: (ScreenUtil.screenHeightDp * 0.15) * 0.40,
                              width: ScreenUtil.screenWidthDp * 0.40,
                              decoration: const BoxDecoration(
                                  color: Color.fromRGBO(89, 148, 172, 1),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(60.0))),
//                          padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
                              child: Text("Continue",
                                  style: new TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'BrandingMedium',
                                      fontSize:
                                          (ScreenUtil.screenWidthDp * 0.40) *
                                              0.10)),
                            ),
                          ),
                          SizedBox(
                            height: ScreenUtil.screenHeightDp * 0.025,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
//          generated.GeneratedWidget()
              ],
            )),
        Visibility(
          visible: _showStaticInfoPage,
          child: Container(
            color: Color.fromRGBO(0, 0, 0, 0.7),
            height: ScreenUtil.screenHeightDp,
            child: Stack(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      "Scroll right to left to see more options and select one option from them.",
                      style: TextStyle(
                          color: Color(0xFFFF9300),
                          fontFamily: 'BrandingMedium',
                          fontSize: (ScreenUtil.screenWidthDp * 0.40) * 0.15),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(15),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        RaisedButton(
                          onPressed: () {},
                          color: Colors.transparent,
                          textColor: Colors.white,
                          padding: const EdgeInsets.all(0.0),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(60.0)),
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                _showStaticInfoPage = false;
                              });
                            },
                            child: Container(
                              alignment: Alignment.center,
                              height: (ScreenUtil.screenHeightDp * 0.15) * 0.40,
                              width: ScreenUtil.screenWidthDp * 0.35,
                              decoration: const BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(60.0))),
//                          padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
                              child: Text("Ok",
                                  style: new TextStyle(
                                      color: Color(0xff454545),
                                      fontFamily: 'BrandingMedium',
                                      fontSize: (ScreenUtil().setSp(14)))),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Image.asset("images/down_arrow.png",
                            color: Colors.white,
                            height: ScreenUtil.screenHeightDp * 0.15,
                            width: ScreenUtil.screenWidthDp * 0.15,
                            alignment: Alignment.center)
                      ],
                    ),
                    SizedBox(
                      height: ScreenUtil.screenHeightDp * 0.12,
                    ),
                  ],
                ),
                SlideTransition(
                  position: _offsetSwipe,
                  child: Container(
                    alignment: Alignment.bottomRight,
                    child: Image.asset("images/swipe.png",
                        color: Colors.white,
                        height: ScreenUtil.screenHeightDp * 0.10,
                        width: ScreenUtil.screenWidthDp * 0.10,
                        alignment: Alignment.bottomRight),
                  ),
                )
              ],
            ),
          ),
        ),
//        ScrollHandAnimation()
      ],
    );
  }

  void showStaticMessages() {
    if (_list.length > 0) {
      setState(() {
        ChatMessage loader = new ChatMessage(
            isEditable: false,
            context: this.context,
            text: "",
            type: MessageType.LOADER,
            optionList: null,
            highlightedWords: medicalTerms);
        _list.add(loader);
      });
      scrollList();
      if (_staticData.length > 0) {
        new Future.delayed(
            Duration(
                milliseconds: Log.getLoaderDelayDuration(
                    _staticData[0].text.toString().length, null)), () {
          setState(() {
            _list.removeLast();
            _list.add(_staticData[0]);
          });

          scrollList();

          new Future.delayed(
              Duration(
                  milliseconds: Log.getMessageDelayDuration(
                      _staticData[0].text.toString().length, null)), () {
            _staticData.removeAt(0);
            if (_staticData.length > 0) {
              showStaticMessages();
            } else {
              // EXIT AND DO NEXT TASK
              handleUIVisibility();
            }
          });
        });
      } else {
        // EXIT AND DO NEXT TASK
        handleUIVisibility();
      }
    } else {
      if (_staticData.length > 0) {
        setState(() {
          _list.add(_staticData[0]);
        });
        scrollList();
        new Future.delayed(
            Duration(
                milliseconds: Log.getMessageDelayDuration(
                    _list.last.text.toString().length, null)), () {
          _staticData.removeAt(0);
          if (_staticData.length > 0) {
            showStaticMessages();
          } else {
            // EXIT AND DO NEXT TASK
            handleUIVisibility();
          }
        });
      } else {
        // EXIT AND DO NEXT TASK
        handleUIVisibility();
      }
    }
  }

  void handleOptionsClicked(
    Item item,
  ) {
    _controller.reverse();

    if (_items.length > 0) {
      model = null;
      model = _items.elementAt(item.index);

      if (model.optionText == "Order Test") {
        displayAlertWithActionLable(
            "Order Test",
            "Your order is under review by our genetic counselors. We will provide you an update by email soon.",
            "Ok");
        return;
      }

      if (model.optionText == "Learn more about HCS") {
        displayAlertWithActionLable("", "Comming soon....", "Ok");
        return;
      }

      setState(() {
        _items.clear();
        _showOptions = true;
        _list.add(ChatMessage(
            isEditable: false,
            context: this.context,
            text: model.optionText,
            type: MessageType.RIGHT));
        _nextQuestionID = model.nextQuestionID;
      });
      _loadNext();
    }
  }

  void handlePersonalDetailForm() {
    if (_tec_Name.text.length == 0) {
      displayAlert("Error", "Please provide all details");
      return;
    }
    if (_selectedAge == "Select age") {
      displayAlert("Error", "Please provide all details");
      return;
    }
    if (_selectedGender == "Select gender") {
      displayAlert("Error", "Please provide all details");
      return;
    }
    if (_selectedEthnicity == "Select ethnicity") {
      displayAlert("Error", "Please provide all details");
      return;
    }

    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        context: this.context,
        text: "",
        type: MessageType.PERSONAL_DETAILS,
        personalDetailModel: PersonalDetailModel(
            _tec_Name.text, _selectedAge, _selectedGender, _selectedEthnicity),
        optionList: null);

    setState(() {
      _showPersonalDetailsForm = false;
      _showOptions = false;
      _list.add(object3);
    });
    scrollList();
    _loadNext();
  }

  void scrollList() {
    /* Timer(
        Duration(milliseconds: 300),
            () => _scrollController
            .jumpTo(_scrollController.position.maxScrollExtent));*/
    Timer(
        Duration(milliseconds: 300),
        () => _scrollController.animateTo(
            _scrollController.position.maxScrollExtent,
            curve: Curves.linear,
            duration: Duration(milliseconds: 300)));
  }

  void handleUIVisibility() {
    if (_list[_list.length - 1].text ==
        "Please revisit GeneFAX™ when you wish and remember to tell your friends and family about me a.k.a. GeneFAX™.") {
      setState(() {
        _showOptions = false;
      });
    } else {
      setState(() {
        _showOptions = true;
      });
    }
    setState(() {
//      _showOptions = true;
      _controller.forward();
      _showPersonalDetailsForm = false;
      _show27 = false;
      _show13 = false;
    });

    scrollList();
  }

  void handle27() {
    if (_tec_relative_age.text.length == 0) {
      displayAlert("Error", "Please provide all details");
      return;
    }
    if (_selectedGender == "Select gender") {
      displayAlert("Error", "Please provide all details");
      return;
    }
    if (_selectedCancerType == "Select cancer") {
      displayAlert("Error", "Please provide all details");
      return;
    }

    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        context: this.context,
        text: "",
        type: MessageType.DIGNOSED_RELATIVE_DETAILS,
        personalDetailModel: null,
        relativeDetailModel: CancerRelativeDetail(_selectedGender,
            _tec_relative_age.text.toString(), _selectedCancerType),
        optionList: null);

    setState(() {
      _showPersonalDetailsForm = false;
      _showOptions = false;
      _show27 = false;
      _list.add(object3);
    });

    scrollList();

    _loadNext();
  }

  void handle13() {
    if (_selectedCancerType == "Select cancer") {
      displayAlert("Error", "Please provide all details");
      return;
    }

    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        context: this.context,
        text: _selectedCancerType,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null);

    switch (_selectedCancerType) {
      case "Breast cancer":
        setState(() {
          _nextQuestionID = 0;
          _cancerTypeValue = "15_16_17";
        });
        break;
      case "Pancreatic cancer":
        setState(() {
          _nextQuestionID = 0;
          _cancerTypeValue = "25";
        });
        break;
      case "Prostate Cancer":
        setState(() {
          _nextQuestionID = 0;
          _cancerTypeValue = "34_35";
        });
        break;
      case "Colorectal Cancer":
        setState(() {
          _nextQuestionID = 0;
          _cancerTypeValue = "43_44";
        });
        break;
      case "Stomach Cancer":
        setState(() {
          _nextQuestionID = 0;
          _cancerTypeValue = "40";
        });
        break;
      case "Other":
        setState(() {
          _nextQuestionID = 0;
          _cancerTypeValue = "46";
        });
        break;
    }

    setState(() {
      _showPersonalDetailsForm = false;
      _showOptions = false;
      _show27 = false;
      _show13 = false;
      _list.add(object3);
    });

    scrollList();

    _loadNext();
  }

  Future<String> _highlightedWordsFunct(String moreInfo) async {
    String url = "${Log.API_STACK}getMedicalTermsFromAnswer?text=$moreInfo";
    Log.printLog("API URL", url);
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.post(url);
        var jsonData = json.decode(data.body);
        Log.printLog("API RESPONSE highlightedWords", jsonData.toString());
        medicalTerms = jsonData.toString();
        if (medicalTerms != null) {
          medicalTerms = medicalTerms.replaceAll('[', '');
          return medicalTerms = medicalTerms.replaceAll(']', '');
        }
        return '';
      }
    } on SocketException catch (_) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
      displayRetry("Error", "Please check internet connection and try again.");
      return '';
    }
  }
}
