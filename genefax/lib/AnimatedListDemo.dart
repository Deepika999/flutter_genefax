import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class AnimatedListDemo extends StatefulWidget {
  @override
  _AnimatedListDemoState createState() => _AnimatedListDemoState();
}

class Item {
  Item({this.name});

  String name;
}

class _AnimatedListDemoState extends State<AnimatedListDemo> {
  List<Item> items = new List<Item>();
  final GlobalKey<AnimatedListState> listKey = GlobalKey<AnimatedListState>();
  var rng = new Random();

  _addItem() {
    setState(() {
      listKey.currentState
          .insertItem(items.length, duration: const Duration(seconds: 1));
      int id = items.length+1;
      items.add(new Item(name: "New Item $id"));
    });
  }

  _removeItem() {
    setState(() {
      int id = items.length-1;
      listKey.currentState.removeItem(
        id,
        (context, animation) => buildItem(context, 0, animation),
        duration: const Duration(milliseconds: 250),
      );
      items.removeAt(id);
    });
  }

  @override
  Widget build(BuildContext context) {
    final Map<int, Animation<double>> animations = <int, Animation<double>>{};
    return Scaffold(
        appBar: AppBar(
          title: Text("Dynamic List"),
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.add),
              onPressed: _addItem,
            ),
            IconButton(
              icon: const Icon(Icons.remove),
              onPressed: _removeItem,
            )
          ],
        ),
        body: Directionality(
            textDirection: TextDirection.ltr,
            child: AnimatedList(
              key: listKey,
              initialItemCount: items.length,
              itemBuilder: (context, index, animation) {
                return buildItem(context, index, animation);
              },
            )));
  }

  buildItem(BuildContext context, int position, Animation<double> animation) {
    return SizeTransition(
      key: ValueKey<int>(position),
      axis: Axis.vertical,
      sizeFactor: animation,
      child: SizedBox(
        child: ListTile(
          title: Text('${items[position].name}'),
        ),
      ),
    );
  }
}
