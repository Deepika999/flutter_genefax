import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ai/Constant.dart';
import 'package:ai/RouteManager.dart';
import 'package:ai/RouteNames.dart';
import 'package:ai/locator.dart';
import 'package:ai/screens/Notifications.dart';
import 'package:ai/services/navigation_service.dart';
import 'package:ai/services/push_notification_service.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uni_links/uni_links.dart';

import 'HomePage.dart';
import 'Log.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

bool isDeepLink = false;

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primaryColor: headerColor,
        accentColor: headerColor,
        primarySwatch: Colors.blue,
      ),
      navigatorKey: locator<NavigationService>().navigationKey,
      initialRoute: RouteSplashScreen,
      onGenerateRoute: generateRoute,
      /* routes: {
        RouteSplashScreen: (_) => new SplashScreen(),
        RouteWelcomeDialog: (_) => new OptraWelcomeDialog(),
        RouteHomePage: (_) => new HomePage(),
      },*/
//        routes: <String, WidgetBuilder>{
//          "/Nexpage1": (BuildContext context) => new OptraHCS(),
//        }
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  Uri _latestUri;

  StreamSubscription _sub;
  UniLinksType _type = UniLinksType.string;

  String _latestLink;
  var buttonHeight = 32.0;
  double buttonTextSize = 15.0;

  bool isNetworkConnected = true;
  bool isRetryPressed = false;
  bool isServerConnected = false;
  AnimationController animationController;
  AnimationController animationController1;
  Animation<double> animation1;
  Animation animation;

  var _showLoader = false;

  FirebaseMessaging _messaging = FirebaseMessaging();

  final PushNotificationService _pushNotificationService =
      locator<PushNotificationService>();

  bool isFromNotification = false;

  var notificationView;

  @override
  void initState() {
    super.initState();

    _messaging.getToken().then((token) => print('ANDROID DEVICE ID : $token'));

    // iOS firebase push notification
    _messaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _messaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });

    _messaging.getToken().then((String token) {
      assert(token != null);
//      setState(() {
//        _homeScreenText = "Push Messaging token: $token";
//      });\
      print('iOS Device ID : $token');
    });
    // _pushNotificationService.initialise();

    _messaging.getToken().then((token) => print('ANDROID DEVICE ID : $token'));

    var myBackgroundMessageHandler;
    _messaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        // THIS CALLBACK GETS INVOKED WHEN APP IS IN THE FOREGROUND
        print("onMessage: $message");

        addNotificationMessage(message);
      },
      onBackgroundMessage: Platform.isIOS ? null : myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        // THIS CALLBACK GETS INVOKED WHEN APP IS NOT RUNNING AND OPENED VIA NOTIFICATION CLICK
        print("onLaunch: $message");
        _serialiseAndNavigate(message);
        // _navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) async {
        // THIS CALLBACK GETS INVOKED WHEN APP IS IN THE BACKGROUND AND OPENED FROM NOTIFICATION
        print("onResume: $message");
        _serialiseAndNavigate(message);
        // _navigateToItemDetail(message);
      },
    );

    _messaging.onTokenRefresh.listen(sendTokenToServer);

//    initPlatformState();
    animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 800),
    )..addListener(() => setState(() {}));

    animationController1 =
        AnimationController(vsync: this, duration: Duration(seconds: 2));

    animation1 = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(animationController1);

//    animation = Tween<Offset>(begin: Offset(-0.4, 0.4), end: Offset(0.0, 2.8)).animate(
//        animationController);

    animation = Tween<Offset>(begin: Offset(0.0, 0.2), end: Offset(0.0, -1.2))
        .animate(animationController)
          ..addStatusListener((AnimationStatus status) {
            if (status == AnimationStatus.completed)
              setState(() {
                _showLoader = true;
              });
            animationController1.forward();
            startTimer();
          });
    new Future.delayed(Duration(milliseconds: 800), () {
      animationController.forward();
    });
  }

  void _serialiseAndNavigate(Map<String, dynamic> message) {
    if (Platform.isIOS) {
      var notificationData;
      notificationView = notificationData['view'];
    } else {
      var notificationData = message['data'];
      notificationView = notificationData['view'];
    }
  }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    final String pagechooser = message['view'];
    Navigator.pushNamed(context, pagechooser);
  }

  void startTimer() {
    Timer(Duration(seconds: 2), () => {isInternectConnected()});
  }

  @override
  void dispose() {
    super.dispose();
  }

  void isInternectConnected() async {
    bool isConnected = await _checkInternetConnectivityWithServer();
    setState(() {
      isNetworkConnected = isConnected;
    });
    if (isConnected == true) {
      initPlatformState();
      Timer(Duration(seconds: 2), () => {navigateToSubPage(context)});
      Timer(Duration(seconds: 2), () => {navigateToSubPage(context)});
    }
  }

  Future<bool> _checkInternetConnectivityWithServer() async {
    String url =
        "${Log.API_STACK_MYRIAD}getQuestionsModuleWise?q_id=1&module=PretestMyriad&type=module&authtoken=tSN4DRaEQ5";

    Log.printLog("API URL", url);

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.post(url);
        var jsonData = json.decode(data.body);
        int code = data.statusCode;
        Log.printLog("API RESPONSE", jsonData.toString());
        print("The code is $code");
        bool isconnected = false;
        if (code == 200) {
          setState(() {
            isconnected = true;
          });
        } else {
          setState(() {
            isconnected = false;
          });
        }

        return isconnected;
      }
    } on SocketException catch (_) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    /*SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: statusBarColor,
        statusBarIconBrightness: Brightness.dark));*/
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: headerColor));

    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            color: headerColor,
          ),
          Center(
            child: SlideTransition(
              position: animation,
              child: Container(
                height: 80,
                child: Image.asset("images/back_logo.9.png",
                    height: 80,
                    width: MediaQuery.of(context).size.width / 2,
                    fit: BoxFit.scaleDown,
                    alignment: Alignment.center),
              ),
            ),
          ),
          Visibility(
            visible: _showLoader,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                isNetworkConnected
                    ? Container(
                        margin: EdgeInsets.only(top: 15.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Loading",
                              style: splashTextStyleBrandingBoldSmall,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            SizedBox(
                              height: 15,
                              width: 15,
                              child: CircularProgressIndicator(
                                strokeWidth: 2.0,
                                backgroundColor: Colors.white,
                              ),
                            )
                          ],
                        ))
                    : Container(
                        // color: Colors.redAccent,
                        margin:
                            EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),

                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              child: Text(
                                'Oops, we are experiencing connectivity issues right now. Please try again in sometime.',
                                style: splashTextStyleBrandingBoldSmall,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 15.0),
                              height: buttonHeight,
                              width: ScreenUtil.screenWidthDp,
                              decoration: BoxDecoration(
                                  borderRadius: new BorderRadius.circular(5.0)),
                              child: RaisedButton(
                                color: splashTryButtonColor,
                                onPressed: () {
                                  setState(() {
                                    isNetworkConnected = true;
                                  });
//                            isInternectConnected();
                                  startTimer();
                                },
                                child: Text(
                                  "Retry",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'BrandingMedium',
                                      fontSize: buttonTextSize),
                                ),
                              ),
                            ),
//                     isRetryPressed ?
//                         Container(
//                           margin: EdgeInsets.only(top: 15.0),
//                           height: 40,
//                           width: 40,
//                           child: CircularProgressIndicator(
//                             //backgroundColor: splashTryButtonColor,
//                           ),
//
//                         ) :
//                     Text('',style: TextStyle(color: Colors.white),)
                          ],
                        ),
                      ),
              ],
            ),
          )
        ],
      ),
    );
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  initPlatformState() async {
    if (_type == UniLinksType.string) {
      await initPlatformStateForStringUniLinks();
    } else {
      await initPlatformStateForUriUniLinks();
    }
  }

  /// An implementation using a [String] link
  initPlatformStateForStringUniLinks() async {
    // Attach a listener to the links stream
    _sub = getLinksStream().listen((String link) {
      if (!mounted) return;
      setState(() {
        _latestLink = link ?? 'Unknown';
        _latestUri = null;
        try {
          if (link != null) {
            _latestUri = Uri.parse(link);
            isDeepLink = true;
          } else {
            isDeepLink = false;
          }
        } on FormatException {}
      });
    }, onError: (err) {
      if (!mounted) return;
      setState(() {
        _latestLink = 'Failed to get latest link: $err.';
        _latestUri = null;
        isDeepLink = false;
      });
    });

    // Attach a second listener to the stream
    getLinksStream().listen((String link) {
      print('got link: $link');
    }, onError: (err) {
      print('got err: $err');
    });

    // Get the latest link
    String initialLink;
    Uri initialUri;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      initialLink = await getInitialLink();
      print('initial link: $initialLink');
      if (initialLink != null) {
        isDeepLink = true;
        initialUri = Uri.parse(initialLink);
      } else {
        isDeepLink = false;
      }
    } on PlatformException {
      initialLink = 'Failed to get initial link.';
      initialUri = null;
      isDeepLink = false;
    } on FormatException {
      initialLink = 'Failed to parse the initial link as Uri.';
      initialUri = null;
      isDeepLink = false;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _latestLink = initialLink;
      _latestUri = initialUri;
    });
  }

  /// An implementation using the [Uri] convenience helpers
  initPlatformStateForUriUniLinks() async {
    // Attach a listener to the Uri links stream
    _sub = getUriLinksStream().listen((Uri uri) {
      if (!mounted) return;
      setState(() {
        _latestUri = uri;
        _latestLink = uri?.toString() ?? 'Unknown';
      });
    }, onError: (err) {
      if (!mounted) return;
      setState(() {
        _latestUri = null;
        _latestLink = 'Failed to get latest link: $err.';
      });
    });

    // Attach a second listener to the stream
    getUriLinksStream().listen((Uri uri) {
      print('got uri: ${uri?.path} ${uri?.queryParametersAll}');
    }, onError: (err) {
      print('got err: $err');
    });

    // Get the latest Uri
    Uri initialUri;
    String initialLink;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      initialUri = await getInitialUri();
      print('initial uri: ${initialUri?.path}'
          ' ${initialUri?.queryParametersAll}');
      initialLink = initialUri?.toString();
    } on PlatformException {
      initialUri = null;
      initialLink = 'Failed to get initial uri.';
    } on FormatException {
      initialUri = null;
      initialLink = 'Bad parse the initial link as Uri.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _latestUri = initialUri;
      _latestLink = initialLink;
      final queryParams = _latestUri?.queryParametersAll?.entries?.toList();
    });
  }

  void sendTokenToServer(String newToken) {}

  void addNotificationMessage(Map<String, dynamic> message) async {
    var notification = message['notification'];
    var title = notification['title'];
    var body = notification['body'];
    var data = message['data'];
    var view = data['view'];
    var type = data['type'];
    var imageUrl = data['image_url'];

    NotificationModel model = new NotificationModel(
        title: title,
        message: body,
        imageUrl: imageUrl,
        view: view,
        type: type,
        isRead: false);
    String json = jsonEncode(model);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> notifications = prefs.getStringList(PREF_NOTIFICATION_DATA);
    if (notifications == null) {
      notifications = new List<String>();
    }
    notifications.insert(0, json);
    prefs.setStringList(PREF_NOTIFICATION_DATA, notifications);
  }
}

Future navigateToSubPage(context) async {
  var notificationView;
  if (notificationView != null) {
    if (notificationView == 'counselor_connect') {
      Navigator.of(context).pushNamedAndRemoveUntil(
          RouteCounselorConnect, (_) => false,
          arguments: 16.0);
    } else if (notificationView == 'hcs') {
      Navigator.of(context)
          .pushNamedAndRemoveUntil(RouteHomePage, (_) => false);
    }
  } else if (isDeepLink == false) {
    if (context != null)
      Navigator.of(context)
          .pushNamedAndRemoveUntil(RouteWelcomeDialog, (_) => false);
  } else {
    if (context != null)
      Navigator.of(context)
          .pushNamedAndRemoveUntil(RouteHomePage, (_) => false);
  }
}
