import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ai/Animations/ListItemAnimator.dart';
import 'package:ai/ChatMessage.dart';
import 'package:ai/Constant.dart';
import 'package:ai/MessageType.dart';
import 'package:ai/PersonalDetailModel.dart';
import 'package:ai/helper/Dialogs.dart';
import 'package:ai/model/CancerRelativeDetail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'Log.dart';
import 'SizeConfig.dart';

var _nextQuestionID = 1;
var _currentQuestionID = 1;
String _cancerTypeValue = "";
final GlobalKey<NavigatorState> navigatorKey =
GlobalKey(debugLabel: "Main Navigator");

class OptraHCS extends StatefulWidget {
  double chatTextSize = 16.0;
  double buttonTextSize = 15.0;
  double formFieldTextSize = 15.0;
  _StateHCSHomeScreen myAppState = new _StateHCSHomeScreen();


  TextStyle optionButtonStyle;

  OptraHCS({this.chatTextSize});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _StateHCSHomeScreen();
  }

  void updateFont(double changingFontSize) {
    chatTextSize = changingFontSize;
    myAppState.updateFont(changingFontSize);
  }
}

class OptionsModel {
  int eqm_id;
  String question;
  String more_info;
  String commnets;
  bool is_descriptive;
  int inserted_timestamp;
  int eom_id;
  int question_id;
  String option;
  int nextq_id;
  int nextq_id2;
  int redirect_to;
  String btn_id;

  OptionsModel(
      {this.eqm_id,
      this.question,
      this.more_info,
      this.commnets,
      this.is_descriptive,
      this.inserted_timestamp,
      this.eom_id,
      this.question_id,
      this.option,
      this.nextq_id,
      this.nextq_id2,
      this.redirect_to,
      this.btn_id});
}

class _StateHCSHomeScreen extends State<OptraHCS>
    with SingleTickerProviderStateMixin {
  FocusNode _focus = new FocusNode();
  final ScrollController _scrollController = new ScrollController();
  final List<ChatMessage> _list = <ChatMessage>[];
  final List<ChatMessage> _staticData = <ChatMessage>[];
  List<OptionsModel> _items = new List<OptionsModel>();
  OptionsModel _model6;
  OptionsModel _model7;
  bool _showPersonalDetailsForm = false;
  bool _showOptions = false;
  bool _show27 = false;
  bool _show13 = false;
  bool _show17 = false;
  bool _show26 = false;
  bool _show31 = false;
  bool _show32 = false;
  bool _show35 = false;
  bool _show36 = false;
  bool _show19 = false;
  bool _show7 = false;
  bool _show21 = false;
  bool _show6 = false;
  bool _show2 = false;
  bool _show28 = false;
  bool _show43 = false;
  bool _show40 = false;

  var _showAgeText = false;

  List<CancerTypes> _cancerTypes = [];

  static final validateName = RegExp(r'^[a-zA-Z_ ]+$');

//  static final validateName = RegExp('/^[A-Za-z][A-Za-z\'\-]+([\ A-Za-z][A-Za-z\'\-]+)*/');

  String _selectedAge = "Select age";
  String _selectedGender = "Select gender";
  String _selectedGenderForRelative = "Select gender";
  String _selectedCancerType = "Select cancer";
  String _selectedEthnicity = "Select ethnicity";

  int _userAge;
  bool _userGender;

  final TextEditingController _tec_Name = new TextEditingController();
  final TextEditingController _tec_relative_age = new TextEditingController();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  var _tagSpacing = 30.0;

  List<Item> cancerData;

  var _columnCount = null;
  var _shouldSymmetry = false;

  int _readSpeed = Log.normal;

  int _intPersonalDetailFormPosition = 0;

  var buttonHeight = 40.0;

  int _intPersonalDetailBreastFormPosition = 0;

  void displayAlert(String title, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("$title"),
        content: new Text("\n$message"),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text(
                "Close",
                style: TextStyle(color: alertBtnClr),
              ),
              onPressed: () {
                Navigator.pop(context, 'Cancel');
              })
        ],
      ),
    );
  }

  void updateFont(double font) {
    setState(() {
      widget.chatTextSize = font;
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      print("PRETEST - SCREEN RESUMED");
    }
  }

  Future<double> updateFontSizeFromPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print(
        'FONT SIZE IS : ${prefs.containsKey(Log.sp_fontSize) ? prefs.getDouble(Log.sp_fontSize) : 0}');
    return prefs.containsKey(Log.sp_fontSize)
        ? prefs.getDouble(Log.sp_fontSize)
        : widget.chatTextSize;
  }

  void displayAlertWithActionLable(
      String title, String message, String actionLable) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("$title"),
        content: new Text("\n$message"),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text(
                actionLable,
                style: TextStyle(color: alertBtnClr),
              ),
              onPressed: () {
                Navigator.pop(context, 'Cancel');
              })
        ],
      ),
    );
  }

  void displayRetry(String title, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("$title"),
        content: new Text("\n$message"),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text("Retry"),
              onPressed: () {
                Navigator.pop(context, 'Cancel');
                if (_currentQuestionID == 1) {
                  _loadData();
                } else {
                  _loadNext();
                }
              })
        ],
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState

    _focus.addListener(_onFocusChange);
    /* Model().getItems().then((items){
      _items = items;
    });*/
    _loadData();

    super.initState();
  }

  Future<String> _loadData() async {
    String url =
        "${Log.API_STACK}getQuestionsModuleWise?q_id=1&module=HCS&type=module&authtoken=tSN4DRaEQ5";

    Log.printLog("API URL", url);

    try {
      setState(() {
        _list.add(ChatMessage(
            isEditable: false,
            textFontSize: widget.chatTextSize,
            type: MessageType.LOADER,
            optionList: null,
            backgroundType: 0));
      });

      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.post(url);
        var jsonData = json.decode(data.body);

        setState(() {
          if (_list.last.type == MessageType.LOADER) {
            _list.removeLast();
          }
        });

        Log.printLog("API RESPONSE", jsonData.toString());

        var jsonObject = jsonData['data'][0];
        Log.printLog("QUESTION : ", "${jsonObject['question_mobile']}");
        Log.printLog("NXT Q ID : ", "${jsonObject['nextq_id']}");
        Log.printLog("CUR Q ID : ", "${jsonObject['question_id']}");
        setState(() {
          _nextQuestionID = jsonObject['nextq_id'] as int;
          _currentQuestionID = jsonObject['question_id'] as int;
        });
        String question = jsonObject['question_mobile'];

        List<String> arrList = question.split(r"$$ ");
        Log.printLog("MY DATA", " $arrList");

        for (int index = 0; index < arrList.length; index++) {
          if (index == 0) {
            _staticData.add(ChatMessage(
              isEditable: false,
              textFontSize: widget.chatTextSize,
              text: arrList[index],
              type: MessageType.LEFT,
              optionList: null,
              backgroundType: _list.length > 0 ? 0 : -1,
            ));
          } else if (index == arrList.length - 1) {
            _staticData.add(ChatMessage(
              isEditable: false,
              textFontSize: widget.chatTextSize,
              text: arrList[index],
              type: MessageType.LEFT,
              optionList: null,
              backgroundType: 2,
            ));
          } else {
            _staticData.add(ChatMessage(
              isEditable: false,
              textFontSize: widget.chatTextSize,
              text: arrList[index],
              type: MessageType.LEFT,
              optionList: null,
              backgroundType: 1,
            ));
          }
        }
        showStaticMessages();
      }
    } on SocketException catch (_) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
      setState(() {
        _list.removeLast();
        /*  _list.add(ChatMessage(isEditable: false,textFontSize: widget.textFontSize,
            text: "Please check your internet connection and try again.",
            type: "ERROR",
            optionList: null,backgroundType: 0));*/
      });
      displayRetry("Error", "Please check internet connection and try again.");
    }
  }

  Future<String> _loadNext() async {
    hideAll();
    String questionID = "";
    new Future.delayed(Duration(milliseconds: Log.LIST_ITEM_ANIM_DURATION * 2),
        () async {
      if (_nextQuestionID == 0) {
        questionID = _cancerTypeValue;
      } else {
        questionID = _nextQuestionID.toString();
      }

      String url =
          "${Log.API_STACK}getQuestionsModuleWise?q_id=$questionID&module=HCS&type=module&authtoken=tSN4DRaEQ5";
      Log.printLog("API URL", url);
      try {
        setState(() {
          _list.add(ChatMessage(
              isEditable: false,
              textFontSize: widget.chatTextSize,
              type: MessageType.LOADER,
              optionList: null,
              backgroundType: 0));
        });
        scrollList();

        final result = await InternetAddress.lookup('google.com');
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
          var data = await http.post(url);
          var jsonData = json.decode(data.body);

          setState(() {
            if (_list.last.type == MessageType.LOADER) {
              _list.removeLast();
            }
          });

          Log.printLog("API RESPONSE", jsonData.toString());
          var jsonResponse = jsonData['data'];

          if (jsonResponse.length > 1) {
            var jsonObject = jsonData['data'][0];
            Log.printLog("QUESTION : ", "${jsonObject['question_mobile']}");
            Log.printLog("NXT Q ID : ", "${jsonObject['nextq_id']}");
            Log.printLog("CUR Q ID : ", "${jsonObject['question_id']}");
            setState(() {
              _nextQuestionID = jsonObject['nextq_id'] as int;
              _currentQuestionID = jsonObject['question_id'] as int;
            });
            if (_currentQuestionID == 37 || _currentQuestionID == 20) {
              String question = jsonObject['question_mobile'];

              List<String> arrList = question.split(r"$$ ");
              Log.printLog("MY DATA", " $arrList");

              _staticData.add(ChatMessage(
                  isEditable: false,
                  textFontSize: widget.chatTextSize,
                  text: arrList[0],
                  type: MessageType.LEFT,
                  optionList: null,
                  backgroundType: 0));

              _items.clear();

              var jsonData = jsonResponse[0];
              _items.add(OptionsModel(
                eqm_id: jsonData['eqm_id'] as int,
                question: jsonData['question_mobile'] as String,
                more_info: jsonData['more_info_mobile'] as String,
                commnets: jsonData['commnets'] as String,
                is_descriptive: jsonData['is_descriptive'] as bool,
                inserted_timestamp: jsonData['inserted_timestamp'] as int,
                eom_id: jsonData['eom_id'] as int,
                question_id: jsonData['question_id'] as int,
                option: "Order Test",
                nextq_id: jsonData['nextq_id'] as int,
                nextq_id2: jsonData['nextq_id2'] as int,
                redirect_to: jsonData['redirect_to'] as int,
                btn_id: jsonData['btn_id'] as String,
              ));

              _items.add(OptionsModel(
                eqm_id: jsonData['eqm_id'] as int,
                question: jsonData['question_mobile'] as String,
                more_info: jsonData['more_info_mobile'] as String,
                commnets: jsonData['commnets'] as String,
                is_descriptive: jsonData['is_descriptive'] as bool,
                inserted_timestamp: jsonData['inserted_timestamp'] as int,
                eom_id: jsonData['eom_id'] as int,
                question_id: jsonData['question_id'] as int,
                option: "Learn more about HCS",
                nextq_id: jsonData['nextq_id'] as int,
                nextq_id2: jsonData['nextq_id2'] as int,
                redirect_to: jsonData['redirect_to'] as int,
                btn_id: jsonData['btn_id'] as String,
              ));

              showStaticMessages();
            } else if (_currentQuestionID == 8) {
              _items.clear();

              Log.printLog("MODEL 6 :", _model6.option);
              Log.printLog("MODEL 7 :", _model7.option);

              if ((_model6.option == "More than 10 polyps") &&
                  (_model7.option == "No" || _model7.option == "Not sure")) {
                Log.printLog("PRASAD", "NOT SURE NOT SURE");

                // Show option : Order Test & Lern more about HCS
                _staticData.add(ChatMessage(
                    isEditable: false,
                    textFontSize: widget.chatTextSize,
                    text:
                        "Based on the information you have provided genetic testing for colorectal cancer might be beneficial. You may discuss with genetic counselor by scheduling appointment to discuss your proactive testing options and take chanrge of your health.",
                    type: MessageType.LEFT,
                    optionList: null,
                    backgroundType: 0));

                var jsonData = jsonResponse[0];
                _items.add(OptionsModel(
                  eqm_id: jsonData['eqm_id'] as int,
                  question: jsonData['question_mobile'] as String,
                  more_info: jsonData['more_info_mobile'] as String,
                  commnets: jsonData['commnets'] as String,
                  is_descriptive: jsonData['is_descriptive'] as bool,
                  inserted_timestamp: jsonData['inserted_timestamp'] as int,
                  eom_id: jsonData['eom_id'] as int,
                  question_id: jsonData['question_id'] as int,
                  option: "Order Test",
                  nextq_id: jsonData['nextq_id'] as int,
                  nextq_id2: jsonData['nextq_id2'] as int,
                  redirect_to: jsonData['redirect_to'] as int,
                  btn_id: jsonData['btn_id'] as String,
                ));

                _items.add(OptionsModel(
                  eqm_id: jsonData['eqm_id'] as int,
                  question: jsonData['question_mobile'] as String,
                  more_info: jsonData['more_info_mobile'] as String,
                  commnets: jsonData['commnets'] as String,
                  is_descriptive: jsonData['is_descriptive'] as bool,
                  inserted_timestamp: jsonData['inserted_timestamp'] as int,
                  eom_id: jsonData['eom_id'] as int,
                  question_id: jsonData['question_id'] as int,
                  option: "Learn more about HCS",
                  nextq_id: jsonData['nextq_id'] as int,
                  nextq_id2: jsonData['nextq_id2'] as int,
                  redirect_to: jsonData['redirect_to'] as int,
                  btn_id: jsonData['btn_id'] as String,
                ));

                setState(() {
                  _shouldSymmetry = true;
                  _columnCount = 0;
                });
              } else if ((_model7.option == "Yes") &&
                  (_model6.option == "Less than 10 polyps" ||
                      _model6.option == "None" ||
                      _model6.option == "Not sure")) {
                // Show option : Order Test & Lern more about HCS
                _staticData.add(ChatMessage(
                    isEditable: false,
                    textFontSize: widget.chatTextSize,
                    text:
                        "Based on the information you have provided, proactive testing for cardiac diseases could be beneficial, You may discuss with genetic counselor by scheduling appointment to discuss your proactive testing options and take charge of your health.",
                    type: MessageType.LEFT,
                    optionList: null,
                    backgroundType: 0));

                var jsonData = jsonResponse[0];
                _items.add(OptionsModel(
                  eqm_id: jsonData['eqm_id'] as int,
                  question: jsonData['question_mobile'] as String,
                  more_info: jsonData['more_info_mobile'] as String,
                  commnets: jsonData['commnets'] as String,
                  is_descriptive: jsonData['is_descriptive'] as bool,
                  inserted_timestamp: jsonData['inserted_timestamp'] as int,
                  eom_id: jsonData['eom_id'] as int,
                  question_id: jsonData['question_id'] as int,
                  option: "Order Test",
                  nextq_id: jsonData['nextq_id'] as int,
                  nextq_id2: jsonData['nextq_id2'] as int,
                  redirect_to: jsonData['redirect_to'] as int,
                  btn_id: jsonData['btn_id'] as String,
                ));

                _items.add(OptionsModel(
                  eqm_id: jsonData['eqm_id'] as int,
                  question: jsonData['question_mobile'] as String,
                  more_info: jsonData['more_info_mobile'] as String,
                  commnets: jsonData['commnets'] as String,
                  is_descriptive: jsonData['is_descriptive'] as bool,
                  inserted_timestamp: jsonData['inserted_timestamp'] as int,
                  eom_id: jsonData['eom_id'] as int,
                  question_id: jsonData['question_id'] as int,
                  option: "Learn more about HCS",
                  nextq_id: jsonData['nextq_id'] as int,
                  nextq_id2: jsonData['nextq_id2'] as int,
                  redirect_to: jsonData['redirect_to'] as int,
                  btn_id: jsonData['btn_id'] as String,
                ));

                setState(() {
                  _shouldSymmetry = true;
                  _columnCount = 0;
                });
              } else if ((_model6.option == "More than 10 polyps") &&
                  (_model7.option == "Yes")) {
                // Show option : Order Test & Lern more about HCS
                _staticData.add(ChatMessage(
                    isEditable: false,
                    textFontSize: widget.chatTextSize,
                    text:
                        "Based on the information you have provided, proactive testing for colorectal cancer and cardiac diseases might help, You may discuss with genetic counselor by scheduling appointment to discuss your proactive testing options and take charge of your health.",
                    type: MessageType.LEFT,
                    optionList: null,
                    backgroundType: 0));

                var jsonData = jsonResponse[0];
                _items.add(OptionsModel(
                  eqm_id: jsonData['eqm_id'] as int,
                  question: jsonData['question_mobile'] as String,
                  more_info: jsonData['more_info_mobile'] as String,
                  commnets: jsonData['commnets'] as String,
                  is_descriptive: jsonData['is_descriptive'] as bool,
                  inserted_timestamp: jsonData['inserted_timestamp'] as int,
                  eom_id: jsonData['eom_id'] as int,
                  question_id: jsonData['question_id'] as int,
                  option: "Order Test",
                  nextq_id: jsonData['nextq_id'] as int,
                  nextq_id2: jsonData['nextq_id2'] as int,
                  redirect_to: jsonData['redirect_to'] as int,
                  btn_id: jsonData['btn_id'] as String,
                ));

                _items.add(OptionsModel(
                  eqm_id: jsonData['eqm_id'] as int,
                  question: jsonData['question_mobile'] as String,
                  more_info: jsonData['more_info_mobile'] as String,
                  commnets: jsonData['commnets'] as String,
                  is_descriptive: jsonData['is_descriptive'] as bool,
                  inserted_timestamp: jsonData['inserted_timestamp'] as int,
                  eom_id: jsonData['eom_id'] as int,
                  question_id: jsonData['question_id'] as int,
                  option: "Learn more about HCS",
                  nextq_id: jsonData['nextq_id'] as int,
                  nextq_id2: jsonData['nextq_id2'] as int,
                  redirect_to: jsonData['redirect_to'] as int,
                  btn_id: jsonData['btn_id'] as String,
                ));

                setState(() {
                  _shouldSymmetry = true;
                  _columnCount = 0;
                });
              } else if ((_model6.option == "Less than 10 polyps" ||
                      _model6.option == "None" ||
                      _model6.option == "Not sure") &&
                  (_model7.option == "No" || _model7.option == "Not sure")) {
                // Show option : Talk to genetic counselor & Lern more about HCS
                _staticData.add(ChatMessage(
                    isEditable: false,
                    textFontSize: widget.chatTextSize,
                    text:
                        "Based on the information you have provided further evaluation is required to see if genetic test will be beneficial to you. You can talk to our genetic counselor to know if your cancer can be hereditary.",
                    type: MessageType.LEFT,
                    optionList: null,
                    backgroundType: 0));

                var jsonData = jsonResponse[0];
                _items.add(OptionsModel(
                  eqm_id: jsonData['eqm_id'] as int,
                  question: jsonData['question_mobile'] as String,
                  more_info: jsonData['more_info_mobile'] as String,
                  commnets: jsonData['commnets'] as String,
                  is_descriptive: jsonData['is_descriptive'] as bool,
                  inserted_timestamp: jsonData['inserted_timestamp'] as int,
                  eom_id: jsonData['eom_id'] as int,
                  question_id: jsonData['question_id'] as int,
                  option: "Talk to genetic counselor",
                  nextq_id: jsonData['nextq_id'] as int,
                  nextq_id2: jsonData['nextq_id2'] as int,
                  redirect_to: jsonData['redirect_to'] as int,
                  btn_id: jsonData['btn_id'] as String,
                ));

                _items.add(OptionsModel(
                  eqm_id: jsonData['eqm_id'] as int,
                  question: jsonData['question_mobile'] as String,
                  more_info: jsonData['more_info_mobile'] as String,
                  commnets: jsonData['commnets'] as String,
                  is_descriptive: jsonData['is_descriptive'] as bool,
                  inserted_timestamp: jsonData['inserted_timestamp'] as int,
                  eom_id: jsonData['eom_id'] as int,
                  question_id: jsonData['question_id'] as int,
                  option: "Learn more about HCS",
                  nextq_id: jsonData['nextq_id'] as int,
                  nextq_id2: jsonData['nextq_id2'] as int,
                  redirect_to: jsonData['redirect_to'] as int,
                  btn_id: jsonData['btn_id'] as String,
                ));
              }

              setState(() {
                _shouldSymmetry = true;
                _columnCount = 0;
              });

              showStaticMessages();
            } else if (_currentQuestionID == 19) {
              _items.clear();
              for (int index = 0; index < jsonResponse.length; index++) {
                var jsonData = jsonResponse[index];
                _items.add(OptionsModel(
                  eqm_id: jsonData['eqm_id'] as int,
                  question: jsonData['question_mobile'] as String,
                  more_info: jsonData['more_info_mobile'] as String,
                  commnets: jsonData['commnets'] as String,
                  is_descriptive: jsonData['is_descriptive'] as bool,
                  inserted_timestamp: jsonData['inserted_timestamp'] as int,
                  eom_id: jsonData['eom_id'] as int,
                  question_id: jsonData['question_id'] as int,
                  option: jsonData['option'] as String,
                  nextq_id: jsonData['nextq_id'] as int,
                  nextq_id2: jsonData['nextq_id2'] as int,
                  redirect_to: jsonData['redirect_to'] as int,
                  btn_id: jsonData['btn_id'] as String,
                ));
              }

              var apiData = jsonResponse[0];
              String question = apiData['question_mobile'];
              List<String> answer = question.split(r" @@ ");
              Log.printLog("Prasanna LOG", " LIST OF MSG : $answer");
              String message = "";
              _userAge = Log.getUserAge(_selectedAge);
              if (_userAge == 2) {
                message = answer[1];
              } else {
                message = answer[0];
              }
              _staticData.add(ChatMessage(
                  isEditable: false,
                  textFontSize: widget.chatTextSize,
                  text: message,
                  type: MessageType.LEFT,
                  optionList: null,
                  backgroundType: 0));

              showStaticMessages();
            } else if (_currentQuestionID == 43) {
              _items.clear();
              for (int index = 0; index < jsonResponse.length; index++) {
                var jsonData = jsonResponse[index];
                _items.add(OptionsModel(
                  eqm_id: jsonData['eqm_id'] as int,
                  question: jsonData['question_mobile'] as String,
                  more_info: jsonData['more_info_mobile'] as String,
                  commnets: jsonData['commnets'] as String,
                  is_descriptive: jsonData['is_descriptive'] as bool,
                  inserted_timestamp: jsonData['inserted_timestamp'] as int,
                  eom_id: jsonData['eom_id'] as int,
                  question_id: jsonData['question_id'] as int,
                  option: jsonData['option'] as String,
                  nextq_id: jsonData['nextq_id'] as int,
                  nextq_id2: jsonData['nextq_id2'] as int,
                  redirect_to: jsonData['redirect_to'] as int,
                  btn_id: jsonData['btn_id'] as String,
                ));
              }

              _staticData.add(ChatMessage(
                  isEditable: false,
                  textFontSize: widget.chatTextSize,
                  text:
                      "Was your ${_selectedCancerType} diagnosed before 45 years of age?",
                  type: MessageType.LEFT,
                  optionList: null,
                  backgroundType: 0));

              showStaticMessages();
            } else if (_currentQuestionID == 44) {
              _items.clear();
              for (int index = 0; index < jsonResponse.length; index++) {
                var jsonData = jsonResponse[index];
                _items.add(OptionsModel(
                  eqm_id: jsonData['eqm_id'] as int,
                  question: jsonData['question_mobile'] as String,
                  more_info: jsonData['more_info_mobile'] as String,
                  commnets: jsonData['commnets'] as String,
                  is_descriptive: jsonData['is_descriptive'] as bool,
                  inserted_timestamp: jsonData['inserted_timestamp'] as int,
                  eom_id: jsonData['eom_id'] as int,
                  question_id: jsonData['question_id'] as int,
                  option: jsonData['option'] as String,
                  nextq_id: jsonData['nextq_id'] as int,
                  nextq_id2: jsonData['nextq_id2'] as int,
                  redirect_to: jsonData['redirect_to'] as int,
                  btn_id: jsonData['btn_id'] as String,
                ));
              }

              _staticData.add(ChatMessage(
                  isEditable: false,
                  textFontSize: widget.chatTextSize,
                  text:
                      "Since your ${_selectedCancerType} was diagnosed before the age of 45, you meet the requirements for Hereditary Cancer Screening.",
                  type: MessageType.LEFT,
                  optionList: null,
                  backgroundType: 0));
              setState(() {
                _shouldSymmetry = true;
                _columnCount = 0;
              });
              showStaticMessages();
            } else {
              _items.clear();
              for (int index = 0; index < jsonResponse.length; index++) {
                var jsonData = jsonResponse[index];
                _items.add(OptionsModel(
                  eqm_id: jsonData['eqm_id'] as int,
                  question: jsonData['question_mobile'] as String,
                  more_info: jsonData['more_info_mobile'] as String,
                  commnets: jsonData['commnets'] as String,
                  is_descriptive: jsonData['is_descriptive'] as bool,
                  inserted_timestamp: jsonData['inserted_timestamp'] as int,
                  eom_id: jsonData['eom_id'] as int,
                  question_id: jsonData['question_id'] as int,
                  option: jsonData['option'] as String,
                  nextq_id: jsonData['nextq_id'] as int,
                  nextq_id2: jsonData['nextq_id2'] as int,
                  redirect_to: jsonData['redirect_to'] as int,
                  btn_id: jsonData['btn_id'] as String,
                ));
              }

              setState(() {
                _shouldSymmetry = true;
                _columnCount = 2;
              });

              String question = jsonObject['question_mobile'];

              List<String> arrList = question.split(r"$$ ");
              Log.printLog("MY DATA", " $arrList");

              for (int index = 0; index < arrList.length; index++) {
                if (index == 0) {
                  _staticData.add(ChatMessage(
                      isEditable: false,
                      textFontSize: widget.chatTextSize,
                      backgroundType: _list.length > 0 ? 0 : -1,
                      text: arrList[index],
                      type: MessageType.LEFT,
                      optionList: null));
                } else if (index == arrList.length - 1) {
                  _staticData.add(ChatMessage(
                      isEditable: false,
                      textFontSize: widget.chatTextSize,
                      backgroundType: 2,
                      text: arrList[index],
                      type: MessageType.LEFT,
                      optionList: null));
                } else {
                  _staticData.add(ChatMessage(
                      isEditable: false,
                      textFontSize: widget.chatTextSize,
                      backgroundType: 1,
                      text: arrList[index],
                      type: MessageType.LEFT,
                      optionList: null));
                }
              }
              showStaticMessages();
            }
          } else if (jsonResponse.length > 0 && jsonResponse.length == 1) {
            var jsonObject = jsonData['data'][0];
            Log.printLog("QUESTION : ", "${jsonObject['question_mobile']}");
            Log.printLog("NXT Q ID : ", "${jsonObject['nextq_id']}");
            Log.printLog("CUR Q ID : ", "${jsonObject['question_id']}");
            setState(() {
              _nextQuestionID = jsonObject['nextq_id'] as int;
              _currentQuestionID = jsonObject['question_id'] as int;
            });

            String question = jsonObject['question_mobile'];

            List<String> arrList = question.split(r"$$ ");
            Log.printLog("MY DATA", " $arrList");

            for (int index = 0; index < arrList.length; index++) {
              if (index == 0) {
                _staticData.add(ChatMessage(
                    isEditable: false,
                    textFontSize: widget.chatTextSize,
                    backgroundType: _list.length > 0 ? 0 : -1,
                    text: arrList[index],
                    type: MessageType.LEFT,
                    optionList: null));
              } else if (index == arrList.length - 1) {
                _staticData.add(ChatMessage(
                    isEditable: false,
                    textFontSize: widget.chatTextSize,
                    backgroundType: 2,
                    text: arrList[index],
                    type: MessageType.LEFT,
                    optionList: null));
              } else {
                _staticData.add(ChatMessage(
                    isEditable: false,
                    textFontSize: widget.chatTextSize,
                    backgroundType: 1,
                    text: arrList[index],
                    type: MessageType.LEFT,
                    optionList: null));
              }
            }

            if (_currentQuestionID == 37 || _currentQuestionID == 20) {
              _items.clear();

              var jsonData = jsonResponse[0];
              _items.add(OptionsModel(
                eqm_id: jsonData['eqm_id'] as int,
                question: jsonData['question_mobile'] as String,
                more_info: jsonData['more_info_mobile'] as String,
                commnets: jsonData['commnets'] as String,
                is_descriptive: jsonData['is_descriptive'] as bool,
                inserted_timestamp: jsonData['inserted_timestamp'] as int,
                eom_id: jsonData['eom_id'] as int,
                question_id: jsonData['question_id'] as int,
                option: "Order Test",
                nextq_id: jsonData['nextq_id'] as int,
                nextq_id2: jsonData['nextq_id2'] as int,
                redirect_to: jsonData['redirect_to'] as int,
                btn_id: jsonData['btn_id'] as String,
              ));

              _items.add(OptionsModel(
                eqm_id: jsonData['eqm_id'] as int,
                question: jsonData['question_mobile'] as String,
                more_info: jsonData['more_info_mobile'] as String,
                commnets: jsonData['commnets'] as String,
                is_descriptive: jsonData['is_descriptive'] as bool,
                inserted_timestamp: jsonData['inserted_timestamp'] as int,
                eom_id: jsonData['eom_id'] as int,
                question_id: jsonData['question_id'] as int,
                option: "Learn more about HCS",
                nextq_id: jsonData['nextq_id'] as int,
                nextq_id2: jsonData['nextq_id2'] as int,
                redirect_to: jsonData['redirect_to'] as int,
                btn_id: jsonData['btn_id'] as String,
              ));
            }

            if (_currentQuestionID == 23) {
              _items.clear();
              _items.add(OptionsModel(
                eqm_id: 0,
                question: "",
                more_info: "",
                commnets: "",
                is_descriptive: false,
                inserted_timestamp: 0,
                eom_id: 0,
                question_id: 0,
                option: "Talk to genetic counselor",
                nextq_id: 0,
                nextq_id2: 0,
                redirect_to: 0,
                btn_id: "",
              ));

              _items.add(OptionsModel(
                eqm_id: 0,
                question: "",
                more_info: "",
                commnets: "",
                is_descriptive: false,
                inserted_timestamp: 0,
                eom_id: 0,
                question_id: 0,
                option: "Learn more about HCS",
                nextq_id: 0,
                nextq_id2: 0,
                redirect_to: 0,
                btn_id: "",
              ));
            }
            showStaticMessages();
          }

          if (_currentQuestionID == 25) {
            setState(() {
              _shouldSymmetry = true;
              _columnCount = 0;
            });
          }

          if (_currentQuestionID == 6) {
            setState(() {
              _shouldSymmetry = false;
              _columnCount = null;
            });
          }

          if (_currentQuestionID == 41) {
            setState(() {
              _shouldSymmetry = true;
              _columnCount = 0;
            });
          }

          if (_items.length > 4) {
            setState(() {
              _tagSpacing = 30.0;
            });
          } else {
            setState(() {
              _tagSpacing = 15.0;
            });
          }
        }
      } on SocketException catch (_) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
        setState(() {
          _list.removeLast();
          /*  _list.add(ChatMessage(isEditable: false,textFontSize: widget.textFontSize,
            text: "Please check your internet connection and try again.",
            type: "ERROR",
            optionList: null,backgroundType: 0));*/
        });
        displayRetry(
            "Error", "Please check internet connection and try again.");
      }
    });
  }

  _onFocusChange() {
    debugPrint("TextField Focus: ${_focus.hasFocus}");
  }

  @override
  Widget build(BuildContext context) {
    navigatorKey: navigatorKey;
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);
    widget.optionButtonStyle = TextStyle(
        color: optionTextColor,
        fontFamily: 'BrandingMedium',
        fontSize: ScreenUtil().setSp(widget.buttonTextSize));
    return SafeArea(
      child: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Container(
          //color: Color.fromRGBO(232, 239, 245, 1),
          color: chatScreenBackground,
          child: Column(
            children: <Widget>[
              Flexible(
//            child: Container(
//              child: ListView.builder(
//                controller: _scrollController,
//                padding: new EdgeInsets.all(8.0),
//                itemBuilder: (_, int index) => _list[index],
//                itemCount: _list.length,
//              ),
//            ),
                child: Container(
                  child: ListView.builder(
                    controller: _scrollController,
                    padding: new EdgeInsets.all(8.0),
                    itemBuilder: _getListItem,
                    itemCount: _list.length,
                  ),
                ),
              ),
              Visibility(
                visible: _showPersonalDetailsForm,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02),
                  child: Column(
                    children: <Widget>[
                      Visibility(
                        visible:
                            _intPersonalDetailFormPosition == 0 ? true : false,
                        child: WidgetAnimator(Container(
                          width: ScreenUtil.screenWidthDp,
                          height: buttonHeight,
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              // border: Border.all(color: Colors.blueGrey),
                              border: Border.all(color: myriadUserIconColor),
                              borderRadius: new BorderRadius.circular(5.0)),
                          padding:
                              EdgeInsets.only(left: 10, right: 10, bottom: 0.5),
                          child: TextField(
                            focusNode: _focus,
                            onEditingComplete: _focus.unfocus,
                            textCapitalization: TextCapitalization.words,
                            controller: _tec_Name,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize:
                                  ScreenUtil().setSp(widget.formFieldTextSize),
                            ),
                            textAlignVertical: TextAlignVertical.center,
                            decoration: InputDecoration(
                              hintText: "Your name",
                              hintStyle: TextStyle(
                                  fontSize: ScreenUtil()
                                      .setSp(widget.formFieldTextSize),
                                  color: Color.fromRGBO(153, 153, 153, 1)),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                            ),
                            keyboardType: TextInputType.text,
                          ),
                        )),
                      ),
                      Visibility(
                        visible:
                            _intPersonalDetailFormPosition == 1 ? true : false,
                        child: WidgetAnimator(Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
//                              width: ScreenUtil.screenWidthDp,
                                height: buttonHeight,
                                padding: EdgeInsets.only(right: 5),
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    // border: Border.all(color: Colors.blueGrey),
                                    border:
                                        Border.all(color: myriadUserIconColor),
                                    borderRadius:
                                        new BorderRadius.circular(5.0)),
                                child: DropdownButtonHideUnderline(
                                  child: Listener(
                                    /*  onPointerDown: (_) => FocusScope.of(context)
                                        .unfocus(focusPrevious: false),*/
                                    child: DropdownButton<String>(
                                      items: <String>[
                                        'Below 45',
                                        'Between 45 - 60',
                                        'Above 60'
                                      ].map((String _selectedValue) {
                                        return new DropdownMenuItem<String>(
                                          value: _selectedValue,
                                          child: new Text(
                                            _selectedValue,
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: ScreenUtil().setSp(
                                                  widget.formFieldTextSize),
                                            ),
                                            textAlign: TextAlign.right,
                                          ),
                                        );
                                      }).toList(),
                                      hint: Container(
                                        padding: EdgeInsets.only(left: 10),
                                        child: Text(_selectedAge,
                                            textAlign: TextAlign.right,
                                            style: TextStyle(
                                                fontSize: ScreenUtil().setSp(
                                                    widget.formFieldTextSize),
                                                color:
                                                    _selectedAge == 'Select age'
                                                        ? Colors.grey
                                                        : Colors.black)),
                                      ),
                                      onChanged: (String _selectedValue) {
                                        vibrateDevice();
                                        setState(() {
                                          /*FocusScope.of(context)
                                              .requestFocus(new FocusNode());*/
                                          _selectedAge = _selectedValue;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: Container(
                                //width: ScreenUtil.screenWidthDp,
                                height: buttonHeight,
                                padding: EdgeInsets.only(right: 5),
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    //border: Border.all(color: Colors.blueGrey),
                                    border:
                                        Border.all(color: myriadUserIconColor),
                                    borderRadius:
                                        new BorderRadius.circular(5.0)),
                                child: DropdownButtonHideUnderline(
                                  child: Listener(
                                    /*onPointerDown: (_) => FocusScope.of(context)
                                        .unfocus(focusPrevious: false),*/
                                    child: DropdownButton<String>(
                                      items: <String>['Female', 'Male']
                                          .map((String _selectedValue) {
                                        return new DropdownMenuItem<String>(
                                          value: _selectedValue,
                                          child: new Text(
                                            _selectedValue,
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: ScreenUtil().setSp(
                                                  widget.formFieldTextSize),
                                            ),
                                            textAlign: TextAlign.right,
                                          ),
                                        );
                                      }).toList(),
                                      hint: Container(
                                        padding: EdgeInsets.only(left: 10),
                                        child: Text(
                                          _selectedGender,
                                          textAlign: TextAlign.right,
                                          style: TextStyle(
                                              fontSize: ScreenUtil().setSp(
                                                  widget.formFieldTextSize),
                                              color: _selectedGender ==
                                                      'Select gender'
                                                  ? Colors.grey
                                                  : Colors.black),
                                        ),
                                      ),
                                      onChanged: (String _selectedValue) {
                                        vibrateDevice();
                                        setState(() {
                                          FocusScope.of(context)
                                              .requestFocus(new FocusNode());
                                          _selectedGender = _selectedValue;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        )),
                      ),
                      Visibility(
                        visible:
                            _intPersonalDetailFormPosition == 3 ? true : false,
                        child: WidgetAnimator(Container(
                          width: ScreenUtil.screenWidthDp,
                          height: buttonHeight,
                          padding: EdgeInsets.only(right: 5),
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              // border: Border.all(color: Colors.blueGrey),
                              border: Border.all(color: myriadUserIconColor),
                              borderRadius: new BorderRadius.circular(5.0)),
                          child: DropdownButtonHideUnderline(
                            child: Listener(
                              /* onPointerDown: (_) => FocusScope.of(context)
                                  .unfocus(focusPrevious: false),*/
                              child: DropdownButton<String>(
                                items: <String>[
                                  'Ashkenazi Jewish',
                                  'Asian',
                                  'African American',
                                  'Caucasian',
                                  'Hispanic',
                                  'Others',
                                  'Not Known'
                                ].map((String _selectedValue) {
                                  return new DropdownMenuItem<String>(
                                    value: _selectedValue,
                                    child: new Text(
                                      _selectedValue,
                                      style: TextStyle(
                                          fontSize: ScreenUtil()
                                              .setSp(widget.formFieldTextSize),
                                          color: Colors.black),
                                      textAlign: TextAlign.right,
                                    ),
                                  );
                                }).toList(),
                                hint: Container(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Text(_selectedEthnicity,
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                          fontSize: ScreenUtil()
                                              .setSp(widget.formFieldTextSize),
                                          color: _selectedEthnicity ==
                                                  'Select ethnicity'
                                              ? Colors.grey
                                              : Colors.black)),
                                ),
                                onChanged: (String _selectedValue) {
                                  vibrateDevice();
                                  setState(() {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                    _selectedEthnicity = _selectedValue;
                                  });
                                },
                              ),
                            ),
                          ),
                        )),
                      ),
                      SizedBox(
                        height: ScreenUtil.screenHeightDp * 0.01,
                      ),
                      Container(
                        width: ScreenUtil.screenWidthDp,
                        height: buttonHeight,
                        decoration: BoxDecoration(
                            borderRadius: new BorderRadius.circular(5.0)),
                        child: RaisedButton(
                          //color: Color.fromRGBO(89, 148, 172, 1),
                          color: myriadUserIconColor,
                          onPressed: () {
                            handlePersonalDetailForm();
                          },
                          child: Text(
                              _intPersonalDetailFormPosition == 3
                                  ? "Done"
                                  : "Next",
                              style: widget.optionButtonStyle),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil.screenHeightDp * 0.01,
                      ),
                    ],
                  ),
                ),
              ),
              Visibility(
                visible: _showAgeText,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02),
                  child: AnimatedContainer(
//                padding: EdgeInsets.all(5),
                    duration: Duration(milliseconds: 500),
                    curve: Curves.elasticIn,
                    child: Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Flexible(
                              flex: 1,
                              child: Container(
                                width: ScreenUtil.screenWidthDp,
                                height: buttonHeight,
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    //border: Border.all(color: Colors.blueGrey),
                                    border:
                                        Border.all(color: myriadUserIconColor),
                                    borderRadius:
                                        new BorderRadius.circular(5.0)),
                                padding: EdgeInsets.only(
                                    left: 10, right: 10, bottom: 0.5),
                                child: TextField(
                                  focusNode: _focus,
                                  onEditingComplete: _focus.unfocus,
                                  textCapitalization: TextCapitalization.words,
                                  controller: _tec_Name,
                                  style: TextStyle(
                                      fontSize: ScreenUtil()
                                          .setSp(widget.formFieldTextSize),
                                      color: Colors.black),
                                  decoration: InputDecoration(
                                    hintText: "Your name",
                                    hintStyle: TextStyle(
                                        fontSize: ScreenUtil()
                                            .setSp(widget.formFieldTextSize),
                                        color:
                                            Color.fromRGBO(153, 153, 153, 1)),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                    ),
                                  ),
                                  keyboardType: TextInputType.text,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.001,
                        ),
                        Container(
                          width: ScreenUtil.screenWidthDp,
                          height: buttonHeight,
                          child: RaisedButton(
//                          color: Color.fromRGBO(89, 148, 172, 1),
                            color: myriadUserIconColor,
                            onPressed: () {
                              handlePersonalDetailForm();
                            },
                            child:
                                Text("Next", style: widget.optionButtonStyle),
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.005,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _showOptions,
                child: FractionallySizedBox(
                  widthFactor: 1.0,
                  child: Container(
                    color: chatBotBottomContainerBG,
                    padding: EdgeInsets.only(
                        left: ScreenUtil.screenWidthDp * 0.02,
                        right: ScreenUtil.screenWidthDp * 0.02,
                        top: ScreenUtil.screenWidthDp * 0.01,
                        bottom: ScreenUtil.screenWidthDp * 0.02),
                    child: Tags(
                      spacing: ScreenUtil.screenWidthDp * 0.02,
                      runSpacing: ScreenUtil.screenWidthDp * 0.02,
                      columns: _columnCount,
                      symmetry: _shouldSymmetry,
                      itemCount: _items.length,
                      // required
                      itemBuilder: (int index) {
                        final item = _items[index];
                        return ItemTags(
                          // Each ItemTags must contain a Key. Keys allow Flutter to
                          // uniquely identify widgets.
                          key: Key(index.toString()),
                          index: index,
                          // required
                          title: item.option,
                          pressEnabled: true,
                          singleItem: true,
                          // activeColor: Color.fromRGBO(89, 148, 172, 1),
                          activeColor: myriadUserIconColor,
                          //color: Color.fromRGBO(89, 148, 172, 1),
                          color: myriadUserIconColor,
                          textColor: Colors.white,
                          borderRadius: BorderRadius.circular(2.0),
                          elevation: 2,
                          padding: EdgeInsets.only(
                              left: 15, right: 15, top: 9, bottom: 9),
                          textStyle: widget.optionButtonStyle,
                          onPressed: (item) => {handleOptionsClicked(item)},
                          onLongPressed: (item) => {
                            Log.printLog(
                                "ITEM LONG PRESSED : ", item.toString())
                          },
                        );
                      },
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show27,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.01),
                  child: Column(
                    children: <Widget>[
                      Visibility(
                        visible: _intPersonalDetailBreastFormPosition == 0
                            ? true
                            : false,
                        child: WidgetAnimator(
                          Container(
                            width: ScreenUtil.screenWidthDp,
                            height: buttonHeight,
                            padding: EdgeInsets.only(left: 5, right: 5),
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                //border: Border.all(color: Colors.blueGrey),
                                border: Border.all(color: myriadUserIconColor),
                                borderRadius: new BorderRadius.circular(5.0)),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                items: <String>['Female', 'Male']
                                    .map((String _selectedValue) {
                                  return new DropdownMenuItem<String>(
                                    value: _selectedValue,
                                    child: new Text(
                                      _selectedValue,
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: ScreenUtil()
                                            .setSp(widget.formFieldTextSize),
                                      ),
                                      textAlign: TextAlign.right,
                                    ),
                                  );
                                }).toList(),
                                hint: Text(
                                  _selectedGenderForRelative,
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      fontSize: ScreenUtil()
                                          .setSp(widget.formFieldTextSize),
                                      color: _selectedGenderForRelative ==
                                              'Select gender'
                                          ? Colors.grey
                                          : Colors.black),
                                ),
                                onChanged: (String _selectedValue) {
                                  vibrateDevice();
                                  setState(() {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                    _selectedGenderForRelative = _selectedValue;
                                  });
                                },
                              ),
                            ),
                          ),
                        ),
                      ),
                      Visibility(
                        visible: _intPersonalDetailBreastFormPosition == 1
                            ? true
                            : false,
                        child: WidgetAnimator(
                          Container(
                            height: buttonHeight,
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                //border: Border.all(color: Colors.blueGrey),
                                border: Border.all(color: myriadUserIconColor),
                                borderRadius: new BorderRadius.circular(5.0)),
                            padding: EdgeInsets.only(
                                left: 10, right: 10, bottom: 0.5),
                            child: TextField(
                              controller: _tec_relative_age,
                              style: TextStyle(
                                  fontSize: ScreenUtil()
                                      .setSp(widget.formFieldTextSize),
                                  color: Colors.black),
                              decoration: InputDecoration(
                                hintText: "Relative's Age",
                                hintStyle: TextStyle(
                                    fontSize: ScreenUtil()
                                        .setSp(widget.formFieldTextSize),
                                    color: Color.fromRGBO(153, 153, 153, 1)),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                ),
                              ),
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ),
                      ),
                      Visibility(
                        visible: _intPersonalDetailBreastFormPosition == 2
                            ? true
                            : false,
                        child: WidgetAnimator(
                          Container(
                            width: ScreenUtil.screenWidthDp,
                            height: buttonHeight,
                            padding: EdgeInsets.only(right: 5, left: 5),
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                // border: Border.all(color: Colors.blueGrey),
                                border: Border.all(color: myriadUserIconColor),
                                borderRadius: new BorderRadius.circular(5.0)),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                items: <String>[
                                  'Bilateral breast cancer',
                                  'Two breast primaries',
                                  'Triple negative breast cancer',
                                  'Other'
                                ].map((String _selectedValue) {
                                  return new DropdownMenuItem<String>(
                                    value: _selectedValue,
                                    child: new Text(
                                      _selectedValue,
                                      style: TextStyle(
                                          fontSize: ScreenUtil()
                                              .setSp(widget.formFieldTextSize),
                                          color: Colors.black),
                                      textAlign: TextAlign.right,
                                    ),
                                  );
                                }).toList(),
                                hint: Text(
                                  _selectedCancerType,
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      fontSize: ScreenUtil()
                                          .setSp(widget.formFieldTextSize),
                                      color:
                                          _selectedCancerType == 'Select cancer'
                                              ? Colors.grey
                                              : Colors.black),
                                ),
                                onChanged: (String _selectedValue) {
                                  vibrateDevice();
                                  setState(() {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                    _selectedCancerType = _selectedValue;
                                  });
                                },
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil.screenHeightDp * 0.01,
                      ),
                      Container(
                        width: ScreenUtil.screenWidthDp,
                        height: buttonHeight,
                        decoration: BoxDecoration(
                            borderRadius: new BorderRadius.circular(5.0)),
                        child: RaisedButton(
//                        color: Color.fromRGBO(89, 148, 172, 1),
                          color: myriadUserIconColor,
                          onPressed: () {
                            handle27();
                          },
                          child: Text(
                              _intPersonalDetailBreastFormPosition == 2
                                  ? "Done"
                                  : "Next",
                              style: widget.optionButtonStyle),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil.screenHeightDp * 0.01,
                      ),
                    ],
                  ),
                ),
              ),
              Visibility(
                visible: _show13,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.01),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: ScreenUtil().setHeight(40),
                          width: ScreenUtil.screenWidthDp,
                          padding: EdgeInsets.only(right: 5),
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              //border: Border.all(color: Colors.blueGrey),
                              border: Border.all(color: myriadUserIconColor),
                              borderRadius: new BorderRadius.circular(5.0)),
                          child: DropdownButtonHideUnderline(
                            child: Listener(
                              /*onPointerDown: (_) => FocusScope.of(context)
                                  .unfocus(focusPrevious: false),*/
                              child: DropdownButton<String>(
                                items: _cancerTypes
                                    .map((CancerTypes _selectedValue) {
                                  return new DropdownMenuItem<String>(
                                    value: _selectedValue.name,
                                    child: new Text(
                                      _selectedValue.name,
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: ScreenUtil()
                                            .setSp(widget.formFieldTextSize),
                                      ),
                                      textAlign: TextAlign.right,
                                    ),
                                  );
                                }).toList(),
                                hint: Container(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Text(_selectedCancerType,
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                          fontSize: ScreenUtil()
                                              .setSp(widget.formFieldTextSize),
                                          color: _selectedCancerType ==
                                                  'Select cancer'
                                              ? Colors.grey
                                              : Colors.black)),
                                ),
                                onChanged: (String _selectedValue) {
                                  vibrateDevice();
                                  setState(() {
                                    /*FocusScope.of(context)
                                                .requestFocus(new FocusNode());*/
                                    _selectedCancerType = _selectedValue;
                                  });
                                },
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.010,
                        ),
                        Container(
                          width: ScreenUtil.screenWidthDp,
                          height: buttonHeight,
                          child: RaisedButton(
                            //color: optionColor,
                            color: myriadUserIconColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(Log.radius),
                                  topRight: Radius.circular(Log.radius),
                                  bottomLeft: Radius.circular(Log.radius),
                                  bottomRight: Radius.circular(Log.radius)),
                            ),
                            child: Text(
                              "Continue",
                              style: new TextStyle(
                                  color: optionTextColor,
                                  fontFamily: 'BrandingMedium',
                                  fontSize: ScreenUtil().setSp(14)),
                              textAlign: TextAlign.center,
                            ),
                            onPressed: () => handle13(),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show17,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: buttonHeight,
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              borderRadius: new BorderRadius.circular(5.0)),
                          padding:
                              EdgeInsets.only(left: 10, right: 10, bottom: 0.5),
                          child: TextField(
                            controller: _tec_relative_age,
                            style: TextStyle(
                                fontSize: ScreenUtil()
                                    .setSp(widget.formFieldTextSize),
                                color: Colors.black),
                            decoration: InputDecoration(
                              hintText: "Enter age e.g. 46",
                              hintStyle: TextStyle(
                                  fontSize: ScreenUtil()
                                      .setSp(widget.formFieldTextSize),
                                  color: Color.fromRGBO(153, 153, 153, 1)),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                            ),
                            keyboardType: TextInputType.number,
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.02,
                        ),
                        Container(
                          width: ScreenUtil.screenWidthDp,
                          height: buttonHeight,
                          child: RaisedButton(
                            //color: optionColor,
                            color: myriadUserIconColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(Log.radius),
                                  topRight: Radius.circular(Log.radius),
                                  bottomLeft: Radius.circular(Log.radius),
                                  bottomRight: Radius.circular(Log.radius)),
                            ),
                            child: Text(
                              "Continue",
                              style: new TextStyle(
                                  color: optionTextColor,
                                  fontFamily: 'BrandingMedium',
                                  fontSize: ScreenUtil().setSp(14)),
                              textAlign: TextAlign.center,
                            ),
                            onPressed: () => handle17(),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show26,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.01),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  onPressed: () {
                                    handle26(0);
                                  },
                                  child: Text("Breast",
                                      style: widget.optionButtonStyle),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  // color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  onPressed: () {
                                    handle26(1);
                                  },
                                  child: Text("Ovarian",
                                      style: widget.optionButtonStyle),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  onPressed: () {
                                    handle26(2);
                                  },
                                  child: Text("Pancreatic",
                                      style: widget.optionButtonStyle),
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 2,
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  onPressed: () {
                                    handle26(3);
                                  },
                                  child: Text("Metastatic prostate",
                                      style: widget.optionButtonStyle),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  onPressed: () {
                                    handle26(4);
                                  },
                                  child: Text("None",
                                      style: widget.optionButtonStyle),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              /*Visibility(
                visible: _show31,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: RaisedButton(
                                color: Color.fromRGBO(89, 148, 172, 1),
                                padding: EdgeInsets.only(
                                    top: 5.0, left: 5.0, right: 5.0, bottom: 10.0),
                                onPressed: () {
                                  handle31(0);
                                },
                                child: Text(
                                  "Yes. Diagnosed before age 50",
                                  style: widget.optionButtonStyle),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: RaisedButton(
                                color: Color.fromRGBO(89, 148, 172, 1),
                                padding: EdgeInsets.only(
                                    top: 5.0, left: 5.0, right: 5.0, bottom: 10.0),
                                onPressed: () {
                                  handle31(1);
                                },
                                child: Text(
                                  "Yes. Diagnosed after age 50",
                                  style: widget.optionButtonStyle),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: RaisedButton(
                                color: Color.fromRGBO(89, 148, 172, 1),
                                onPressed: () {
                                  handle31(2);
                                },
                                child: Text("No",
                                    style: widget.optionButtonStyle),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: RaisedButton(
                                color: Color.fromRGBO(89, 148, 172, 1),
                                onPressed: () {
                                  handle31(3);
                                },
                                child: Text("Not sure",
                                    style: widget.optionButtonStyle),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),*/
              Visibility(
                visible: _show31,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.01,
                      top: ScreenUtil.screenWidthDp * 0.01),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: RaisedButton(
                                // color: optionColor,
                                color: myriadUserIconColor,
                                padding: EdgeInsets.only(
                                    top: 10.0,
                                    left: 5.0,
                                    right: 5.0,
                                    bottom: 10.0),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(Log.radius),
                                      topRight: Radius.circular(Log.radius),
                                      bottomLeft: Radius.circular(Log.radius),
                                      bottomRight: Radius.circular(Log.radius)),
                                ),
                                child: Text(
                                  "Yes. Diagnosed before age 50",
                                  style: widget.optionButtonStyle,
                                  textAlign: TextAlign.center,
                                ),
                                onPressed: () {
                                  handle31(0);
                                },
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: RaisedButton(
                                //color: optionColor,
                                color: myriadUserIconColor,
                                padding: EdgeInsets.only(
                                    top: 10.0,
                                    left: 5.0,
                                    right: 5.0,
                                    bottom: 10.0),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(Log.radius),
                                      topRight: Radius.circular(Log.radius),
                                      bottomLeft: Radius.circular(Log.radius),
                                      bottomRight: Radius.circular(Log.radius)),
                                ),
                                child: Text(
                                  "Yes. Diagnosed after age 50",
                                  style: widget.optionButtonStyle,
                                  textAlign: TextAlign.center,
                                ),
                                onPressed: () {
                                  handle31(1);
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.01,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  // color: optionColor,
                                  color: myriadUserIconColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text(
                                    "No",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                  onPressed: () {
                                    handle31(2);
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: optionColor,
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text(
                                    "Not sure",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                  onPressed: () {
                                    handle31(3);
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show32,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.01,
                      bottom: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: optionColor,
                                  color: myriadUserIconColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Yes",
                                      style: widget.optionButtonStyle),
                                  onPressed: () {
                                    handle32(0);
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: optionColor,
                                  color: myriadUserIconColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("No",
                                      style: widget.optionButtonStyle),
                                  onPressed: () {
                                    handle32(1);
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: optionColor,
                                  color: myriadUserIconColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Not sure",
                                      style: widget.optionButtonStyle),
                                  onPressed: () {
                                    handle32(2);
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show19,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.01),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle19(0);
                                  },
                                  child: Text(
                                    "Yes",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  // color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle19(1);
                                  },
                                  child: Text(
                                    "No",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  // color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  onPressed: () {
                                    handle19(2);
                                  },
                                  child: Text("Not sure",
                                      style: widget.optionButtonStyle),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show7,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  // color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle7(0);
                                  },
                                  child: Text(
                                    "Yes",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle7(1);
                                  },
                                  child: Text(
                                    "No",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  onPressed: () {
                                    handle7(2);
                                  },
                                  child: Text("Not sure",
                                      style: widget.optionButtonStyle),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show21,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.01),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  // color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle21(0);
                                  },
                                  child: Text(
                                    "Bilateral breast cancer",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  // color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle21(1);
                                  },
                                  child: Text(
                                    "Two breast primaries",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  onPressed: () {
                                    handle21(2);
                                  },
                                  child: Text("None",
                                      style: widget.optionButtonStyle),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  onPressed: () {
                                    handle21(3);
                                  },
                                  child: Text("Not sure",
                                      style: widget.optionButtonStyle),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show6,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.01),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle6(0);
                                  },
                                  child: Text(
                                    "More than 10 polyps",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle6(1);
                                  },
                                  child: Text(
                                    "Less than 10 polyps",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.01,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  onPressed: () {
                                    handle6(2);
                                  },
                                  child: Text("None",
                                      style: widget.optionButtonStyle),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  // color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  onPressed: () {
                                    handle6(3);
                                  },
                                  child: Text("Not sure",
                                      style: widget.optionButtonStyle),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show28,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.01,
                      top: ScreenUtil.screenWidthDp * 0.01),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle28(0);
                                  },
                                  child: Text(
                                    "Yes",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle28(1);
                                  },
                                  child: Text(
                                    "No",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show2,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.01),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle2(0);
                                  },
                                  child: Text(
                                    "Myself",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle2(1);
                                  },
                                  child: Text(
                                    "Close relative",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.010,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  // color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  onPressed: () {
                                    handle2(2);
                                  },
                                  child: Text("Both",
                                      style: widget.optionButtonStyle),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
//                                color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  onPressed: () {
                                    handle2(3);
                                  },
                                  child: Text("None",
                                      style: widget.optionButtonStyle),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show35,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
//                                color: optionColor,
                                  color: myriadUserIconColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text(
                                    "Less than 7",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                  onPressed: () {
                                    handle35(0);
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  // color: optionColor,
                                  color: myriadUserIconColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text(
                                    "7 and above",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                  onPressed: () {
                                    handle35(1);
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: optionColor,
                                  color: myriadUserIconColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Not sure",
                                      style: widget.optionButtonStyle),
                                  onPressed: () {
                                    handle35(2);
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show36,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.02,
                      top: ScreenUtil.screenWidthDp * 0.02),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: optionColor,
                                  color: myriadUserIconColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Yes",
                                      style: widget.optionButtonStyle),
                                  onPressed: () {
                                    handle36(0);
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: optionColor,
                                  color: myriadUserIconColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("No",
                                      style: widget.optionButtonStyle),
                                  onPressed: () {
                                    handle36(1);
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: optionColor,
                                  color: myriadUserIconColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(Log.radius),
                                        topRight: Radius.circular(Log.radius),
                                        bottomLeft: Radius.circular(Log.radius),
                                        bottomRight:
                                            Radius.circular(Log.radius)),
                                  ),
                                  child: Text("Not sure",
                                      style: widget.optionButtonStyle),
                                  onPressed: () {
                                    handle36(2);
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show43,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.01,
                      top: ScreenUtil.screenWidthDp * 0.01),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle43(0);
                                  },
                                  child: Text(
                                    "Yes",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle43(1);
                                  },
                                  child: Text(
                                    "No",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: _show40,
                child: Container(
                  color: chatBotBottomContainerBG,
                  padding: EdgeInsets.only(
                      left: ScreenUtil.screenWidthDp * 0.02,
                      right: ScreenUtil.screenWidthDp * 0.02,
                      bottom: ScreenUtil.screenWidthDp * 0.01,
                      top: ScreenUtil.screenWidthDp * 0.01),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle40(0);
                                  },
                                  child: Text(
                                    "Yes",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: Container(
                                height: buttonHeight,
                                child: RaisedButton(
                                  //color: Color.fromRGBO(89, 148, 172, 1),
                                  color: myriadUserIconColor,
                                  padding: EdgeInsets.only(
                                      top: 5.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0),
                                  onPressed: () {
                                    handle40(1);
                                  },
                                  child: Text(
                                    "No",
                                    style: widget.optionButtonStyle,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void showStaticMessages() {
    _getReadSpeed();
    if (_list.length > 0) {
      setState(() {
        ChatMessage loader = new ChatMessage(
            isEditable: false,
            textFontSize: widget.chatTextSize,
            text: "",
            type: MessageType.LOADER,
            optionList: null,
            backgroundType: 0);
        _list.add(loader);
      });
      scrollList();
      if (_staticData.length > 0) {
        new Future.delayed(
            Duration(
                milliseconds: Log.getLoaderDelayDuration(
                    _staticData[0].text.toString().length, _readSpeed)), () {
          setState(() {
            _list.removeLast();
            _list.add(_staticData[0]);
          });

          scrollList();
          _staticData.removeAt(0);
          if (_staticData.length == 0) {
            handleUIVisibility();
          } else {
            new Future.delayed(
                Duration(
                    milliseconds: Log.getMessageDelayDuration(
                        _staticData[0].text.toString().length, _readSpeed)),
                () {
              showStaticMessages();
            });
          }
        });
      } else {
        // EXIT AND DO NEXT TASK
        handleUIVisibility();
      }
    } else {
      if (_staticData.length > 0) {
        setState(() {
          _list.add(_staticData[0]);
        });
        scrollList();

        print(
            "DURATION IS : ${Log.getMessageDelayDuration(_list.last.text.toString().length, _readSpeed)}");

        new Future.delayed(
            Duration(
                milliseconds: Log.getMessageDelayDuration(
                    _list.last.text.toString().length, _readSpeed)), () {
          _staticData.removeAt(0);
          if (_staticData.length > 0) {
            showStaticMessages();
          }
        });
      }
    }
  }

  void handleOptionsClicked(Item item) {
    vibrateDevice();
    if (_items.length > 0) {
      var model = _items.elementAt(item.index);
      if (_currentQuestionID == 6) {
        _model6 = model;
      }
      if (_currentQuestionID == 7) {
        _model7 = model;
      }

      if (model.option == "Order Test") {
        displayAlertWithActionLable(
            "Order Test",
            "Your order is under review by our genetic counselors. We will provide you an update by email soon.",
            "Ok");
        return;
      }

      if (model.option == "Learn more about HCS") {
        displayAlertWithActionLable("", "Comming soon....", "Ok");
        return;
      }

      if (model.option == "Talk to genetic counselor") {
        displayAlertWithActionLable("", "Comming soon....", "Ok");
        return;
      }

      setState(() {
        _items.clear();
        _list.add(ChatMessage(
            isEditable: false,
            textFontSize: widget.chatTextSize,
            text: model.option,
            type: MessageType.RIGHT,
            backgroundType: 0));
        _nextQuestionID = model.nextq_id;
      });
      _loadNext();
    }
  }

  void handlePersonalDetailForm() {
    vibrateDevice();
    switch (_intPersonalDetailFormPosition) {
      case 0:
        if (_tec_Name.text.length == 0) {
          displayAlert("", "Please enter name");
          return;
        } else if (validateName.hasMatch(_tec_Name.text)) {
          // Show Age Widget
          setState(() {
            _intPersonalDetailFormPosition = 1;
          });
        } else {
          displayAlert("", "Invalid name");
          return;
        }
        break;
      case 1:
        if (_selectedAge == "Select age") {
          displayAlert("", "Please select age");
          return;
        } else if (_selectedGender == "Select gender") {
          displayAlert("", "Please select gender");
          return;
        } else {
          // Show Gender Widget
          setState(() {
            _intPersonalDetailFormPosition = 3;
          });
        }
        break;
//      case 2:
//        if (_selectedGender == "Select gender") {
//          displayAlert("Error", "Please select gender");
//          return;
//        } else {
//          // Show Gender Widget
//          setState(() {
//            _intPersonalDetailFormPosition = 3;
//   43       });
//        }
//        break;
      case 3:
        if (_selectedEthnicity == "Select ethnicity") {
          displayAlert("", "Please select ethnicity");
          return;
        } else {
          // Upload Details
          _userAge = Log.getUserAge(_selectedAge);
          _userGender = Log.isUserMale(_selectedGender);

          ChatMessage object3 = new ChatMessage(
              isEditable: false,
              textFontSize: widget.chatTextSize,
              text: "",
              type: MessageType.PERSONAL_DETAILS,
              personalDetailModel: PersonalDetailModel(_tec_Name.text,
                  _selectedAge, _selectedGender, _selectedEthnicity),
              optionList: null,
              backgroundType: 0);

          setState(() {
            _list.add(object3);
          });
          scrollList();
          _loadNext();
        }
        break;
    }
  }

  void scrollList() {
    /* Timer(
        Duration(milliseconds: 300),
            () => _scrollController
            .jumpTo(_scrollController.position.maxScrollExtent));*/
    Timer(
        Duration(milliseconds: 200),
        () => _scrollController.animateTo(
            _scrollController.position.maxScrollExtent,
            curve: Curves.linear,
            duration: Duration(milliseconds: 300)));
  }

  void handleUIVisibility() {
    hideAll();
    switch (_currentQuestionID) {
      case 1:
        // Show personal details form
        setState(() {
          _showPersonalDetailsForm = true;
        });
        break;
      case 2:
        setState(() {
          _show2 = true;
        });
        break;
      case 6:
        setState(() {
          _show6 = true;
        });
        break;
      case 27:
        // Show Form with 2 dropdown and 1 text field
        setState(() {
          _selectedGender = "Selected gender";
          _show27 = true;
        });
        break;
      case 13:
        // Show Form with 2 dropdown and 1 text field
        setState(() {
          _selectedCancerType = "Select cancer";
          if (_userGender) {
            // MALE
            String data =
                '[{"name": "Breast cancer"},{"name": "Pancreatic cancer"},{"name": "Prostate cancer"},{"name": "Colorectal cancer"},{"name": "Stomach cancer"},{"name": "Other"}]';
            final json = JsonDecoder().convert(data);
            setState(() {
              _cancerTypes = (json)
                  .map<CancerTypes>((item) => CancerTypes.fromJson(item))
                  .toList();
            });
          } else {
            String data =
                '[{"name": "Breast cancer"},{"name": "Pancreatic cancer"},{"name": "Colorectal cancer"},{"name": "Stomach cancer"},{"name": "Uterine cancer"},{"name": "Ovarian cancer"},{"name": "Other"}]';
            final json = JsonDecoder().convert(data);
            setState(() {
              _cancerTypes = (json)
                  .map<CancerTypes>((item) => CancerTypes.fromJson(item))
                  .toList();
            });
          }
          _show13 = true;
        });
        break;
      case 17:
        setState(() {
          _show17 = true;
        });
        break;
      case 26:
        setState(() {
          _show26 = true;
        });
        break;
      case 31:
        setState(() {
          _show31 = true;
        });
        break;
      case 32:
        setState(() {
          _show32 = true;
        });
        break;
      case 35:
        setState(() {
          _show35 = true;
        });
        break;
      case 36:
        setState(() {
          _show36 = true;
        });
        break;
      case 28:
        setState(() {
          _show28 = true;
        });
        break;
      case 15:
      case 18:
      case 20:
      case 23:
      case 25:
      case 29:
      case 45:
      case 46:
      case 22:
      case 8:
      case 30:
      case 38:
      case 39:

      case 42:

      case 34:

      case 37:

      case 44:
      case 43:
      case 41:
        setState(() {
          _showOptions = true;
          _shouldSymmetry = true;
          _columnCount = 1;
        });
        break;
      case 19:
        setState(() {
          _show19 = true;
        });
        break;
      case 7:
        setState(() {
          _show7 = true;
        });
        break;
      case 21:
        setState(() {
          _show21 = true;
        });
        break;
      case 43:
        setState(() {
          _show43 = true;
        });
        break;
      case 40:
        setState(() {
          _show40 = true;
        });
        break;
//      default:
//        setState(() {
//          _selectedGender = "Selected gender";
//          _showOptions = true;
//        });
//        break;
    }
    scrollList();
  }

  void handle27() {
    vibrateDevice();
    switch (_intPersonalDetailBreastFormPosition) {
      case 0:
        if (_selectedGenderForRelative == "Select gender") {
          displayAlert("", "Please select gender");
          return;
        } else {
          // Show Gender Widget
          setState(() {
            _intPersonalDetailBreastFormPosition = 1;
          });
        }
        break;
      case 1:
        if (_tec_relative_age.text.length == 0) {
          displayAlert("", "Please enter age");
          return;
        } else if (int.parse(_tec_relative_age.text) > 0) {
          // Show Age Widget
          setState(() {
            _intPersonalDetailBreastFormPosition = 2;
          });
        } else {
          displayAlert("", "Invalid age");
          return;
        }
        break;
      case 2:
        if (_selectedCancerType == "Select cancer") {
          displayAlert("", "Please select cancer type");
          return;
        } else {
          // Upload Details

          ChatMessage object3 = new ChatMessage(
              isEditable: false,
              textFontSize: widget.chatTextSize,
              text: "",
              type: MessageType.DIGNOSED_RELATIVE_DETAILS,
              personalDetailModel: null,
              relativeDetailModel: CancerRelativeDetail(
                  _selectedGenderForRelative,
                  _tec_relative_age.text.toString(),
                  _selectedCancerType),
              optionList: null,
              backgroundType: 0);

          setState(() {
            _list.add(object3);
          });

          scrollList();

          _loadNext();
        }
        break;
    }
  }

  void handle13() {
    vibrateDevice();
    if (_selectedCancerType == "Select cancer") {
      displayAlert("", "Please provide all details");
      return;
    }

    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _selectedCancerType,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    _userAge = Log.getUserAge(_selectedAge);
    _userGender = Log.isUserMale(_selectedGender);

    switch (_selectedCancerType) {
      case "Breast cancer":
        if (_userGender) {
          // MALE
          setState(() {
            _nextQuestionID = 0;
            _cancerTypeValue = "15";
          });
        } else {
          //FEMALE
          if (_selectedEthnicity == "Ashkenazi Jewish") {
            setState(() {
              _nextQuestionID = 0;
              _cancerTypeValue = "16";
            });
          } else {
            Log.printLog("PERSON AGE", "$_userAge");
            switch (_userAge) {
              case 0:
                setState(() {
                  _nextQuestionID = 0;
                  _cancerTypeValue = "18";
                });
                break;
              case 1:
                setState(() {
                  _nextQuestionID = 0;
                  _cancerTypeValue = "17";
                });
                break;
              case 2:
                setState(() {
                  _nextQuestionID = 0;
                  _cancerTypeValue = "17";
                });
                break;
            }
          }
        }
        break;
      case "Pancreatic cancer":
        setState(() {
          _nextQuestionID = 0;
          _cancerTypeValue = "25";
        });
        break;
      case "Prostate cancer":
        if (_selectedEthnicity == "Ashkenazi Jewish") {
          setState(() {
            _nextQuestionID = 0;
            _cancerTypeValue = "34";
          });
        } else {
          setState(() {
            _nextQuestionID = 0;
            _cancerTypeValue = "35";
          });
        }
        break;
      case "Colorectal cancer":
        switch (_userAge) {
          case 0:
            setState(() {
              _nextQuestionID = 0;
              _cancerTypeValue = "44";
            });
            break;
          case 1:
          case 2:
            setState(() {
              _nextQuestionID = 0;
              _cancerTypeValue = "43";
            });
            break;
        }
        break;
      case "Stomach cancer":
        setState(() {
          _nextQuestionID = 0;
          _cancerTypeValue = "40";
        });
        break;
      case "Uterine cancer":
        switch (_userAge) {
          case 0:
            setState(() {
              _nextQuestionID = 0;
              _cancerTypeValue = "44";
            });
            break;
          case 1:
          case 2:
            setState(() {
              _nextQuestionID = 0;
              _cancerTypeValue = "43";
            });
            break;
        }
        break;
      case "Ovarian cancer":
        setState(() {
          _nextQuestionID = 0;
          _cancerTypeValue = "25";
        });
        break;
      case "Other":
        setState(() {
          _nextQuestionID = 0;
          _cancerTypeValue = "46";
        });
        break;
    }

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  void handle17() {
    vibrateDevice();
    if (_tec_relative_age.text.length == 0) {
      displayAlert("", "Please provide relative's age");
      return;
    }

    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _tec_relative_age.text,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _nextQuestionID = 19;
    });

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  void handle26(int position) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _nextQuestionID = _items[position].nextq_id;
    });

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  void handle31(int position) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _nextQuestionID = _items[position].nextq_id;
    });

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  void handle32(int position) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    if (position != 0) {
      setState(() {
        _nextQuestionID = 23;
      });
    } else {
      setState(() {
        _nextQuestionID = 29;
      });
    }

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  _getReadSpeed() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('CURRENT READ SPEED IS : ${prefs.getInt(Log.sp_readSpeed)}');
    setState(() {
      if (prefs.containsKey(Log.sp_readSpeed)) {
        _readSpeed = prefs.getInt(Log.sp_readSpeed);
      } else {
        _readSpeed = Log.normal;
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _scrollController.dispose();
    super.dispose();
  }

  Widget _getListItem(BuildContext context, int index) {
    double maxWidth = MediaQuery.of(context).size.width * 0.70;
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    double _spacingRound = 12;
    var _elevation = 0.9;

    ChatMessage message = _list[index];

    if (message.type == (MessageType.RIGHT)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              //height: _getVerticalSpacing(message.backgroundType),
              height: 8.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerRight,
                        child: WidgetAnimator(Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(5),
                                  bottomLeft: Radius.circular(15),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
//                            color: Color(0xff03A9F4),
                            color: myriadUserIconColor,
                            margin: EdgeInsets.only(
                                right: 2.0, top: 5.0, left: 10.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(3.5),
                                    child: new Text(message.text,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                ],
                              ),
                            ))))),
                Visibility(
                  visible: message.isEditable,
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          print("EDIT BUTTON CLICKED : " + "${message.ID}");
//                          displayAlertForChangeResponse(
//                              "Change Response",
//                              "Please note that changing a response will require you to restart the conversation from that point in the chat history.",
//                              message.ID);
                        },
                        child: Image.asset(
                          'images/edit.png',
                          height: 16,
                          width: 16,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                    ],
                  ),
                ),
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/user.png",
                          color: myriadUserIconColor,
                          width: 35,
                          height: 35,
                          alignment: Alignment.center),
                    ))
              ],
            ),
            SizedBox(
              height: 10,
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.LEFT)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: WidgetAnimator(
                          new Card(
                              shape: _renderBackground(message.backgroundType),
                              elevation: _elevation,
                              color: leftBubbleBG,
                              margin: EdgeInsets.only(
                                  right: 10.0, top: 0.0, left: 7.0),
                              child: new Container(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      /* padding: EdgeInsets.all(5.0),
                                      margin: EdgeInsets.all(8),
                                      child: Text(message.text,
                                          style: TextStyle(
//                                            color: Color(0xff51575C),
                                              color: Colors.black87,
                                              fontFamily: 'BrandingMedium',
                                              fontSize: ScreenUtil()
                                                  .setSp(widget.chatTextSize)),
                                          overflow: TextOverflow.clip),*/

                                      /* padding: EdgeInsets.all(0.0),
                                      margin: EdgeInsets.all(2),
                                      child: Column(
                                        children: <Widget>[
                                          HtmlWidget(
//                                            """ ${message.text.toString().split(r"##")[0]} """,
                                            """ ${message.text} """,
                                            textStyle: TextStyle(
                                                fontFamily: 'BrandingMedium',
                                                fontSize: ScreenUtil()
                                                    .setSp(widget.chatTextSize),
                                                color: Colors.black87
//                                            color: Color(0xff51575C)
                                                ),
                                          ),
//                                          renderResources(message.text)
                                        ],
                                      ),*/

                                      padding: EdgeInsets.all(5.0),
                                      margin: EdgeInsets.all(8),
                                      child: Html(
                                        data: """ ${message.text} """,
                                        defaultTextStyle: TextStyle(
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize),
                                            color: Colors.black87
//                                            color: Color(0xff51575C)
                                            ),
                                        linkStyle: const TextStyle(
                                          color: Colors.blue,
                                          decoration: TextDecoration.underline,
                                        ),
                                        onLinkTap: (url) {
                                          vibrateDevice();
                                          if (_staticData.length > 0 ||
                                              _list[_list.length - 1].type ==
                                                  MessageType.LOADER) {
                                            return;
                                          }
                                          Dialogs.showLoadingDialog(
                                              context, _keyLoader);

                                          getTermDetails(url).then((value) {
                                            Navigator.of(
                                                    _keyLoader.currentContext)
                                                .pop();
                                            Log.printLog("TERM DEFI", value);
                                            showDialog(
                                              context: this.context,
                                              builder: (BuildContext context) =>
                                                  new CupertinoAlertDialog(
                                                title: new Text("$url",
                                                    style: TextStyle(
                                                        color:
                                                            myriadUserIconColor)),
                                                content: new Text("\n${value}"),
                                                actions: [
                                                  CupertinoDialogAction(
                                                      isDefaultAction: true,
                                                      child: new Text(
                                                        "Close",
                                                        style: TextStyle(
                                                            color:
                                                                myriadUserIconColor),
                                                      ),
                                                      onPressed: () {
                                                        vibrateDevice();
                                                        Navigator.pop(
                                                            context, 'Cancel');
                                                      })
                                                ],
                                              ),
                                            );
                                          });
                                        },
                                      ),
                                    ),
//                                  _highlightText(text),
                                  ],
                                ),
                              )),
                        )))
              ],
            )
          ],
        ),
      );
    } else if (message.type == ("ERROR")) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: WidgetAnimator(
                          Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(12),
                                    topRight: Radius.circular(12),
                                    bottomLeft: Radius.circular(5),
                                    bottomRight: Radius.circular(12)),
                              ),
                              elevation: _elevation,
                              color: Colors.white,
                              margin: EdgeInsets.only(
                                  right: 10.0, top: 5.0, left: 7.0),
                              child: new Container(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    /* new Container(
                          margin: EdgeInsets.only(right: 5.0),
                          child: new Text(
                            _name,
                            style: Theme.of(context).textTheme.subhead,
                          ),
                      ),*/
                                    new Container(
                                      padding: EdgeInsets.all(8.0),
                                      margin: EdgeInsets.all(3.5),
                                      child: new Text(message.text,
                                          style: TextStyle(
                                              color: Colors.red,
                                              fontFamily: 'BrandingMedium',
                                              fontSize: ScreenUtil()
                                                  .setSp(widget.chatTextSize)),
                                          overflow: TextOverflow.clip),
                                    ),
                                  ],
                                ),
                              )),
                        )))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.LOADER)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 8,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                      padding: EdgeInsets.all(2.5),
                                      margin: EdgeInsets.all(3.5),
                                      child: new Image(
                                          width: 35,
                                          fit: BoxFit.fitWidth,
                                          alignment: Alignment.center,
                                          height: 27,
                                          image: new AssetImage(
                                              "images/loader.gif"))),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == ("OPTIONS")) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: WidgetAnimator(
                          Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(15),
                                    topRight: Radius.circular(15),
                                    bottomLeft: Radius.circular(5),
                                    bottomRight: Radius.circular(15)),
                              ),
                              elevation: _elevation,
                              color: Colors.white,
                              margin: EdgeInsets.only(
                                  right: 10.0, top: 5.0, left: 7.0),
                              child: new Container(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    /* new Container(
                          margin: EdgeInsets.only(right: 5.0),
                          child: new Text(
                            _name,
                            style: Theme.of(context).textTheme.subhead,
                          ),
                      ),*/
                                    new Container(
                                      width: maxWidth,
                                      padding: EdgeInsets.all(8.0),
                                      child: new ListView.builder(
                                        shrinkWrap: true,
                                        itemBuilder: (_, int index) =>
                                            message.optionList[index],
                                        itemCount: message.optionList.length,
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                        )))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.VERIFY_OTP)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: WidgetAnimator(
                          Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(15),
                                    topRight: Radius.circular(15),
                                    bottomLeft: Radius.circular(5),
                                    bottomRight: Radius.circular(15)),
                              ),
                              elevation: _elevation,
                              color: Colors.white,
                              margin: EdgeInsets.only(
                                  right: 10.0,
                                  top: 5.0,
                                  left: 10.0,
                                  bottom: 5.0),
                              child: new Container(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      padding: EdgeInsets.all(8.0),
                                      margin: EdgeInsets.all(_spacingRound),
                                      child: new Text(message.text,
                                          style: TextStyle(
                                              //color: Color(0xff51575C),
                                              color: Colors.black87,
                                              fontFamily: 'BrandingMedium',
                                              fontSize: ScreenUtil()
                                                  .setSp(widget.chatTextSize)),
                                          overflow: TextOverflow.clip),
                                    ),
                                    new Container(
                                        padding: EdgeInsets.all(15.0),
                                        child: new Image(
                                            width: 35,
                                            fit: BoxFit.fitWidth,
                                            alignment: Alignment.center,
                                            height: 27,
                                            image: new AssetImage(
                                                "images/loader.gif"))),
                                    SizedBox(
                                      height: 10,
                                    ),
                                  ],
                                ),
                              )),
                        )))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.PERSONAL_DETAILS)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: WidgetAnimator(
                          Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(15),
                                    topRight: Radius.circular(15),
                                    bottomLeft: Radius.circular(5),
                                    bottomRight: Radius.circular(15)),
                              ),
                              elevation: _elevation,
                              color: Colors.white,
                              margin: EdgeInsets.only(
                                  right: 10.0, top: 5.0, left: 8.0),
                              child: new Container(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      padding: EdgeInsets.all(8.0),
                                      margin: EdgeInsets.all(_spacingRound),
                                      child: Text("Details provided",
                                          style: TextStyle(
                                              //color: Color(0xff51575C),
                                              color: Colors.black87,
                                              fontFamily: 'BrandingSemiBold',
                                              fontSize: ScreenUtil()
                                                  .setSp(widget.chatTextSize)),
                                          overflow: TextOverflow.clip),
                                    ),
                                    Divider(
                                      color: Color.fromRGBO(52, 52, 52, 1),
                                      height: 0.5,
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 20, top: 18, bottom: 7),
                                      child: Text("Name",
                                          style: TextStyle(
                                              color: myriadUserIconColor,
                                              fontFamily: 'BrandingMedium',
                                              fontSize: ScreenUtil()
                                                  .setSp(widget.chatTextSize)),
                                          overflow: TextOverflow.clip),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: Text(
                                          message.personalDetailModel.name,
                                          style: TextStyle(
                                              color:
                                                  Color.fromRGBO(52, 52, 52, 1),
                                              fontFamily: 'BrandingLight',
                                              fontSize: ScreenUtil()
                                                  .setSp(widget.chatTextSize)),
                                          overflow: TextOverflow.clip),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 20, top: 7, bottom: 7),
                                      child: Text("Gender",
                                          style: TextStyle(
                                              color: myriadUserIconColor,
                                              fontFamily: 'BrandingMedium',
                                              fontSize: ScreenUtil()
                                                  .setSp(widget.chatTextSize)),
                                          overflow: TextOverflow.clip),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: Text(
                                          message.personalDetailModel.gender,
                                          style: TextStyle(
                                              color:
                                                  Color.fromRGBO(52, 52, 52, 1),
                                              fontFamily: 'BrandingLight',
                                              fontSize: ScreenUtil()
                                                  .setSp(widget.chatTextSize)),
                                          overflow: TextOverflow.clip),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 20, top: 7, bottom: 7),
                                      child: Text("Age",
                                          style: TextStyle(
                                              color: myriadUserIconColor,
                                              fontFamily: 'BrandingMedium',
                                              fontSize: ScreenUtil()
                                                  .setSp(widget.chatTextSize)),
                                          overflow: TextOverflow.clip),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: Text(
                                          message.personalDetailModel.age,
                                          style: TextStyle(
                                              color:
                                                  Color.fromRGBO(52, 52, 52, 1),
                                              fontFamily: 'BrandingLight',
                                              fontSize: ScreenUtil()
                                                  .setSp(widget.chatTextSize)),
                                          overflow: TextOverflow.clip),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: 20, top: 7, bottom: 7),
                                      child: Text("Ethnicity",
                                          style: TextStyle(
                                              color: myriadUserIconColor,
                                              fontFamily: 'BrandingMedium',
                                              fontSize: ScreenUtil()
                                                  .setSp(widget.chatTextSize)),
                                          overflow: TextOverflow.clip),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: Text(
                                          message.personalDetailModel.ethnicity,
                                          style: TextStyle(
                                              color:
                                                  Color.fromRGBO(52, 52, 52, 1),
                                              fontFamily: 'BrandingLight',
                                              fontSize: ScreenUtil()
                                                  .setSp(widget.chatTextSize)),
                                          overflow: TextOverflow.clip),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                  ],
                                ),
                              )),
                        ))),
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      );
    } else if (message.type == (MessageType.DIGNOSED_RELATIVE_DETAILS)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                  flex: 1,
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: WidgetAnimator(
                        Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(8),
                                  topRight: Radius.circular(8),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(8)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 10.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.all(8.0),
                                  margin: EdgeInsets.only(
                                      top: 3.5,
                                      bottom: 3.5,
                                      right: 15,
                                      left: 3.5),
                                  child: Text("Relative's Details provided",
                                      style: TextStyle(
                                          color: Color(0xff51575C),
                                          fontFamily: 'BrandingSemiBold',
                                          fontSize: ScreenUtil()
                                              .setSp(widget.chatTextSize)),
                                      overflow: TextOverflow.clip),
                                ),
                                Divider(
                                    color: Color.fromRGBO(52, 52, 52, 1),
                                    height: 0.5),
                                Container(
                                  margin: EdgeInsets.only(
                                      left: 10, top: 7, bottom: 7),
                                  child: Text("Gender",
                                      style: TextStyle(
                                          color: myriadUserIconColor,
                                          fontFamily: 'BrandingMedium',
                                          fontSize: ScreenUtil()
                                              .setSp(widget.chatTextSize)),
                                      overflow: TextOverflow.clip),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Text(
                                      message.relativeDetailModel.gender,
                                      style: TextStyle(
                                          color: Color.fromRGBO(52, 52, 52, 1),
                                          fontFamily: 'BrandingLight',
                                          fontSize: ScreenUtil()
                                              .setSp(widget.chatTextSize)),
                                      overflow: TextOverflow.clip),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      left: 10, top: 7, bottom: 7),
                                  child: Text("Age",
                                      style: TextStyle(
                                          color: myriadUserIconColor,
                                          fontFamily: 'BrandingMedium',
                                          fontSize: ScreenUtil()
                                              .setSp(widget.chatTextSize)),
                                      overflow: TextOverflow.clip),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Text(message.relativeDetailModel.age,
                                      style: TextStyle(
                                          color: Color.fromRGBO(52, 52, 52, 1),
                                          fontFamily: 'BrandingLight',
                                          fontSize: ScreenUtil()
                                              .setSp(widget.chatTextSize)),
                                      overflow: TextOverflow.clip),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      left: 10, top: 7, bottom: 7),
                                  child: Text("Cancer Type",
                                      style: TextStyle(
                                          color: myriadUserIconColor,
                                          fontFamily: 'BrandingMedium',
                                          fontSize: ScreenUtil()
                                              .setSp(widget.chatTextSize)),
                                      overflow: TextOverflow.clip),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Text(
                                      message.relativeDetailModel.cancer,
                                      style: TextStyle(
                                          color: Color.fromRGBO(52, 52, 52, 1),
                                          fontFamily: 'BrandingLight',
                                          fontSize: ScreenUtil()
                                              .setSp(widget.chatTextSize)),
                                      overflow: TextOverflow.clip),
                                ),
                                SizedBox(
                                  height: 12,
                                ),
                              ],
                            )),
                      )),
                )
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      );
    } else if (message.type == (MessageType.DIVIDER)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: ScreenUtil.screenHeightDp * 0.045,
            ),
            Row(children: <Widget>[
              Expanded(
                child: new Container(
                    margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                    child: Divider(
                      color: Color(0xFF798797),
                      height: 36,
                    )),
              ),
              Text(
                message.text,
                style: TextStyle(color: Color(0xFF687889)),
              ),
              Expanded(
                child: new Container(
                    margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                    child: Divider(
                      color: Color(0xFF798797),
                      height: 36,
                    )),
              ),
            ]),
          ],
        ),
      );
    }
  }

  double _getVerticalSpacing(int backgroundType) {
    switch (backgroundType) {
      case -1:
        return 8.0;
        break;
      case 0:
        return 8.0;
        break;
      case 1:
        return 8.0;
        break;
      case 2:
        return 8.0;
        break;
      default:
        return 8.0;
    }
  }

  RoundedRectangleBorder _renderBackground(int backgroundType) {
    switch (backgroundType) {
      case 0:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(12),
              topRight: Radius.circular(12),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
//        return RoundedRectangleBorder(
//          borderRadius: BorderRadius.only(
//              topLeft: Radius.circular(3.0),
//              topRight: Radius.circular(18.0),
//              bottomLeft: Radius.circular(18.0),
//              bottomRight: Radius.circular(8.0)),
//        );
        break;
      case 1:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
//        return RoundedRectangleBorder(
//          borderRadius: BorderRadius.only(
//              topLeft: Radius.circular(3.0),
//              topRight: Radius.circular(18.0),
//              bottomLeft: Radius.circular(18.0),
//              bottomRight: Radius.circular(8.0)),
//        );
        break;
      case 2:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(12),
              bottomRight: Radius.circular(12)),
        );
//        return RoundedRectangleBorder(
//          borderRadius: BorderRadius.only(
//              topLeft: Radius.circular(3.0),
//              topRight: Radius.circular(18.0),
//              bottomLeft: Radius.circular(18.0),
//              bottomRight: Radius.circular(8.0)),
//        );

        break;
      default:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(12),
              topRight: Radius.circular(12),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
//        return RoundedRectangleBorder(
//          borderRadius: BorderRadius.only(
//              topLeft: Radius.circular(3.0),
//              topRight: Radius.circular(18.0),
//              bottomLeft: Radius.circular(18.0),
//              bottomRight: Radius.circular(8.0)),
//        );
        break;
    }
  }

  hideAll() {
    setState(() {
      _showPersonalDetailsForm = false;
      _showOptions = false;
      _show27 = false;
      _show13 = false;
      _show17 = false;
      _show26 = false;
      _show31 = false;
      _show32 = false;
      _show19 = false;
      _show21 = false;
      _show6 = false;
      _show7 = false;
      _show28 = false;
      _show2 = false;
      _show35 = false;
      _show36 = false;
      _show43 = false;
      _show40 = false;
    });
  }

  void handle19(int position) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _nextQuestionID = _items[position].nextq_id;
    });

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  void handle21(int position) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _nextQuestionID = _items[position].nextq_id;
    });

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  void handle6(int position) {
    vibrateDevice();
    if (_items.length > 0) {
      _model6 = _items.elementAt(position);
    }
    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _nextQuestionID = _items[position].nextq_id;
    });

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  void handle7(int position) {
    vibrateDevice();
    if (_items.length > 0) {
      _model7 = _items.elementAt(position);
    }

    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _nextQuestionID = _items[position].nextq_id;
    });

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  void handle28(int position) {
    vibrateDevice();
    _userAge = Log.getUserAge(_selectedAge);

    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      if (_userAge == 1 &&
          _selectedCancerType == 'Triple negative breast cancer') {
        _nextQuestionID = 29;
      } else if (_selectedCancerType == 'Bilateral breast cancer' ||
          _selectedCancerType == 'Two breast Primaries') {
        _nextQuestionID = 29;
      } else {
        _nextQuestionID = _items[position].nextq_id;
      }
    });

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  void handle2(int position) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _nextQuestionID = _items[position].nextq_id;
    });

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  void handle35(int position) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _nextQuestionID = _items[position].nextq_id;
    });

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  void handle36(int position) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _nextQuestionID = _items[position].nextq_id;
    });

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  void handle43(int position) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _nextQuestionID = _items[position].nextq_id;
    });

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  void handle40(int position) {
    vibrateDevice();
    ChatMessage object3 = new ChatMessage(
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: _items[position].option,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _nextQuestionID = _items[position].nextq_id;
    });

    setState(() {
      _list.add(object3);
    });

    scrollList();
    _loadNext();
  }

  Future<String> getTermDetails(String term) async {
    String url =
        "${Log.API_STACK}getAnnotationDesc?term=$term&termtype=term&authtoken=tSN4DRaEQ5";
    Log.printLog("API URL", url);
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.post(url);
        var jsonData = json.decode(data.body);
        Log.printLog("API RESPONSE", jsonData.toString());
        var jsonResponse = jsonData['data'][0];
        return jsonResponse['description'];
      }
    } on SocketException catch (_) {}
  }
}

class CancerTypes {
  final String name;

  CancerTypes({this.name});

  factory CancerTypes.fromJson(Map<String, dynamic> json) {
    return new CancerTypes(name: json['name']);
  }
}
