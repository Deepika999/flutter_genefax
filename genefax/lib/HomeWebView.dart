import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeWebView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _StateHomeWebView();
  }

}

class _StateHomeWebView extends State<HomeWebView> {
  double progress;

  @override
  Widget build(BuildContext context) {

    // URL to load
    var URL = "https://google.com.tr";

    // TODO: implement build
    return Container(
        child: Column(
            children: <Widget>[
              (progress != 1.0)
                  ? LinearProgressIndicator(
                  value: progress,
                  backgroundColor: Colors.grey[200],
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.purple))
                  : null,    // Should be removed while showing
              Expanded(
                child: Container(
                  /*child: InAppWebView(
                    initialUrl: URL,
                    initialHeaders: {},
                    onWebViewCreated: (InAppWebViewController controller) {
                    },
                    onLoadStart: (InAppWebViewController controller, String url) {
                    },
                    onProgressChanged:
                        (InAppWebViewController controller, int progress) {
                      setState(() {
                        this.progress = progress / 100;
                      });
                    },
                  ),*/
                ),
              )
            ].where((Object o) => o != null).toList()));
  }
}
