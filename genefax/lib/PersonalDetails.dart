import 'package:flutter/material.dart';

class PersonalDetails extends StatelessWidget {

  final String personName;
  final String personAge;
  final String personGender;
  final String personEthnicity;

  const PersonalDetails({Key key, this.personName,this.personAge,this.personGender,this.personEthnicity}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        Padding(padding: EdgeInsets.only(top: 8.0),),
        new Container(
          padding: EdgeInsets.all(12.0),
          decoration: BoxDecoration(color: Color(0xff2C92E8), borderRadius: new BorderRadius.circular(25.0)),
          child: Text(personName,style: TextStyle(color: Colors.white,fontFamily: 'BrandingMedium'),textAlign: TextAlign.center,),
        ),
        Padding(padding: EdgeInsets.only(top: 8.0),),
      ],
    );
  }
}
