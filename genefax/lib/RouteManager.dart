import 'package:ai/CounselorConnect.dart';
import 'package:ai/HomePage.dart';
import 'package:ai/OptraWelcomeDialog.dart';
import 'package:ai/main.dart';
import 'package:ai/RouteNames.dart';
import 'package:flutter/material.dart';

import 'screens/Notifications.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  final args = settings.arguments;
  switch (settings.name) {
    case RouteSplashScreen:
      return _getPageRoute(routeName: settings.name,viewToShow: new SplashScreen());
    case RouteHomePage:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: new HomePage(),
      );
    case RouteWelcomeDialog:
      return _getPageRoute(
          routeName: settings.name, viewToShow: new OptraWelcomeDialog());
    case RouteCounselorConnect:
      if(args is double){
        return _getPageRoute(
          routeName: settings.name,
          viewToShow: new CounselorConnect(chatTextSize: args)
        );
      }
      return errorRoute();
    case RouteNotifications:
      if(args is double){
        return _getPageRoute(
            routeName: settings.name,
            viewToShow: new Notifications(chatTextSize: args)
        );
      }
      return errorRoute();
    default:
      return MaterialPageRoute(
          builder: (_) => Scaffold(
                body: Center(
                    child: Text('No route defined for ${settings.name}')),
              ));
  }
}

Route<dynamic> errorRoute() {
  return MaterialPageRoute(
    builder: (_){
      return Scaffold(
        appBar: AppBar(title: Text('Error'),),
        body: Center(child: Text('Error in route navigation'),),
      );
    }
  );
}

PageRoute _getPageRoute({String routeName, Widget viewToShow}) {
  return MaterialPageRoute(
      settings: RouteSettings(
        name: routeName,
      ),
      builder: (_) => viewToShow);
}
