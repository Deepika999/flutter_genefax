import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'SizeConfig.dart';

class CategoryPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _stateCategoryPage();
  }
}

class _stateCategoryPage extends State<CategoryPage> {
  double screenHeightDp;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Color(0xffFFFFFF)
    ));
    SizeConfig().init(context);
    screenHeightDp = MediaQuery.of(context).size.width;
    ScreenUtil.init(context, width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height, allowFontScaling: true);

    return new Material(
      child: Stack(
        children: <Widget>[
          Container(
            alignment: Alignment.topRight,
            child: Image.asset("images/top_shape.png",
                height: ScreenUtil.screenHeightDp * 0.40,
                width: ScreenUtil.screenHeightDp * 0.50,
                alignment: Alignment.topRight),
          ),
          SingleChildScrollView(
            child: new ConstrainedBox(
              constraints: BoxConstraints(),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: ScreenUtil.screenHeightDp * 0.15,
                    ),
                    Text(
                      "Select any module to converse with our AI enabled knowledgebase",
                      style: TextStyle(
                          color: Color(0xff5A6076),
                          fontSize: ScreenUtil().setSp(18),
                          fontFamily: 'BrandingMedium'),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: ScreenUtil.screenHeightDp * 0.05,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 8, right: 8),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: ScreenUtil.screenHeightDp,
                            height: ScreenUtil.screenHeightDp * 0.20,
                            child: new Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      topRight: Radius.circular(15),
                                      bottomLeft: Radius.circular(15),
                                      bottomRight: Radius.circular(15)),
                                ),
                                elevation: 4.0,
                                color: Color(0xffFFFFFF),
                                margin: EdgeInsets.all(7),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                child: Stack(
                                  children: <Widget>[
                                    new ClipRRect(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      child: Image.network(
                                        'http://www.invictagenetics.com/wp-content/uploads/2016/11/serenity-1024x445.jpg',
                                        width: ScreenUtil.screenHeightDp,
                                        height:
                                            ScreenUtil.screenHeightDp *
                                                0.20,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    new Container(
                                      child: new Stack(
                                        children: <Widget>[
                                          Positioned(
                                            child: new Container(
                                              padding: EdgeInsets.all(8.0),
                                              margin: EdgeInsets.all(3.5),
                                              child: new Text("",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(20)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                              alignment: Alignment.centerLeft,
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 0,
                                            child: Container(
                                              decoration: new BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(15),
                                                    bottomRight:
                                                        Radius.circular(15)),
                                                color: Colors.white54,
                                              ),
                                              height: 50,
                                              padding: EdgeInsets.only(
                                                  left: 12, bottom: 12),
                                              alignment: Alignment.bottomLeft,
                                              width: ScreenUtil.screenHeightDp,
                                              child: new Text(
                                                  "Noninvasive\nPrenatal testing",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(15)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                            ),
                                          )

                                          /*Expanded(
                                            child: Container(
                                              alignment: Alignment.center,
                                              child: Image.network(
                                                'https://previews.123rf.com/images/falonkoontz/falonkoontz1502/falonkoontz150200131/36997175-soft-newborn-baby-feet-against-a-pink-blanket-.jpg',
                                                width: ScreenUtil().width *
                                                    0.40,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          )*/
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          /*Container(
                            margin: EdgeInsets.only(left: 12),
                            child: new Text("Noninvasive\nPrenatal testing",
                                style: TextStyle(
                                    color: Colors.black54,
                                    fontFamily: 'BrandingMedium',
                                    fontSize: ScreenUtil().setSp(18)),
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.clip),
                          ),*/
                          // HCS
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            width: ScreenUtil.screenHeightDp,
                            height: ScreenUtil.screenHeightDp * 0.20,
                            child: new Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      topRight: Radius.circular(15),
                                      bottomLeft: Radius.circular(15),
                                      bottomRight: Radius.circular(15)),
                                ),
                                elevation: 4.0,
                                color: Color(0xffFFFFFF),
                                margin: EdgeInsets.all(7),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                child: Stack(
                                  children: <Widget>[
                                    new ClipRRect(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      child: Image.network(
                                        'https://mypositiveparenting.org/wp-content/uploads/2014/08/family-hand.jpg',
                                        width: ScreenUtil.screenHeightDp,
                                        height:
                                            ScreenUtil.screenHeightDp *
                                                0.20,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    new Container(
                                      child: new Stack(
                                        children: <Widget>[
                                          Positioned(
                                            child: new Container(
                                              padding: EdgeInsets.all(8.0),
                                              margin: EdgeInsets.all(3.5),
                                              child: new Text("",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(20)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                              alignment: Alignment.centerLeft,
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 0,
                                            child: Container(
                                              decoration: new BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(15),
                                                    bottomRight:
                                                        Radius.circular(15)),
                                                color: Colors.white54,
                                              ),
                                              height: 50,
                                              padding: EdgeInsets.only(
                                                  left: 12, bottom: 12),
                                              alignment: Alignment.bottomLeft,
                                              width: ScreenUtil.screenHeightDp,
                                              child: new Text(
                                                  "Hereditary\nCancer Screening",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(15)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                            ),
                                          )

                                          /*Expanded(
                                              child: Container(
                                                alignment: Alignment.center,
                                                child: Image.network(
                                                  'https://previews.123rf.com/images/falonkoontz/falonkoontz1502/falonkoontz150200131/36997175-soft-newborn-baby-feet-against-a-pink-blanket-.jpg',
                                                  width: ScreenUtil.screenHeightDp *
                                                      0.40,
                                                  fit: BoxFit.fill,
                                                ),
                                              ),
                                            )*/
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          // New Born
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            width: ScreenUtil.screenHeightDp,
                            height: ScreenUtil.screenHeightDp * 0.20,
                            child: new Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      topRight: Radius.circular(15),
                                      bottomLeft: Radius.circular(15),
                                      bottomRight: Radius.circular(15)),
                                ),
                                elevation: 4.0,
                                color: Color(0xffDDD3C0),
                                margin: EdgeInsets.all(7),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                child: Stack(
                                  children: <Widget>[
                                    new ClipRRect(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      child: Image.network(
                                        'https://i0.wp.com/www.aamirsphotography.com/wp-content/uploads/2018/04/5-newborn-baby-feet-mother.jpg',
                                        width: ScreenUtil.screenHeightDp,
                                        height:
                                            ScreenUtil.screenHeightDp *
                                                0.20,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    new Container(
                                      child: new Stack(
                                        children: <Widget>[
                                          Positioned(
                                            child: new Container(
                                              padding: EdgeInsets.all(8.0),
                                              margin: EdgeInsets.all(3.5),
                                              child: new Text("",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(20)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                              alignment: Alignment.centerLeft,
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 0,
                                            child: Container(
                                              decoration: new BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(15),
                                                    bottomRight:
                                                        Radius.circular(15)),
                                                color: Colors.white54,
                                              ),
                                              height: 50,
                                              padding: EdgeInsets.only(
                                                  left: 12, bottom: 12),
                                              alignment: Alignment.bottomLeft,
                                              width: ScreenUtil.screenHeightDp,
                                              child: new Text("New Born",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(15)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                            ),
                                          )

                                          /*Expanded(
                                            child: Container(
                                              alignment: Alignment.center,
                                              child: Image.network(
                                                'https://previews.123rf.com/images/falonkoontz/falonkoontz1502/falonkoontz150200131/36997175-soft-newborn-baby-feet-against-a-pink-blanket-.jpg',
                                                width: ScreenUtil.screenHeightDp *
                                                    0.40,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          )*/
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          // Health Insurance
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            width: ScreenUtil.screenHeightDp,
                            height: ScreenUtil.screenHeightDp * 0.20,
                            child: new Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      topRight: Radius.circular(15),
                                      bottomLeft: Radius.circular(15),
                                      bottomRight: Radius.circular(15)),
                                ),
                                elevation: 4.0,
                                color: Color(0xffC2CFD8),
                                margin: EdgeInsets.all(7),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                child: Stack(
                                  children: <Widget>[
                                    new ClipRRect(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      child: Image.network(
                                        'https://www.workmaninsurancegroup.com/wp-content/uploads/2019/09/health-insurance-banner-850x320.jpg',
                                        width: ScreenUtil.screenHeightDp,
                                        height:
                                            ScreenUtil.screenHeightDp *
                                                0.20,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    new Container(
                                      child: new Stack(
                                        children: <Widget>[
                                          Positioned(
                                            child: new Container(
                                              padding: EdgeInsets.all(8.0),
                                              margin: EdgeInsets.all(3.5),
                                              child: new Text("",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(20)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                              alignment: Alignment.centerLeft,
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 0,
                                            child: Container(
                                              decoration: new BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(15),
                                                    bottomRight:
                                                        Radius.circular(15)),
                                                color: Colors.white54,
                                              ),
                                              height: 50,
                                              padding: EdgeInsets.only(
                                                  left: 12, bottom: 12),
                                              alignment: Alignment.bottomLeft,
                                              width: ScreenUtil.screenHeightDp,
                                              child: new Text(
                                                  "Health Insurance",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(15)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                            ),
                                          )

                                          /*Expanded(
                                            child: Container(
                                              alignment: Alignment.center,
                                              child: Image.network(
                                                'https://previews.123rf.com/images/falonkoontz/falonkoontz1502/falonkoontz150200131/36997175-soft-newborn-baby-feet-against-a-pink-blanket-.jpg',
                                                width: ScreenUtil.screenWidthDp *
                                                    0.40,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          )*/
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          // General Genetics
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            width: ScreenUtil.screenWidthDp,
                            height: ScreenUtil.screenHeightDp * 0.20,
                            child: new Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      topRight: Radius.circular(15),
                                      bottomLeft: Radius.circular(15),
                                      bottomRight: Radius.circular(15)),
                                ),
                                elevation: 4.0,
                                color: Color(0xffEBEDF9),
                                margin: EdgeInsets.all(7),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                child: Stack(
                                  children: <Widget>[
                                    new ClipRRect(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      child: Image.network(
                                        'https://www.einstein.edu/upload/images/Einstein/Services/victor-center/evc-banner-nr-103813802-848.jpeg',
                                        width: ScreenUtil.screenWidthDp,
                                        height:
                                            ScreenUtil.screenHeightDp *
                                                0.20,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    new Container(
                                      child: new Stack(
                                        children: <Widget>[
                                          Positioned(
                                            child: new Container(
                                              padding: EdgeInsets.all(8.0),
                                              margin: EdgeInsets.all(3.5),
                                              child: new Text("",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(20)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                              alignment: Alignment.centerLeft,
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 0,
                                            child: Container(
                                              decoration: new BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(15),
                                                    bottomRight:
                                                        Radius.circular(15)),
                                                color: Colors.white54,
                                              ),
                                              height: 50,
                                              padding: EdgeInsets.only(
                                                  left: 12, bottom: 12),
                                              alignment: Alignment.bottomLeft,
                                              width: ScreenUtil.screenWidthDp,
                                              child: new Text(
                                                  "General Genetics",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(15)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                            ),
                                          )

                                          /*Expanded(
                                            child: Container(
                                              alignment: Alignment.center,
                                              child: Image.network(
                                                'https://previews.123rf.com/images/falonkoontz/falonkoontz1502/falonkoontz150200131/36997175-soft-newborn-baby-feet-against-a-pink-blanket-.jpg',
                                                width: ScreenUtil.screenWidthDp *
                                                    0.40,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          )*/
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          // Heart
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            width: ScreenUtil.screenWidthDp,
                            height: ScreenUtil.screenHeightDp * 0.20,
                            child: new Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      topRight: Radius.circular(15),
                                      bottomLeft: Radius.circular(15),
                                      bottomRight: Radius.circular(15)),
                                ),
                                elevation: 4.0,
                                color: Color(0xffFFFFFF),
                                margin: EdgeInsets.all(7),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                child: Stack(
                                  children: <Widget>[
                                    new ClipRRect(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      child: Image.network(
                                        'https://i0.wp.com/sussexheart.com/wp-content/uploads/2017/01/mainbanner3.jpg?resize=1140%2C560&ssl=1',
                                        width: ScreenUtil.screenWidthDp,
                                        height:
                                            ScreenUtil.screenHeightDp *
                                                0.20,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    new Container(
                                      child: new Stack(
                                        children: <Widget>[
                                          Positioned(
                                            child: new Container(
                                              padding: EdgeInsets.all(8.0),
                                              margin: EdgeInsets.all(3.5),
                                              child: new Text("",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(20)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                              alignment: Alignment.centerLeft,
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 0,
                                            child: Container(
                                              decoration: new BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(15),
                                                    bottomRight:
                                                        Radius.circular(15)),
                                                color: Colors.white54,
                                              ),
                                              height: 50,
                                              padding: EdgeInsets.only(
                                                  left: 12, bottom: 12),
                                              alignment: Alignment.bottomLeft,
                                              width: ScreenUtil.screenWidthDp,
                                              child: new Text("Heart",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(15)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                            ),
                                          )

                                          /*Expanded(
                                            child: Container(
                                              alignment: Alignment.center,
                                              child: Image.network(
                                                'https://previews.123rf.com/images/falonkoontz/falonkoontz1502/falonkoontz150200131/36997175-soft-newborn-baby-feet-against-a-pink-blanket-.jpg',
                                                width: ScreenUtil.screenWidthDp *
                                                    0.40,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          )*/
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          // Hematology
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            width: ScreenUtil.screenWidthDp,
                            height: ScreenUtil.screenHeightDp * 0.20,
                            child: new Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      topRight: Radius.circular(15),
                                      bottomLeft: Radius.circular(15),
                                      bottomRight: Radius.circular(15)),
                                ),
                                elevation: 4.0,
                                color: Color(0xffC97877),
                                margin: EdgeInsets.all(7),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                child: Stack(
                                  children: <Widget>[
                                    new ClipRRect(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      child: Image.network(
                                        'https://ehaweb.org/assets/fallback/EHA-News-header-septemer-2017.jpg',
                                        width: ScreenUtil.screenWidthDp,
                                        height:
                                            ScreenUtil.screenHeightDp *
                                                0.20,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    new Container(
                                      child: new Stack(
                                        children: <Widget>[
                                          Positioned(
                                            child: new Container(
                                              padding: EdgeInsets.all(8.0),
                                              margin: EdgeInsets.all(3.5),
                                              child: new Text("",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(20)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                              alignment: Alignment.centerLeft,
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 0,
                                            child: Container(
                                              decoration: new BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(15),
                                                    bottomRight:
                                                        Radius.circular(15)),
                                                color: Colors.white54,
                                              ),
                                              height: 50,
                                              padding: EdgeInsets.only(
                                                  left: 12, bottom: 12),
                                              alignment: Alignment.bottomLeft,
                                              width: ScreenUtil.screenWidthDp,
                                              child: new Text("Hematology",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(15)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                            ),
                                          )

                                          /*Expanded(
                                            child: Container(
                                              alignment: Alignment.center,
                                              child: Image.network(
                                                'https://previews.123rf.com/images/falonkoontz/falonkoontz1502/falonkoontz150200131/36997175-soft-newborn-baby-feet-against-a-pink-blanket-.jpg',
                                                width: ScreenUtil.screenWidthDp *
                                                    0.40,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          )*/
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          // Metabolic
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            width: ScreenUtil.screenWidthDp,
                            height: ScreenUtil.screenHeightDp * 0.20,
                            child: new Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      topRight: Radius.circular(15),
                                      bottomLeft: Radius.circular(15),
                                      bottomRight: Radius.circular(15)),
                                ),
                                elevation: 4.0,
                                color: Color(0xff032B2B),
                                margin: EdgeInsets.all(7),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                child: Stack(
                                  children: <Widget>[
                                    new ClipRRect(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      child: Image.network(
                                        'https://d1heoihvzm7u4h.cloudfront.net/5a323c74c3f77abc5f61ba794cd376979006b7b5_August_banner_29.jpg',
                                        width: ScreenUtil.screenWidthDp,
                                        height:
                                            ScreenUtil.screenHeightDp *
                                                0.20,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    new Container(
                                      child: new Stack(
                                        children: <Widget>[
                                          Positioned(
                                            child: new Container(
                                              padding: EdgeInsets.all(8.0),
                                              margin: EdgeInsets.all(3.5),
                                              child: new Text("",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(20)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                              alignment: Alignment.centerLeft,
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 0,
                                            child: Container(
                                              decoration: new BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(15),
                                                    bottomRight:
                                                        Radius.circular(15)),
                                                color: Colors.white54,
                                              ),
                                              height: 50,
                                              padding: EdgeInsets.only(
                                                  left: 12, bottom: 12),
                                              alignment: Alignment.bottomLeft,
                                              width: ScreenUtil.screenWidthDp,
                                              child: new Text("Metabolic",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(15)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                            ),
                                          )

                                          /*Expanded(
                                            child: Container(
                                              alignment: Alignment.center,
                                              child: Image.network(
                                                'https://previews.123rf.com/images/falonkoontz/falonkoontz1502/falonkoontz150200131/36997175-soft-newborn-baby-feet-against-a-pink-blanket-.jpg',
                                                width: ScreenUtil.screenWidthDp *
                                                    0.40,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          )*/
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          // Metabolic
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            width: ScreenUtil.screenWidthDp,
                            height: ScreenUtil.screenHeightDp * 0.20,
                            child: new Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      topRight: Radius.circular(15),
                                      bottomLeft: Radius.circular(15),
                                      bottomRight: Radius.circular(15)),
                                ),
                                elevation: 4.0,
                                color: Color(0xff233158),
                                margin: EdgeInsets.all(7),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                child: Stack(
                                  children: <Widget>[
                                    new ClipRRect(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      child: Image.network(
                                        'https://www.oticon.com/~/media/Oticon%20US/main/Your%20Hearing/Hearing%20Health/bannerspot-cognitiveDecline-1920x900.jpg?h=900&la=en&w=1920&hash=8F65830CF0DD1C107CC8C97440BA830200D69ACA',
                                        width: ScreenUtil.screenWidthDp,
                                        height:
                                            ScreenUtil.screenHeightDp *
                                                0.20,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    new Container(
                                      child: new Stack(
                                        children: <Widget>[
                                          Positioned(
                                            child: new Container(
                                              padding: EdgeInsets.all(8.0),
                                              margin: EdgeInsets.all(3.5),
                                              child: new Text("",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(20)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                              alignment: Alignment.centerLeft,
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 0,
                                            child: Container(
                                              decoration: new BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(15),
                                                    bottomRight:
                                                        Radius.circular(15)),
                                                color: Colors.white54,
                                              ),
                                              height: 50,
                                              padding: EdgeInsets.only(
                                                  left: 12, bottom: 12),
                                              alignment: Alignment.bottomLeft,
                                              width: ScreenUtil.screenWidthDp,
                                              child: new Text("Neurogenetics",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(15)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                            ),
                                          )

                                          /*Expanded(
                                            child: Container(
                                              alignment: Alignment.center,
                                              child: Image.network(
                                                'https://previews.123rf.com/images/falonkoontz/falonkoontz1502/falonkoontz150200131/36997175-soft-newborn-baby-feet-against-a-pink-blanket-.jpg',
                                                width: ScreenUtil.screenWidthDp *
                                                    0.40,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          )*/
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          // Metabolic
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            width: ScreenUtil.screenWidthDp,
                            height: ScreenUtil.screenHeightDp * 0.20,
                            child: new Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      topRight: Radius.circular(15),
                                      bottomLeft: Radius.circular(15),
                                      bottomRight: Radius.circular(15)),
                                ),
                                elevation: 4.0,
                                color: Color(0xffFFFFFF),
                                margin: EdgeInsets.all(7),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                child: Stack(
                                  children: <Widget>[
                                    new ClipRRect(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      child: Image.network(
                                        'https://lakeviewpediatrics.net/getattachment/bbea46db-4c08-4dba-9872-dab55acc7c5c/Replace.aspx',
                                        width: ScreenUtil.screenWidthDp,
                                        height:
                                            ScreenUtil.screenHeightDp *
                                                0.20,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    new Container(
                                      child: new Stack(
                                        children: <Widget>[
                                          Positioned(
                                            child: new Container(
                                              padding: EdgeInsets.all(8.0),
                                              margin: EdgeInsets.all(3.5),
                                              child: new Text("",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(20)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                              alignment: Alignment.centerLeft,
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 0,
                                            child: Container(
                                              decoration: new BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(15),
                                                    bottomRight:
                                                        Radius.circular(15)),
                                                color: Colors.white54,
                                              ),
                                              height: 50,
                                              padding: EdgeInsets.only(
                                                  left: 12, bottom: 12),
                                              alignment: Alignment.bottomLeft,
                                              width: ScreenUtil.screenWidthDp,
                                              child: new Text("Pediatric",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          'BrandingMedium',
                                                      fontSize: ScreenUtil
                                                              ()
                                                          .setSp(15)),
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.clip),
                                            ),
                                          )

                                          /*Expanded(
                                            child: Container(
                                              alignment: Alignment.center,
                                              child: Image.network(
                                                'https://previews.123rf.com/images/falonkoontz/falonkoontz1502/falonkoontz150200131/36997175-soft-newborn-baby-feet-against-a-pink-blanket-.jpg',
                                                width: ScreenUtil.screenWidthDp *
                                                    0.40,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          )*/
                                        ],
                                      ),
                                    )
                                  ],
                                )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
