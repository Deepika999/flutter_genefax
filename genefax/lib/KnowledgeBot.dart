import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:ai/Animations/ListItemAnimator.dart';
import 'package:ai/ChatMessage.dart';
import 'package:ai/Constant.dart';
import 'package:ai/Log.dart';
import 'package:ai/MessageType.dart';
import 'package:ai/MyriadPretest.dart';
import 'package:ai/SizeConfig.dart';
import 'package:ai/helper/Dialogs.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class KnowledgeBot extends StatefulWidget {
  double chatTextSize = 16.0;
  TextStyle optionButtonStyle;
  double buttonTextSize = 15.0;
  double formFieldTextSize = 15.0;

  KnowledgeBot({this.chatTextSize});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _StateKnowledgeBot();
  }
}

class _StateKnowledgeBot extends State<KnowledgeBot>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  final ScrollController _scrollController = new ScrollController();
  List<ChatMessage> _list = <ChatMessage>[];
  final List<ChatMessage> _staticData = <ChatMessage>[];
  static List<AutoSuggetionModel> autoSuggestionList =
      new List<AutoSuggetionModel>();
  bool loading = true;

  var _currentQuestionID;

  int _readSpeed;

  var buttonHeight = 40.0;

  double _autoSuggestionHeight = 132.0;
  static List<DiseaseModel> recommendationList = new List<DiseaseModel>();

  final TextEditingController _tec_ask_question = new TextEditingController();

  FocusNode _focus = new FocusNode();

  bool _showRecommendation = false;
  bool _isDisabled = true;

  bool _isAutoSuggestionEnabled = false;

  String _headingTextSuggestion = "You might want to ask the following";
  String _headingTextAutoSuggestion = "Did you mean?";

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  @override
  Widget build(BuildContext context) {
    widget.optionButtonStyle = TextStyle(
        color: optionTextColor,
        fontFamily: 'BrandingMedium',
        fontSize: ScreenUtil().setSp(widget.buttonTextSize));

    return SafeArea(
      child: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          appBar: AppBar(title: Text("Knowledge Bot"),
          backgroundColor: headerColor,),
          body: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
//                colors: [Color(0xFFf7f2f2), Color(0xFFf7f2f2),Color(0xFFf7f2f2)])),
                    colors: [
                      chatScreenBackground,
                      chatScreenBackground,
                      chatScreenBackground
                    ])),
//        color: Color.fromRGBO(232, 239, 245, 1),
//        color: Color(0xffEAEEF3),
            child: Column(
              children: <Widget>[
                Flexible(
                  child: Container(
                    padding:
                    EdgeInsets.only(bottom: ScreenUtil.screenHeightDp * 0.01),
                    child: ListView.builder(
                      controller: _scrollController,
                      padding: new EdgeInsets.all(8.0),
                      itemCount: _list.length,
                      itemBuilder: _getListItem,
                    ),
                  ),
                ),
                Visibility(
                  visible: _showRecommendation,
                  child: Container(
                    color: chatBotBottomContainerBG,
//                  color: Color.fromARGB(1,237, 242, 247),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Divider(
                          height: 1,
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.01,
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              left: ScreenUtil.screenWidthDp * 0.02,
                              right: ScreenUtil.screenWidthDp * 0.02),
                          child: Text(
                            _isAutoSuggestionEnabled
                                ? _headingTextAutoSuggestion
                                : _headingTextSuggestion,
                            textAlign: TextAlign.start,
                            style: TextStyle(fontSize: ScreenUtil().setSp(12)),
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.01,
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              left: ScreenUtil.screenWidthDp * 0.02,
                              right: ScreenUtil.screenWidthDp * 0.02),
                          child: Container(
                            height: _autoSuggestionHeight,
                            child: _isAutoSuggestionEnabled
                                ? ListView.builder(
                              padding: EdgeInsets.only(bottom: 5.0),
                              itemCount: autoSuggestionList == null
                                  ? 0
                                  : autoSuggestionList.length,
                              itemBuilder: _getAutoSuggestionsWidget,
                            )
                                : ListView.builder(
                              padding: EdgeInsets.only(bottom: 5.0),
                              itemCount: recommendationList == null
                                  ? 0
                                  : recommendationList.length,
                              itemBuilder: _getRecommendationWidget,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil.screenHeightDp * 0.000,
                        ),
                        Stack(
                          children: <Widget>[
                            Container(
                                color: headerColor.withOpacity(0.2),
                                height: ScreenUtil().setHeight(52)),
                            Container(
                              height: ScreenUtil().setHeight(42),
                              decoration: BoxDecoration(
                                  color: _isDisabled? Colors.grey.withOpacity(0.1) : Color.fromRGBO(255, 255, 255, 1),
                                  border:
                                  Border.all(width: 1.2, color: headerColor),
                                  borderRadius: new BorderRadius.circular(27.0)),
                              padding: EdgeInsets.only(left: 20, right: 0),
                              margin: EdgeInsets.only(
                                  left: ScreenUtil.screenWidthDp * 0.02,
                                  right: ScreenUtil.screenWidthDp * 0.02,
                                  top: ScreenUtil.screenWidthDp * 0.01,
                                  bottom: ScreenUtil.screenWidthDp * 0.01),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Flexible(
                                    child: TextField(
                                      enabled: !_isDisabled,
                                      onChanged: (String value) {
                                        _getAutoSuggestionQuestionList(value);
                                      },
                                      focusNode: _focus,
                                      onEditingComplete: _focus.unfocus,
                                      textCapitalization:
                                      TextCapitalization.sentences,
                                      controller: _tec_ask_question,
                                      style: TextStyle(color: Colors.black),
                                      decoration: InputDecoration(
                                        contentPadding:
                                        const EdgeInsets.symmetric(
                                            vertical: 15.0),
                                        hintText:
                                        "I want to ask my own question....",
                                        hintStyle: TextStyle(
                                            color:
                                            Color.fromRGBO(153, 153, 153, 1),
                                            fontSize: ScreenUtil().setSp(14)),
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.transparent),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.transparent),
                                        ),
                                      ),
                                      keyboardType: TextInputType.multiline,
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(right: 0, bottom: 0),
                                    child: new IconButton(
                                        icon: new Icon(
                                          Icons.send,
                                          color: sendIconColor,
                                        ),
                                        onPressed: () => _askQuestion(
                                            "${_tec_ask_question.text}?")),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        /*SizedBox(
                            height: ScreenUtil.screenHeightDp * 0.01,
                          ),*/
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    _loadInitialMessages();

    WidgetsBinding.instance.addObserver(this);

    super.initState();
  }

  void showStaticMessages() {
    _getReadSpeed();
    if (_list.length > 0) {
      setState(() {
        ChatMessage loader = new ChatMessage(
            showReadMore: false,
            nextMessage: "",
            ID: _currentQuestionID,
            isEditable: false,
            textFontSize: widget.chatTextSize,
            text: "",
            type: MessageType.LOADER,
            optionList: null,
            backgroundType: 0);
        _list.add(loader);
      });
      scrollList();
      if (_staticData.length > 0) {
        new Future.delayed(
            Duration(
                milliseconds: Log.getLoaderDelayDuration(
                    _staticData[0].text.toString().length, _readSpeed)), () {
          setState(() {
            _list.removeLast();
            _list.add(_staticData[0]);
          });

          scrollList();

          _staticData.removeAt(0);
          if (_staticData.length == 0) {
            handleUIVisibility();
          } else if (_staticData.length > 0) {
            // This is for actual message reading duration
            new Future.delayed(
                Duration(
                    milliseconds: Log.getMessageDelayDuration(
                        _staticData[0].text.toString().length, _readSpeed)),
                () {
              showStaticMessages();
            });
          }
        });
      } else {
        // EXIT AND DO NEXT TASK
        handleUIVisibility();
      }
    } else {
      if (_staticData.length > 0) {
        setState(() {
          _list.add(_staticData[0]);
        });
        scrollList();
/*
        print(
            "DURATION IS : ${Log.getMessageDelayDuration(_list.last.text.toString().length, _readSpeed)}");*/

        new Future.delayed(
            Duration(
                milliseconds: Log.getMessageDelayDuration(
                    _list.last.text.toString().length, _readSpeed)), () {
          _staticData.removeAt(0);
          if (_staticData.length > 0) {
            showStaticMessages();
          }
        });
      }
    }
  }

  void showReadMoreMessage(int indexToShowMsgAfter) {
    int loadingIndex = indexToShowMsgAfter + 1;
    if (_staticData.length > 0) {
      setState(() {
        _list.insert(loadingIndex, _staticData[0]);
      });
      _staticData.removeAt(0);
      scrollListToPosition(loadingIndex);
      if (_staticData.length == 0) {
        handleUIVisibility();
      } else if (_staticData.length > 0) {
        // This is for actual message reading duration
        showReadMoreMessage(loadingIndex);
      }
    } else {
      // EXIT AND DO NEXT TASK
      handleUIVisibility();
    }
  }

  void buildAnswer(String answer) {
    String nextMessage = "";
    if (answer.split(r". ").length > 1) {
      nextMessage =
          answer.substring(answer.split(r". ")[0].length + 1, answer.length);
    }
    List<String> arrList = answer.split(r". ");

    _staticData.clear();

    if (arrList.length == 1) {
      // THERE IS ONLY SINGLE SENTENCE TO DISPLAY
      _staticData.add(ChatMessage(
          showReadMore: false,
          nextMessage: nextMessage,
          ID: _currentQuestionID,
          isEditable: false,
          textFontSize: widget.chatTextSize,
          backgroundType: 2,
          text: arrList[0],
          type: MessageType.LEFT,
          optionList: null));
    } else if (arrList.length > 1) {
      // THERE IS MORE THEN 1 SENTENCE. DISPLAY FIRST SENTENCE WITH READ MORE OPTION.
      _staticData.add(ChatMessage(
          showReadMore: true,
          nextMessage: nextMessage,
          ID: _currentQuestionID,
          isEditable: false,
          textFontSize: widget.chatTextSize,
          backgroundType: 2,
          text: "${arrList[0]}.",
          type: MessageType.LEFT,
          optionList: null));
    }
  }

  Widget _getListItem(BuildContext context, int index) {
    double maxWidth = MediaQuery.of(context).size.width * 0.70;
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    double _spacingRound = 12;
    var _elevation = 0.9;

    ChatMessage message = _list[index];

    if (message.type == (MessageType.RIGHT)) {
      print("QUESTION LENGTH : ${message.text.length}");
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
//              height: _getVerticalSpacing(message.backgroundType),
              height: 8,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerRight,
                        child: WidgetAnimator(
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(5),
                                  bottomLeft: Radius.circular(15),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
//                            color: Color(0xff59A9B7),
//                            color: Color(0xff03A9F4),
                            color: rightBubbleBG,
                            margin: EdgeInsets.only(
                                right: 3.0, top: 5.0, left: 10.0),
                            child: new Container(
                              padding: EdgeInsets.all(8.0),
                              margin: EdgeInsets.all(3.5),
                              child: new Text(message.text,
                                  style: TextStyle(
                                      color: rightTextColor,
                                      fontFamily: 'BrandingMedium',
                                      fontSize: ScreenUtil()
                                          .setSp(widget.chatTextSize)),
                                  overflow: TextOverflow.clip),
                            ),
                          ),
                        ))),
                Visibility(
                  visible: message.isEditable,
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          /*HapticFeedback.vibrate();
                          print("EDIT BUTTON CLICKED : " + "${message.ID}");
                          if (_staticData.length == 0 &&
                              _list[_list.length - 1].type !=
                                  MessageType.LOADER) {
                            displayAlertForChangeResponse(
                                "Change Response",
                                "Please note that changing a response will require you to restart the conversation from that point in the chat history.",
                                message.ID);
                          }*/
                        },
                        child: Container(
                          padding: EdgeInsets.all(5.0),
                          margin: EdgeInsets.only(top: 5.0),
                          child: Image.asset(
                            'images/edit.png',
                            color: headerColor,
                            height: 16,
                            width: 16,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/user.png",
                          color: userIconColor,
                          width: 35,
                          height: 35,
                          alignment: Alignment.center),
                    ))
              ],
            ),
            SizedBox(
              height: 10,
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.LEFT)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: WidgetAnimator(
                          Column(
                            children: <Widget>[
                              Card(
                                  shape:
                                      _renderBackground(message.backgroundType),
                                  elevation: _elevation,
                                  color: leftBubbleBG,
                                  margin: EdgeInsets.only(
                                      right: 10.0, top: 0.0, left: 7.0),
                                  child: new Container(
                                    child: new Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        new Container(
                                          /*child: Text(message.text,
                                              style: TextStyle(
                                                  color: Color(0xff51575C),
                                                  fontFamily: 'BrandingMedium',
                                                  fontSize: ScreenUtil()
                                                      .setSp(widget.textFontSize)),
                                              overflow: TextOverflow.clip),*/
                                          padding: EdgeInsets.all(5.0),
                                          margin: EdgeInsets.all(8),
                                          child: Column(
                                            children: <Widget>[
                                              Html(
//                                            data: """ ${message.text} """,
                                                data:
                                                    """ ${message.text.toString().split(r"##")[0]} """,
                                                defaultTextStyle: TextStyle(
                                                    fontFamily:
                                                        'BrandingMedium',
                                                    fontSize: ScreenUtil()
                                                        .setSp(widget
                                                            .chatTextSize),
                                                    color: Colors.black87
//                                            color: Color(0xff51575C)
                                                    ),
                                                linkStyle: const TextStyle(
                                                  color: Colors.blue,
                                                  decoration:
                                                      TextDecoration.underline,
                                                ),
                                                onLinkTap: (url) {
                                                  vibrateDevice();
                                                  if (_staticData.length > 0 ||
                                                      _list[_list.length - 1]
                                                              .type ==
                                                          MessageType.LOADER) {
                                                    return;
                                                  }
                                                  Dialogs.showLoadingDialog(
                                                      context, _keyLoader);

                                                  getTermDetails(url)
                                                      .then((value) {
                                                    Navigator.of(_keyLoader
                                                            .currentContext)
                                                        .pop();
                                                    Log.printLog(
                                                        "TERM DEFI", value);
                                                    showDialog(
                                                      context: this.context,
                                                      builder: (BuildContext
                                                              context) =>
                                                          new CupertinoAlertDialog(
                                                        title: new Text("$url",
                                                            style: TextStyle(
                                                                color:
                                                                    myriadUserIconColor)),
                                                        content: new Text(
                                                            "\n${value}"),
                                                        actions: [
                                                          CupertinoDialogAction(
                                                              isDefaultAction:
                                                                  true,
                                                              child: new Text(
                                                                "Close",
                                                                style: TextStyle(
                                                                    color:
                                                                        myriadUserIconColor),
                                                              ),
                                                              onPressed: () {
                                                                vibrateDevice();
                                                                Navigator.pop(
                                                                    context,
                                                                    'Cancel');
                                                              })
                                                        ],
                                                      ),
                                                    );
                                                  });
                                                },
                                              ),
//                                          renderResources(message.text)
                                            ],
                                          ),

                                          /*padding: EdgeInsets.all(0.0),
                                          margin: EdgeInsets.all(2),
                                          child: Column(
                                            children: <Widget>[
                                              HtmlWidget(
//                                            """ ${message.text.toString().split(r"##")[0]} """,
                                                """ ${message.text} """,
                                                hyperlinkColor: Colors.blue,
                                                textStyle: TextStyle(
                                                    fontFamily: 'BrandingMedium',
                                                    fontSize: widget.chatTextSize,
                                                    color: Colors.black87
//                                            color: Color(0xff51575C)
                                                    ),
                                              ),
                                              renderResources(message.text)
                                            ],
                                          ),*/
                                        ),
//                                  _highlightText(text),
                                      ],
                                    ),
                                  )),
                              Visibility(
                                visible: message.showReadMore,
                                child: Container(
                                  margin: EdgeInsets.only(right: 30),
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: GestureDetector(
                                      onTap: () {
                                        handleReadMore(message,
                                            message.nextMessage, index);
                                      },
                                      child: Container(
                                        padding: EdgeInsets.only(
                                            left: 10, right: 10, top: 3),
                                        height: 22,
                                        decoration: BoxDecoration(
//                      color: Color(0xFF4CAF50),
                                            color: headerColor,
                                            borderRadius: new BorderRadius.only(
                                                topLeft: Radius.circular(0),
                                                topRight: Radius.circular(0),
                                                bottomLeft: Radius.circular(7),
                                                bottomRight:
                                                    Radius.circular(7))),
                                        child: Text(
                                          "read more...",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: ScreenUtil().setSp(12)),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )))
              ],
            )
          ],
        ),
      );
    } else if (message.type == ("ERROR")) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  topRight: Radius.circular(12),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(12)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(3.5),
                                    child: new Text(message.text,
                                        style: TextStyle(
                                            color: Colors.red,
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.LOADER)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(0),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                      padding: EdgeInsets.all(2.5),
                                      margin: EdgeInsets.all(3.5),
                                      child: new Image(
                                          width: 35,
                                          fit: BoxFit.fitWidth,
                                          alignment: Alignment.center,
                                          height: 27,
                                          image: new AssetImage(
                                              "images/loader.gif"))),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == ("OPTIONS")) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                    width: maxWidth,
                                    padding: EdgeInsets.all(8.0),
                                    child: new ListView.builder(
                                      shrinkWrap: true,
                                      itemBuilder: (_, int index) =>
                                          message.optionList[index],
                                      itemCount: message.optionList.length,
                                    ),
                                  ),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.VERIFY_OTP)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0, bottom: 5.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(_spacingRound),
                                    child: new Text(message.text,
                                        style: TextStyle(
                                            color: Color(0xff51575C),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  new Container(
                                      padding: EdgeInsets.all(15.0),
                                      child: new Image(
                                          width: 35,
                                          fit: BoxFit.fitWidth,
                                          alignment: Alignment.center,
                                          height: 27,
                                          image: new AssetImage(
                                              "images/typing_loader.gif"))),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (message.type == (MessageType.PERSONAL_DETAILS)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 7.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(_spacingRound),
                                    child: Text("Details provided",
                                        style: TextStyle(
                                            color: Color(0xff51575C),
                                            fontFamily: 'BrandingSemiBold',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Divider(
                                    color: Color.fromRGBO(52, 52, 52, 1),
                                    height: 0.5,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 18, bottom: 7),
                                    child: Text("Name",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                        message.personalDetailModel.name,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Gender",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                        message.personalDetailModel.gender,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Age",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(message.personalDetailModel.age,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Ethnicity",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                        message.personalDetailModel.ethnicity,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil()
                                                .setSp(widget.chatTextSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                ],
                              ),
                            ))))
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      );
    } else if (message.type == (MessageType.DIGNOSED_RELATIVE_DETAILS)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(message.backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                  flex: 1,
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: new Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15),
                                topRight: Radius.circular(15),
                                bottomLeft: Radius.circular(5),
                                bottomRight: Radius.circular(15)),
                          ),
                          elevation: _elevation,
                          color: Colors.white,
                          margin:
                              EdgeInsets.only(right: 10.0, top: 5.0, left: 7.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                padding: EdgeInsets.all(8.0),
                                margin: EdgeInsets.only(
                                    top: 3.5,
                                    bottom: 3.5,
                                    right: 15,
                                    left: 3.5),
                                child: Text("Relative's Details provided",
                                    style: TextStyle(
                                        color: Color(0xff51575C),
                                        fontFamily: 'BrandingSemiBold',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Divider(
                                  color: Color.fromRGBO(52, 52, 52, 1),
                                  height: 0.5),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Gender",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(message.relativeDetailModel.gender,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Age",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(message.relativeDetailModel.age,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Cancer Type",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(message.relativeDetailModel.cancer,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil()
                                            .setSp(widget.chatTextSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                            ],
                          ))),
                )
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      );
    } else if (message.type == (MessageType.DIVIDER)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: ScreenUtil.screenHeightDp * 0.045,
            ),
            Row(children: <Widget>[
              Expanded(
                child: new Container(
                    margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                    child: Divider(
                      color: Color(0xFF798797),
                      height: 36,
                    )),
              ),
              Text(
                message.text,
                style: TextStyle(color: Color(0xFF687889)),
              ),
              Expanded(
                child: new Container(
                    margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                    child: Divider(
                      color: Color(0xFF798797),
                      height: 36,
                    )),
              ),
            ]),
            SizedBox(
              height: ScreenUtil.screenHeightDp * 0.022,
            ),
          ],
        ),
      );
    } else if (message.type == (MessageType.WELCOME_IMAGE)) {
      return new Container(
        width: ScreenUtil.screenWidthDp,
        margin: EdgeInsets.all(50),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Flexible(
                    child: new Container(
                        alignment: Alignment.center,
                        child: Html(
                          data: """ ${message.text} """,
                          defaultTextStyle: TextStyle(
                              fontFamily: 'BrandingMedium',
                              color: Color(0xff51575C)),
                          linkStyle: const TextStyle(
                            color: Colors.redAccent,
                          ),
                        )))
              ],
            )
          ],
        ),
      );
    }
  }

  double _getVerticalSpacing(int backgroundType) {
    switch (backgroundType) {
      case -1:
        return 8.0;
        break;
      case 0:
        return 8.0;
        break;
      case 1:
        return 8.0;
        break;
      case 2:
        return 8.0;
        break;
      default:
        return 8.0;
    }
  }

  RoundedRectangleBorder _renderBackground(int backgroundType) {
    /* return RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(2),
          topRight: Radius.circular(2),
          bottomLeft: Radius.circular(12),
          bottomRight: Radius.circular(12)),
    );*/

    switch (backgroundType) {
      case 0:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
        break;
      case 1:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
        break;
      case 2:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15)),
        );
        break;
      default:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
    }
  }

  void scrollList() {
    /* Timer(
        Duration(milliseconds: 300),
            () => _scrollController
            .jumpTo(_scrollController.position.maxScrollExtent));*/
    Timer(
        Duration(milliseconds: 200),
        () => _scrollController.animateTo(
            _scrollController.position.maxScrollExtent,
            curve: Curves.linear,
            duration: Duration(milliseconds: 300)));
  }

  void scrollListToPosition(int position) {
    print(
        "SCOLLED POSITION (${position + 1}) AND LIST LENGTH (${_list.length})");
    if ((position + 1) == _list.length) {
      print("SCOLLED LIST TO LAST");
      scrollList();
      return;
    }
    print("SCOLLED LIST TO NEWLY ADDED CELL");
    Timer(
        Duration(milliseconds: 200),
        () => _scrollController.animateTo(_scrollController.offset + 50,
            curve: Curves.linear, duration: Duration(milliseconds: 300)));
  }

  void handleUIVisibility() {
    setState(() {
      _isDisabled = false;
    });
    hideAll();
    if (_list.length == 3) {
      _getRecommendations();
    } else {
      setState(() {
        _autoSuggestionHeight = 132.0;
        _showRecommendation = true;
        _isAutoSuggestionEnabled = false;
      });
    }
  }

  void hideAll() {
    // Hide All UI Here under setState
    setState(() {
//      _showRecommendation = false;
      _isAutoSuggestionEnabled = false;
    });
  }

  void displayRetry(String title, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("$title"),
        content: new Text("$message"),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text("Retry", style: TextStyle(color: alertBtnClr)),
              onPressed: () {
                Navigator.pop(context, 'Cancel');
                if (_currentQuestionID == 1) {
//                  _loadData();
                } else {
//                  _loadData();
                }
              })
        ],
      ),
    );
  }

  _getReadSpeed() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('CURRENT READ SPEED IS : ${prefs.getInt(Log.sp_readSpeed)}');
    setState(() {
      if (prefs.containsKey(Log.sp_readSpeed)) {
        _readSpeed = prefs.getInt(Log.sp_readSpeed);
      } else {
        _readSpeed = Log.normal;
      }
    });
  }

  void _loadInitialMessages() {
    try {
      List<String> arrList = new List();
      arrList.add(
          "Hi, welcome to one more of my cognitive abilities. My creators loaded my memory with more than 500,000 Q&As with 65 million concepts related to diseases, genes, drugs, variants etc.");
      arrList.add(
          "I will provide you with help along the way, I have placed many prompts for you to get started and I can provide related information to your questions also.");
      arrList.add("Ask me a question.");
      Log.printLog("MY DATA", " $arrList");
      for (int index = 0; index < arrList.length; index++) {
        if (index == 0) {
          _staticData.add(ChatMessage(
              showReadMore: false,
              nextMessage: "",
              ID: _currentQuestionID,
              isEditable: false,
              textFontSize: widget.chatTextSize,
              backgroundType: _list.length > 0 ? 0 : -1,
              text: arrList[index],
              type: MessageType.LEFT,
              optionList: null));
        } else if (index == arrList.length - 1) {
          _staticData.add(ChatMessage(
              showReadMore: false,
              nextMessage: "",
              ID: _currentQuestionID,
              isEditable: false,
              textFontSize: widget.chatTextSize,
              backgroundType: 2,
              text: arrList[index],
              type: MessageType.LEFT,
              optionList: null));
        } else {
          _staticData.add(ChatMessage(
              showReadMore: false,
              nextMessage: "",
              ID: _currentQuestionID,
              isEditable: false,
              textFontSize: widget.chatTextSize,
              backgroundType: 1,
              text: arrList[index],
              type: MessageType.LEFT,
              optionList: null));
        }
      }
      showStaticMessages();
    } on SocketException catch (_) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
      setState(() {
        _list.removeLast();
        /*  _list.add(ChatMessage(
        ID: _currentQuestionID,isEditable: false,textFontSize: widget.textFontSize,
            text: "Please check your internet connection and try again.",
            type: "ERROR",
            optionList: null,backgroundType: 0));*/
      });
      displayRetry("", "Please check internet connection and try again.");
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _scrollController.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  _getRecommendations() async {
    String url =
        "http://pro.genefax.ai/GeneFAXWebRestHCS/mastercontroller/getRecommendationsCategory?cat=4&pin=51218&authtoken=tSN4DRaEQ5";

    Log.printLog("API URL", url);

    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
      var data = await http.post(url);
      var jsonData = json.decode(data.body);
      Log.printLog("API RESPONSE", jsonData["data"].toString());

      var jsonArray = jsonData["data"];
      List<String> _questionList = new List();
      for (int index = 0; index < jsonArray.length; index++) {
        _questionList.add("${jsonArray[index]["Utterance"]}?");
      }
      if (_questionList.length > 4) {
        recommendationList.clear();
        for (int index = 0; index < 4; index++) {
          recommendationList
              .add(new DiseaseModel(id: index, name: _questionList[index]));
        }
      } else if (_questionList.length < 4) {
        recommendationList.clear();
        for (int index = 0; index < _questionList.length; index++) {
          recommendationList
              .add(new DiseaseModel(id: index, name: _questionList[index]));
        }
      }

      setState(() {
        if (_questionList.length > 4) {
          _autoSuggestionHeight = 132.0;
        } else if (_questionList.length >= 1 && _questionList.length <= 4) {
          _autoSuggestionHeight = (33 * _questionList.length).toDouble();
        } else {
          _autoSuggestionHeight = 0;
        }
        loading = false;
        _showRecommendation = true;
        _isAutoSuggestionEnabled = false;
      });
      scrollList();
    }
  }

  _getAutoSuggestionQuestionList(String search) async {
    /*String url =
        "${Log.API_STACK_MYRIAD}getAutosuggestMPretest?text=${search.length > 0 ? search : ""}";*/
    if (search.length == 0) {
      setState(() {
        _isAutoSuggestionEnabled = false;
      });
      return;
    }

    String url =
        "http://pro.genefax.ai/GeneFAXWebRestHCS/mastercontroller/getAutosuggestNewWithDistinct?text=$search";

    Log.printLog("API URL", url);

    setState(() {
      _isAutoSuggestionEnabled = true;
    });

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.post(url);
        var jsonData = json.decode(data.body);
        Log.printLog("API RESPONSE", jsonData.toString());
        List<String> _suggestionList = List.from(jsonData);
        Log.printLog("DISEASE LIST : ", "$_suggestionList");
        autoSuggestionList.clear();
        setState(() {
          loading = false;
        });
        if (_suggestionList.length == 0) {
          setState(() {
            _autoSuggestionHeight = 0;
            autoSuggestionList.clear();
          });
          return;
        }

        for (int index = 0; index < _suggestionList.length; index++) {
          autoSuggestionList
              .add(AutoSuggetionModel(id: index, name: _suggestionList[index]));
        }
        setState(() {
          if (_suggestionList.length > 4) {
            _autoSuggestionHeight = 132.0;
          } else if (_suggestionList.length >= 1 &&
              _suggestionList.length <= 4) {
            _autoSuggestionHeight = (33 * _suggestionList.length).toDouble();
          } else {
            _autoSuggestionHeight = 0;
          }
        });
        scrollList();
      }
    } on SocketException catch (_) {
      Log.printLog("INTERNET CONNECTION STATUS : ", "NOT CONNECTED");
//      displayRetry("", "Please check internet connection and try again.");
    }
  }

  _askQuestion(String question) async {
    FocusScope.of(context).requestFocus(FocusNode());
    if (_isDisabled) {
      return;
    }

    if (question.trim().length <= 1) {
      Toast.show("Please enter your question", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      return;
    }

    _tec_ask_question.clear();
    hideAll();
    setState(() {
      _isDisabled = true;
    });
    ChatMessage object3 = new ChatMessage(
        showReadMore: false,
        nextMessage: "",
        ID: _currentQuestionID,
        isEditable: false,
        textFontSize: widget.chatTextSize,
        text: question,
        type: MessageType.RIGHT,
        personalDetailModel: null,
        relativeDetailModel: null,
        optionList: null,
        backgroundType: 0);

    setState(() {
      _list.add(object3);
    });
    scrollList();

    Future.delayed(Duration(milliseconds: 300), () async {
      String url =
          "http://pro.genefax.ai/GeneFAXWebRestHCS/mastercontroller/getAnswerFreeTextWithoutCrawlerandNeutral?question=$question&category_type=webHCSPro&pin=51218&instance=webHCSPro&authtoken=tSN4DRaEQ5";
      Log.printLog("API URL", url);
      try {
        setState(() {
          _list.add(ChatMessage(
              showReadMore: false,
              nextMessage: "",
              ID: _currentQuestionID,
              isEditable: false,
              textFontSize: widget.chatTextSize,
              type: MessageType.LOADER,
              optionList: null,
              backgroundType: 0));
        });
        final result = await InternetAddress.lookup('google.com');
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
          var data = await http.post(url);
          var jsonData = json.decode(data.body);

          Log.printLog("API RESPONSE", jsonData.toString());

          var jsonObject = jsonData['data'][0];
          Log.printLog("QUESTION : ", "${jsonObject['answer']}");

          String serverAnswer = jsonObject['answer'];

          try {
            String TermUrl =
                "http://pro.genefax.ai/GeneFAXWebRestHCS/mastercontroller/getMedicalTermsFromAnswer?text=$serverAnswer";
            final result = await InternetAddress.lookup('google.com');
            if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
              Log.printLog("API URL", TermUrl);
              var data = await http.post(TermUrl);
              var jsonData = json.decode(data.body);
              Log.printLog("API RESPONSE", jsonData.toString());

              setState(() {
                if (_list.last.type == MessageType.LOADER) {
                  _list.removeLast();
                }
              });

              List<String> termList = List.from(jsonData);
              Log.printLog("TERM LIST : ", "$termList");

              String answer = getTermReplacedAnswer(termList, serverAnswer);

              String recommendedQuestions = jsonObject['questions'];
              if (recommendedQuestions.length > 0) {
                List<String> stringList =
                    (jsonDecode(recommendedQuestions) as List<dynamic>)
                        .cast<String>();
                Log.printLog("Next Recommentions", " $stringList");
                pushRecommendations(stringList);
              }

              buildAnswer(answer);
              showStaticMessages();
            }
          } on SocketException catch (_) {
//        displayRetry("", "Please check internet connection and try again.");
          }
        }
      } on SocketException catch (_) {
//        displayRetry("", "Please check internet connection and try again.");
      }
    });
  }

  Widget _getRecommendationWidget(BuildContext context, int index) {
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    return GestureDetector(
      onTap: () {
        vibrateDevice();
        _askQuestion(recommendationList[index].name);
      },
      child: WidgetAnimator(Column(
        children: <Widget>[
          Container(
            color: Colors.white,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 30,
                  decoration: BoxDecoration(
//                      color: Color(0xFF4CAF50),
                      color: headerColor,
                      borderRadius: new BorderRadius.only(
                          topLeft: Radius.circular(0),
                          bottomLeft: Radius.circular(0))),
                  width: ScreenUtil.screenWidthDp * 0.01,
                ),
                SizedBox(
                  width: 5,
                ),
                Text(recommendationList[index].name,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        fontSize: recommendationList[index].name.length > 60
                            ? ScreenUtil().setSp(10)
                            : ScreenUtil().setSp(12)))
              ],
            ),
          ),
          SizedBox(
            height: ScreenUtil.screenHeightDp * 0.005,
          )
        ],
      )),
    );
  }

  Widget _getAutoSuggestionsWidget(BuildContext context, int index) {
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

    return GestureDetector(
      onTap: () {
        vibrateDevice();
        _askQuestion(autoSuggestionList[index].name);
//            _autoSuggestionHeight = 0;
//            autoSuggestionList.clear();
      },
      child: WidgetAnimator(Column(
        children: <Widget>[
          Container(
            color: Colors.white,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 30,
                  decoration: BoxDecoration(
//                      color: Color(0xFF4CAF50),
                      color: headerColor,
                      borderRadius: new BorderRadius.only(
                          topLeft: Radius.circular(0),
                          bottomLeft: Radius.circular(0))),
                  width: ScreenUtil.screenWidthDp * 0.01,
                ),
                SizedBox(
                  width: 5,
                ),
                Flexible(
                    child: Text(autoSuggestionList[index].name,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                            fontSize: autoSuggestionList[index].name.length > 60
                                ? ScreenUtil().setSp(10)
                                : ScreenUtil().setSp(12))))
              ],
            ),
          ),
          SizedBox(
            height: ScreenUtil.screenHeightDp * 0.005,
          )
        ],
      )),
    );
  }

  void pushRecommendations(List<String> jsonArray) {
    recommendationList.clear();
    for (int index = 0; index < jsonArray.length; index++) {
      recommendationList
          .add(new DiseaseModel(id: index, name: jsonArray[index]));
    }
  }

  String getTermReplacedAnswer(List<String> termList, String serverAnswer) {
    String tempAns = serverAnswer;
    List<String> tempTerms = new List<String>();
    /* for (int index = 0; index < termList.length; index++) {
      var regExp = new RegExp(
        "\\b(${termList[index]})\\b",
        caseSensitive: false,
      );
      if (regExp.hasMatch(tempAns)) {
        print("STRING MATCHED : ${termList[index]}");
        final matches = regExp
            .allMatches(tempAns);
        if(matches!=null) {
          final match = matches.elementAt(0);
          if (match != null) {
            tempTerms.add(tempAns.substring(match.start,match.end));
            tempAns = tempAns.substring(match.end + 1,tempAns.length);
            print(" PRASANNA : AFTER SUBSTING : $tempAns");
          }
        }
      }
    }*/

    int counter = 0;

    for (int index = 0; index < termList.length; index++) {
      var regExp = new RegExp(
        "\\b(${termList[index]})\\b",
        caseSensitive: false,
      );
      if (regExp.hasMatch(tempAns)) {
//        print("STRING MATCHED : ${termList[index]}");
        final matches = regExp.allMatches(tempAns);
        if (matches != null) {
          final match = matches.elementAt(0);
          if (match != null) {
            tempTerms.add(tempAns.substring(match.start, match.end));
            counter = counter + 1;
            tempAns = tempAns.replaceFirst(
                tempAns.substring(match.start, match.end), "#${counter}#");
//            print(" PRASANNA : AFTER SUBSTING : $tempAns");
          }
        }
      }
    }

//    print("FINAL MATCHED : $tempTerms");

    for (int index = 0; index < tempTerms.length; index++) {
      String term = "<a href='${tempTerms[index]}'>${tempTerms[index]}</a>";
      tempAns = tempAns.replaceFirst("#${index + 1}#", term);
    }

    /* for(int index=0;index<tempTerms.length;index++){
      var regExp = new RegExp(
        "\\b(${tempTerms[index]})\\b",
        caseSensitive: false,
      );
      if (regExp.hasMatch(Original)) {
        final matches = regExp.allMatches(Original);
       if(matches!=null){
         final match = matches.elementAt(0);
         if (match != null) {
           String word = Original.substring(match.start,match.end);
           print("ORIGINAL WORD : $word");
           String term = "<a href='${word}'>${word}</a>";
           finalAnswer = finalAnswer.replaceFirst(regExp, term);
         }
       }
      }
    }*/
    print("FINAL ANSWER : $tempAns");
    return tempAns;
  }

  Future<String> getTermDetails(String term) async {
    String url =
        "http://pro.genefax.ai/GeneFAXWebRestHCS/mastercontroller/getAnnotationDesc?term=$term&termtype=term&authtoken=tSN4DRaEQ5";
    Log.printLog("API URL", url);
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.post(url);
        var jsonData = json.decode(data.body);
        Log.printLog("API RESPONSE", jsonData.toString());
        var jsonResponse = jsonData['data'][0];
        return jsonResponse['description'];
      }
    } on SocketException catch (_) {}
  }

  void handleReadMore(ChatMessage message, String nextMessage, int index) {
    message.showReadMore = false;
    setState(() {
      _list[index] = message;
    });
    buildAnswer(nextMessage);
    showReadMoreMessage(index);
  }
}
