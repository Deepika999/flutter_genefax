import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:ai/MessageType.dart';
import 'package:ai/PersonalDetailModel.dart';
import 'package:ai/model/CancerRelativeDetail.dart';
import 'package:http/http.dart' as http;
import 'Log.dart';
import 'Options.dart';
import 'SizeConfig.dart';

class ChatMessage extends StatelessWidget {
  BuildContext context;
  String text;
  bool isEditable = false;
  int backgroundType = 0;
  MessageType type;
  int ID;
  PersonalDetailModel personalDetailModel;
  CancerRelativeDetail relativeDetailModel;
  List<Options> optionList;
  String highlightedWords;
  List<TextSpan> spannedList = null;
  List<TextSpan> children = [];

  TextStyle posStyle = TextStyle(
    color: Colors.blue,
    fontFamily: 'BrandingMedium',
  ),
      negStyle = TextStyle(
    color: Color(0xff51575C),
    fontFamily: 'BrandingMedium',
  );

  var _logoSize = 30.0;

  double _spacingRound = 12;

  var _elevation = 3.5;

  double textFontSize = 15.0;
  String nextMessage = "";
  bool showReadMore = false;

  ChatMessage(
      {this.context,
      @required this.isEditable,
      this.text,
      this.ID,
      this.backgroundType,
      this.type,
      this.personalDetailModel,
      this.relativeDetailModel,
      this.optionList,
      this.highlightedWords,
      this.textFontSize,
      this.nextMessage,
      this.showReadMore
      });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    double maxWidth = MediaQuery.of(context).size.width * 0.70;
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);
    if (type==(MessageType.RIGHT)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerRight,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(15),
                                  bottomRight: Radius.circular(5)),
                            ),
                            elevation: _elevation,
                            color: Color(0xff59A9B7),
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 10.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(3.5),
                                    child: new Text(text,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil().setSp(
                                                textFontSize == null
                                                    ? 15.0
                                                    : textFontSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                ],
                              ),
                            )))),
                Visibility(
                  visible: isEditable,
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: (){
                          print("EDIT BUTTON CLICKED : " + text);
                        },
                        child: Image.asset(
                          'images/edit.png',
                          height: 20,
                          width: 20,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                    ],
                  ),
                ),
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/user.png",
                          width: 35, height: 35, alignment: Alignment.center),
                    ))
              ],
            ),
            SizedBox(
              height: 10,
            )
          ],
        ),
      );
    } else if (type==(MessageType.LEFT)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: _renderBackground(backgroundType),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 0.0, left: 10.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  /*new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(12),
                                    child: highlightedWords == null
                                        ? Text(text,
                                        style: TextStyle(
                                            color: Color(0xff51575C),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil
                                                ()().setSp(textSize==null?15.0:textSize)),
                                        overflow: TextOverflow.clip)
                                        : TextHighlight(
                                        words: highlightedWords,
                                        text: text,
                                        textStyle: TextStyle(
                                            color: Color(0xff51575C),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil
                                                ()().setSp(textSize==null?15.0:textSize)),
                                        overflow: TextOverflow.clip),
                                  ),*/
                                  _highlightText(text),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (type==("ERROR")) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 10.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(3.5),
                                    child: new Text(text,
                                        style: TextStyle(
                                            color: Colors.red,
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil().setSp(
                                                textFontSize == null
                                                    ? 15.0
                                                    : textFontSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (type==(MessageType.LOADER)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 12,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 10.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                      padding: EdgeInsets.all(2.5),
                                      margin: EdgeInsets.all(3.5),
                                      child: new Image(
                                          width: 35,
                                          fit: BoxFit.fitWidth,
                                          alignment: Alignment.center,
                                          height: 27,
                                          image: new AssetImage(
                                              "images/typing_loader.gif"))),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (type==("OPTIONS")) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 10.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  /* new Container(
                        margin: EdgeInsets.only(right: 5.0),
                        child: new Text(
                          _name,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ),*/
                                  new Container(
                                    width: maxWidth,
                                    padding: EdgeInsets.all(8.0),
                                    child: new ListView.builder(
                                      shrinkWrap: true,
                                      itemBuilder: (_, int index) =>
                                          optionList[index],
                                      itemCount: optionList.length,
                                    ),
                                  ),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (type==(MessageType.VERIFY_OTP)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 10.0, bottom: 5.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(_spacingRound),
                                    child: new Text(text,
                                        style: TextStyle(
                                            color: Color(0xff51575C),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil().setSp(
                                                textFontSize == null
                                                    ? 15.0
                                                    : textFontSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  new Container(
                                      padding: EdgeInsets.all(15.0),
                                      child: new Image(
                                          width: 35,
                                          fit: BoxFit.fitWidth,
                                          alignment: Alignment.center,
                                          height: 27,
                                          image: new AssetImage(
                                              "images/typing_loader.gif"))),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ))))
              ],
            )
          ],
        ),
      );
    } else if (type==(MessageType.PERSONAL_DETAILS)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                    child: new Align(
                        alignment: Alignment.centerLeft,
                        child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                  bottomLeft: Radius.circular(5),
                                  bottomRight: Radius.circular(15)),
                            ),
                            elevation: _elevation,
                            color: Colors.white,
                            margin: EdgeInsets.only(
                                right: 10.0, top: 5.0, left: 10.0),
                            child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    padding: EdgeInsets.all(8.0),
                                    margin: EdgeInsets.all(_spacingRound),
                                    child: Text("Details provided",
                                        style: TextStyle(
                                            color: Color(0xff51575C),
                                            fontFamily: 'BrandingSemiBold',
                                            fontSize: ScreenUtil().setSp(
                                                textFontSize == null
                                                    ? 15.0
                                                    : textFontSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Divider(
                                    color: Color.fromRGBO(52, 52, 52, 1),
                                    height: 0.5,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 18, bottom: 7),
                                    child: Text("Name",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil().setSp(
                                                textFontSize == null
                                                    ? 15.0
                                                    : textFontSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(personalDetailModel.name,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil().setSp(
                                                textFontSize == null
                                                    ? 15.0
                                                    : textFontSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Gender",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil().setSp(
                                                textFontSize == null
                                                    ? 15.0
                                                    : textFontSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(personalDetailModel.gender,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil().setSp(
                                                textFontSize == null
                                                    ? 15.0
                                                    : textFontSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Age",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil().setSp(
                                                textFontSize == null
                                                    ? 15.0
                                                    : textFontSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(personalDetailModel.age,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil().setSp(
                                                textFontSize == null
                                                    ? 15.0
                                                    : textFontSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 20, top: 7, bottom: 7),
                                    child: Text("Ethnicity",
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(91, 151, 241, 1),
                                            fontFamily: 'BrandingMedium',
                                            fontSize: ScreenUtil().setSp(
                                                textFontSize == null
                                                    ? 15.0
                                                    : textFontSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(personalDetailModel.ethnicity,
                                        style: TextStyle(
                                            color:
                                                Color.fromRGBO(52, 52, 52, 1),
                                            fontFamily: 'BrandingLight',
                                            fontSize: ScreenUtil().setSp(
                                                textFontSize == null
                                                    ? 15.0
                                                    : textFontSize)),
                                        overflow: TextOverflow.clip),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                ],
                              ),
                            ))))
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      );
    } else if (type==(MessageType.DIGNOSED_RELATIVE_DETAILS)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: _getVerticalSpacing(backgroundType),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(right: 0.0),
                    /* child: new CircleAvatar(
                  child: new Text(_name[0]),
                ),*/
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(50.0),
                      child: Image.asset("images/left_logo.png",
                          width: 31, height: 31, alignment: Alignment.center),
                    )),
                Flexible(
                  flex: 1,
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: new Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15),
                                topRight: Radius.circular(15),
                                bottomLeft: Radius.circular(5),
                                bottomRight: Radius.circular(15)),
                          ),
                          elevation: _elevation,
                          color: Colors.white,
                          margin: EdgeInsets.only(
                              right: 10.0, top: 5.0, left: 10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                padding: EdgeInsets.all(8.0),
                                margin: EdgeInsets.only(
                                    top: 3.5,
                                    bottom: 3.5,
                                    right: 15,
                                    left: 3.5),
                                child: Text("Relative's Details provided",
                                    style: TextStyle(
                                        color: Color(0xff51575C),
                                        fontFamily: 'BrandingSemiBold',
                                        fontSize: ScreenUtil().setSp(
                                            textFontSize == null
                                                ? 15.0
                                                : textFontSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Divider(
                                  color: Color.fromRGBO(52, 52, 52, 1),
                                  height: 0.5),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Gender",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil().setSp(
                                            textFontSize == null
                                                ? 15.0
                                                : textFontSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(relativeDetailModel.gender,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil().setSp(
                                            textFontSize == null
                                                ? 15.0
                                                : textFontSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Age",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil().setSp(
                                            textFontSize == null
                                                ? 15.0
                                                : textFontSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(relativeDetailModel.age,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil().setSp(
                                            textFontSize == null
                                                ? 15.0
                                                : textFontSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 7, bottom: 7),
                                child: Text("Cancer Type",
                                    style: TextStyle(
                                        color: Color.fromRGBO(91, 151, 241, 1),
                                        fontFamily: 'BrandingMedium',
                                        fontSize: ScreenUtil().setSp(
                                            textFontSize == null
                                                ? 15.0
                                                : textFontSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(relativeDetailModel.cancer,
                                    style: TextStyle(
                                        color: Color.fromRGBO(52, 52, 52, 1),
                                        fontFamily: 'BrandingLight',
                                        fontSize: ScreenUtil().setSp(
                                            textFontSize == null
                                                ? 15.0
                                                : textFontSize)),
                                    overflow: TextOverflow.clip),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                            ],
                          ))),
                )
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      );
    } else if (type==(MessageType.DIVIDER)) {
      return new Container(
        width: maxWidth,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: ScreenUtil.screenHeightDp * 0.045,
            ),
            Row(children: <Widget>[
              Expanded(
                child: new Container(
                    margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                    child: Divider(
                      color: Color(0xFF798797),
                      height: 36,
                    )),
              ),
              Text(
                text,
                style: TextStyle(color: Color(0xFF687889)),
              ),
              Expanded(
                child: new Container(
                    margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                    child: Divider(
                      color: Color(0xFF798797),
                      height: 36,
                    )),
              ),
            ]),
            SizedBox(
              height: ScreenUtil.screenHeightDp * 0.045,
            ),
          ],
        ),
      );
    }
  }

  RoundedRectangleBorder _renderBackground(int backgroundType) {
    switch (backgroundType) {
      case 0:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
        break;
      case 1:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
        break;
      case 2:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20)),
        );
        break;
      default:
        return RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)),
        );
    }
  }

  double _getVerticalSpacing(int backgroundType) {
    switch (backgroundType) {
      case -1:
        return 12.0;
        break;
      case 0:
        return 30.0;
        break;
      case 1:
        return 12.0;
        break;
      case 2:
        return 12.0;
        break;
      default:
        return 30.0;
    }
  }

  double _getVerticalMargin(int backgroundType) {
    switch (backgroundType) {
      case 0:
        return 5.0;
        break;
      case 1:
        return 5.0;
        break;
      case 2:
        return 5.0;
        break;
      default:
        return 5.0;
    }
  }

  _highlightText(message) {
    if (this.highlightedWords != null && this.highlightedWords.isNotEmpty) {
      spannedList = List<TextSpan>();
      /* List<String> arrWords = highlightedWords.toString().split(',').toList();

    for(int index = 0;index<arrWords.length;index++) {
        spannedList.add(TextSpan(style: TextStyle(fontWeight: FontWeight.bold), text: arrWords[index]));
      }*/

      /*List<String> arrHighlightedWords =
          highlightedWords.toString().split(',').toList();
      Log.printLog("PRASAD : ", "${arrHighlightedWords.length}");

      arrHighlightedWords =
          LinkedHashSet<String>.from(arrHighlightedWords).toList();
      Log.printLog("PRASAD", "AFTER REMOVE : $arrHighlightedWords");*/

      List<String> arrHighlightedWords = [];
      arrHighlightedWords.add("hereditary cancer screening");
      arrHighlightedWords.add("genetic test");
      arrHighlightedWords.add("risk");
      arrHighlightedWords.add("family history");
      List<int> arrMatchingWordsIndex = [];
      for (int index = 0; index < arrHighlightedWords.length; index++) {
        Log.printLog(
            "PRASAD TERM : ", "${arrHighlightedWords[index].toString()}");
        final matches = arrHighlightedWords[index]
            .toLowerCase()
            .allMatches(message.toString().toLowerCase());
        for (var i = 0; i < matches.length; i++) {
          final match = matches.elementAt(i);
          if (match != null) {
            arrMatchingWordsIndex.add(match.start);
            arrMatchingWordsIndex.add(match.end);
            break;
          }
        }
      }
      Log.printLog("FINAL INDEXS : ", "${arrMatchingWordsIndex}");
      if (arrMatchingWordsIndex.length > 0) {
        children = [];
        prepareChildrens(0, message.toString(), arrMatchingWordsIndex);
        Log.printLog("TREM : WORD ARRAY : ", "${children.length}");
        return Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(15.0),
              margin: EdgeInsets.all(3.5),
              child: Text.rich(TextSpan(children: children)),
            ),
            getImageComponent(message)
          ],
        );
      } else {
        return Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(15.0),
              margin: EdgeInsets.all(3.5),
              child: Text(text,
                  style: TextStyle(
                    color: Color(0xff51575C),
                    fontFamily: 'BrandingMedium',
                    fontSize: ScreenUtil()
                        .setSp(textFontSize == null ? 15.0 : textFontSize),
                  ),
                  overflow: TextOverflow.clip),
            ),
            getImageComponent(text)
          ],
        );
      }
    } else {
      return Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(15.0),
            margin: EdgeInsets.all(3.5),
            child: Text(text,
                style: TextStyle(
                  color: Color(0xff51575C),
                  fontFamily: 'BrandingMedium',
                  fontSize: ScreenUtil()
                      .setSp(textFontSize == null ? 15.0 : textFontSize),
                ),
                overflow: TextOverflow.clip),
          ),
          getImageComponent(text)
        ],
      );
    }
  }

  void prepareChildrens(
      int lastIndex, String source, List<int> arrMatchingWordsIndex) {
    Log.printLog("TODAY :",
        "NEW LAST INDEX IS : ${lastIndex} & NEXT TERM WORD START INDEX IS ${arrMatchingWordsIndex[0]}");
    StringBuffer buffer = new StringBuffer();

    while (lastIndex != arrMatchingWordsIndex[0]) {
      buffer.write(source[lastIndex]);
      lastIndex = lastIndex + 1;
    }

    children.add(TextSpan(
      text: buffer.toString(),
      style: TextStyle(
        color: Color(0xff51575C),
        fontFamily: 'BrandingMedium',
        fontSize:
            ScreenUtil().setSp(textFontSize == null ? 15.0 : textFontSize),
      ),
    ));

    String matchingWord = source
        .toString()
        .substring(arrMatchingWordsIndex[0], arrMatchingWordsIndex[1]);

    children.add(TextSpan(
      text: matchingWord,
      recognizer: new TapGestureRecognizer()
        ..onTap = () => showTermDefination(matchingWord),
      style: TextStyle(
        fontFamily: 'BrandingMedium',
        fontSize:
            ScreenUtil().setSp(textFontSize == null ? 15.0 : textFontSize),
        fontWeight: FontWeight.normal,
        color: Colors.blue,
        decoration: TextDecoration.underline,
      ),
    ));

    arrMatchingWordsIndex.removeAt(0);
    lastIndex = arrMatchingWordsIndex[0];
    arrMatchingWordsIndex.removeAt(0);
    if (arrMatchingWordsIndex.length > 0) {
      prepareChildrens(lastIndex, source, arrMatchingWordsIndex);
    } else {
      if (source.length > lastIndex) {
        String matchingWord =
            source.toString().substring(lastIndex, source.length);
        children.add(TextSpan(
          text: matchingWord,
          style: TextStyle(
            color: Color(0xff51575C),
            fontFamily: 'BrandingMedium',
            fontSize:
                ScreenUtil().setSp(textFontSize == null ? 15.0 : textFontSize),
          ),
        ));
      }
      return;
    }
  }

  List<TextSpan> getSpannedString(String message) {
    return children;
  }

  showTermDefination(String term) {
    /*showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return Dialog(
          child: new CircularProgressIndicator(),
        );
      },
    );*/

    getTermDetails(term).then((value) {
      Log.printLog("TERM DEFI", value);
      showDialog(
        context: this.context,
        builder: (BuildContext context) => new CupertinoAlertDialog(
          title: new Text("$term",
              style: TextStyle(color: Color.fromRGBO(22, 175, 201, 1.0))),
          content: new Text("\n${value}"),
          actions: [
            CupertinoDialogAction(
                isDefaultAction: true,
                child: new Text("Close"),
                onPressed: () {
                  Navigator.pop(context, 'Cancel');
                })
          ],
        ),
      );
    });
  }

  Future<String> getTermDetails(String term) async {
    String url =
        "${Log.API_STACK}getAnnotationDesc?term=$term&termtype=term&authtoken=tSN4DRaEQ5";
    Log.printLog("API URL", url);
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.printLog("INTERNET CONNECTION STATUS : ", "CONNECTED");
        var data = await http.post(url);
        var jsonData = json.decode(data.body);
        Log.printLog("API RESPONSE", jsonData.toString());
        var jsonResponse = jsonData['data'][0];
        return jsonResponse['description'];
      }
    } on SocketException catch (_) {}
  }

  getImageComponent(String message) {
    if (message.contains('src=') && message.contains(".png")) {
      var match = message.indexOf(r'src=');
      var format = message.indexOf(r'.png');
      String url = "";

      if (match != null && format != null) {
        url = message.substring(match + 5, format + 4);
        url = url.replaceAll('https', 'http');
        Log.printLog("IMAGE FOUND : ", url);
        if (url.length > 0) {
          return Image.network(url);
        } else {
          return Visibility(
            visible: false,
            child: Image.asset('images/ic_info.png'),
          );
        }
      } else {
        return Visibility(
          visible: false,
          child: Image.asset('images/ic_info.png'),
        );
      }
    } else {
      return Visibility(
        visible: false,
        child: Image.asset('images/ic_info.png'),
      );
    }
  }
}
