import 'dart:math';

import 'package:flutter/material.dart';
import 'package:vector_math/vector_math_64.dart';

class ScrollHandAnimation extends StatefulWidget {
  @override
  _ScrollHandAnimationState createState() => _ScrollHandAnimationState();
}

class _ScrollHandAnimationState extends State<ScrollHandAnimation> with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 5),
    )..addListener(() => setState(() {}));

    animation = Tween<double>(
      begin: 50.0,
      end: 120.0,
    ).animate(animationController);

    animationController.forward();
  }


  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Vector3 _shake() {
    double progress = animationController.value;
    double offset = sin(progress * pi * 8.0);
    return Vector3(0.0, offset * 30 , 0.0);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Transform(
        transform: Matrix4.translation(_shake()),
        child: Container(
          child: Image.asset("images/swipe.png",
              height: 70, width: 70, alignment: Alignment.bottomRight),
        ),
      ),
    );
  }
}