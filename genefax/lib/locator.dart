import 'package:ai/services/dialog_service.dart';
import 'package:ai/services/push_notification_service.dart';
import 'package:get_it/get_it.dart';
import 'package:ai/services/navigation_service.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => DialogService());
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => PushNotificationService());
}