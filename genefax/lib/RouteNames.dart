// Specify route names here

const String RouteSplashScreen = "/SplashScreen";
const String RouteHomePage = "/HomePage";
const String RouteWelcomeDialog = "/OptraWelcomeDialog";
const String RouteCounselorConnect = "/CounselorConnectView";
const String RouteNotifications = "/Notifications";