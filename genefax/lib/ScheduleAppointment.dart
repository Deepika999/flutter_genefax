import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:ai/Constant.dart';

import 'SizeConfig.dart';

class ScheduleAppointment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _StateScheduleAppointment();
  }
}

class _StateScheduleAppointment extends State<ScheduleAppointment> {
  String _email;

  bool _checkedValue = true;

  void displayAlert(String title, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("$title"),
        content: new Text("$message"),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text("Close"),
              onPressed: () {
                Navigator.pop(context, 'Cancel');
              })
        ],
      ),
    );
  }

  Country _selectedDialogCountry = CountryPickerUtils.getCountryByIsoCode('US');

  final TextEditingController _textEditControllerName =
      new TextEditingController();
  final TextEditingController _textEditControllerEmail =
      new TextEditingController();
  final TextEditingController _textEditControllerPhone =
      new TextEditingController();

  @override
  Widget build(BuildContext context) {
    //    0xff4981F9
    SizeConfig().init(context);
    ScreenUtil.init(context,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        allowFontScaling: true);

//    return Scaffold(
//        appBar: AppBar(
//          title: Center(
//            child: Text("Counselor Connect"),
//          ),
//          brightness: Brightness.dark,
//          backgroundColor: headerColor,
//        ),
//        body: Center(
//          child: Text("Comming soon"),
//        ));

    return new Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text("Counselor Connect"),
          ),
          brightness: Brightness.dark,
          backgroundColor: headerColor,
        ),
        body: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Container(
            height: ScreenUtil.screenHeightDp,
            width: ScreenUtil.screenHeightDp,
            color: Color(0xff59A9B7),
            child: Container(
                padding: EdgeInsets.only(left: 32, right: 32),
                child: new SingleChildScrollView(
                    child: new ConstrainedBox(
                  constraints: new BoxConstraints(),
                  child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 25),
                        ),
                        Text(
                          "Please fill below details",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: ScreenUtil().setSp(18),
                              fontFamily: 'BrandingSemiLight'),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 25),
                        ),
                        Text(
                          "We will book an appointment for you\nwith board Certified Genetic Counselor.",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: ScreenUtil().setSp(16),
                              fontFamily: 'BrandingLight'),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 25),
                        ),
                        Text(
                          "Phone Number*",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: ScreenUtil().setSp(20),
                              fontFamily: 'BrandingLight'),
                        ),
                        Row(
                          children: <Widget>[
                            Flexible(
                              child: Container(
                                child: ListTile(
                                  onTap: _openCountryPickerDialog,
                                  title:
                                      _buildDialogItem(_selectedDialogCountry),
                                ),
                              ),
                            ),
                            Flexible(
                              child: Theme(
                                data: new ThemeData(
                                  primaryColor: Colors.white54,
                                  cursorColor: Colors.white,
                                ),
                                child: TextField(
                                  controller: _textEditControllerPhone,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                    hintStyle: TextStyle(color: Colors.white),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white38),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                    ),
                                  ),
                                  keyboardType: TextInputType.phone,
                                ),
                              ),
                              flex: 1,
                            )
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 35),
                        ),
                        Text(
                          "Appointment Date*",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: ScreenUtil().setSp(20),
                              fontFamily: 'BrandingLight'),
                        ),
                        new Theme(
                          data: new ThemeData(
                            primaryColor: Colors.white54,
                            cursorColor: Colors.white,
                          ),
                          child: TextField(
                            controller: _textEditControllerName,
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(color: Colors.white),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white38),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                            ),
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.words,
                            onTap: () {
                              _CustomDatePicker();
                            },
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 15),
                        ),
                        Text(
                          "Available Time Slots*",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: ScreenUtil().setSp(20),
                              fontFamily: 'BrandingLight'),
                        ),
                        new Theme(
                          data: new ThemeData(
                            primaryColor: Colors.white54,
                            cursorColor: Colors.white,
                          ),
                          child: TextFormField(
                            validator: (val) =>
                                !EmailValidator.validate(val, true)
                                    ? 'Not a valid email.'
                                    : null,
                            onSaved: (val) => _email = val,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(color: Colors.white),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white38),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                            ),
                            keyboardType: TextInputType.emailAddress,
                            onTap: () {
                              _CustomSlotsPicker();
                            },
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 15),
                        ),
                        Text(
                          "Notes",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: ScreenUtil().setSp(20),
                              fontFamily: 'BrandingLight'),
                        ),
                        new Theme(
                          data: new ThemeData(
                            primaryColor: Colors.white54,
                            cursorColor: Colors.white,
                          ),
                          child: TextFormField(
                            style: TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(color: Colors.white),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white38),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                            ),
                            keyboardType: TextInputType.text,
                          ),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        CheckboxListTile(
                          controlAffinity: ListTileControlAffinity.leading,
                          title: Text(
                              "I agree to share my details wit GeneFAX and my data will not be shared or used for any other purpose."), //    <-- label
                          value: _checkedValue, onChanged: (bool value) {},
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: RaisedButton(
                            onPressed: () {},
                            color: Colors.transparent,
                            textColor: Colors.white,
                            padding: const EdgeInsets.all(0.0),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(60.0)),
                            child: GestureDetector(
                              onTap: () {
                                _validateData();
                              },
                              child: Container(
                                alignment: Alignment.center,
                                height:
                                    (ScreenUtil.screenHeightDp * 0.15) * 0.40,
                                width: ScreenUtil.screenHeightDp * 0.40,
                                decoration: const BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(60.0))),
//                          padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
                                child: Text("Submit",
                                    style: new TextStyle(
                                        color: Color(0xff454545),
                                        fontFamily: 'BrandingMedium',
                                        fontSize:
                                            (ScreenUtil.screenHeightDp * 0.40) *
                                                0.10)),
                              ),
                            ),
                          ),
                        ),
                      ]),
                ))),
          ),
        ));
  }

  void _openCountryPickerDialog() => showDialog(
        context: context,
        builder: (context) => Theme(
            data: Theme.of(context).copyWith(primaryColor: Colors.pink),
            child: CountryPickerDialog(
                titlePadding: EdgeInsets.all(8.0),
                searchCursorColor: Colors.pinkAccent,
                searchInputDecoration: InputDecoration(hintText: 'Search...'),
                isSearchable: true,
                title: Text('Select your phone code'),
                onValuePicked: (Country country) =>
                    setState(() => _selectedDialogCountry = country),
                itemBuilder: _buildDialogItemForPicker)),
      );

  Widget _buildDialogItem(Country country) => Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(width: 8.0),
          Text(
            "+${country.phoneCode}",
            style: TextStyle(
                color: Colors.white, fontSize: ScreenUtil().setSp(14)),
          )
          /*,
      SizedBox(width: 8.0),
      Flexible(child: Text(country.name))*/
        ],
      );

  Widget _buildDialogItemForPicker(Country country) => Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(width: 8.0),
          Text(
            "+${country.phoneCode}",
            style: TextStyle(color: Colors.black),
          )
          /*,
      SizedBox(width: 8.0),
      Flexible(child: Text(country.name))*/
        ],
      );

//  appBar: new AppBar(
//  title:new Text ("Friendly Chat"),
//  leading: new Container(),
//  ),

  void _CustomDatePicker() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext builder) {
          return Scaffold(
              appBar: AppBar(
                leading: Center(
                  child: new Text("Cancel",
                      textAlign: TextAlign.justify,
                      style: TextStyle(color: Colors.white, fontSize: 17)),
                ),
                backgroundColor: Colors.teal,
                actions: <Widget>[
                  Center(
                    child: Text("Done",
                        style: TextStyle(color: Colors.white, fontSize: 17)),
                  )
                ],
              ),
              body: Container(
                child: CupertinoPicker(
                  magnification: 1.5,
                  backgroundColor: Colors.black87,
                  children: <Widget>[
                    Text(
                      "TextWidget",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    MaterialButton(
                      child: Text(
                        "Button Widget",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.redAccent,
                    ),
                    IconButton(
                      icon: Icon(Icons.home),
                      color: Colors.white,
                      iconSize: 40,
                      onPressed: () {},
                    )
                  ],
                  itemExtent: 50, //height of each item
                  looping: true,
                  onSelectedItemChanged: (int index) {
                    var selectitem = index;
                  },
                ),
              ));
        });
  }

  void _CustomSlotsPicker() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext builder) {
          return Scaffold(
              appBar: AppBar(
                leading: Center(
                  child: new Text("Cancel",
                      textAlign: TextAlign.justify,
                      style: TextStyle(color: Colors.white, fontSize: 17)),
                ),
                backgroundColor: Colors.teal,
                actions: <Widget>[
                  Center(
                    child: Text("Done",
                        style: TextStyle(color: Colors.white, fontSize: 17)),
                  )
                ],
              ),
              body: Container(
                child: CupertinoPicker(
                  magnification: 1.5,
                  backgroundColor: Colors.black87,
                  children: <Widget>[
                    Text(
                      "TextWidget",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    MaterialButton(
                      child: Text(
                        "Button Widget",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.redAccent,
                    ),
                    IconButton(
                      icon: Icon(Icons.home),
                      color: Colors.white,
                      iconSize: 40,
                      onPressed: () {},
                    )
                  ],
                  itemExtent: 50, //height of each item
                  looping: true,
                  onSelectedItemChanged: (int index) {
                    var selectitem = index;
                  },
                ),
              ));
        });
  }

  void _validateData() {
    print("ENTERED EMAIL ID : $_email");
    String name = _textEditControllerName.text;
    String email = _textEditControllerEmail.text;
    String phone = _textEditControllerPhone.text;
    String phoneCode = _selectedDialogCountry.phoneCode.toString();

    if (name.isEmpty) {
      displayAlert("Error", "Please enter your name");
      return;
    }

    /*if (email.length==0) {
      displayAlert("Error", "Please enter your email");
      return;
    }*/
  }
}
