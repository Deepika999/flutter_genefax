import 'package:flutter/material.dart';

class Options extends StatelessWidget {

  final String optionName;

  const Options({Key key, this.optionName}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        Padding(padding: EdgeInsets.only(top: 8.0),),
        new Container(
          padding: EdgeInsets.all(12.0),
          decoration: BoxDecoration(color: Color(0xff2C92E8), borderRadius: new BorderRadius.circular(25.0)),
          child: Text(optionName,style: TextStyle(color: Colors.white,fontFamily: 'BrandingMedium'),textAlign: TextAlign.center,),
        ),
        Padding(padding: EdgeInsets.only(top: 8.0),),
      ],
    );
  }
}
