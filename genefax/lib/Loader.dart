import 'dart:math';

import 'package:flutter/material.dart';
import 'dart:math';


class Loader extends StatefulWidget{

  @override
  _LoaderSate createState() => _LoaderSate();

}

class _LoaderSate extends State<Loader> with SingleTickerProviderStateMixin{
  AnimationController controller;
  Animation<double> animation_Rotation;
  Animation<double> animation_Radius_In;
  Animation<double> animation_Radius_Out;

  final double initialRadius = 30.0;
   double radius = 0.0;

   @override
  void dispose() {
    // TODO: implement dispose
     controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = AnimationController(vsync: this, duration: Duration(seconds: 5),) ;

    animation_Rotation = Tween<double>(
        begin: 0.0,
        end: 1.0
    ).animate(CurvedAnimation(parent: controller,curve: Interval(0.0,1.0,curve: Curves.linear)));


    animation_Radius_In = Tween<double>(
      begin: 1.0,
      end: 0.0
    ).animate(CurvedAnimation(parent: controller,curve: Interval(0.75,1.0,curve: Curves.elasticIn)));

    animation_Radius_Out = Tween<double>(
        begin: 0.0,
        end: 1.0
    ).animate(CurvedAnimation(parent: controller,curve: Interval(0.0,0.25,curve: Curves.elasticOut)));

    controller.addListener((){

      setState(() {
        if(controller.value >= 0.75 && controller.value <= 1.0){
          radius = animation_Radius_In.value * initialRadius;
        }else  if(controller.value >= 0.0 && controller.value <= 0.25) {
          radius = animation_Radius_Out.value * initialRadius;
        }
       });


    });
    controller.repeat();

  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: 100.0,
      height: 100.0,
      child: Center(
        child: RotationTransition(
          turns: animation_Rotation,
          child: Stack(
            children: <Widget>[
              Dot(
                radius: 20.0,
                color: Colors.transparent,
              ),
              Transform.translate(
                offset: Offset(radius * cos(pi/4), radius * sin(pi/4)),
                child: Dot(
                  radius: 7.0,
                  color: Color(0xFF71C043),
                ),
              ),
              Transform.translate(
                offset: Offset(radius * cos(2*pi/4), radius * sin(2*pi/4)),
                child: Dot(
                  radius: 7.0,
                  color: Color(0xFF49AB52),
                ),
              ),
              Transform.translate(
                offset: Offset(radius * cos(3*pi/4), radius * sin(3*pi/4)),
                child: Dot(
                  radius: 7.0,
                  color: Color(0xFFF77053),
                ),
              ),
              Transform.translate(
                offset: Offset(radius * cos(4*pi/4), radius * sin(4*pi/4)),
                child: Dot(
                  radius: 7.0,
                  color: Color(0xFFE43F69),
                ),
              ),
              Transform.translate(
                offset: Offset(radius * cos(5*pi/4), radius * sin(5*pi/4)),
                child: Dot(
                  radius: 7.0,
                  color: Color(0xFF855A8E),
                ),
              ),
              Transform.translate(
                offset: Offset(radius * cos(6*pi/4), radius * sin(6*pi/4)),
                child: Dot(
                  radius: 7.0,
                  color: Color(0xFFB459A0),
                ),
              ),
              Transform.translate(
                offset: Offset(radius * cos(7*pi/4), radius * sin(7*pi/4)),
                child: Dot(
                  radius: 7.0,
                  color: Color(0xFF63CCC6),
                ),
              ),
              Transform.translate(
                offset: Offset(radius * cos(8*pi/4), radius * sin(8*pi/4)),
                child: Dot(
                  radius: 7.0,
                  color: Color(0xFF03ADD0),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Dot extends StatelessWidget{
  final double radius;
  final  Color color;

  Dot({this.radius,this.color});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Container(
        width: this.radius,
        height: this.radius,
        decoration: BoxDecoration(
          color: this.color,
          shape: BoxShape.circle
        ),
      ),
    );
  }
}