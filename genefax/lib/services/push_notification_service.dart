import 'dart:io';

import 'package:ai/RouteNames.dart';
import 'package:ai/locator.dart';
import 'package:ai/services/navigation_service.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class PushNotificationService {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  final NavigationService _navigationService = locator<NavigationService>();

  void onNotificationReceived(bool notificationReceived){

  }

  Future initialise() async {
    if (Platform.isIOS) {
      // request permissions if we're on android
      _fcm.requestNotificationPermissions(IosNotificationSettings());
    }

    _fcm.configure(
      // Called when the app is in the foreground and we receive a push notification
      onMessage: (Map<String, dynamic> message) async {
        print('PRASANNA onMessage: $message');
        onNotificationReceived(true);
      },
      // Called when the app has been closed comlpetely and it's opened
      // from the push notification.
      onLaunch: (Map<String, dynamic> message) async {
        print('PRASANNA onLaunch: $message');
        onNotificationReceived(true);
        _serialiseAndNavigate(message);
      },
      // Called when the app is in the background and it's opened
      // from the push notification.
      onResume: (Map<String, dynamic> message) async {
        print('PRASANNA onResume: $message');
        onNotificationReceived(true);
        _serialiseAndNavigate(message);
      },
    );
  }

  void _serialiseAndNavigate(Map<String, dynamic> message) {
    var notificationData = message['data'];
    var view = notificationData['view'];

    if (view != null) {
      // Navigate to the create post view
      if (view == 'counselor_connect')
        {
          print('PRASANNA COUNSELOR CONNECT');
        _navigationService.navigateTo(RouteCounselorConnect);
      }
    }
  }
}