import 'dart:async';

import 'package:ai/Log.dart';
import 'package:flutter/material.dart';

class ListItemAnimator extends StatefulWidget {
  final Widget child;
  final Duration time;

  ListItemAnimator(this.child, this.time);

  @override
  _ListItemAnimatorState createState() => _ListItemAnimatorState();
}

class _ListItemAnimatorState extends State<ListItemAnimator>
    with SingleTickerProviderStateMixin {
  Timer timer;
  AnimationController animationController;
  Animation animation;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(duration: Duration(milliseconds: Log.LIST_ITEM_ANIM_DURATION), vsync: this);
    animation = CurvedAnimation(parent: animationController, curve: Curves.easeInOut);
    timer = Timer(widget.time, animationController.forward);
  }

  @override
  void dispose() {
    animationController.dispose();
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animation,
      child: widget.child,
      builder: (BuildContext context, Widget child) {
        return Opacity(
          opacity: animation.value,
          child: Transform.translate(
            offset: Offset(0.0, (1 - animation.value) * 10),
            child: child,
          ),
        );
        /*return ScaleTransition(
          scale: animation,
          child: child,
        );*/
      },
    );
  }
}

Timer timer;
Duration duration = Duration();

wait() {
  if (timer == null || !timer.isActive) {
    timer = Timer(Duration(microseconds: 30), () {
      duration = Duration();
    });
  }
  duration += Duration(milliseconds: 30);
  return duration;
}

class WidgetAnimator extends StatelessWidget {
  final Widget child;

  WidgetAnimator(this.child);

  @override
  Widget build(BuildContext context) {
    return ListItemAnimator(child, wait());
  }
}
