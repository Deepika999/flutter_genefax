import 'package:country_pickers/country.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:email_validator/email_validator.dart';

import 'SizeConfig.dart';

//Transition Animation Link
//http://myhexaville.com/2018/05/14/flutter-circular-reveal-with-progress-button/

class RegisterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _StateRegisterPage();
  }
}

class _StateRegisterPage extends State<RegisterPage> {
  String _email;

  void displayAlert(String title, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("$title"),
        content: new Text("$message"),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: new Text("Close"),
              onPressed: () {
                Navigator.pop(context, 'Cancel');
              })
        ],
      ),
    );
  }

  double screenHeightDp;

  Country _selectedDialogCountry = CountryPickerUtils.getCountryByIsoCode('US');

  final TextEditingController _textEditControllerName =
      new TextEditingController();
  final TextEditingController _textEditControllerEmail =
      new TextEditingController();
  final TextEditingController _textEditControllerPhone =
      new TextEditingController();

  @override
  Widget build(BuildContext context) {
    //    0xff4981F9
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Color(0xff4981F9)));
    SizeConfig().init(context);
    screenHeightDp = MediaQuery.of(context).size.width;
    ScreenUtil.init(context, width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height, allowFontScaling: true);

    return new Scaffold(
        body: Container(
      height: ScreenUtil.screenHeightDp,
      width: ScreenUtil.screenHeightDp,
      color: Color(0xff4981F9),
      child: Stack(
        children: <Widget>[
          Container(
            alignment: Alignment.topRight,
            child: Image.asset("images/top_shape.png",
                height: ScreenUtil.screenHeightDp * 0.40,
                width: ScreenUtil.screenHeightDp * 0.50,
                alignment: Alignment.topRight),
          ),
          Container(
              padding: EdgeInsets.only(left: 32, right: 32),
              child: new SingleChildScrollView(
                  child: new ConstrainedBox(
                constraints: new BoxConstraints(),
                child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Container(
                        height: ScreenUtil().setHeight(50),
                        width: ScreenUtil().setWidth(50),
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(
                            top: ScreenUtil.screenHeightDp * 0.07),
//                      0x9978A2FB
                        decoration: new BoxDecoration(
                            color: Color(0x99ecf9fd),
                            borderRadius: new BorderRadius.circular(50.0)),
                        child: BackButton(color: Colors.white),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 40),
                      ),
                      Text(
                        "New\nPromo Code\nGenerate",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(35),
                            fontFamily: 'BrandingBold'),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 25),
                      ),
                      Text(
                        "Please fill below details",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(18),
                            fontFamily: 'BrandingSemiLight'),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 25),
                      ),
                      Text(
                        "We will generate promo code for you\nand share it on your email.",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(18),
                            fontFamily: 'BrandingLight'),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 35),
                      ),
                      Text(
                        "Name*",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(20),
                            fontFamily: 'BrandingLight'),
                      ),
                      new Theme(
                        data: new ThemeData(
                          primaryColor: Colors.white54,
                          cursorColor: Colors.white,
                        ),
                        child: TextField(
                          controller: _textEditControllerName,
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            hintStyle: TextStyle(color: Colors.white),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white38),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                          ),
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.words,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 15),
                      ),
                      Text(
                        "Email*",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(20),
                            fontFamily: 'BrandingLight'),
                      ),
                      new Theme(
                        data: new ThemeData(
                          primaryColor: Colors.white54,
                          cursorColor: Colors.white,
                        ),
                        child: TextFormField(
                          validator: (val) => !EmailValidator.validate(val, true)
                              ? 'Not a valid email.'
                              : null,
                          onSaved: (val) => _email = val,
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            hintStyle: TextStyle(color: Colors.white),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white38),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                          ),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 15),
                      ),
                      Text(
                        "Phone Number*",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(20),
                            fontFamily: 'BrandingLight'),
                      ),
                      Row(
                        children: <Widget>[
                          Flexible(
                            child: Container(
                              child: ListTile(
                                onTap: _openCountryPickerDialog,
                                title: _buildDialogItem(_selectedDialogCountry),
                              ),
                            ),
                          ),
                          Flexible(
                            child: Theme(
                              data: new ThemeData(
                                primaryColor: Colors.white54,
                                cursorColor: Colors.white,
                              ),
                              child: TextField(
                                controller: _textEditControllerPhone,
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                  hintStyle: TextStyle(color: Colors.white),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white38),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                  ),
                                ),
                                keyboardType: TextInputType.phone,
                              ),
                            ),
                            flex: 2,
                          )
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 35),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: RaisedButton(
                          onPressed: () {},
                          color: Colors.transparent,
                          textColor: Colors.white,
                          padding: const EdgeInsets.all(0.0),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(60.0)),
                          child: GestureDetector(
                            onTap: () {
                              _validateData();
                            },
                            child: Container(
                              alignment: Alignment.center,
                              height: (ScreenUtil.screenHeightDp * 0.15) *
                                  0.40,
                              width: ScreenUtil.screenHeightDp * 0.40,
                              decoration: const BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(60.0))),
//                          padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
                              child: Text("Submit",
                                  style: new TextStyle(
                                      color: Color(0xff454545),
                                      fontFamily: 'BrandingMedium',
                                      fontSize:
                                          (ScreenUtil.screenHeightDp *
                                                  0.40) *
                                              0.10)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 25,
                      )
                    ]),
              ))),
        ],
      ),
    ));
  }

  void _openCountryPickerDialog() => showDialog(
        context: context,
        builder: (context) => Theme(
            data: Theme.of(context).copyWith(primaryColor: Colors.pink),
            child: CountryPickerDialog(
                titlePadding: EdgeInsets.all(8.0),
                searchCursorColor: Colors.pinkAccent,
                searchInputDecoration: InputDecoration(hintText: 'Search...'),
                isSearchable: true,
                title: Text('Select your phone code'),
                onValuePicked: (Country country) =>
                    setState(() => _selectedDialogCountry = country),
                itemBuilder: _buildDialogItemForPicker)),
      );

  Widget _buildDialogItem(Country country) => Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(width: 8.0),
          Text(
            "+${country.phoneCode}",
            style: TextStyle(
                color: Colors.white,
                fontSize: ScreenUtil().setSp(14)),
          )
          /*,
      SizedBox(width: 8.0),
      Flexible(child: Text(country.name))*/
        ],
      );

  Widget _buildDialogItemForPicker(Country country) => Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(width: 8.0),
          Text(
            "+${country.phoneCode}",
            style: TextStyle(color: Colors.black),
          )
          /*,
      SizedBox(width: 8.0),
      Flexible(child: Text(country.name))*/
        ],
      );

  void _validateData() {

    print("ENTERED EMAIL ID : $_email");

    String name = _textEditControllerName.text;
    String email = _textEditControllerEmail.text;
    String phone = _textEditControllerPhone.text;
    String phoneCode = _selectedDialogCountry.phoneCode.toString();

    if (name.isEmpty) {
      displayAlert("Error", "Please enter your name");
      return;
    }

    /*if (email.length==0) {
      displayAlert("Error", "Please enter your email");
      return;
    }*/



  }
}

class ScrollView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil.screenHeightDp,
      width: ScreenUtil.screenHeightDp,
      color: Color(0xff59A9B7),
      child: Stack(
        children: <Widget>[
          Container(
            alignment: Alignment.topRight,
            child: Image.asset("images/top_shape.png",
                height: ScreenUtil.screenHeightDp * 0.40,
                width: ScreenUtil.screenHeightDp * 0.50,
                alignment: Alignment.topRight),
          ),
          Container(
              padding: EdgeInsets.only(left: 32, right: 32),
              child: new SingleChildScrollView(
                  child: new ConstrainedBox(
                constraints: new BoxConstraints(),
                child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Container(
                        height: ScreenUtil().setHeight(50),
                        width: ScreenUtil().setWidth(50),
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(
                            top: ScreenUtil.screenHeightDp * 0.07),
//                      0x9978A2FB
                        decoration: new BoxDecoration(
                            color: Color(0x99ecf9fd),
                            borderRadius: new BorderRadius.circular(50.0)),
                        child: BackButton(color: Colors.white),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 40),
                      ),
                      Text(
                        "New\nPromo Code\nGenerate",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(35),
                            fontFamily: 'BrandingBold'),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 25),
                      ),
                      Text(
                        "Please fill below details",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(18),
                            fontFamily: 'BrandingSemiLight'),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 25),
                      ),
                      Text(
                        "We will generate promo code for you\nand share it on your email.",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(18),
                            fontFamily: 'BrandingLight'),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 35),
                      ),
                      Text(
                        "Name*",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(20),
                            fontFamily: 'BrandingLight'),
                      ),
                      new Theme(
                        data: new ThemeData(
                          primaryColor: Colors.white54,
                        ),
                        child: TextField(
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              hintStyle: TextStyle(color: Colors.white)),
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.words,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 15),
                      ),
                      Text(
                        "Email*",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(20),
                            fontFamily: 'BrandingLight'),
                      ),
                      new Theme(
                        data: new ThemeData(
                          primaryColor: Colors.white54,
                          accentColor: Colors.amber,
                        ),
                        child: TextField(
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              hintStyle: TextStyle(color: Colors.white)),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 15),
                      ),
                      Text(
                        "Phone Number*",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(20),
                            fontFamily: 'BrandingLight'),
                      ),
                      /*CountryPickerDropdown(
                        initialValue: 'tr',
                        itemBuilder: _buildDropdownItem,
                        onValuePicked: (Country country) {
                          print("${country.name}");
                        },
                      ),*/
                      new Theme(
                        data: new ThemeData(
                          primaryColor: Colors.white54,
                          accentColor: Colors.amber,
                        ),
                        child: TextField(
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              hintStyle: TextStyle(color: Colors.white)),
                          keyboardType: TextInputType.phone,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 35),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: RaisedButton(
                          onPressed: () {},
                          color: Colors.transparent,
                          textColor: Colors.white,
                          padding: const EdgeInsets.all(0.0),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(60.0)),
                          child: Container(
                            alignment: Alignment.center,
                            height:
                                (ScreenUtil.screenHeightDp * 0.15) * 0.40,
                            width: ScreenUtil.screenHeightDp * 0.40,
                            decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(60.0))),
//                          padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
                            child: Text("Submit",
                                style: new TextStyle(
                                    color: Color(0xff454545),
                                    fontFamily: 'BrandingMedium',
                                    fontSize: (ScreenUtil.screenHeightDp *
                                            0.40) *
                                        0.10)),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 25,
                      )
                    ]),
              ))),
        ],
      ),
    );
  }
}

Widget _buildDropdownItem(Country country) => Container(
      child: Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(
            width: 8.0,
          ),
          Text("+${country.phoneCode}(${country.isoCode})"),
        ],
      ),
    );
