package com.genefax.ai.chat_widget;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.genefax.ai.helper.AppConstants;
import com.flutterdemo.zendeskflutterdemo.helper.RequestManager;
import com.genefax.ai.R;
import com.genefax.ai.helper.CustomEditText;
import com.genefax.ai.helper.DrawableClickListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.zopim.android.sdk.api.ChatApi;
import com.zopim.android.sdk.api.ChatSession;
import com.zopim.android.sdk.api.ZopimChat;
import com.zopim.android.sdk.api.ZopimChatApi;
import com.zopim.android.sdk.data.LivechatChatLogPath;
import com.zopim.android.sdk.data.observers.AccountObserver;
import com.zopim.android.sdk.data.observers.AgentsObserver;
import com.zopim.android.sdk.data.observers.ChatItemsObserver;
import com.zopim.android.sdk.data.observers.ConnectionObserver;
import com.zopim.android.sdk.data.observers.FileSendingObserver;
import com.zopim.android.sdk.model.Account;
import com.zopim.android.sdk.model.Agent;
import com.zopim.android.sdk.model.Connection;
import com.zopim.android.sdk.model.FileSending;
import com.zopim.android.sdk.model.VisitorInfo;
import com.zopim.android.sdk.model.items.RowItem;
import com.zopim.android.sdk.prechat.EmailTranscript;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;


// Total Android Guide Integration
//

// Customising Android Chat Window
//https://developer.zendesk.com/embeddables/docs/android-chat-sdk/customization

// Embedded Chat
// https://developer.zendesk.com/embeddables/docs/android-chat-sdk/sessionapi#starting-a-chat-session

// zendesk Login
// https://optraventureshelp.zendesk.com

// zendesk my login
// https://pkeskaroptraventures19.zendesk.com/chat/login?redirect_to=%2Fchat%2Fagent%3F##!9346684-suiKQ8CIQFDcAx

public class ChatActivity extends AppCompatActivity {

    private static final int PICK_IMAGE_REQUEST = 450;
    private static final int REQUEST_STORAGE_PERMISSION = 150;
    private MyChatAdapter adapter = null;

    ArrayList<MyChatModel> arrMyChatModel = new ArrayList<>();
    ArrayList<MyChatModel> arrStoredChatMessages = new ArrayList<>();

    private LivechatChatLogPath.ChatTimeoutReceiver chatTimeoutReceiver = new LivechatChatLogPath.ChatTimeoutReceiver();

    ConnectionObserver connectionObserver = new ConnectionObserver() {

        public void update(Connection connection) {
            // update is received on a non-UI thread
            Log.e("PRASAD", "Connection Status : " + connection.getStatus());
            Connection.Status status = connection.getStatus();
            switch (status) {
                case CONNECTED:
                    ChatActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (progressor != null && progressor.isShowing()) {
                                    progressor.dismiss();
                                }
                                progressBarChatConnecting.setVisibility(View.GONE);
                                linearLayoutBottom.setVisibility(View.VISIBLE);
                                recyclerViewAnswers.setVisibility(View.GONE);
                                editTextMessage.setEnabled(true);
                                editTextMessage.requestFocus();
                                if (arrMyChatModel.size() == 0) {
                                    startRecurSiveCall(0);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    break;
                case DISCONNECTED:
                    try {
                        if (progressor != null && progressor.isShowing()) {
                            progressor.dismiss();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                case CLOSED:
                    try {
                        if (progressor != null && progressor.isShowing()) {
                            progressor.dismiss();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    /*try {
                        if (progressor != null && progressor.isShowing()) {
                            progressor.dismiss();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                    break;
            }
        }
    };
    private ListView listViewChatMessage = null;
    private ImageView imageViewBackArrow = null;
    private ImageView imageViewConnectionStatus = null;
    private ImageView imageViewCounselorProfilePic = null;
    private ImageView imageViewCounselorProfileStatus = null;
    private TextView textViewCounselorName = null;
    private TextView textViewCounselorDegree = null;

    private ScrollView LinearLayoutAgentAwayOffline = null;
    private AppCompatTextView textViewOfflineMessage = null;
    private CustomEditText editTextMessage = null;
    private ChatApi chat = null;
    private int thisSessionMessageCount = 0;
    private AppCompatEditText editTextEmail = null;
    private AppCompatEditText editTextLeaveMessage = null;
    private AppCompatButton buttonSubmit = null;
    private AppCompatImageView btnEndChat = null;
    private boolean isEndChatPressed = false;
    private ProgressBar progressBarChatConnecting = null;
    private LinearLayout linearLayoutBottom = null;
    private boolean sendButtonNotPressed = false;

    ArrayList<String> arrStartOfSession = new ArrayList<String>();
    private boolean isFirstTime = true;
    private RecyclerView recyclerViewAnswers = null;
    private boolean showDateSeperator = false;
    private KProgressHUD progressor = null;
    private String currentStatus = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);


        /*Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 12) {
            arrStartOfSession.add("Good Morning!");
        } else if (timeOfDay >= 12 && timeOfDay < 17) {
            arrStartOfSession.add("Good Afternoon!");
        } else if (timeOfDay >= 17 && timeOfDay < 21) {
            arrStartOfSession.add("Good Evening!");
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            arrStartOfSession.add("Good Night!");
        }

        arrStartOfSession.add("I'm here to help you figure out what type of genetic testing makes the most sense for you");
        arrStartOfSession.add("You may ask your own question here");*/


        arrStartOfSession.add("Hello! How can I help you?");

       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }*/

        try {
          /*  progressor = new RequestManager().showProgressDialog(ChatActivity.this, "Connecting with Genetic Expert....");
            if (progressor != null && !progressor.isShowing()) {
                progressor.show();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        // ZopimChat.init("RbX1L5qVjdiLVeuG0SWP8Za5R5nXk0dI");
        // ZopimChat.init("H6hF0R1mZSDCocdmie0vL7Y9kX8mRydW");
//        ZopimChat.init("SMz02Q5ULnGpZch1MAcLh1kuhEqqPWwR");
        ZopimChat.init("16xspF9sbIp82AFfl2gvF6lk41Nmqbym").emailTranscript(EmailTranscript.DISABLED); // Nitin Sir Key
//        ZopimChat.init("tDD1j5avhjrahIh3zYaY78zEu2BHK3UA").emailTranscript(EmailTranscript.DISABLED); // MY Account Key
        String chatInfo = "";
//        if (AppConstants.Companion.getENVIRONMENT().equals("development")) {
        if (true) {
            chatInfo = "MOB-DEV-Android-";
        } else {
            chatInfo = "MOB-PROD-Android-";
        }

        VisitorInfo visitorInfo = new VisitorInfo.Builder()
                .name(chatInfo + new RequestManager().getRefereneIDWithoutTimeStamp())
                .build();
        // set visitor info
        ZopimChat.setVisitorInfo(visitorInfo);


        editTextMessage = (CustomEditText) findViewById(R.id.userInputEditText);
        final ImageButton buttonSendMessage = (ImageButton) findViewById(R.id.btnSend);
        final ImageButton buttonAttachment = (ImageButton) findViewById(R.id.button_attachment);
        listViewChatMessage = (ListView) findViewById(R.id.listView_chat_message);
        linearLayoutBottom = (LinearLayout) findViewById(R.id.bottomlayout);
        linearLayoutBottom.setVisibility(View.GONE);
        LinearLayoutAgentAwayOffline = (ScrollView) findViewById(R.id.relativeLayout_agent_offline);
        textViewOfflineMessage = (AppCompatTextView) findViewById(R.id.textView_agent_offline_message);
        imageViewBackArrow = (ImageView) findViewById(R.id.imageView_back_arrow);
        imageViewConnectionStatus = (ImageView) findViewById(R.id.imageView_connection_status_icon);
        imageViewCounselorProfilePic = (ImageView) findViewById(R.id.imageView_agent_profile_pic);
        imageViewCounselorProfileStatus = (ImageView) findViewById(R.id.imageView_agent_profile_status);
        textViewCounselorName = (TextView) findViewById(R.id.textView_counselor_name);
        textViewCounselorDegree = (TextView) findViewById(R.id.textView_counselor_status);

        editTextEmail = (AppCompatEditText) findViewById(R.id.editTextEmail);
        editTextLeaveMessage = (AppCompatEditText) findViewById(R.id.editText_comment);
        buttonSubmit = (AppCompatButton) findViewById(R.id.button_submit);
        btnEndChat = (AppCompatImageView) findViewById(R.id.btnChat);
        progressBarChatConnecting = (ProgressBar) findViewById(R.id.progressBar);
        recyclerViewAnswers = (RecyclerView) findViewById(R.id.recyclerView_answers);
        editTextMessage.setDrawableClickListener(new DrawableClickListener() {
            @Override
            public void onClick(DrawablePosition target) {
                switch (target) {
                    case RIGHT:
//                        new RequestManager().hideSoftKeyboard(ChatActivity.this);
//                        editTextMessage.setText("");
                        break;
                }
            }
        });


        ArrayList<BotSuggestedAnswerModel> arrAnswers = new ArrayList<>();
        arrAnswers.add(new BotSuggestedAnswerModel("Yes"));
        arrAnswers.add(new BotSuggestedAnswerModel("No"));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (progressor != null && progressor.isShowing()) {
                                progressor.dismiss();
                                textViewOfflineMessage.setText(getResources().getString(R.string.zendesk_no_agents_available));
                                textViewCounselorDegree.setText("Offline");
                                LinearLayoutAgentAwayOffline.setVisibility(View.VISIBLE);
                                editTextMessage.setEnabled(false);
                                imageViewConnectionStatus.setImageResource(R.drawable.agent_offline);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }, 1000 * 15);

        /*// Create the FlexboxLayoutMananger, only flexbox library version 0.3.0 or higher support.
        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(getApplicationContext());
        // Set flex direction.
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        // Set JustifyContent.
        flexboxLayoutManager.setJustifyContent(JustifyContent.SPACE_AROUND);*/

        @SuppressLint("WrongConstant") LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayout.VERTICAL, false);
        recyclerViewAnswers.setLayoutManager(manager);

        // Set adapter object.
        BotSuggestedAnswerAdapter viewAdapter = new BotSuggestedAnswerAdapter(arrAnswers, ChatActivity.this);
        recyclerViewAnswers.setAdapter(viewAdapter);


        progressBarChatConnecting.setVisibility(View.GONE);

        btnEndChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new RequestManager().hideSoftKeyboard(ChatActivity.this);
                isEndChatPressed = true;
                onBackPressed();
            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidEmail(editTextEmail.getText().toString())) {
                    if (editTextLeaveMessage.getText().toString().length() > 0) {
                        Toast.makeText(ChatActivity.this, "Message submitted successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(ChatActivity.this, "Please enter message", Toast.LENGTH_SHORT).show();
                    }
                } else if (editTextEmail.getText().toString().length() == 0) {
                    Toast.makeText(ChatActivity.this, "Please enter contact email", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChatActivity.this, "Please enter valid email", Toast.LENGTH_SHORT).show();
                }
            }
        });

        try {
            if (arrMyChatModel == null) {
                arrMyChatModel = new ArrayList<MyChatModel>();
            }
            adapter = new MyChatAdapter(this, arrMyChatModel);
            listViewChatMessage.setAdapter(adapter);

            if (getArrayList("CHAT") != null) {
                arrStoredChatMessages = getArrayList("CHAT");
                /*if (arrStoredChatMessages.size() == 0 && arrMyChatModel.size() == 0) {
                    arrStoredChatMessages.add(new MyChatModel(MessageType.AGENT, "Hello! How can i help you?", "", 0));
                }*/
                if (arrStoredChatMessages != null && arrStoredChatMessages.size() > 0) {
                    arrMyChatModel.addAll(arrStoredChatMessages);
                }
                saveChatMessages(arrMyChatModel, "CHAT");
            } /*else {
                arrStoredChatMessages.add(new MyChatModel(MessageType.AGENT, "Hello! How can i help you?", "", 0));
                arrMyChatModel.addAll(arrStoredChatMessages);
                saveChatMessages(arrMyChatModel, "CHAT");
            }*/
            arrMyChatModel = getDateSeparator();
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }

        imageViewBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new RequestManager().hideSoftKeyboard(ChatActivity.this);
                onBackPressed();
            }
        });

//        editTextMessage.setFocusable(false);

        buttonAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                            ChatActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQUEST_STORAGE_PERMISSION
                    );
                } else {
                    openImagePickerChooser();
                }
            }
        });

        editTextMessage.setEnabled(false);

        chat = new ZopimChatApi.SessionConfig()
                .department("DemoDepartment")
                .tags("tag1", "tag2")
                .build(this);

        adapter = new MyChatAdapter(this, arrMyChatModel);
        listViewChatMessage.setAdapter(adapter);

        /*if (arrMyChatModel.size() > 0) {
            btnEndChat.setVisibility(View.VISIBLE);
        } else {
            btnEndChat.setVisibility(View.GONE);
        }*/

        scrollToLast();

        editTextMessage.setDrawableClickListener(new DrawableClickListener() {
            @Override
            public void onClick(DrawablePosition target) {
                if (target == DrawablePosition.RIGHT) {
                    editTextMessage.setText("");
                }
            }
        });

        editTextMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                /*if (charSequence.length() > 0) {
                    buttonSendMessage.setVisibility(View.VISIBLE);
                } else {
                    buttonSendMessage.setVisibility(View.GONE);
                }*/
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        buttonSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextMessage.getText().toString().trim().length() > 0) {
                    isFirstTime = true;
                    sendButtonNotPressed = true;
                    chat.send(editTextMessage.getText().toString().trim());
                    editTextMessage.setText("");
                } else {
                    Snackbar.make(view, "Please enter message", 500).show();
                }
                /**
                 * If you want zendesk own UI for chat screen. Call below line to do that.
                 * startActivity(new Intent(getApplicationContext(), ZopimChatActivity.class));
                 * */
            }
        });
    }

    public void submitAnswer(String strAnswer) {
        isFirstTime = true;
        sendButtonNotPressed = true;
        recyclerViewAnswers.setVisibility(View.GONE);
        chat.send(strAnswer);
    }

    private void startRecurSiveCall(final int index) {
        if (index < arrStartOfSession.size()) {
            removeTypingData(); // startRecurSiveCall
            arrMyChatModel.add(new MyChatModel(MessageType.AGENT, arrStartOfSession.get(index), "", Calendar.getInstance().getTimeInMillis(), false));
            Log.e("DATA", "ARRAY SIZE : " + arrMyChatModel.size());
            saveChatMessages(arrMyChatModel, "CHAT");
            if (adapter == null) {
                adapter = new MyChatAdapter(ChatActivity.this, arrMyChatModel);
                listViewChatMessage.setAdapter(adapter);
            } else {
                adapter.notifyDataSetChanged();
//                adapter.updateItems(arrMyChatModel);
            }
            Log.e("INDEX :", String.valueOf(index));
            if (index < arrStartOfSession.size() - 1) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        arrMyChatModel.add(new MyChatModel(MessageType.AGENT, "", "", 00, false));
                        adapter.notifyDataSetChanged();
//                        adapter.updateItems(arrMyChatModel);
                        scrollToLast();
                    }
                }, 1500);
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startRecurSiveCall(index + 1);
                }
            }, 6000);
        }
    }

    private ArrayList<MyChatModel> getDateSeparator() {

        ArrayList<MyChatModel> temp = new ArrayList<>();

        if (showDateSeperator) {
            SimpleDateFormat dateFormator = new SimpleDateFormat("dd-MM-yyyy");
            String strCurrentDate = dateFormator.format(Calendar.getInstance().getTime());
            Log.d("DAY ARRAY SIZE : ", String.valueOf(arrMyChatModel.size()));
            String strSeparator = "";
            for (int index = 0; index < arrMyChatModel.size(); index++) {
                Log.d("DAY DIFFERENCE INDEX : ", String.valueOf(index));
                Log.d("DAY DIFFERENCE DATE : ", String.valueOf(dateFormator.format(arrMyChatModel.get(index).getTimeStamp())));
                if (arrMyChatModel.get(index).getTimeStamp() > 0) {
                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.setTimeInMillis(arrMyChatModel.get(index).getTimeStamp());
                    if (strSeparator.length() == 0) {
                        strSeparator = dateFormator.format(calendar1.getTime());
                        // CHECK AND ADD SEPARATOR TO ARRAY
                        if (strSeparator.equals(strCurrentDate)) {
                            // DISPLAY TODAY
                            temp.add(new MyChatModel(MessageType.DATE_SEPARATOR, "Today", "", 0, false));
                            temp.add(arrMyChatModel.get(index));
                        } else {
                            String date[] = strSeparator.split("-");
                            String currentDate[] = strCurrentDate.split("-");
                            if ((Integer.parseInt(currentDate[0])) - (Integer.parseInt(date[0])) == 1) {
                                if ((Integer.parseInt(currentDate[1])) - (Integer.parseInt(date[1])) == 0) {
                                    // DISPLAY YESTERDAY
                                    temp.add(new MyChatModel(MessageType.DATE_SEPARATOR, "Yesterday", "", 0, false));
                                    temp.add(arrMyChatModel.get(index));
                                } else if ((Integer.parseInt(currentDate[1])) - (Integer.parseInt(date[1])) < 0) {
                                    // DISPLAY DATE
                                    temp.add(new MyChatModel(MessageType.DATE_SEPARATOR, strSeparator, "", 0, false));
                                    temp.add(arrMyChatModel.get(index));
                                }
                            } else {
                                // if ((Integer.parseInt(currentDate[0])) - (Integer.parseInt(date[0])) < 0)
                                // DISPLAY DATE
                                temp.add(new MyChatModel(MessageType.DATE_SEPARATOR, strSeparator, "", 0, false));
                                temp.add(arrMyChatModel.get(index));
                            }
                        }
                    } else {
                        if (strSeparator.equals(dateFormator.format(calendar1.getTime()))) {
                            // SKIP
                            temp.add(arrMyChatModel.get(index));
                        } else {
                            // NEW DAY FOUND
                            strSeparator = dateFormator.format(calendar1.getTime());
                            // CHECK AND ADD SEPARATOR TO ARRAY
                            if (strSeparator.equals(strCurrentDate)) {
                                // DISPLAY TODAY
                                temp.add(new MyChatModel(MessageType.DATE_SEPARATOR, "Today", "", 0, false));
                                temp.add(arrMyChatModel.get(index));
                            } else {
                                String date[] = strSeparator.split("-");
                                String currentDate[] = strCurrentDate.split("-");
                                if ((Integer.parseInt(currentDate[0])) - (Integer.parseInt(date[0])) == 1) {
                                    if ((Integer.parseInt(currentDate[1])) - (Integer.parseInt(date[1])) == 0) {
                                        // DISPLAY YESTERDAY
                                        temp.add(new MyChatModel(MessageType.DATE_SEPARATOR, "Yesterday", "", 0, false));
                                        temp.add(arrMyChatModel.get(index));
                                    } else if ((Integer.parseInt(currentDate[1])) - (Integer.parseInt(date[1])) < 0) {
                                        // DISPLAY DATE
                                        temp.add(new MyChatModel(MessageType.DATE_SEPARATOR, strSeparator, "", 0, false));
                                        temp.add(arrMyChatModel.get(index));
                                    }
                                } else {
                                    // if ((Integer.parseInt(currentDate[0])) - (Integer.parseInt(date[0])) < 0)
                                    // DISPLAY DATE
                                    temp.add(new MyChatModel(MessageType.DATE_SEPARATOR, strSeparator, "", 0, false));
                                    temp.add(arrMyChatModel.get(index));
                                }
                            }
                        }
                    }
                /*Date date1 = new Date(temp.get(index).getTimeStamp());
                long difference = Math.abs(date1.getTime() - date2.getTime());
                long differenceDates = difference / (24 * 60 * 60 * 1000);
                Log.d("DAY DIFFERENCE : ", String.valueOf(differenceDates));*/
                } else {
                    Log.d("DAY DIFFERENCE : ", String.valueOf("NO TIMESTAMP FOUND"));
                    temp.add(arrMyChatModel.get(index));
                }
            }
        } else {
            temp.addAll(arrMyChatModel);
        }
        arrMyChatModel.clear();
        return temp;
    }

    public Boolean isValidEmail(String email) {
        return email.length() != 0 && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public void saveChatMessages(ArrayList<MyChatModel> list, String key) {

        removeDateSeparator();

        SharedPreferences prefs = getSharedPreferences(AppConstants.Companion.getPREFS_NAME(), MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        Log.e("CHAT DATA", json);
        editor.putString(key, json);
        editor.apply();
    }

    public ArrayList<MyChatModel> getArrayList(String key) {
        try {
            SharedPreferences prefs = getSharedPreferences(AppConstants.Companion.getPREFS_NAME(), MODE_PRIVATE);
            Gson gson = new Gson();
            String json = prefs.getString(key, null);
            Type type = new TypeToken<ArrayList<MyChatModel>>() {
            }.getType();
            return gson.fromJson(json, type);
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        } catch (IndexOutOfBoundsException ie) {
            ie.printStackTrace();
        }
        return null;
    }

    private void scrollToLast() {
        if (listViewChatMessage != null) {
            listViewChatMessage.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
//            listViewChatMessage.setSelection(adapter.getCount());
//            listViewChatMessage.post(new PositionScroller(this));
//            listViewChatMessage.setFriction(10000);
            listViewChatMessage.postDelayed(new Runnable() {
                @Override
                public void run() {
                    listViewChatMessage.setSmoothScrollbarEnabled(true);
                    listViewChatMessage.smoothScrollToPosition(adapter.getCount() - 1);
                }
            }, 2000);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        ZopimChatApi.getDataSource().addConnectionObserver(connectionObserver);
        ZopimChatApi.getDataSource().addAccountObserver(accountObserver);
        ZopimChatApi.getDataSource().addFileSendingObserver(fileSendingObserver);
        ZopimChatApi.getDataSource().addChatLogObserver(chatItemsObserver);
        ZopimChatApi.getDataSource().addAgentsObserver(agentsObserver);
        LocalBroadcastManager.getInstance(this).registerReceiver(chatTimeoutReceiver, new IntentFilter(ChatSession.ACTION_CHAT_SESSION_TIMEOUT));
    }


    @Override
    protected void onStop() {
        super.onStop();
        // and remember to remove the observer onStop()
        ZopimChatApi.getDataSource().deleteAccountObserver(accountObserver);
        ZopimChatApi.getDataSource().deleteFileSendingObserver(fileSendingObserver);
        ZopimChatApi.getDataSource().deleteAgentsObserver(agentsObserver);
        ZopimChatApi.getDataSource().deleteChatLogObserver(chatItemsObserver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(chatTimeoutReceiver);
        ZopimChatApi.getDataSource().deleteConnectionObserver(connectionObserver);
    }

    @Override
    protected void onPause() {
        if (arrMyChatModel != null && arrMyChatModel.size() > 0) {
            saveChatMessages(arrMyChatModel, "CHAT");
        }
        super.onPause();
    }

    public class ChatTimeoutReceiver extends BroadcastReceiver {

        public void onReceive(Context context, Intent intent) {
            if (intent != null && ChatSession.ACTION_CHAT_SESSION_TIMEOUT.equals(intent.getAction())) {
                Log.e("PRASAD", "TIME OUT: ");
            }
        }
    }

    ChatItemsObserver chatItemsObserver = new ChatItemsObserver(ChatActivity.this) {
        @Override
        protected void updateChatItems(final TreeMap<String, RowItem> chatItems) {

            ChatActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("CHAT ITEMS", "updateChatItems called");
                    if (arrMyChatModel != null) {
                        arrMyChatModel.clear();
                        if (getArrayList("CHAT") != null) {
                            arrStoredChatMessages = getArrayList("CHAT");
                            if (arrStoredChatMessages != null && arrStoredChatMessages.size() > 0) {
                                arrMyChatModel.addAll(arrStoredChatMessages);
                            }
                        }

                        try {
//                        if (chatItems.size() > 0 && sendButtonNotPressed) {
                            if (chatItems.size() > 0) {
//                            Log.e("LAST TIME STAMP : ", "1 : " + chatItems.lastEntry().getValue().getTimestamp() + " 2 : " + arrMyChatModel.get(arrMyChatModel.size() - 1).getTimeStamp());
                                Map.Entry<String, RowItem> lastEntry = chatItems.lastEntry();
                                if (lastEntry.getValue().getType() == RowItem.Type.AGENT_MESSAGE || lastEntry.getValue().getType() == RowItem.Type.VISITOR_MESSAGE || lastEntry.getValue().getType() == RowItem.Type.VISITOR_ATTACHMENT || lastEntry.getValue().getType() == RowItem.Type.AGENT_ATTACHMENT || lastEntry.getValue().getType() == RowItem.Type.MEMBER_EVENT || lastEntry.getValue().getType() == RowItem.Type.CHAT_EVENT) {
                                    if (arrMyChatModel.get(arrMyChatModel.size() - 1).getTimeStamp() == 0 || lastEntry.getValue().getTimestamp() != arrMyChatModel.get(arrMyChatModel.size() - 1).getTimeStamp()) {
                                        try {
                                            for (Map.Entry<String, RowItem> entry : chatItems.entrySet()) {
                                                String key = entry.getKey();
                                                Log.e("MESSAGE", "USER TYPE : " + entry.getValue().getType());
                                                Log.e("MESSAGE", "USER NAME : " + entry.getValue().getDisplayName());
                                                Log.e("MESSAGE", "USER MESSAGE : " + entry.getValue().toString());

                                                String msg = chatItems.get(key).toString().replace("msg:", "").toString().split(":")[0];
                                                String finalMsg = "";
                                                try {
                                                    if (msg.length() > 6) {
                                                        String finalString = msg.substring(msg.length() - 6, msg.length());
                                                        Log.d("TAG", "LAST KEYWORD : " + finalString);
                                                        if (finalString.equals("failed")) {
                                                            // failed
                                                            int lastIndex = msg.lastIndexOf("f");
                                                            finalMsg = msg.substring(0, lastIndex);
                                                        } else {
                                                            // type
                                                            int lastIndex = msg.lastIndexOf(" ");
                                                            finalMsg = msg.substring(0, lastIndex);
                                                        }
                                                    } else {
                                                        finalMsg = msg;
                                                    }
                                                    Log.e("MESSAGE", "USER MSG  : " + finalMsg);
                                                    if (entry.getValue().getType() != RowItem.Type.CHAT_EVENT && entry.getValue().getType() != RowItem.Type.MEMBER_EVENT) {
                                                        if (entry.getValue().getType() == RowItem.Type.VISITOR_MESSAGE) {
                                                            // VISITOR MESSAGE
                                                            arrMyChatModel.add(new MyChatModel(MessageType.USER, finalMsg, "", entry.getValue().getTimestamp(), false));
                                                        } else if (entry.getValue().getType() == RowItem.Type.AGENT_MESSAGE) {
                                                            // AGENT MESSAGE
                                                            arrMyChatModel.add(new MyChatModel(MessageType.AGENT, finalMsg, entry.getValue().getDisplayName().toString(), entry.getValue().getTimestamp(), false));
                                                        } else if (entry.getValue().getType() == RowItem.Type.VISITOR_ATTACHMENT) {
                                                            // VISITOR ATTACHMENT
                                                            int lastIndex = chatItems.toString().lastIndexOf("uploadUrl");
                                                            String strUploadedImageUrl = chatItems.toString().substring(lastIndex, chatItems.toString().length());
                                                            strUploadedImageUrl = strUploadedImageUrl.replace("uploadUrl:", "");
                                                            Log.e("FILE URL", strUploadedImageUrl.split(" ")[0]);
                                                            arrMyChatModel.add(new MyChatModel(MessageType.USER_ATTACHMENT, strUploadedImageUrl.split(" ")[0], "", entry.getValue().getTimestamp(), false));
                                                        } else if (entry.getValue().getType() == RowItem.Type.AGENT_ATTACHMENT) {
                                                            Log.e("AGENT", "USER NAME : " + entry.getValue().getType());
                                                            // AGENT ATTACHMENT
                                                            int lastIndex = chatItems.toString().lastIndexOf("attachUrl");
                                                            String strUploadedImageUrl = chatItems.toString().substring(lastIndex, chatItems.toString().length());
                                                            strUploadedImageUrl = strUploadedImageUrl.replace("attachUrl:", "");
                                                            Log.e("FILE URL", strUploadedImageUrl.split(" ")[0]);
                                                            arrMyChatModel.add(new MyChatModel(MessageType.AGENT_ATTACHMENT, strUploadedImageUrl.split(" ")[0], "", entry.getValue().getTimestamp(), false));
                                                        }
                                                    } else if (entry.getValue().getType() == RowItem.Type.MEMBER_EVENT) {
                                                        Log.e("MEMBER EVENT OCCUR", entry.getValue().toString());
                                                        arrMyChatModel.add(new MyChatModel(MessageType.AGENT, entry.getValue().toString().split("event:")[1], entry.getValue().getDisplayName(), entry.getValue().getTimestamp(), false));
                                                    } else if (entry.getValue().getType() == RowItem.Type.CHAT_EVENT) {
                                                        Log.e("CHAT EVENT OCCUR", "We apologize for keeping you waiting. Our operators are busy at the moment, please leave us a message with your email address and we'll get back to you shortly.");
                                                        if (arrMyChatModel.size() > 0 && arrMyChatModel.get(arrMyChatModel.size() - 1).getType().equals(MessageType.AGENT) && arrMyChatModel.get(arrMyChatModel.size() - 1).getMessage().equals("We apologize for keeping you waiting. Our operators are busy at the moment, please leave us a message with your email address and we'll get back to you shortly.")) {
                                                            // Do Nothing
                                                        } else {
                                                            arrMyChatModel.add(new MyChatModel(MessageType.AGENT, "We apologize for keeping you waiting. Our operators are busy at the moment, please leave us a message with your email address and we'll get back to you shortly.", entry.getValue().getDisplayName(), entry.getValue().getTimestamp(), false));
                                                        }
                                                    }
                                                } catch (StringIndexOutOfBoundsException se) {
                                                    // Right now this exception occurs when agent leave chats room from website.
                                                }
                                            }
                                        } catch (NullPointerException ne) {
                                            ne.printStackTrace();
                                        }
                                    }
                                } else {
                                    Log.e("CHAT EVENT OCCUR", "We apologize for keeping you waiting. Our operators are busy at the moment, please leave us a message with your email address and we'll get back to you shortly.");
                                /*LinearLayoutAgentAwayOffline.setVisibility(View.VISIBLE);
                                textViewOfflineMessage.setText("We apologize for keeping you waiting. Our operators are busy at the moment, please leave us a message with your email address and we'll get back to you shortly.");
                                editTextMessage.setEnabled(false);*/
                                }
                            }
                        } catch (NullPointerException ne) {
                            ne.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (isFirstTime) {

                           /* Log.e("LOG", "LAST MESSAGE" + arrMyChatModel.get(arrMyChatModel.size() - 1).getMessage().trim());

                            if (arrMyChatModel.get(arrMyChatModel.size() - 1).getMessage().trim().equalsIgnoreCase("Do you want to know more about carrier screening?")) {
                                ChatActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        recyclerViewAnswers.setVisibility(View.VISIBLE);
                                    }
                                });
                            } else {
                                ChatActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        recyclerViewAnswers.setVisibility(View.GONE);
                                    }
                                });
                            }*/

                            arrMyChatModel = getDateSeparator();
                            Log.e("DATA SIZE", String.valueOf(arrMyChatModel.size()));
                            if (adapter == null) {
                                adapter = new MyChatAdapter(ChatActivity.this, arrMyChatModel);
                                listViewChatMessage.setAdapter(adapter);
                            } else {
                                new Handler().post(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapter.updateItems(arrMyChatModel);
                                    }
                                });
                            }
                            scrollToLast();
                            /*if (arrMyChatModel.size() > 1) {
                                btnEndChat.setVisibility(View.VISIBLE);
                            } else {
                                btnEndChat.setVisibility(View.GONE);
                            }*/
                            isFirstTime = false;
                        } else if (arrMyChatModel.size() > 0 && arrMyChatModel.get(arrMyChatModel.size() - 1).getType() == MessageType.AGENT) {
                            /*Log.e("LOG", "LAST MESSAGE" + arrMyChatModel.get(arrMyChatModel.size() - 1).getMessage().trim());

                            if (arrMyChatModel.get(arrMyChatModel.size() - 1).getMessage().trim().equalsIgnoreCase("Do you want to know more about carrier screening?")) {
                                ChatActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        recyclerViewAnswers.setVisibility(View.VISIBLE);
                                    }
                                });
                            } else {
                                ChatActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        recyclerViewAnswers.setVisibility(View.GONE);
                                    }
                                });
                            }*/

                            arrMyChatModel = getDateSeparator();
                            Log.e("DATA SIZE", String.valueOf(arrMyChatModel.size()));
                            //                            adapter = null;
                            if (adapter == null) {
                                adapter = new MyChatAdapter(ChatActivity.this, arrMyChatModel);
                                listViewChatMessage.setAdapter(adapter);
                            } else {
                                new Handler().post(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapter.updateItems(arrMyChatModel);
                                    }
                                });
                            }
                            scrollToLast();
/*                            if (arrMyChatModel.size() > 1) {
                                btnEndChat.setVisibility(View.VISIBLE);
                            } else {
                                btnEndChat.setVisibility(View.GONE);
                            }*/
                            isFirstTime = false;
                        }
                    }
                }
            });
        }
    };

    private void removeDateSeparator() {
        for (int index = 0; index < arrMyChatModel.size(); index++) {
            if (arrMyChatModel.get(index).getType().equals(MessageType.DATE_SEPARATOR)) {
                arrMyChatModel.remove(index);
            }
        }
    }

    AgentsObserver agentsObserver = new AgentsObserver() {
        @Override
        protected void update(final Map<String, Agent> map) {
            Log.e("PRASAD", "AGENT ITEM : " + map);
            if (map != null) {
                ChatActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (Map.Entry<String, Agent> entry : map.entrySet()) {
                            Log.e("PRASAD", "AGENT NAME :::: : " + map.get(entry.getValue().getDisplayName()));
                            Log.e("PRASAD", "AGENT NAME :::: : " + map.get(entry.getKey()));
                            removeTypingData();
                            if (entry.getValue().isTyping() != null && entry.getValue().isTyping()) {
                                // Show Typing
                                /*arrMyChatModel.add(new MyChatModel(MessageType.AGENT, "", map.get(entry.getKey()).toString().trim().split(",")[0], 00, false));
                                new Handler().post(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapter.updateItems(arrMyChatModel);
                                    }
                                });
                                scrollToLast();*/
                                ChatActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        textViewCounselorDegree.setText("typing...");
                                    }
                                });
                            } else {
                                // Remove Typing
                                removeTypingData();
                                ChatActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        textViewCounselorDegree.setText(currentStatus);
                                    }
                                });

                            }
                        }
                    }
                });
            }
        }
    };

    private void removeTypingData() {
        for (int index = 0; index < arrMyChatModel.size(); index++) {
            if (arrMyChatModel.get(index).getMessage().isEmpty()) {
                arrMyChatModel.remove(index);
                adapter.notifyDataSetChanged();
                scrollToLast();
            }
        }
    }

    FileSendingObserver fileSendingObserver = new FileSendingObserver() {
        @Override
        protected void update(final FileSending fileSending) {
            Log.e("PRASAD", "FILE STATUS : " + fileSending.getExtensions());
        }
    };

    AccountObserver accountObserver = new AccountObserver() {
        @Override
        public void update(final Account account) {
            Account.Status status = account.getStatus();
            Log.e("PRASAD", "ACCOUNT STATUS : " + status.getValue());
            if (status.getValue().equals("online")) {
                // ONLINE
                ChatActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        currentStatus = "Online";
                        textViewCounselorDegree.setText("Online");
                        imageViewConnectionStatus.setImageResource(R.drawable.agent_online);
                        if (editTextEmail.getText().toString().length() > 0 || editTextLeaveMessage.getText().toString().length() > 0 && LinearLayoutAgentAwayOffline.getVisibility() == View.VISIBLE) {
                            textViewOfflineMessage.setText(getResources().getString(R.string.zendesk_no_agents_available));
                            LinearLayoutAgentAwayOffline.setVisibility(View.VISIBLE);
                            editTextMessage.setEnabled(false);
                        } else {
                            LinearLayoutAgentAwayOffline.setVisibility(View.GONE);
                            editTextMessage.setEnabled(true);
                        }
                    }
                });
            } else if (status.getValue().equals("offline")) {
                // OFFLINE
                ChatActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        currentStatus = "Offline";
                        textViewCounselorDegree.setText("Offline");
                        imageViewConnectionStatus.setImageResource(R.drawable.agent_offline);
                        /*RequestManager rManager = new RequestManager();
                        rManager.showAgentOfflineDialog(ChatActivity.this, "", ChatActivity.this.getResources().getString(R.string.zendesk_no_agents_available));*/
                        textViewOfflineMessage.setText(getResources().getString(R.string.zendesk_no_agents_available));
                        LinearLayoutAgentAwayOffline.setVisibility(View.VISIBLE);
                        editTextMessage.setEnabled(false);
                    }
                });
            } else {
                // AWAY
                ChatActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        currentStatus = "Away";
                        textViewCounselorDegree.setText("Away");
                        imageViewConnectionStatus.setImageResource(R.drawable.agent_away);
                        if (editTextEmail.getText().toString().length() > 0 || editTextLeaveMessage.getText().toString().length() > 0 && LinearLayoutAgentAwayOffline.getVisibility() == View.VISIBLE) {
                            textViewOfflineMessage.setText(getResources().getString(R.string.zendesk_no_agents_available));
                            LinearLayoutAgentAwayOffline.setVisibility(View.VISIBLE);
                            editTextMessage.setEnabled(false);
                        } else {
                            LinearLayoutAgentAwayOffline.setVisibility(View.GONE);
                            editTextMessage.setEnabled(true);
                        }
                    }
                });
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_STORAGE_PERMISSION) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                openImagePickerChooser();
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                new AlertDialog.Builder(ChatActivity.this).setTitle("Alert").setMessage(R.string.permission_denied_explanation).setPositiveButton("Setting", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                }).setNegativeButton("Cancel", null).show();
            }
        }
    }

    // Function to remove duplicates from an ArrayList
    public static <MyChatModel> ArrayList<MyChatModel> removeDuplicates(ArrayList<MyChatModel> list) {

        // Create a new ArrayList
        ArrayList<MyChatModel> newList = new ArrayList<>();

        // Traverse through the first list
        for (int i = 0; i < list.size(); i++) {
            if (newList.size() > 0) {
                for (int index = 0; index < newList.size(); index++) {
//                    if (!newList.get(index).contains(element)) {
//                        newList.add(element);
//                    }
                }
            } else {
                newList.add(list.get(i));
            }
        }
        // return the new list
        return newList;
    }

    private void openImagePickerChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                File filesDir = this.getFilesDir();
                File imageFile = new File(filesDir, "image0" + ".jpg");

                OutputStream os;
                try {
                    os = new FileOutputStream(imageFile);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, os);
                    os.flush();
                    os.close();
                    chat.send(imageFile);
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (isEndChatPressed) {
            arrMyChatModel.clear();
            arrMyChatModel = null;
            SharedPreferences prefs = getSharedPreferences(AppConstants.Companion.getPREFS_NAME(), MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("CHAT", "");
            editor.commit();
            Log.e("AFTER CHAT", "HISTORY : " + prefs.getString("CHAT", ""));
            isEndChatPressed = false;
        } else {
            saveChatMessages(arrMyChatModel, "CHAT");
        }
        if (chat != null) {
            ZopimChat.resume(ChatActivity.this).endChat();
//            chat.disconnect();
            chat.endChat();
        }
        finish();
//        overridePendingTransition(R.anim.no_animation, R.anim.slide_out_down);
        super.onBackPressed();
    }

    private static final class PositionScroller implements Runnable {

        private static final int SMOOTH_SCROLL_DURATION = 25;
        private int mSelectedPosition;

        private final WeakReference<ChatActivity> mParent;

        private PositionScroller(ChatActivity parent) {
            mParent = new WeakReference<ChatActivity>(parent);
        }

        @Override
        public void run() {
            final ListView list = mParent.get().listViewChatMessage;
            if (mSelectedPosition <= 1000) {
                if (list.postDelayed(this, SMOOTH_SCROLL_DURATION)) {
                    list.setSelection(mSelectedPosition++);
                }
            }
        }

    }


}
