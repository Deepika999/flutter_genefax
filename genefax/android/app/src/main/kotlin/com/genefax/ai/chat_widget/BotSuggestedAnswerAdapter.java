package com.genefax.ai.chat_widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.genefax.ai.R;

import java.util.List;

public class BotSuggestedAnswerAdapter extends RecyclerView.Adapter<BotSuggestedAnswerAdapter.ViewHolder> {


    private List<BotSuggestedAnswerModel> itemDtoList;
    private Context context = null;

    public BotSuggestedAnswerAdapter(List<BotSuggestedAnswerModel> itemDtoList, Context context) {
        this.itemDtoList = itemDtoList;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_answer, parent, false);

        final TextView textItem = (TextView) itemView.findViewById(R.id.textView_answer);
        textItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ChatActivity) context).submitAnswer(textItem.getText().toString());
            }
        });

        ViewHolder ret = new ViewHolder(itemView);
        return ret;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BotSuggestedAnswerModel itemDto = itemDtoList.get(position);
        // Set item image resource id.
        // holder.getImageItem().setImageResource(itemDto.getImageId());
        // Set item text.
        holder.textItem.setText(itemDto.getAnswer());
    }

    @Override
    public int getItemCount() {
        int ret = 0;
        if (this.itemDtoList != null) {
            ret = itemDtoList.size();
        }
        return ret;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textItem;

        public ViewHolder(View itemView) {
            super(itemView);
            if (itemView != null) {
                this.textItem = itemView.findViewById(R.id.textView_answer);
            }
        }

        public TextView getTextItem() {
            return textItem;
        }
    }
}
