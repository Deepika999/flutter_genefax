package com.flutterdemo.zendeskflutterdemo.helper

enum class Environment {
    production,
    production_prev,
    development,
    staging
}