package com.genefax.ai.chat_widget;

public enum MessageType {
    USER,AGENT,USER_ATTACHMENT,AGENT_ATTACHMENT,DATE_SEPARATOR
}
