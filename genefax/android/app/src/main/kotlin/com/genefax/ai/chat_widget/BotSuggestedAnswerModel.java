package com.genefax.ai.chat_widget;

public class BotSuggestedAnswerModel {

    private String answer = "";

    public BotSuggestedAnswerModel(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
