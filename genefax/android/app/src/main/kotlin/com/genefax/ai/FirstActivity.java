package com.genefax.ai;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.flutter.keyboardvisibility.KeyboardVisibilityPlugin;
import com.genefax.ai.chat_widget.ChatActivity;
import com.getchange.zendesk_flutter_plugin.ZendeskFlutterPlugin;

import io.flutter.app.FlutterActivity;
import io.flutter.app.FlutterFragmentActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin;
import io.flutter.plugins.imagepicker.ImagePickerPlugin;
import io.flutter.plugins.packageinfo.PackageInfoPlugin;
import io.flutter.plugins.sharedpreferences.SharedPreferencesPlugin;
import name.avioli.unilinks.UniLinksPlugin;

public class FirstActivity extends FlutterFragmentActivity {
    private static final String CHANNEL = "flutter.native/helper";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        KeyboardVisibilityPlugin.registerWith(this.registrarFor("com.github.adee42.keyboardvisibility"));
        UniLinksPlugin.registerWith(this.registrarFor("name.avioli.unilinks"));
        PackageInfoPlugin.registerWith(this.registrarFor("io.flutter.plugins.packageinfo"));
        SharedPreferencesPlugin.registerWith(this.registrarFor("io.flutter.plugins.sharedpreferences"));
        ZendeskFlutterPlugin.registerWith(this.registrarFor("com.getchange.zendesk_flutter_plugin"));
        ImagePickerPlugin.registerWith(this.registrarFor("io.flutter.plugins.imagepicker"));
        FirebaseMessagingPlugin.registerWith(this.registrarFor("io.flutter.plugins.firebasemessaging"));
        new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(
                new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall call, MethodChannel.Result result) {
                        if (call.method.equals("chatWithCounselor")) {
                           helloFromNativeCode();
                            result.success("success");
                        }
                    }});
    }
    private void helloFromNativeCode() {
        startActivity(new Intent(this, ChatActivity.class));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }
}