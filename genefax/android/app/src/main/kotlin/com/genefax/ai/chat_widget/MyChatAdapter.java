package com.genefax.ai.chat_widget;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.genefax.ai.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MyChatAdapter extends BaseAdapter {

    private ArrayList<MyChatModel> objects = new ArrayList<MyChatModel>();

    private Context context;
    private LayoutInflater layoutInflater;

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private int lastPosition = 0;
//    private TimeAgo timeAgo = null;

    public MyChatAdapter(Context context, ArrayList<MyChatModel> objects) {
        this.context = context;
        this.objects = objects;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        timeAgo = new TimeAgo().locale(context);
    }

    public void updateItems(ArrayList<MyChatModel> arrMyChatModel) {
        this.objects.clear();
        this.objects.addAll(arrMyChatModel);
        this.notifyDataSetChanged();
    }

    @Override
    public int getViewTypeCount() {
        return 5;
    }

    @Override
    public int getItemViewType(int position) {
        if (objects.get(position).getType() == MessageType.USER) {
            return 0;
        } else if (objects.get(position).getType() == MessageType.AGENT) {
            return 1;
        } else if (objects.get(position).getType() == MessageType.USER_ATTACHMENT) {
            return 2;
        } else if (objects.get(position).getType() == MessageType.AGENT_ATTACHMENT) {
            return 3;
        } else if (objects.get(position).getType() == MessageType.DATE_SEPARATOR) {
            return 4;
        } else {
            return super.getItemViewType(position);
        }
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public MyChatModel getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            if (convertView == null) {
                Log.e("ADAPTER", "VIEW TYPE : " + getItemViewType(position));
                if (getItemViewType(position) == 0) {
                    convertView = layoutInflater.inflate(R.layout.chat_right, null);
                    convertView.setTag(new ViewHolderUser(convertView));
                } else if (getItemViewType(position) == 1) {
                    convertView = layoutInflater.inflate(R.layout.chat_left, null);
                    convertView.setTag(new ViewHolderAgent(convertView));
                } else if (getItemViewType(position) == 2) {
                    convertView = layoutInflater.inflate(R.layout.chat_right_attachment, null);
                    convertView.setTag(new ViewHolderUserAttachment(convertView));
                } else if (getItemViewType(position) == 3) {
                    convertView = layoutInflater.inflate(R.layout.chat_left_attachment, null);
                    convertView.setTag(new ViewHolderAgentAttachment(convertView));
                } else if (getItemViewType(position) == 4) {
                    convertView = layoutInflater.inflate(R.layout.chat_date_layout, null);
                    convertView.setTag(new ViewHolderDateSeparator(convertView));
                }
                /*if (position == getCount() - 1) {
                    Animation animation = AnimationUtils.loadAnimation(context, R.anim.up_from_bottom);
                    convertView.startAnimation(animation);
                }*/
            }
            if (getItemViewType(position) == 0) {
                initializeViews((MyChatModel) getItem(position), (ViewHolderUser) convertView.getTag(), position, convertView);
            } else if (getItemViewType(position) == 1) {
                initializeViews((MyChatModel) getItem(position), (ViewHolderAgent) convertView.getTag(), position, convertView);
            } else if (getItemViewType(position) == 2) {
                initializeViews((MyChatModel) getItem(position), (ViewHolderUserAttachment) convertView.getTag());
            } else if (getItemViewType(position) == 3) {
                initializeViews((MyChatModel) getItem(position), (ViewHolderAgentAttachment) convertView.getTag());
            } else if (getItemViewType(position) == 4) {
                initializeViews((MyChatModel) getItem(position), (ViewHolderDateSeparator) convertView.getTag());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    private void initializeViews(MyChatModel object, ViewHolderDateSeparator holder) {
        try {
            String date[] = object.getMessage().split("-");
            String month = "";
            switch (Integer.parseInt(date[1])) {
                case 1:
                    month = "January";
                    break;
                case 2:
                    month = "February";
                    break;
                case 3:
                    month = "March";
                    break;
                case 4:
                    month = "April";
                    break;
                case 5:
                    month = "May";
                    break;
                case 6:
                    month = "June";
                    break;
                case 7:
                    month = "July";
                    break;
                case 8:
                    month = "August";
                    break;
                case 9:
                    month = "September";
                    break;
                case 10:
                    month = "October";
                    break;
                case 11:
                    month = "November";
                    break;
                case 12:
                    month = "December";
                    break;
                default:
                    month = "" + date[1];
            }
            holder.textViewTime.setText(date[0] + " " + month + " " + date[2]);
        } catch (Exception e) {
            holder.textViewTime.setText(object.getMessage());
        }
    }

    private void initializeViews(final MyChatModel object, ViewHolderUser holder, int position, final View view) {
        //TODO implement
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(object.getTimeStamp());
        holder.textViewMessage.setText(object.getMessage());
        holder.textViewTime.setText(object.getDisplayName());
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        holder.textViewTimeStamp.setText(format.format(calendar.getTime()));
        holder.textViewTimeStamp.setVisibility(View.GONE);
//        holder.textViewTimeStamp.setText(getTimeAgo(object.getTimeStamp()));
       /* String result = timeAgo.getTimeAgo(new Date(object.getTimeStamp()));
        holder.textViewTimeStamp.setText(result);
        Log.e("RESULT USER", result);*/
//        holder.textViewTimeStamp.setText(TimeAgo.using(object.getTimeStamp()));
        Glide.with(context)
                .load(R.drawable.ic_user)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.imageViewProfile);

        if (position == objects.size() - 1 && !object.getAnimated()) {
            view.setVisibility(View.INVISIBLE);
            view.post(new Runnable() {
                @Override
                public void run() {
                    Animation anim = AnimationUtils.loadAnimation(context, R.anim.fade_in);
                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            view.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    view.startAnimation(anim);
                    object.setAnimated(true);
                    notifyDataSetChanged();
                }
            });
        }
    }

    private void initializeViews(final MyChatModel object, ViewHolderAgent holder, int position, final View view) {
        //TODO implement
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(object.getTimeStamp());
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        holder.textViewMessage.setText(object.getMessage());
        holder.textViewTime.setText(object.getDisplayName());

        holder.textViewTimeStamp.setText(format.format(calendar.getTime()));
//        holder.textViewTimeStamp.setText(getTimeAgo(object.getTimeStamp()));
        /*String result = timeAgo.getTimeAgo(new Date(object.getTimeStamp()));
        Log.e("RESULT AGENT", result);
        holder.textViewTimeStamp.setText(result);*/
//        holder.textViewTimeStamp.setText(TimeAgo.using(object.getTimeStamp()));

        if (object.getMessage().equals("Hello! How can i help you?")) {
            holder.textViewTimeStamp.setVisibility(View.GONE);
        } else {
            holder.textViewTimeStamp.setVisibility(View.VISIBLE);
        }
        holder.textViewTimeStamp.setVisibility(View.GONE);

        Glide.with(context)
                .load(R.drawable.ic_response)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.imageViewProfile);

        if (object.getMessage().isEmpty()) {
            holder.textViewMessage.setVisibility(View.GONE);
            holder.constraintLayoutBody.setVisibility(View.GONE);
            holder.imageViewLoading.setVisibility(View.VISIBLE);
            holder.textViewTyping.setVisibility(View.GONE);
            /*Glide.with(context)
                    .load("https://cconnect.s3.amazonaws.com/wp-content/uploads/2018/08/loading.gif")
                    .into(holder.imageViewLoading);*/
            Glide.with(context)
                    .load(R.drawable.typing_loader)
                    .into(holder.imageViewLoading);
            // https://cconnect.s3.amazonaws.com/wp-content/uploads/2018/08/loading.gif
            // https://www.awnic.com/eng/assets/images/loading-page.gif
        } else {
            holder.textViewMessage.setVisibility(View.VISIBLE);
            holder.constraintLayoutBody.setVisibility(View.VISIBLE);
            holder.imageViewLoading.setVisibility(View.GONE);
            holder.textViewTyping.setVisibility(View.GONE);
        }

        if (position == objects.size() - 1 && !object.getAnimated()) {
            view.setVisibility(View.INVISIBLE);
            view.post(new Runnable() {
                @Override
                public void run() {
                    Animation anim = AnimationUtils.loadAnimation(context, R.anim.fade_in);
                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            view.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    view.startAnimation(anim);
                    object.setAnimated(true);
                    notifyDataSetChanged();
                }
            });
        }
    }

    private void initializeViews(MyChatModel object, ViewHolderUserAttachment holder) {
        //TODO implement
        holder.textViewTime.setText(object.getDisplayName());
        Glide.with(context)
                .load(R.drawable.ic_user)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.imageViewProfile);
        Glide.with(context)
                .load(object.getMessage())
                .into(holder.imageViewAttachment);
    }

    private void initializeViews(MyChatModel object, ViewHolderAgentAttachment holder) {
        //TODO implement
        holder.textViewTime.setText(object.getDisplayName());
        Glide.with(context)
                .load(R.drawable.ic_response)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.imageViewProfile);

        Glide.with(context)
                .load(object.getMessage())
                .into(holder.imageViewAttachment);
    }


    protected class ViewHolderUser {
        private ImageView imageViewProfile;
        private TextView textViewTime;
        private TextView textViewTimeStamp;
        private TextView textViewMessage;

        public ViewHolderUser(View view) {
            imageViewProfile = (ImageView) view.findViewById(R.id.imageView_profile);
            textViewTime = (TextView) view.findViewById(R.id.textView_time);
            textViewTimeStamp = (TextView) view.findViewById(R.id.textView_timestamp);
            textViewMessage = (TextView) view.findViewById(R.id.textView_message);
        }
    }

    protected class ViewHolderAgent {
        private ImageView imageViewProfile;
        private TextView textViewTime;
        private TextView textViewMessage;
        private TextView textViewTimeStamp;
        private ImageView imageViewLoading;
        private TextView textViewTyping;
        private ConstraintLayout constraintLayoutBody;

        public ViewHolderAgent(View view) {
            imageViewProfile = (ImageView) view.findViewById(R.id.imageView_profile);
            textViewTime = (TextView) view.findViewById(R.id.textView_time);
            textViewMessage = (TextView) view.findViewById(R.id.textView_message);
            textViewTimeStamp = (TextView) view.findViewById(R.id.textView_timestamp);
            imageViewLoading = (ImageView) view.findViewById(R.id.imageView_loading);
            textViewTyping = (TextView) view.findViewById(R.id.textView_typing);
            constraintLayoutBody = (ConstraintLayout) view.findViewById(R.id.constraintLayout_body);
        }
    }

    protected class ViewHolderUserAttachment {
        private ImageView imageViewProfile;
        private TextView textViewTime;
        private ImageView imageViewAttachment;

        public ViewHolderUserAttachment(View view) {
            imageViewProfile = (ImageView) view.findViewById(R.id.imageView_profile);
            textViewTime = (TextView) view.findViewById(R.id.textView_time);
            imageViewAttachment = (ImageView) view.findViewById(R.id.imageView_attachment);
        }
    }

    protected class ViewHolderAgentAttachment {
        private ImageView imageViewProfile;
        private TextView textViewTime;
        private ImageView imageViewAttachment;

        public ViewHolderAgentAttachment(View view) {
            imageViewProfile = (ImageView) view.findViewById(R.id.imageView_profile);
            textViewTime = (TextView) view.findViewById(R.id.textView_time);
            imageViewAttachment = (ImageView) view.findViewById(R.id.imageView_attachment);
        }
    }

    protected class ViewHolderDateSeparator {
        private TextView textViewTime;

        public ViewHolderDateSeparator(View view) {
            textViewTime = (TextView) view.findViewById(R.id.textView_separation_date);
        }
    }
}