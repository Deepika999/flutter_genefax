package com.flutterdemo.zendeskflutterdemo.helper

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.LabeledIntent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Rect
import android.net.ConnectivityManager
import android.provider.Telephony
import android.text.Html
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.genefax.ai.BuildConfig
import com.genefax.ai.helper.AppConstants
import com.genefax.ai.helper.DeviceInfo
import com.kaopiz.kprogresshud.KProgressHUD
import java.text.SimpleDateFormat
import java.util.*

class RequestManager {

    val TAG = "RequestManager"

    companion object {
    }

    internal fun isConnected(activity: AppCompatActivity): Boolean {
        val connMgr = activity.getSystemService(Activity.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return (networkInfo != null && networkInfo.isConnected)
//        val cm = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
//        return cm!!.activeNetworkInfo != null
    }

    internal fun Loger(tag: String, msg: String) {
        if (AppConstants.LOGS == true) {
            Log.e(tag, msg) //  disable for release}
        }
    }

    internal fun getInstance(from: String, activity: AppCompatActivity?): String {
        val pinfo = BuildConfig.VERSION_NAME
        if (from.equals(AppConstants.RECOMM)) {
            return "Android_V${pinfo}_Recomm"
        } else {
            return "Android_V${pinfo}_FreeText"
        }
    }

    internal fun showToastAlert(activity: Context, message: String) {
        Toast.makeText(
            activity,
            message,
            Toast.LENGTH_SHORT
        ).show()
    }

    internal fun showToastAlertWithDuration(activity: Context, message: String, duration: Int) {
        Toast.makeText(
            activity,
            message,
            duration
        ).show()
    }


    /*fun showAgentOfflineDialog(
        activity: AppCompatActivity,
        title: String,
        message: String
    ) {
        val view = activity.layoutInflater.inflate(R.layout.popup_agent_offline, null)
        val alertBuilder = AlertDialog.Builder(activity)
        val alertDialog = alertBuilder.create()

        alertDialog.setCancelable(false)
        alertDialog.setCanceledOnTouchOutside(false)
        if (!activity.isFinishing) {
            alertDialog.setView(view)
            setUpAgentOfflineDialog(activity, alertDialog, view, title, message)
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.show()
            val displayMetrics = DisplayMetrics()
            activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
            val height = displayMetrics.heightPixels
            val width = displayMetrics.widthPixels
            alertDialog.window?.setLayout(
                width - activity!!.resources.getInteger(R.integer.popup_width_space),
                WindowManager.LayoutParams.WRAP_CONTENT
            )
        }
    }

    private fun setUpAgentOfflineDialog(
        activity: AppCompatActivity,
        alertDialog: AlertDialog,
        view: View,
        title: String,
        message: String
    ) {
        val btnSendRequest = view.findViewById<AppCompatButton>(R.id.button_send_request)
        val btnOk = view.findViewById<AppCompatTextView>(R.id.button_submit)
        val tvTitle = view.findViewById<TextView>(R.id.tvTitle)
        val dTextView = view.findViewById<TextView>(R.id.textview)
        val w = ContextCompat.getColor(activity, R.color.colorDecentWhite)
        tvTitle.textSize = 18f
        tvTitle.text = title
        tvTitle.visibility = View.GONE
        dTextView.text = message

        btnSendRequest.visibility = View.GONE
        btnOk.text = activity.resources.getString(R.string.ok)
        val llp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        btnOk.layoutParams = llp
        btnOk.setOnClickListener {
            alertDialog.dismiss()
            try {
                activity.finish()
            } catch (e: ActivityNotFoundException) {
            }
        }
    }*/


    fun fastblur(sentBitmap: Bitmap, radius: Int): Bitmap? {
        val bitmap = sentBitmap.copy(sentBitmap.config, true)

        if (radius < 1) {
            return null
        }

        val w = bitmap.width
        val h = bitmap.height

        val pix = IntArray(w * h)
        Log.e("pix", w.toString() + " " + h + " " + pix.size)
        bitmap.getPixels(pix, 0, w, 0, 0, w, h)

        val wm = w - 1
        val hm = h - 1
        val wh = w * h
        val div = radius + radius + 1

        val r = IntArray(wh)
        val g = IntArray(wh)
        val b = IntArray(wh)
        var rsum: Int
        var gsum: Int
        var bsum: Int
        var x: Int
        var y: Int
        var i: Int
        var p: Int
        var yp: Int
        var yi: Int
        var yw: Int
        val vmin = IntArray(Math.max(w, h))

        var divsum = div + 1 shr 1
        divsum *= divsum
        val dv = IntArray(256 * divsum)
        i = 0
        while (i < 256 * divsum) {
            dv[i] = i / divsum
            i++
        }

        yi = 0
        yw = yi

        val stack = Array(div) { IntArray(3) }
        var stackpointer: Int
        var stackstart: Int
        var sir: IntArray
        var rbs: Int
        val r1 = radius + 1
        var routsum: Int
        var goutsum: Int
        var boutsum: Int
        var rinsum: Int
        var ginsum: Int
        var binsum: Int

        y = 0
        while (y < h) {
            bsum = 0
            gsum = bsum
            rsum = gsum
            boutsum = rsum
            goutsum = boutsum
            routsum = goutsum
            binsum = routsum
            ginsum = binsum
            rinsum = ginsum
            i = -radius
            while (i <= radius) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))]
                sir = stack[i + radius]
                sir[0] = p and 0xff0000 shr 16
                sir[1] = p and 0x00ff00 shr 8
                sir[2] = p and 0x0000ff
                rbs = r1 - Math.abs(i)
                rsum += sir[0] * rbs
                gsum += sir[1] * rbs
                bsum += sir[2] * rbs
                if (i > 0) {
                    rinsum += sir[0]
                    ginsum += sir[1]
                    binsum += sir[2]
                } else {
                    routsum += sir[0]
                    goutsum += sir[1]
                    boutsum += sir[2]
                }
                i++
            }
            stackpointer = radius

            x = 0
            while (x < w) {

                r[yi] = dv[rsum]
                g[yi] = dv[gsum]
                b[yi] = dv[bsum]

                rsum -= routsum
                gsum -= goutsum
                bsum -= boutsum

                stackstart = stackpointer - radius + div
                sir = stack[stackstart % div]

                routsum -= sir[0]
                goutsum -= sir[1]
                boutsum -= sir[2]

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm)
                }
                p = pix[yw + vmin[x]]

                sir[0] = p and 0xff0000 shr 16
                sir[1] = p and 0x00ff00 shr 8
                sir[2] = p and 0x0000ff

                rinsum += sir[0]
                ginsum += sir[1]
                binsum += sir[2]

                rsum += rinsum
                gsum += ginsum
                bsum += binsum

                stackpointer = (stackpointer + 1) % div
                sir = stack[stackpointer % div]

                routsum += sir[0]
                goutsum += sir[1]
                boutsum += sir[2]

                rinsum -= sir[0]
                ginsum -= sir[1]
                binsum -= sir[2]

                yi++
                x++
            }
            yw += w
            y++
        }
        x = 0
        while (x < w) {
            bsum = 0
            gsum = bsum
            rsum = gsum
            boutsum = rsum
            goutsum = boutsum
            routsum = goutsum
            binsum = routsum
            ginsum = binsum
            rinsum = ginsum
            yp = -radius * w
            i = -radius
            while (i <= radius) {
                yi = Math.max(0, yp) + x

                sir = stack[i + radius]

                sir[0] = r[yi]
                sir[1] = g[yi]
                sir[2] = b[yi]

                rbs = r1 - Math.abs(i)

                rsum += r[yi] * rbs
                gsum += g[yi] * rbs
                bsum += b[yi] * rbs

                if (i > 0) {
                    rinsum += sir[0]
                    ginsum += sir[1]
                    binsum += sir[2]
                } else {
                    routsum += sir[0]
                    goutsum += sir[1]
                    boutsum += sir[2]
                }

                if (i < hm) {
                    yp += w
                }
                i++
            }
            yi = x
            stackpointer = radius
            y = 0
            while (y < h) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = -0x1000000 and pix[yi] or (dv[rsum] shl 16) or (dv[gsum] shl 8) or dv[bsum]

                rsum -= routsum
                gsum -= goutsum
                bsum -= boutsum

                stackstart = stackpointer - radius + div
                sir = stack[stackstart % div]

                routsum -= sir[0]
                goutsum -= sir[1]
                boutsum -= sir[2]

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w
                }
                p = x + vmin[y]

                sir[0] = r[p]
                sir[1] = g[p]
                sir[2] = b[p]

                rinsum += sir[0]
                ginsum += sir[1]
                binsum += sir[2]

                rsum += rinsum
                gsum += ginsum
                bsum += binsum

                stackpointer = (stackpointer + 1) % div
                sir = stack[stackpointer]

                routsum += sir[0]
                goutsum += sir[1]
                boutsum += sir[2]

                rinsum -= sir[0]
                ginsum -= sir[1]
                binsum -= sir[2]

                yi += w
                y++
            }
            x++
        }

        Log.e("pix", w.toString() + " " + h + " " + pix.size)
        bitmap.setPixels(pix, 0, w, 0, 0, w, h)

        return bitmap
    }

    private fun takeScreenShot(activity: Activity): Bitmap {
        val view = activity.window.decorView
        view.isDrawingCacheEnabled = true
        view.buildDrawingCache()
        val b1 = view.drawingCache
        val frame = Rect()
        activity.window.decorView.getWindowVisibleDisplayFrame(frame)
        val statusBarHeight = frame.top
        val width = activity.windowManager.defaultDisplay.width
        val height = activity.windowManager.defaultDisplay.height

        val b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height - statusBarHeight)
        view.destroyDrawingCache()
        return b
    }

    public fun hideSoftKeyboard(activity: Activity?) {
        val inputMethodManager = activity?.getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            activity.currentFocus?.windowToken, 0
        )
    }

    internal fun showSoftKeyboard(activity: AppCompatActivity?, view: View?) {
        if (view != null) {
            val inputMethodManager = activity?.getSystemService(
                Activity.INPUT_METHOD_SERVICE
            ) as InputMethodManager
            inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    internal fun isValidEmail(target: String): Boolean {
        val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
        return target.matches(emailPattern.toRegex())
    }

    internal fun isValidPhone1(target: String): Boolean {
        val emailPattern = "[0-9]"
        return target.matches(emailPattern.toRegex())
    }

    internal fun isValidPhone(target: String): Boolean {
        val emailPattern = "[0-9-]{12}"
        return target.matches(emailPattern.toRegex())
    }

    @SuppressLint("NewApi")
    internal fun onShareClick(context: Context) {
        val emailIntent = Intent()
        emailIntent.action = Intent.ACTION_SEND
        var shareMessage = "Let me recommend you this application\n\n"
//        shareMessage =
//            shareMessage + "https://play.google.com/store/apps/details?id=com.genefax"
        shareMessage =
            "I loved using GeneFAX, it's very informational and easy-to-use.\n\nDownload GeneFAX : https://www.optrahealth.com/download-GeneFAX.html\n\n #AskGeneFAX"
        val pm = context.packageManager
        /*val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "You have been invited to use GeneFAX");


        IntentChoserBuilder.createChoserIntent(context, shareIntent, pm, shareMessage, "You have been invited to use GeneFAX");*/
        // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
        emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(shareMessage))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "You have been invited to use GeneFAX.")
//        emailIntent.type = "message/rfc822"

        val sendIntent = Intent(Intent.ACTION_SEND)
        sendIntent.type = "text/plain"

        val openInChooser = Intent.createChooser(emailIntent, "Choose Application")

        val resInfo = pm.queryIntentActivities(sendIntent, 0)
        val intentList = ArrayList<LabeledIntent>()
        for (i in resInfo.indices) {
            // Extract the label, append it, and repackage it in a LabeledIntent
            val ri = resInfo[i]
            val packageName = ri.activityInfo.packageName
            if (packageName.contains("android.email")) {
                emailIntent.setPackage(packageName)
            } else if (packageName.equals(
                    "com.twitter.android"
                ) || packageName.equals(
                    "com.facebook.katana"
                ) || packageName.equals(
                    "com.facebook.orca"
                ) || packageName.equals(
                    Telephony.Sms.getDefaultSmsPackage(context)
                ) || packageName.contains(
                    "android.gm"
                ) || packageName.equals(
                    "com.whatsapp"
                ) || packageName.contains(
                    "com.linkedin.android"
                ) || packageName.equals(
                    "com.microsoft.office.outlook"
                ) || packageName.equals(
                    "com.instagram.android"
                ) || packageName.equals(
                    "com.google.android.apps.docs"
                ) /*|| packageName.contains(
                    "com.pinterest"
                )*/ || packageName.equals(
                    "com.tencent.mm"
                ) || packageName.equals(
                    "com.bsb.hike"
                ) || packageName.equals(
                    "com.skype.raider"
                )
            ) {
                val intent = Intent()
                intent.component = ComponentName(packageName, ri.activityInfo.name)
                intent.action = Intent.ACTION_SEND
                intent.type = "text/plain"
                if (packageName.equals("com.twitter.android")) {
                    intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                } else if (packageName.equals("com.facebook.katana")) {
                    // Warning: Facebook IGNORES our text. They say "These fields are intended for users to express themselves. Pre-filling these fields erodes the authenticity of the user voice."
                    // One workaround is to use the Facebook SDK to post, but that doesn't allow the user to choose how they want to share. We can also make a custom landing page, and the link
                    // will show the <meta content ="..."> text from that page with our link in Facebook.
                    intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                } else if (packageName.equals("com.facebook.orca")) {
                    intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                } else if (packageName.equals(Telephony.Sms.getDefaultSmsPackage(context))) {
                    intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                } else if (packageName.contains("android.gm")) {
                    // If Gmail shows up twice, try removing this else-if clause and the reference to "android.gm" above
                    intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(shareMessage))
                    intent.putExtra(Intent.EXTRA_SUBJECT, "You have been invited to use GeneFAX.")
//                    intent.type = "message/rfc822"
                    intent.type = "text/plain"
                } else if (packageName.equals("com.whatsapp")) {
                    intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                } else if (packageName.equals("com.linkedin.android")) {
                    intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                } else if (packageName.equals("com.microsoft.office.outlook")) {
                    intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                    intent.putExtra(Intent.EXTRA_SUBJECT, "You have been invited to use GeneFAX.")
                } else if (packageName.equals("com.instagram.android")) {
                    intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                } else if (packageName.equals("com.google.android.apps.docs")) {
                    intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                } /*
                https://stackoverflow.com/questions/27388056/android-share-intent-for-pinterest-not-working
                else if (packageName.contains("com.pinterest")) {
                    val shareUrl = "https://www.optrahealth.com/download-GeneFAX.html"
                    val mediaUrl = "https://www.optrahealth.com/images/slider/analyze-genetic-report.jpg"
                    val description = "I loved using GeneFAX, it's very informational and easy-to-use."
                    val url = String.format(
                            "https://www.pinterest.com/pin/create/button/?url=%s&media=%s&description=%s",
                    IntentChoserBuilder.urlEncode(shareUrl), IntentChoserBuilder.urlEncode(mediaUrl), IntentChoserBuilder.urlEncode(description))
                    intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    filterByPackageName(context, intent, "com.pinterest")
                    context.startActivity(intent)
                }*/ else if (packageName.equals("com.tencent.mm")) {
                    intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                } else if (packageName.equals("com.bsb.hike")) {
                    intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                } else if (packageName.equals("com.skype.raider")) {
                    intent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                }
                intentList.add(LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon))
            }
        }
        // convert intentList to array
        val extraIntents = intentList.toTypedArray()
        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents)
        context.startActivity(openInChooser)
    }

    @SuppressLint("SimpleDateFormat")
    internal fun isFreeTrialPeriodActive(context: Context): Boolean {
        val pref = context.getSharedPreferences(AppConstants.PREFS_NAME, Context.MODE_PRIVATE)
        if (pref.getBoolean(AppConstants.IS_FREE_TRIAL_ENDED, false) == false) {
            val installedDate = Calendar.getInstance()
            val date = pref.getLong(AppConstants.PREF_FREE_TRIAL_DATE, 0)
            installedDate.timeInMillis = date
            Loger("TIME STAMP : ", installedDate.timeInMillis.toString())
            val cal2 = Calendar.getInstance()
            val dateFormat = SimpleDateFormat("yyyy-MM-dd")
            val dayCount = (cal2.timeInMillis - installedDate.timeInMillis) / (24 * 60 * 60 * 1000)
            RequestManager().Loger("DATE 1", dateFormat.format(installedDate.time).toString())
            RequestManager().Loger("DATE 2", dateFormat.format(cal2.time).toString())
            RequestManager().Loger("DATE DIFF", dayCount.toString())
            if ((dayCount + 1) <= 3) {
                return true
            } else {
                pref.edit().putBoolean(AppConstants.IS_FREE_TRIAL_ENDED, true).apply()
                return false
            }
        } else {
            // USER IS ALREADY REGISTERED WITH SYSTEM
            pref.edit().putBoolean(AppConstants.IS_FREE_TRIAL_ENDED, true).apply()
            return false
        }
    }

    @SuppressLint("HardwareIds")
    internal fun getRefereneID(context: Context): String? {
        val macAddress1 = DeviceInfo().macAddr
        Loger("DEVICE INFO", macAddress1)
        val macAddress =
            android.provider.Settings.Secure.getString(context.getContentResolver(), "android_id")
        val pref = context.getSharedPreferences(AppConstants.PREFS_NAME, Context.MODE_PRIVATE)
        return macAddress1 + AppConstants.DATE_SEPARATOR + pref.getLong(AppConstants.PREF_FREE_TRIAL_DATE, 0)
    }

    fun getRefereneIDWithoutTimeStamp(): String? {
        val macAddress1 = DeviceInfo().macAddr
        Loger("DEVICE MAC ADDRESS ", macAddress1)
        return macAddress1
    }

    fun getNewSessionRef(activity: AppCompatActivity?): String? {
        val tsLong = System.currentTimeMillis() / 1000
        val ts = tsLong.toString()
        val pinfo = activity!!.packageManager.getPackageInfo(activity!!.packageName, 0)
        Loger("NEW GENERATED SESSION REF", "Android_V${pinfo.versionName}_${ts}")
        return "Android_V${pinfo.versionName}_${ts}"
    }

    fun showProgressDialog(context: AppCompatActivity?, message: String): KProgressHUD {
        val progressor = KProgressHUD.create(context)
        progressor.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel(message)
            .setCancellable(false)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .setBackgroundColor(Color.parseColor("#5CB1C7"))
        return progressor
    }

}
