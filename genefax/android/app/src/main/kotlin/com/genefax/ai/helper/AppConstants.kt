package com.genefax.ai.helper

import com.flutterdemo.zendeskflutterdemo.helper.Environment

public final class AppConstants {
    companion object {
        val TAG = AppConstants::class.java.simpleName

        // shared prefs
        public val PREFS_NAME = "optraguru_prefs"
        public val IS_LOGIN = "is_Login"
        public val PREF_FORGROUND_BACKGROUND = "application_status"
        public val PREF_IS_INTRO_SCREEN_SHOWN = "is_intro_screens_shown"
        public val PREF_FREE_TRIAL_DATE = "free_trial_date"
        public val GENEFAX_BOT_DETAILS = "genefax_bot_details"
        public val MYGENEFAX_BOT_DETAILS = "mygenefax_bot_details"
        public val FREE_TRIAL_USER_ID = -2
        public val FREE_TRIAL = "FreeTrial"
        public val DATE_SEPARATOR = "_"
        public val IS_FREE_TRIAL_ENDED = "is_free_trial_ended"
        public val PREF_CHAT_HISTORY = "chat_history"

        //key const
        public val USER_EMAIL = "sps_email"
        public val PIN = "sps_pin"
        public val SESSION_REF = "session_ref"
        public val RECOMM = "RECOMM"
        public val FREETEXT = "FREE_TEXT"
        public val CAT_ID = "cat_id"
        public val USERID = "gf_userid"
        public val USER_NAME = "gf_userid"
        public val WAYOFLOGIN = "gf_loginway"
        public val DEVICE_TOKEN = "gf_devicetoken"
        public val AUTHTOKEN = "authtoken=tSN4DRaEQ5"
        public val INSTANCE_RECOMM = "ANDROID-Recomm"
        public val INSTANCE_FREE_TEXT = "ANDROID-FreeText"
        public val PLATFORM = "platform=Android"
        public val APPVERSION = "app_version"
        public val FEEDBACK = "show_feedback"
        public val ACTIVE_STATUS: String = "active_status"
        public val ALLOWED_QUESTIONS: String = "no_of_questions"
        public val USER_ACCESS = "user_access"
        public val USER_ACCESS_MSG = "user_access_msg"

        public val LOGOUTTITLE = "Logout"
        public val REQUEST_TITLE = "Upgrade"
        public val LOGOUTMSG = "Are you sure you want to logout?"


        // Make false when log should be disabled. And to enable it make true.
        // When this flag is false. Crashlytics will be on
        public val LOGS = true
        public val IS_SSL_ENABLED = true

        //URL DEVELOPMENT
//        public val BASE_URL = "http://209.126.127.6:8080/GeneFAXDevpAPI/mastercontrollerV1/"
//        public val BASE_URL_TRANS = "http://209.126.127.6:8080/GeneFAXDevpAPI/transcriptcontrollerV1/"

        public val API_STACK = "https://trial.genefax.ai/GeneFAXWebRestMobileV1.2/mastercontroller/"
//        public val API_STACK = "http://192.168.100.190:8081/GeneFAXWebRest/mastercontroller/" // NITIN
//        public val API_STACK = "http://192.168.100.144:8080/GeneFAXWebRest/mastercontroller/" // CHETNA

        //URL PRODUCTION
        public val FORCE_UPDATE_BASE_URL = "https://apps.genefax.ai:8445/GeneFaxDevRestV1/mastercontrollerV1/"
        public val BASE_URL = API_STACK
        public val BASE_URL_TRANS = "https://apps.genefax.ai:8445/GeneFaxDevRestV1/transcriptcontrollerV1/"


        // TRIAL URL
//        public val BASE_URL = "https://trial.genefax.ai/GeneFaxDevRestV1/mastercontrollerV1/"
//        public val BASE_URL_TRANS = "https://trial.genefax.ai/GeneFaxDevRestV1/transcriptcontrollerV1/"

        //        public val APIAutoSuggetion = "http://209.126.127.6:4000/api/v1.0/auto_suggestion"
        public val APIAutoSuggetion = BASE_URL

        // ENVIRONMENT
        public var ENVIRONMENT = Environment.development

        //font
        public val FONT_OPT_BOLD = "opt_font_bold.otf"
        public val FONT_OPT_LIGHT = "opt_font_light.otf"

        public val INACTIVE = "inactive"
        public val ACTIVE = "active"
        public val SEND_REQUEST = "send request"
        public val REQUESTED = "requested"
        public val EXPIRED = "expired"
        public val EMAIL_NOT_FOUND = "email not found"

        // API's
        public val API_LOG_INSERTION = BASE_URL + "logsinsertion?"
        public val API_UPDATE_DEVICE_TOKEN = BASE_URL + "updateDeviceToken?"
        public val API_FORCE_UPGRADE = FORCE_UPDATE_BASE_URL + "forceUpgradeStatus?"
    }
}