package com.genefax.ai.chat_widget;

public class MyChatModel {

    private MessageType Type = MessageType.USER;
    private String Message = "";
    private String DisplayName = "";
    private long TimeStamp = 0;
    private Boolean isAnimated = false;

    public MyChatModel(MessageType type, String message, String displayName, long timeStamp, Boolean isAnimated) {
        Type = type;
        Message = message;
        DisplayName = displayName;
        TimeStamp = timeStamp;
        isAnimated = isAnimated;

    }

    public Boolean getAnimated() {
        return isAnimated;
    }

    public void setAnimated(Boolean animated) {
        isAnimated = animated;
    }

    public MessageType getType() {
        return Type;
    }

    public void setType(MessageType type) {
        Type = type;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public long getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        TimeStamp = timeStamp;
    }
}
